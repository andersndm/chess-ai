#ifndef CHESS_BOARD_H
#define CHESS_BOARD_H

#include "chess.h"
#include "chess_move.h"
#include "nnue.h"

// #note currently just implementing the 3 repetitions equals an automatic draw version of the rules
typedef struct chess_repetition
{
	u64 hash;
	u64 count; 	// #note only 64-bit because of alignment, could be u8
} chess_repetition;

typedef struct state_info state_info;
struct state_info
{
	u8 castling_rights;
	u8 en_passant;
	chess_piece captured_piece;
	u8 fifty_rule;
	u16 ply;
	u64 hash;
	chess_color side;

	state_info *previous;
};

#define MAX_GAME_MOVES 2048	// #note theoretically possible to have ~8000 maximum before a draw from 50 move,
								// longest known game is 269 moves, would have ended earlier with modern 50 rule

typedef struct chess_board
{
	chess_piece squares[64];

	u64 color_bbs[NUM_COLORS];
	// #todo actually have piece type none, could use that for empty squares
	u64 type_bbs[NUM_PIECE_TYPES];

	state_info *state;
	//chess_repetition reps[MAX_GAME_MOVES];
	hashmap repmap;

	nnue_acc accumulators[NUM_COLORS];
} chess_board;

void init_accumulators(chess_board *board);
// #note board ctors
chess_board start_board(state_info *state);
bool board_from_fen(chess_board *board, state_info *state, const char *fen);
chess_board from_fen(const char *fen, state_info *state);
const char *to_fen(const chess_board *board);
void copy_board(chess_board *dst, const chess_board *src);

void add_piece(chess_board *board, u8 pos, chess_piece piece);
void remove_piece(chess_board *board, u8 pos);
void move_king(chess_board *board, u8 from, u8 to);

chess_result apply_move(chess_board *board, state_info *new_state, chess_move move);
void undo_move(chess_board *board, chess_move move);
void apply_null_move(chess_board *board, state_info *new_state);
void undo_null_move(chess_board *board);

check_status get_check_status(const chess_board *board);

u64 position_hash(chess_board *board);

void print_board(const chess_board *board);
void print_bitboard(u64 bitboard);
void print_bitboard_small(u64 bitboard);
void print_full_bitboard(const chess_board *board);
void print_compare_bitboards(u64 a, u64 b);
void print_bitboard_with_pos(u64 bitboard, u8 pos);
void print_bitboard_with_2_pos(u64 bitboard, u8 a, u8 b);

#endif // !CHESS_BOARD_H
