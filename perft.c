#include "perft.h"

#include "chess_board.h"
#include "move_generation.h"

void print_perft_move(chess_move move)
{
	print_square(move_from(move));
	print_square(move_to(move));
	if (is_promotion(move))
	{
		chess_piece_type type = promotion_type(move);
		if (type == QUEEN)           { printf("q"); }
		else if (type == ROOK)       { printf("r"); }
		else if (type == BISHOP)     { printf("b"); }
		else { assert(type == KNIGHT); printf("n"); }
	}
}

#define CHECK_BOARD 0
#include "platform_common.h"

#if CHECK_BOARD
internal bool compare_bitboards(const char *msg, u64 a, u64 b)
{
	if (a != b)
	{
		PC_RED_BOLD("%s:\n", msg);
		print_compare_bitboards(a, b);
		return true;
	}
	return false;
}

internal bool compare_states(state_info *a, state_info *b)
{
	bool failed = false;

	if (a->castling_rights != b->castling_rights)
	{
		PC_RED_BOLD("castling rights: ");
		printf("%2x - %2x\n", a->castling_rights, b->castling_rights);
		failed = true;
	}

	if (a->en_passant != b->en_passant)
	{
		PC_RED_BOLD("en passent: ");
		if (a->en_passant != 0)
		{
			printf("%c%c", get_file_char(a->en_passant),
				   get_rank_char(a->en_passant));
		}
		else
		{
			printf("-");
		}
		printf(" ");
		if (b->en_passant != 0)
		{
			printf("%c%c", get_file_char(b->en_passant),
				   get_rank_char(b->en_passant));
		}
		else
		{
			printf("-");
		}
		printf("\n");

		failed = true;
	}

	if (a->captured_piece != b->captured_piece)
	{
		PC_RED_BOLD("captured piece: ");
		if (a->captured_piece != EMPTY_SQ)
		{
			printf("%c", get_piece_char(a->captured_piece));
		}
		else
		{
			printf("-");
		}
		printf(" ");
		if (b->captured_piece != EMPTY_SQ)
		{
			printf("%c", get_piece_char(b->captured_piece));
		}
		else
		{
			printf("-");
		}
		printf("\n");
		failed = true;
	}

	if (a->fifty_rule != b->fifty_rule)
	{
		PC_RED_BOLD("fifty move: ");
		printf("%3u %3u\n", a->fifty_rule, b->fifty_rule);
		failed = true;
	}

	if (a->ply != b->ply)
	{
		PC_RED_BOLD("ply: ");
		printf("%3u %3u\n", a->ply, b->ply);
		failed = true;
	}

	if (a->hash != b->hash)
	{
		PC_RED_BOLD("hash: ");
		printf("%016lx %016lx\n", a->hash, b->hash);
		failed = true;
	}

	if (a->side != b->side)
	{
		PC_RED_BOLD("side to play: ");
		printf("%s %s\n", a->side == WHITE ? "white" : "black",
			   b->side == WHITE ? "white" : "black");
		failed = true;
	}

	if (a->previous != b->previous)
	{
		PC_RED_BOLD("previous\n");
		failed = true;
	}

	return failed;
}

internal bool compare_boards(const chess_board *a, const chess_board *b)
{
	bool failed = false;
	for (u8 pos = 0; pos < 64; ++pos)
	{
		if (a->squares[pos] != b->squares[pos])
		{
			PC_RED_BOLD("%c%c: ", get_file_char(pos), get_rank_char(pos));
			print_piece(a->squares[pos]); printf(" ");
			print_piece(b->squares[pos]); printf("\n");
			failed = true;
		}
	}

	failed |= compare_bitboards("white", a->color_bbs[WHITE], b->color_bbs[WHITE]);
	failed |= compare_bitboards("black", a->color_bbs[BLACK], b->color_bbs[BLACK]);

	failed |= compare_bitboards("pawns", a->type_bbs[PAWN], b->type_bbs[PAWN]);
	failed |= compare_bitboards("knights", a->type_bbs[KNIGHT], b->type_bbs[KNIGHT]);
	failed |= compare_bitboards("bishops", a->type_bbs[BISHOP], b->type_bbs[BISHOP]);
	failed |= compare_bitboards("rooks", a->type_bbs[ROOK], b->type_bbs[ROOK]);
	failed |= compare_bitboards("queens", a->type_bbs[QUEEN], b->type_bbs[QUEEN]);
	failed |= compare_bitboards("kings", a->type_bbs[KING], b->type_bbs[KING]);

	if (a->repmap.cap != b->repmap.cap)
	{
		PC_RED_BOLD("repmap.cap: ");
		printf("%zu %zu\n", a->repmap.cap, b->repmap.cap);
		failed = true;
	}
	if (a->repmap.len != b->repmap.len)
	{
		PC_RED_BOLD("repmap.len: ");
		printf("%zu %zu\n", a->repmap.len, b->repmap.len);
		failed = true;
	}

	if (failed)
	{
		print_board(a);
	}
	return failed;
}
#endif


u64 perft_recursive(chess_board *board, u32 depth)
{
	if (depth == 0)
	{
		return 1;
	}

	u64 nodes = 0;

	chess_move moves[MAX_LEGAL_MOVES];
	u32 num_moves = generate_all_moves(board, moves);
	if (num_moves == 0)
	{
		return 0;
	}

	if (depth == 1)
	{
		return num_moves;
	}

	for (u32 i = 0; i < num_moves; ++i)
	{
		state_info state;
		chess_move move = moves[i];
		#if CHECK_BOARD
		chess_board old_board = *board;
		state_info old_state = *board->state;
		if (move == 0)
		{
			print_board(board);
			debug_break;
		}
		#endif
		chess_result result = apply_move(board, &state, move);
		#if 0
		if (move == 0 && state.hash == 4985105336309727928ull)
		{
			print_board(board);
			debug_break;
		}
		#endif
		if (result < CR_PERFT_NO_IGNORE)
		{
			nodes += perft_recursive(board, depth - 1);
		}
		undo_move(board, move);
		#if CHECK_BOARD
		if (compare_boards(&old_board, board) || compare_states(&old_state, board->state))
		{
			print_move(move); printf("\n");
			PC_CYAN("old\n");
			print_board(&old_board);
			PC_CYAN("new\n");
			print_board(board);
			print_compare_bytes(&old_board, board, sizeof(old_board), 0);
			debug_break;
		}
		#endif
	}
	return nodes;
}

typedef struct perft_thread_info
{
	chess_board board;
	chess_move move;
	u64 nodes;
	u32 depth;
} perft_thread_info;

void perft_threaded(platform_work_queue *queue, void *data)
{
	perft_thread_info *info = (perft_thread_info *)data;
	info->board.repmap = hashmap_new(64);
	u64 local_nodes = 1;
	chess_move move = info->move;
	state_info state;
	chess_result result = apply_move(&info->board, &state, move);
	if (result < CR_PERFT_NO_IGNORE)
	{
		local_nodes = perft_recursive(&info->board, info->depth);
	}
	platform_mutex_lock(&queue->print_mutex);
	print_perft_move(info->move);
	printf(": %" PRIu64 "\n", local_nodes);
	platform_mutex_unlock(&queue->print_mutex);
	info->nodes = local_nodes;
	hashmap_clear(&info->board.repmap);
}

u64 perft(chess_board *board, u32 depth, platform_work_queue *queue)
{
	u64 nodes = 0;
	chess_move moves[MAX_LEGAL_MOVES];
	u32 num_moves = generate_all_moves(board, moves);
	if (queue != NULL)
	{
		perft_thread_info *infos = malloc(num_moves * sizeof(perft_thread_info));
		for (u32 i = 0; i < num_moves; ++i)
		{
			perft_thread_info *info = infos + i;
			info->board = *board;
			info->move = moves[i];
			info->nodes = 0;
			info->depth = depth - 1;

			add_work_queue_entry(queue, perft_threaded, info);
		}
		complete_all_work(queue);
		for (u32 i = 0; i < num_moves; ++i)
		{
			nodes += (infos + i)->nodes;
		}
		free(infos);
	}
	else
	{
		for (u32 i = 0; i < num_moves; ++i)
		{
			u64 local_nodes = 1;
			state_info state;
			chess_move move = moves[i];
			chess_result result = apply_move(board, &state, move);
			if (result < CR_PERFT_NO_IGNORE)
			{
				local_nodes = perft_recursive(board, depth - 1);
			}
			undo_move(board, move);
			#if 1
			print_perft_move(move);
			printf(": %" PRIu64 "\n", local_nodes);
			#endif
			nodes += local_nodes;
		}
	}

	return nodes;
}

#if 0
typedef struct perft_check
{
	const char *fen;
	u32 depth;
	u64 count;
} perft_check;

perft_check perft_checks[] =
{
	{
		.fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0",
		.depth = 1,
		.count = 20
	},
	{
		.fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0",
		.depth = 2,
		.count = 400
	},
	{
		.fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0",
		.depth = 3,
		.count = 8902
	},
	{
		.fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0",
		.depth = 4,
		.count = 197281
	},
	{
		.fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0",
		.depth = 5,
		.count = 4865609
	},
	{
		.fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0",
		.depth = 6,
		.count = 119060324
	},
	{
		.fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0",
		.depth = 7,
		.count = 3195901860
	},
	{
		.fen = "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -",
		.depth = 5,
		.count = 193690690
	},
	{
		.fen = "8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -",
		.depth = 7,
		.count = 178633661
	},
	{
		.fen = "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
		.depth = 6,
		.count = 706045033
	},
	{
		.fen = "1k6/1b6/8/8/7R/8/8/4K2R b K - 0 1",
		.depth = 5,
		.count = 1063513
	},
	{
		.fen = "3k4/3p4/8/K1P4r/8/8/8/8 b - - 0 1",
		.depth = 6,
		.count = 1134888
	},
	{
		.fen = "8/8/4k3/8/2p5/8/B2P2K1/8 w - - 0 1",
		.depth = 6,
		.count = 1015133
	},
	{
		.fen = "8/8/1k6/2b5/2pP4/8/5K2/8 b - d3 0 1",
		.depth = 6,
		.count = 1440467
	},
	{
		.fen = "5k2/8/8/8/8/8/8/4K2R w K - 0 1",
		.depth = 6,
		.count = 661072
	},
	{
		.fen = "3k4/8/8/8/8/8/8/R3K3 w Q - 0 1",
		.depth = 6,
		.count = 803711
	},
	{
		.fen = "r3k2r/1b4bq/8/8/8/8/7B/R3K2R w KQkq - 0 1",
		.depth = 4,
		.count = 1274206
	},
	{
		.fen = "r3k2r/8/3Q4/8/8/5q2/8/R3K2R b KQkq - 0 1",
		.depth = 4,
		.count = 1720476
	},
	{
		.fen = "2K2r2/4P3/8/8/8/8/8/3k4 w - - 0 1",
		.depth = 6,
		.count = 3821001
	},
	{
		.fen = "8/8/1P2K3/8/2n5/1q6/8/5k2 b - - 0 1",
		.depth = 5,
		.count = 1004658
	},
	{
		.fen = "4k3/1P6/8/8/8/8/K7/8 w - - 0 1",
		.depth = 6,
		.count = 217342
	},
	{
		.fen = "8/P1k5/K7/8/8/8/8/8 w - - 0 1",
		.depth = 6,
		.count = 92683
	},
	{
		.fen = "K1k5/8/P7/8/8/8/8/8 w - - 0 1",
		.depth = 6,
		.count = 2217
	},
	{
		.fen = "8/k1P5/8/1K6/8/8/8/8 w - - 0 1",
		.depth = 7,
		.count = 567584
	},
	{
		.fen = "8/8/2k5/5q2/5n2/8/5K2/8 b - - 0 1",
		.depth = 4,
		.count = 23527
	}
};

void run_perft_checks(platform_work_queue *queue)
{
	for (u32 perft_index = 0; perft_index < array_count(perft_checks); ++perft_index)
	{
		state_info state;
		chess_board board;
		if (!board_from_fen(&board, &state, perft_checks[perft_index].fen))
		{
			board = start_board(&state);
		}
		//print_board(&board);
		u64 start_time = get_time_ns();
		u64 total_nodes = perft(&board, perft_checks[perft_index].depth, queue);
		u64 end_time = get_time_ns();
		if (total_nodes != perft_checks[perft_index].count)
		{
			PC_RED_BOLD("failed to pass check %u\n", perft_index);
			break;
		}

		assert(end_time >= start_time);
		u64 nsec_diff = end_time - start_time;
		f64 time_taken_s = (f64)nsec_diff / 1e9;
		f64 nodes_per_sec = (f64)total_nodes / time_taken_s;
		PC_GREEN_BOLD("%u: %" PRIu64 " nodes at depth of %u | %.1f n/s\n", perft_index,
					  total_nodes, perft_checks[perft_index].depth, nodes_per_sec);
		printf("time taken: ");
		print_time_spent(end_time - start_time);
		printf("\n");
	}
}
#endif

void *run_perft(void *perft_data)
{
	run_perft_thread_info *info = perft_data;
	#if 0
	if (!board_from_fen(&board, &state, "8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -"))
	{
		board = standard_board(&state);
	}
	depth = 7;
	#endif
	#if 0
	chess_move moves[] =
	{
		#if 1
		gen_push(D2, D4),
		gen_push(E7, E6),
		gen_push(E1, D2),
		gen_push(B7, B5),
		gen_push(D2, C3),
		gen_push(B5, B4)
		#endif
	};
	assert(depth > array_count(moves));
	depth -= array_count(moves);
	state_info states[array_count(moves)];

	for (u32 i = 0; i < array_count(moves); ++i)
	{
		apply_move(&board, &states[i], moves[i]);
	}
	print_board(&board);
	printf("go perft %u\n", depth);
	#endif

	platform_work_queue queue;
	make_work_queue(&queue, info->thread_count);

	u64 start_time = get_time_ns();
	u64 total_nodes = perft(info->board, info->depth, &queue);
	u64 end_time = get_time_ns();

	assert(end_time >= start_time);
	u64 nsec_diff = end_time - start_time;
	f64 time_taken_s = (f64)nsec_diff / 1e9;
	f64 nodes_per_sec = (f64)total_nodes / time_taken_s;
	printf("%" PRIu64 " nodes at depth of %u | %.1f n/s\n", total_nodes, info->depth, nodes_per_sec);
	printf("time taken: ");
	print_time_spent(end_time - start_time);
	printf("\n");

	join_work_queue(&queue);
	free(info);

	return NULL;
}
