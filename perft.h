#ifndef PERFT_H
#define PERFT_H

#include "common.h"
#include "threading.h"
#include "chess_board.h"

typedef struct run_perft_thread_info
{
	chess_board *board;
	u32 depth;
	u32 thread_count;
} run_perft_thread_info;

void *run_perft(void *perft_data);

#endif // !PERFT_H
