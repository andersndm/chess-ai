#include "common.h"

void print_u8_binary(u8 value)
{
	for (u32 i = 0; i < 8; ++i)
	{
		printf("%u", (value >> (7 - i)) & 0x1);
	}
}

void *xmalloc(usize num_bytes)
{
	void *ptr = malloc(num_bytes);
	if (!ptr)
	{
		log_fatal("malloc failed to allocate %zu bytes", num_bytes);
	}
	//log_info("malloc allocated %zu bytes", num_bytes);
	return ptr;
}

void *xcalloc(usize num_elems, usize elem_size)
{
	void *ptr = calloc(num_elems, elem_size);
	if (!ptr)
	{
		log_fatal("calloc failed to allocate %zu bytes", num_elems * elem_size);
	}
	//log_info("calloc allocated %zu bytes", num_elems * elem_size);
	return ptr;
}

void *xrealloc(void *ptr, usize num_bytes)
{
	void *new_ptr = realloc(ptr, num_bytes);
	if (!new_ptr)
	{
		log_fatal("realloc failed to allocate %zu bytes", num_bytes);
	}
	//log_info("realloc allocated %zu bytes", num_bytes);
	return new_ptr;
}

void xfree(void *ptr)
{
	if (ptr)
	{
		free(ptr);
		//log_info("freed memory");
	}
	else
	{
		log_fatal("attempted free on null pointer");
	}
}

void *buf__grow(const void *buf, usize new_len, usize elem_size)
{
	assert(buf_cap(buf) <= (SIZE_MAX - 1) / 2);
	usize new_cap = clamp_min(2 * buf_cap(buf), max(new_len, 16));
	assert(new_len <= new_cap);
	assert(new_cap <= (SIZE_MAX - offsetof(buf_header, buffer)) / elem_size);
	usize new_size = offsetof(buf_header, buffer) + new_cap * elem_size;

	buf_header *new_hdr;
	if (buf)
	{
		new_hdr = xrealloc(buf__hdr(buf), new_size);
	}
	else
	{
		new_hdr = xmalloc(new_size);
		new_hdr->len = 0;
	}
	new_hdr->cap = new_cap;
	return new_hdr->buffer;
}

char *buf__printf(char *buf, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	usize cap = buf_cap(buf) - buf_len(buf);
	usize n = 1 + (usize)vsnprintf(buf_end(buf), cap, fmt, args);
	va_end(args);

	if (n > cap)
	{
		buf_fit(buf, n + buf_len(buf));
		va_start(args, fmt);
		usize new_cap = buf_cap(buf) - buf_len(buf);
		n = 1 + (usize)vsnprintf(buf_end(buf), new_cap, fmt, args);
		assert(n <= new_cap);
		va_end(args);
	}

	buf__hdr(buf)->len += n - 1;
	return buf;
}

#if 0
// #todo may not be needed
internal u64 hash_u64(u64 hash)
{
	// #todo obligatory check if new hash function is needed
	hash ^= (hash * 0xff51afd7ed558ccd) >> 32;
	return hash;
}

internal u64 hash_ptr(const void *ptr)
{
	return hash_u64((uintptr_t)ptr);
}

internal u64 hash_mix(u64 x, u64 y)
{
	x ^= y;
	x *= 0xff51afd7ed558ccd;
	x ^= x >> 32;
	return x;
}

internal u64 hash_bytes(const void *ptr, usize len)
{
	u64 hash = 0xcbf29ce484222325;
	const char *buf = (const char *)ptr;
	for (usize i = 0; i < len; ++i)
	{
		hash ^= (u64)buf[i];
		hash *= 0x100000001b3;
		hash ^= hash >> 32;
	}
	return hash;
}

internal u64 hash_bytes_range(const char *start, const char *end)
{
	u64 hash = 0xcbf29ce484222325ull;
	while (start != end)
	{
		hash ^= (u64)*start++;
		hash *= 0x100000001b3;
		hash ^= hash >> 32;
	}
	return hash;
}
#endif

// #todo if no other hashmaps needed, make own repetition hashmap
internal void hashmap_grow(hashmap *map, usize new_cap)
{
	new_cap = clamp_min(new_cap, 16);
	hashmap new_map = (hashmap)
	{
		.keys = xcalloc(new_cap, sizeof(u64)),
		.vals = xcalloc(new_cap, sizeof(u64)),
		.len = 0,
		.cap = new_cap
	};

	for (usize i = 0; i < map->cap; ++i)
	{
		if (map->keys[i])
		{
			hashmap_put(&new_map, map->keys[i], map->vals[i]);
		}
	}

	if (map->keys)
	{
		xfree((void *)map->keys);
		map->keys = 0;
	}
	if (map->vals)
	{
		xfree(map->vals);
		map->vals = 0;
	}

	*map = new_map;
}

hashmap hashmap_new(u32 count)
{
	hashmap result = { 0 };
	hashmap_grow(&result, count);

	return result;
}

u64 hashmap_get(hashmap *map, u64 key)
{
	if (map->len == 0)
	{
		return 0;
	}
	assert(is_pow2(map->cap));
	assert(map->len < map->cap);

	#if 0
	usize i = (usize)hash_u64(key);
	#else
	// #note currently all keys are zobrist hashes, so rehashing isn't needed
	usize i = key;
	#endif
	for (;;)
	{
		i &= map->cap - 1;
		if (map->keys[i] == key)
		{
			return map->vals[i];
		}
		else if (!map->keys[i])
		{
			return 0;
		}
		++i;
	}

	return 0;
}

u64 hashmap_remove(hashmap *map, u64 key)
{
	if (map->len == 0)
	{
		return 0;
	}
	assert(is_pow2(map->cap));
	assert(map->len < map->cap);

	#if 0
	usize i = (usize)hash_u64(key);
	#else
	// #note currently all keys are zobrist hashes, so rehashing isn't needed
	usize i = key;
	#endif
	for (;;)
	{
		i &= map->cap - 1;
		if (map->keys[i] == key)
		{
			u64 result = map->vals[i];
			map->vals[i] = 0;
			map->keys[i] = 0;
			--map->len;
			return result;
		}
		else if (!map->keys[i])
		{
			--map->len;
			return 0;
		}
		++i;
	}
}

void hashmap_put(hashmap *map, u64 key, u64 val)
{
	assert(key);
	if (!val) { return; }
	if (2 * map->len >= map->cap)
	{
		hashmap_grow(map, 2 * map->cap);
	}
	assert(2 * map->len <= map->cap);
	assert(is_pow2(map->cap));
	#if 0
	usize i = (usize)hash_u64(key);
	#else
	// #note currently all keys are zobrist hashes, so rehashing isn't needed
	usize i = key;
	#endif
	for (;;)
	{
		i &= map->cap - 1;
		if (!map->keys[i])
		{
			++map->len;
			map->keys[i] = key;
			map->vals[i] = val;
			return;
		}
		else if (map->keys[i] == key)
		{
			map->vals[i] = val;
			return;
		}
		++i;
	}
}

void hashmap_clear(hashmap *map)
{
	if (map->keys)
	{
		xfree((void *)map->keys);
		map->keys = 0;
	}
	if (map->vals)
	{
		xfree(map->vals);
		map->vals = 0;
	}
	map->cap = 0;
	map->len = 0;
}

#define POSIX_VERSION 199309L
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE POSIX_VERSION
#else
#if _POSIX_C_SOURCE < POSIX_VERSION
#undef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE POSIX_VERSION
#endif
#endif

#include <time.h>
u64 get_time_ns(void)
{
	u64 result = 0;
	struct timespec ts;
	int clock_result = clock_gettime(CLOCK_MONOTONIC, &ts);
	assert(clock_result == 0);
	if (clock_result != 0) { return 0; }
	result = ts.tv_sec * 1000000000;
	result += ts.tv_nsec;
	return result;
}

void print_time_spent(u64 nsec_diff)
{
	f64 time_taken_s = (f64)nsec_diff / 1e9;
	if (nsec_diff < 1000)
	{
		printf("%ld ns", nsec_diff);
	}
	else if (nsec_diff < 1000000)
	{
		printf("%.1f us", (f64)nsec_diff / 1e3);
	}
	else if (nsec_diff < 1000000000)
	{
		printf("%.1f ms", (f64)nsec_diff / 1e6);
	}
	else if (time_taken_s > 60.0)
	{
		long min_taken = time_taken_s / 60.0;
		f64 s_taken = (f64)(time_taken_s - min_taken * 60.0);
		printf("%ld m %.1f s", min_taken, s_taken);
	}
	else
	{
		printf("%.2f s", time_taken_s);
	}
}

char *get_stdin(u32 max)
{
#ifdef _WIN32
	// #todo
	return "error\n";
#else
	char *input_buffer = NULL;
	char c;
	while (read(STDIN_FILENO, &c, 1))
	{
		if (c == '\n') { break; }
		buf_push(input_buffer, c);
		if (max != 0)
		{
			if (buf_len(input_buffer) > max)
			{
				buf_free(input_buffer);
				return NULL;
			}
		}
	}
	if (input_buffer != NULL)
	{
		buf_push(input_buffer, '\n');
		buf_push(input_buffer, 0);
	}
	return input_buffer;
#endif
}
