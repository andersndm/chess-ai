#include "threading.h"

#include <x86intrin.h>
#include <sys/syscall.h>
#include <signal.h>

void platform_sleep(u32 s)
{
	sleep(s);
}

void platform_msleep(u32 ms)
{
	usleep(ms * 1000);
}

void platform_usleep(u32 us)
{
	usleep(us);
}

platform_thread platform_thread_create(platform_thread_proc start, void *data)
{
	pthread_t thread;
	int err = pthread_create(&thread, NULL, start, data);
	if (err != 0)
	{
		// #todo error
		assert(0);
	}
	platform_thread result;
	result.handle = thread;
	return result;
}

void platform_thread_join(platform_thread thread)
{

	pthread_join(thread.handle, NULL);
}

void platform_thread_kill(platform_thread thread)
{
	pthread_kill(thread.handle, SIGKILL);
}

u32 platform_get_thread_id(void)
{
	pid_t result = syscall(__NR_gettid);
	return (u32)result;
}

sem_t platform_semaphore_create(void)
{
	sem_t semaphore;
	if (sem_init(&semaphore, 0, 1) != 0)
	{
		log_fatal("failed to create a semaphore.");
	}
	return semaphore;
}

void platform_semaphore_release(sem_t *semaphore)
{
	if (sem_post(semaphore) != 0)
	{
		log_fatal("failed to release a semaphore.");
	}
}

void platform_semaphore_wait(sem_t *semaphore)
{
	if (sem_wait(semaphore) != 0)
	{
		log_fatal("failed to wait for a semaphore.");
	}
}

pthread_mutex_t platform_mutex_create(void)
{
	pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
	return mutex;
}

void platform_mutex_lock(pthread_mutex_t *mutex)
{
	pthread_mutex_lock(mutex);
}

void platform_mutex_unlock(pthread_mutex_t *mutex)
{
	pthread_mutex_unlock(mutex);
}

void work_queue_exit_entry(platform_work_queue *queue, void *data)
{
	pthread_exit(NULL);
}

void add_work_queue_entry(platform_work_queue *queue, platform_work_queue_callback callback,
						  void *data)
{
	u32 new_next_entry_to_write = (queue->next_entry_to_write + 1) % array_count(queue->entries);
	assert(new_next_entry_to_write != queue->next_entry_to_read);
	platform_work_queue_entry *entry = queue->entries + queue->next_entry_to_write;
	entry->data = data;
	entry->callback = callback;
	++queue->completion_goal;

	write_barrier;

	queue->next_entry_to_write = new_next_entry_to_write;
	platform_semaphore_release(&queue->semaphore);
}

// #note returns true if there is nothing to do
internal bool process_next_work_queue_entry(platform_work_queue *queue)
{
	bool should_sleep = false;

	u32 original_next_entry_to_read = queue->next_entry_to_read;
	u32 new_next_entry_to_read = (original_next_entry_to_read + 1) % array_count(queue->entries);
	if (original_next_entry_to_read != queue->next_entry_to_write)
	{
		u32 old_next_entry_to_read = atomic_compare_exchange_u32(&queue->next_entry_to_read,
																 original_next_entry_to_read,
																 new_next_entry_to_read);
		if (old_next_entry_to_read == original_next_entry_to_read)
		{
			platform_work_queue_entry *entry = queue->entries + old_next_entry_to_read;
			entry->callback(queue, entry->data);
			atomic_inc_u32(&queue->completion_count);
		}
	}
	else
	{
		should_sleep = true;
	}

	return should_sleep;
}

void complete_all_work(platform_work_queue *queue)
{
	while (queue->completion_goal != queue->completion_count)
	{
		process_next_work_queue_entry(queue);
	}
	queue->completion_count = 0;
	queue->completion_goal = 0;
}

bool all_work_completed(platform_work_queue *queue)
{
	if (queue->completion_goal != queue->completion_count)
	{
		return false;
	}

	queue->completion_count = 0;
	queue->completion_goal = 0;
	return true;
}

void cancel_remaining_work(platform_work_queue *queue)
{
	bool overwrote = false;
	u32 safety_counter = 0;

	while (!overwrote && safety_counter < 10000)
	{
		u32 original_next_entry_to_read = queue->next_entry_to_read;
		u32 old_next_entry_to_read = atomic_compare_exchange_u32(&queue->next_entry_to_read,
																original_next_entry_to_read,
																queue->next_entry_to_write);
		if (old_next_entry_to_read != original_next_entry_to_read)
		{
			overwrote = true;
		}
		++safety_counter;
	}
	queue->completion_count = 0;
	queue->completion_goal = 0;
}

internal void *thread_proc(void *param)
{
	platform_work_queue *queue = param;

	//u32 id = platform_get_thread_id();

	for (;;)
	{
		if (process_next_work_queue_entry(queue))
		{
			platform_semaphore_wait(&queue->semaphore);
		}
	}

	return NULL;
}

void make_work_queue(platform_work_queue *queue, u32 thread_count)
{
	queue->completion_count = 0;
	queue->completion_goal = 0;
	queue->next_entry_to_read = 0;
	queue->next_entry_to_write = 0;

	queue->semaphore = platform_semaphore_create();
	queue->print_mutex = platform_mutex_create();
	queue->table_mutex = platform_mutex_create();
	// #todo sleep needed?
	platform_msleep(1);

	if (thread_count)
	{
		queue->thread_count = thread_count;
		queue->threads = malloc(sizeof(platform_thread) * thread_count);

		for (u32 thread_index = 0; thread_index < thread_count; ++thread_index)
		{
			queue->threads[thread_index] = platform_thread_create(thread_proc, queue);
		}
	}
}

void join_work_queue(platform_work_queue *queue)
{
	for (u32 i = 0; i < queue->thread_count; ++i)
	{
		add_work_queue_entry(queue, work_queue_exit_entry, NULL);
	}

	for (u32 i = 0; i < queue->thread_count; ++i)
	{
		platform_thread_join(queue->threads[i]);
	}
	free(queue->threads);
}

void destroy_work_queue(platform_work_queue *queue)
{
	for (u32 i = 0; i < queue->thread_count; ++i)
	{
		platform_thread_kill(queue->threads[i]);
	}
	free(queue->threads);
}

