#ifndef PGN_H
#define PGN_H

#include "common.h"
#include "chess_move.h"

typedef struct pgn_info
{
	const char *event;
	const char *site;
	int year;
	int month;
	int day;
	u64 round;
	const char *white;
	const char *black;
	chess_result result;
} pgn_info;

void fprint_pgn(chess_move *moves, pgn_info info, FILE *stream);

#endif // !PGN_H
