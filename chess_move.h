#ifndef CHESS_MOVE_H
#define CHESS_MOVE_H

#include "common.h"
#include "chess.h"

/*!
	\brief  Chess moves are represented by a u16.

	| First Bit | Last Bit (inclusive) | Function                |
	| --------: | -------------------: | :---------------------- |
	|         0 |                    5 | The square to move from |
	|         6 |                   11 | The square to move to   |
	|        12 |                   13 | The piece to promote to |
	|        14 |                   15 | The move type           |

	If the move is a promotion the piece to promote to is represented as:
	| 2-bit Representation | Piece Type |
	| -------------------: | :--------- |
	|                   00 | Knight     |
	|                   01 | Bishop     |
	|                   10 | Rook       |
	|                   11 | Queen      |

	The type of move is represented by:
	| 2-bit Representation | Move Type  |
	| -------------------: | :--------- |
	|                   00 | None       |
	|                   01 | Promotion  |
	|                   10 | En Passant |
	|                   11 | Castling   |

	\note None represents any normal move or capture.
	\note An en passant move is a move that creates an en passant square.
*/
typedef u16 chess_move;

/*!
	\brief Generates a basic move.

	\param from	The square 
*/
chess_move gen_push(u8 from, u8 to);
chess_move gen_promotion(u8 from, u8 to, chess_piece_type type);
chess_move gen_en_passant(u8 from, u8 to);
chess_move gen_castling(u8 from, u8 to);
u8 move_from(chess_move m);
u8 move_to(chess_move m);
u64 move_from_bit(chess_move m);
u64 move_to_bit(chess_move m);
bool is_promotion(chess_move m);
bool is_castling(chess_move m);
bool is_en_passant(chess_move m);
chess_piece_type promotion_type(chess_move m);
bool move_ok(chess_move m);

void print_move(chess_move move);
void print_move_uci(chess_move move);
void print_move_buffer(chess_move *move_buffer, s16 *score_buffer, u32 num_moves);
void fill_move_string(chess_move move, char *str);
void fill_move_string_uci(chess_move move, char *str);

#endif // !CHESS_MOVE_H

