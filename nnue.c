#include "nnue.h"
#include "platform_common.h"
#include "gl_math.h"
#include "random.h"

#include <immintrin.h>

internal s16 nnue_rnd_init_val(random_series *rnd)
{
	s16 val = rnd_u8_in_range(rnd, 0, 16);
	if (rnd_bool(rnd))
	{
		val = -val;
	}
	return val;
}

void nnue_random_init(nnue_net *net, random_series *rnd)
{
	for (u32 i = 0; i < NNUE_ACCUMULATOR_SIZE * NUM_NEURONS; ++i)
	{
		net->layer1.curr_weights[i] = nnue_rnd_init_val(rnd);
		net->layer1.oppo_weights[i] = nnue_rnd_init_val(rnd);
	}

	for (u32 i = 0; i < NUM_NEURONS; ++i)
	{
		net->layer1.curr_bias[i] = nnue_rnd_init_val(rnd);
		net->layer1.oppo_bias[i] = nnue_rnd_init_val(rnd);
	}

	for (u32 i = 0; i < NUM_NEURONS * 2 * HIDDEN_LAYER_SIZE; ++i)
	{
		net->layer2.weights[i] = nnue_rnd_init_val(rnd);
	}

	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		net->layer2.bias[i] = nnue_rnd_init_val(rnd);
		net->layer3.weights[i] = nnue_rnd_init_val(rnd);
	}

	net->layer3.bias = nnue_rnd_init_val(rnd);
}

void nnue_to_file(nnue_net *net, const char *file, u64 num_games)
{
	if (!write_file(file, (const char *)&num_games, sizeof(num_games)))
	{
		log_fatal("failed to write the neural net to file '%s'\n", file);
	}
	if (!write_file_append(file, (const char *)net, sizeof(nnue_net)))
	{
		log_fatal("failed to write the neural net to file '%s'\n", file);
	}
}

u64 nnue_from_file(nnue_net *net, const char *file)
{
	file_contents fc = read_file(file);
	assert(fc.size == sizeof(nnue_net) + sizeof(u64));
	u64 num_games = *(u64 *)fc.contents;
	memcpy(net, fc.contents + sizeof(u64), sizeof(nnue_net));
	free(fc.contents);
	return num_games;
}

static inline s16 relu(s16 input)
{
	// #todo quantize
	s16 result = 0;
	if (input > 0)
	{
		if (input < INT8_MAX)
		{
			result = input;
		}
		else
		{
			result = INT8_MAX;
		}
	}
	return result;
}

#if 0
// #todo could also be multithreaded
void nnue_update_accumulators(nnue_net *net, chess_board *board)
{
	// #todo sparse updating, pass the move to know what needs to be updated
	memset(&net->white_acc, 0, sizeof(nnue_acc));
	memset(&net->black_acc, 0, sizeof(nnue_acc));

	assert(board->white_king != 0 && board->black_king != 0);
	u8 white_king_sq = bitscan_forward(board->white_king) - 1;
	u8 black_king_sq = bitscan_forward(board->black_king) - 1;

	for (u8 sq = 0; sq < 64; ++sq)
	{
		chess_piece piece = board->squares[sq] & 0x7;
		chess_piece color = board->squares[sq] & 0x18;
		if (piece == EMPTY_SQ || piece == KING) { continue; }
		u16 index = nnue_halfpk_index(white_king_sq, sq, piece, color);
		net->white_acc[index] = 1;
		index = nnue_halfpk_index(black_king_sq, sq, piece, color);
		net->black_acc[index] = 1;
	}
}
#endif

/*
	#note performance increase
	start ticks per call = 156786 -> 91149

	both of my computers seem to support sse4.1-2 and avx2

	avx: 	8 32-bit values
			4 64-bit values
	sse: 	4 32-bit floats
	sse2: 	2 64-bit floats
			2 64-bit ints
			4 32-bit ints
			8 16-bit ints
			16 8-bit ints
*/

u64 num_calls = 0;
u64 num_ticks = 0;

static void layer1_relu(s16 *output, s16 *input)
{
#if __SSE2__
	const u32 in_width = 128 / 16;
	const u32 out_width = 128 / 16;

	const u32 num_out_chunks = NUM_NEURONS / out_width;
	const __m128i zero = _mm_set1_epi16(0);
	const __m128i max = _mm_set1_epi16(127);

	for (u32 i = 0; i < num_out_chunks; ++i)
	{
		const __m128i in = _mm_load_si128((const __m128i *)(input + i * in_width));
		const __m128i positive_only = _mm_max_epi16(in, zero);
		const __m128i capped = _mm_min_epi16(positive_only, max);
		_mm_store_si128((__m128i *)(output + i * out_width), capped);
	}
#else
	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		output[i] = relu(input[i]);
	}
#endif
}

static nnue_layer1_out calc_layer1_output(const nnue_layer_1 *layer, const nnue_acc curr, const nnue_acc oppo)
{
#if __SSE2__
	s16 sse_curr_output[NUM_NEURONS] align32;
	s16 sse_oppo_output[NUM_NEURONS] align32;

	for (u32 neuron_index = 0; neuron_index < NUM_NEURONS / 8; ++neuron_index)
	{
		__m128i curr8 = _mm_load_si128((const __m128i *)(layer->curr_bias + neuron_index * 8));
		__m128i oppo8 = _mm_load_si128((const __m128i *)(layer->oppo_bias + neuron_index * 8));
		for (u32 acc_index = 0; acc_index < NNUE_ACCUMULATOR_SIZE; ++acc_index)
		{
			u32 offset = NUM_NEURONS * acc_index;
			// #todo quicker to make the accumulator load the weight instead of setting to 1 and
			// always add, or have the branching?
			if (curr[acc_index] != 0)
			{
				__m128i weight8 = _mm_load_si128((const __m128i *)(layer->curr_weights + offset + neuron_index * 8));
				curr8 = _mm_add_epi16(curr8, weight8);
			}
			if (oppo[acc_index] != 0)
			{
				__m128i weight8 = _mm_load_si128((const __m128i *)(layer->oppo_weights + offset + neuron_index * 8));
				oppo8 = _mm_add_epi16(oppo8, weight8);
			}
		}
		_mm_store_si128((__m128i *)(sse_curr_output + neuron_index * 8), curr8);
		_mm_store_si128((__m128i *)(sse_oppo_output + neuron_index * 8), oppo8);
	}
	nnue_layer1_out sse_out;
	layer1_relu(sse_out.curr_output, sse_curr_output);
	layer1_relu(sse_out.oppo_output, sse_oppo_output);
	return sse_out;
#else
	s16 curr_output[NUM_NEURONS] align32;
	s16 oppo_output[NUM_NEURONS] align32;

	// #note initialize the neurons to the biases
	for (u32 neuron_index = 0; neuron_index < NUM_NEURONS; ++neuron_index)
	{
		curr_output[neuron_index] = layer->curr_bias[neuron_index];
		oppo_output[neuron_index] = layer->oppo_bias[neuron_index];
	}

	// #note multiply the input and weights
	for (u32 i = 0; i < NNUE_ACCUMULATOR_SIZE; ++i)
	{
		u8 value = curr[i];
		if (value == 0) { continue; }
		for (u32 neuron_index = 0; neuron_index < NUM_NEURONS; ++neuron_index)
		{
			s16 weight = layer->curr_weights[i * (NUM_NEURONS) + neuron_index];
			curr_output[neuron_index] += weight;
		}
	}

	for (u32 i = 0; i < NNUE_ACCUMULATOR_SIZE; ++i)
	{
		u8 value = oppo[i];
		if (value == 0) { continue; }
		for (u32 neuron_index = 0; neuron_index < NUM_NEURONS; ++neuron_index)
		{
			s16 weight = layer->oppo_weights[i * (NUM_NEURONS) + neuron_index];
			oppo_output[neuron_index] += weight;
		}
	}

	nnue_layer1_out out;
	layer1_relu(out.curr_output, curr_output);
	layer1_relu(out.oppo_output, oppo_output);

	return out;
#endif
}

/*
	#note performance increase
	start ticks per call = 33376 -> 18382
*/

// #todo
internal void layer2_relu(s16 *output, s16 *input)
{
#if __SSE2__
	const u32 in_width = 128 / 16;
	const u32 out_width = 128 / 16;

	const u32 num_out_chunks = HIDDEN_LAYER_SIZE / out_width;
	const __m128i zero = _mm_set1_epi16(0);
	const __m128i max = _mm_set1_epi16(127);

	for (u32 i = 0; i < num_out_chunks; ++i)
	{
		const __m128i in = _mm_load_si128((const __m128i *)(input + i * in_width));
		const __m128i positive_only = _mm_max_epi16(in, zero);
		const __m128i capped = _mm_min_epi16(positive_only, max);
		_mm_store_si128((__m128i *)(output + i * out_width), capped);
	}
#else
	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		output[i] = relu(input[i]);
	}
#endif
}

internal nnue_layer2_out calc_layer2_output(const nnue_layer_2 *layer, const nnue_layer1_out *input)
{
	// #note input: 2 * num_neurons
	// #note layer: 2 * num_neurons * hidden_layer_size, hidden_layer_size bias
	// #note output: hidden_layer_size

#if __SSE2__
	s16 sse_output[HIDDEN_LAYER_SIZE];
	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		__m128i sse_sum = _mm_set1_epi32(0);
		for (u32 n = 0; n < NUM_NEURONS / 32; ++n)
		{
			u32 input_index = n * 32;
			__m128i input0 = _mm_load_si128((const __m128i *)(input->curr_output + input_index +  0));
			__m128i input1 = _mm_load_si128((const __m128i *)(input->curr_output + input_index +  8));
			__m128i input2 = _mm_load_si128((const __m128i *)(input->curr_output + input_index + 16));
			__m128i input3 = _mm_load_si128((const __m128i *)(input->curr_output + input_index + 24));

			u32 weight_index = NUM_NEURONS * i;
			__m128i weight0 = _mm_load_si128((const __m128i *)(layer->weights + weight_index + input_index +  0));
			__m128i weight1 = _mm_load_si128((const __m128i *)(layer->weights + weight_index + input_index +  8));
			__m128i weight2 = _mm_load_si128((const __m128i *)(layer->weights + weight_index + input_index + 16));
			__m128i weight3 = _mm_load_si128((const __m128i *)(layer->weights + weight_index + input_index + 24));

			__m128i madd0 = _mm_madd_epi16(input0, weight0);
			__m128i madd1 = _mm_madd_epi16(input1, weight1);
			__m128i madd2 = _mm_madd_epi16(input2, weight2);
			__m128i madd3 = _mm_madd_epi16(input3, weight3);

			__m128i add0 = _mm_add_epi32(madd0, madd1);
			__m128i add1 = _mm_add_epi32(madd2, madd3);
			__m128i add2 = _mm_add_epi32(add0, add1);

			input0 = _mm_load_si128((const __m128i *)(input->oppo_output + input_index +  0));
			input1 = _mm_load_si128((const __m128i *)(input->oppo_output + input_index +  8));
			input2 = _mm_load_si128((const __m128i *)(input->oppo_output + input_index + 16));
			input3 = _mm_load_si128((const __m128i *)(input->oppo_output + input_index + 24));

			weight_index = NUM_NEURONS * (HIDDEN_LAYER_SIZE + i);
			weight0 = _mm_load_si128((const __m128i *)(layer->weights + weight_index + input_index +  0));
			weight1 = _mm_load_si128((const __m128i *)(layer->weights + weight_index + input_index +  8));
			weight2 = _mm_load_si128((const __m128i *)(layer->weights + weight_index + input_index + 16));
			weight3 = _mm_load_si128((const __m128i *)(layer->weights + weight_index + input_index + 24));

			madd0 = _mm_madd_epi16(input0, weight0);
			madd1 = _mm_madd_epi16(input1, weight1);
			madd2 = _mm_madd_epi16(input2, weight2);
			madd3 = _mm_madd_epi16(input3, weight3);

			add0 = _mm_add_epi32(madd0, madd1);
			add1 = _mm_add_epi32(madd2, madd3);
			__m128i add3 = _mm_add_epi32(add0, add1);
			add3 = _mm_add_epi32(add2, add3);
			sse_sum = _mm_add_epi32(add3, sse_sum);
		}
		s32 partial_sums[4] align16;
		_mm_store_si128((__m128i *)partial_sums, sse_sum);
		s32 sum = partial_sums[0] + partial_sums[1] + partial_sums[2] + partial_sums[3];
		sse_output[i] = sum + layer->bias[i];
	}

	nnue_layer2_out sse_out;
	layer2_relu(sse_out.output, sse_output);
	return sse_out;
	
#else

	s16 output[HIDDEN_LAYER_SIZE];
	// #note initialize the neurons to the biases
	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		output[i] = layer->bias[i];
	}

	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		for (u32 neuron_index = 0; neuron_index < NUM_NEURONS; ++neuron_index)
		{
			output[i] += input->curr_output[neuron_index] *
						 layer->weights[NUM_NEURONS * i + neuron_index];
			output[i] += input->oppo_output[neuron_index] *
						 layer->weights[HIDDEN_LAYER_SIZE * NUM_NEURONS + NUM_NEURONS * i + neuron_index];
		}
	}

	nnue_layer2_out out;
	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		out.output[i] = relu(output[i]);
	}
	return out;
#endif
}

internal s16 calc_layer3_output(const nnue_layer_3 *layer, const nnue_layer2_out *input)
{
#if __SSE2__
	__m128i weight0 = _mm_load_si128((const __m128i *)(layer->weights +  0)); // #note contians 8 s16 integers
	__m128i weight1 = _mm_load_si128((const __m128i *)(layer->weights +  8)); // #note contians 8 s16 integers
	__m128i weight2 = _mm_load_si128((const __m128i *)(layer->weights + 16)); // #note contians 8 s16 integers
	__m128i weight3 = _mm_load_si128((const __m128i *)(layer->weights + 24)); // #note contians 8 s16 integers

	__m128i input0 = _mm_load_si128((const __m128i *)(input->output +  0));
	__m128i input1 = _mm_load_si128((const __m128i *)(input->output +  8));
	__m128i input2 = _mm_load_si128((const __m128i *)(input->output +  16));
	__m128i input3 = _mm_load_si128((const __m128i *)(input->output +  24));

	__m128i madd0 = _mm_madd_epi16(weight0, input0); // #note should contain 4 32-bit integer sums
	__m128i madd1 = _mm_madd_epi16(weight1, input1); // #note should contain 4 32-bit integer sums
	__m128i madd2 = _mm_madd_epi16(weight2, input2); // #note should contain 4 32-bit integer sums
	__m128i madd3 = _mm_madd_epi16(weight3, input3); // #note should contain 4 32-bit integer sums

	__m128i add0 = _mm_add_epi32(madd0, madd1);
	__m128i add1 = _mm_add_epi32(madd2, madd3);
	__m128i add2 = _mm_add_epi32(add0, add1);

	s32 partial_sums[4] align16;
	_mm_store_si128((__m128i *)partial_sums, add2);
	s32 sum = partial_sums[0] + partial_sums[1] + partial_sums[2] + partial_sums[3];
	s16 sse_result = sum + layer->bias;
	return sse_result;
#else
	s32 sum32 = 0;
	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		sum32 += input->output[i] * layer->weights[i];
	}
	s16 result = sum32 + layer->bias;
	return result;
#endif
}

void nnue_test(const nnue_net *net, const u8 *curr_acc, const u8 *oppo_acc)
{
	nnue_layer1_out out1 = calc_layer1_output(&net->layer1, curr_acc, oppo_acc);
	nnue_layer2_out out2 = calc_layer2_output(&net->layer2, &out1);
	s16 result = calc_layer3_output(&net->layer3, &out2);
	printf("eval: %d\n", result);
}

s16 nnue_evaluate(const nnue_net *net, const u8 *curr_acc, const u8 *oppo_acc)
{
	nnue_layer1_out out1 = calc_layer1_output(&net->layer1, curr_acc, oppo_acc);
	#if 0
	printf("current output:\n");
	for (u32 i = 0; i < NUM_NEURONS; ++i)
	{
		printf("%6d, ", curr_net->layer1.curr_output[i]);
		printf("\n");
		//if (((i + 1) % 8) == 0) { printf("\n"); }
	}
	printf("\nopponent output:\n");
	for (u32 i = 0; i < NUM_NEURONS; ++i)
	{
		printf("%6d, ", curr_net->layer1.oppo_output[i]);
		printf("\n");
		//if (((i + 1) % 8) == 0) { printf("\n"); }
	}
	#endif
	nnue_layer2_out out2 = calc_layer2_output(&net->layer2, &out1);
	s16 result = calc_layer3_output(&net->layer3, &out2);
	//printf("eval: %d\n", result);
	return result;
}

static f64 relu_der(f64 x)
{
	if (x > 0.0) { return 1; }
	return 0.0;
}

// #note learning rate
f64 eta = 0.2;
#define SIG(x) sigmoidf64((f64)(x) / scaling_factor)
#define LOGIT(x) (s16)(safe_logitf64(x) * scaling_factor)
const f64 scaling_factor = 410.85;

void nnue_backpropagate(nnue_net *net, const u8 *curr_acc, const u8 *oppo_acc, nnue_update_net *update_net, s16 eval, s16 target)
{
	f64 wdl_eval = SIG(eval);
	f64 wdl_target = SIG(target);
	// #todo unused?
	f64 wdl_error = 0.5 * squaref64(wdl_target - wdl_eval);

	// #note accumulators should already be updated
	nnue_layer1_out out1 = calc_layer1_output(&net->layer1, curr_acc, oppo_acc);
	nnue_layer2_out out2 = calc_layer2_output(&net->layer2, &out1);

	// #note update layer3 weights | Output layer
	f64 out_o = wdl_eval;
	// #note same for entire output layer
	f64 delta_o = (out_o - wdl_target) * relu_der(out_o);
	f64 h_error[HIDDEN_LAYER_SIZE];
	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		f64 out_h = SIG(out2.output[i]);
		f64 de_dw = delta_o * out_h;
		f64 dw = eta * de_dw;
		update_net->l3_weights[i] += dw;
		// #note update error
		f64 w = SIG(net->layer3.weights[i]);
		h_error[i] = wdl_error * w * relu_der(out_h);
	}

	// #note update bias
	f64 de_db = delta_o;
	f64 db = eta * de_db;
	update_net->l3_bias += db;

	f64 n1_error[NUM_NEURONS] = { 0 };
	f64 n2_error[NUM_NEURONS] = { 0 };

	// #note update layer2
	for (u32 h_index = 0; h_index < HIDDEN_LAYER_SIZE; ++h_index)
	{
		for (u32 n_index = 0; n_index < NUM_NEURONS; ++n_index)
		{
			// curr
			s16 weight = net->layer2.weights[NUM_NEURONS * h_index + n_index];
			f64 w = SIG(weight);
			f64 error = 0.0;
			for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
			{
				error += h_error[i] * w;
			}
			s8 input = out1.curr_output[n_index];
			f64 i = SIG(input);

			f64 dw = eta * error * i;
			update_net->l2_weights[NUM_NEURONS * h_index + n_index] += dw;

			s8 out = out1.curr_output[n_index];
			f64 out_h = SIG(out);
			n1_error[n_index] += w * h_error[h_index] * relu_der(out_h);

			// oppo
			weight = net->layer2.weights[NUM_NEURONS * HIDDEN_LAYER_SIZE +
										 NUM_NEURONS * h_index + n_index];
			w = SIG(weight);
			error = 0.0;
			for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
			{
				error += h_error[i] * w;
			}
			dw = eta * error * i;
			update_net->l2_weights[NUM_NEURONS * HIDDEN_LAYER_SIZE +
								   NUM_NEURONS * h_index + n_index] += dw;

			out = out1.curr_output[n_index];
			out_h = SIG(out);
			n2_error[n_index] += w * h_error[h_index] * relu_der(out_h);
		}

		// #note update bias
		f64 b_error = 0.0;
		for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
		{
			b_error += h_error[i] * SIG(net->layer2.bias[i]);
		}
		f64 db = eta * b_error;
		update_net->l2_bias[h_index] += db;
	}

	// #note update layer 1 weights
	// #todo not 100% sure this layer should be updated in the same way
	for (u32 acc_index = 0; acc_index < NNUE_ACCUMULATOR_SIZE; ++acc_index)
	{
		// #note input not required because always 0 or 1
		if (curr_acc[acc_index] != 0)
		{
			for (u32 n_index = 0; n_index < NUM_NEURONS; ++n_index)
			{
				f64 error = 0.0;
				for (u32 i = 0; i < NUM_NEURONS; ++i)
				{
					error += n1_error[i] * SIG(net->layer1.curr_weights[NUM_NEURONS * acc_index + i]);
				}
				f64 dw = eta * error;
				update_net->curr_weights[NUM_NEURONS * acc_index + n_index] += dw;
			}
		}
		if (oppo_acc[acc_index] != 0)
		{
			for (u32 n_index = 0; n_index < NUM_NEURONS; ++n_index)
			{
				f64 error = 0.0;
				for (u32 i = 0; i < NUM_NEURONS; ++i)
				{
					error += n2_error[i] * SIG(net->layer1.oppo_weights[NUM_NEURONS * acc_index + i]);
				}
				f64 dw = eta * error;
				update_net->oppo_weights[NUM_NEURONS * acc_index + n_index] += dw;
			}
		}
	}

	// #note update bias
	for (u32 n_index = 0; n_index < NUM_NEURONS; ++n_index)
	{
		f64 curr_error = 0.0;
		f64 oppo_error = 0.0;
		for (u32 i = 0; i < NUM_NEURONS; ++i)
		{
			curr_error += n1_error[i] * SIG(net->layer1.curr_bias[n_index]);
			oppo_error += n2_error[i] * SIG(net->layer1.oppo_bias[n_index]);
		}
		f64 db = eta * curr_error;
		update_net->curr_bias[n_index] += db;
		db = eta * oppo_error;
		update_net->oppo_bias[n_index] += db;
	}
}

/*!
	dv: update value (i.e. from the update net)
	n: num_moves
	ov: original value (i.e. from the original net)
*/
internal s16 update_value(f64 dv, usize n, s16 ov)
{
	f64 dw = dv / (f64)n;
	f64 w = SIG(ov);
	f64 wp = w + dw;
	s16 result = LOGIT(wp);
	return result;
}

void nnue_net_update(nnue_net *net, nnue_update_net *unet, usize num_moves)
{
	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		net->layer3.weights[i] = update_value(unet->l3_weights[i], num_moves, net->layer3.weights[i]);
	}

	// #note update bias
	net->layer3.bias = update_value(unet->l3_bias, num_moves, net->layer3.bias);

	// #note update layer2
	for (u32 h_index = 0; h_index < HIDDEN_LAYER_SIZE; ++h_index)
	{
		for (u32 n_index = 0; n_index < NUM_NEURONS; ++n_index)
		{
			u64 index = NUM_NEURONS * h_index + n_index;
			net->layer2.weights[index] = update_value(unet->l2_weights[index], num_moves, net->layer2.weights[index]);
			usize offset = NUM_NEURONS * HIDDEN_LAYER_SIZE;
			net->layer2.weights[offset + index] = update_value(unet->l2_weights[offset + index], num_moves, net->layer2.weights[offset + index]);
		}

		// #note update bias
		net->layer2.bias[h_index] = update_value(unet->l2_bias[h_index], num_moves, net->layer2.bias[h_index]);
	}

	// #note update layer1
	for (u32 acc_index = 0; acc_index < NNUE_ACCUMULATOR_SIZE; ++acc_index)
	{
		for (u32 n_index = 0; n_index < NUM_NEURONS; ++n_index)
		{
			u64 index = acc_index * NUM_NEURONS + n_index;
			net->layer1.curr_weights[index] = update_value(unet->curr_weights[index], num_moves, net->layer1.curr_weights[index]);
			net->layer1.oppo_weights[index] = update_value(unet->oppo_weights[index], num_moves, net->layer1.oppo_weights[index]);
		}
	}

	// #note update bias
	for (u32 n_index = 0; n_index < NUM_NEURONS; ++n_index)
	{
		net->layer1.curr_bias[n_index] = update_value(unet->curr_bias[n_index], num_moves, net->layer1.curr_bias[n_index]);
		net->layer1.oppo_bias[n_index] = update_value(unet->oppo_bias[n_index], num_moves, net->layer1.oppo_bias[n_index]);
	}
}

typedef struct bitmap_header
{
	u16 file_type;
	u32 file_size;
	u16 reserved0;
	u16 reserved1;
	u32 bitmap_offset;
	u32 size;
	s32 width;
	s32 height;
	u16 planes;
	u16 bits_per_pixel;
	u32 compression;
	u32 size_of_bitmap;
	s32 horz_resolution;
	s32 vert_resolution;
	u32 colors_used;
	u32 colors_important;
} __attribute((packed)) bitmap_header;

typedef struct image_u32
{
	u32 width;
	u32 height;
	u32 *pixels;
} image_u32;

static u32 get_total_pixel_size(image_u32 image)
{
	u32 result = image.width * image.height * sizeof(u32);
	return result;
}

static image_u32 allocate_image(u32 width, u32 height)
{
	image_u32 image;

	image.width = width;
	image.height = height;

	u32 output_pixel_size = get_total_pixel_size(image);
	image.pixels = xmalloc(output_pixel_size);

	return image;
}

static void write_image(image_u32 image, const char *output_filename)
{
	u32 output_pixel_size = get_total_pixel_size(image);

	bitmap_header header = { 0 };
	header.file_type = 0x4D42;
	header.file_size = sizeof(header) + output_pixel_size;
	header.bitmap_offset = sizeof(header);
	header.size = sizeof(header) - 14;
	header.width = image.width;
	header.height = image.height;
	header.planes = 1;
	header.bits_per_pixel = 32;
	header.compression = 0;
	header.size_of_bitmap = output_pixel_size;

#if 0
	// #note c standard library method
 	FILE *out_file = fopen(output_filename, "wb");
	if (out_file)
	{
		size_t chunks_written = fwrite(&header, sizeof(header), 1, out_file);
		if (chunks_written == 0)
		{
			printf("failed to write the bitmap header\n");
		}
		chunks_written = fwrite(image.pixels, output_pixel_size, 1, out_file);
		if (chunks_written == 0)
		{
			printf("failed to write the pixel data\n");
		}
		fclose(out_file);
	}
	else
	{
		printf("unable to write to the output file: %s\n", output_filename);
	}
#endif
	// #note linux method
	int fd = open(output_filename, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (fd != -1)
	{
		ssize_t chunks_written = write(fd, &header, sizeof(header));
		if (chunks_written != sizeof(header))
		{
			printf("failed to write the bitmap header, exiting\n");
			close(fd);
		}

		chunks_written = write(fd, image.pixels, output_pixel_size);
		if (chunks_written != output_pixel_size)
		{
			printf("failed to write the pixel data\n");
		}
		close(fd);
	}
	else
	{
		printf("unable to write to the output file: %s\n", output_filename);
	}
}

static double s16_to_uniform(s16 x)
{
	double d = (double)x / INT16_MAX;
	double result = 0.5 * d + 0.5;
	return result;
}

static u32 net_lerp(vec4 lo, vec4 mid, vec4 hi, double t)
{
	color result;
	if (t < 0.5)
	{
		result = color_lerp(lo, mid, 2.0 * t);
	}
	else
	{
		result = color_lerp(mid, hi, 2.0 * (t - 0.5));
	}
	return (u8)(result.a * 255) << 24 | (u8)(result.r * 255) << 16 | (u8)(result.g * 255) << 8 | (u8)(result.b * 255);
}

internal void nnue_net_to_pixels(u32 *ptr, u32 width, nnue_net *net, color lo, color mid, color hi)
{
	for (u32 y = 0; y < 10 * NUM_NEURONS; ++y)
	{
		for (u32 x = 0; x < width; ++x)
		{
			double t = s16_to_uniform(net->layer1.curr_weights[y * width + x]);
			*ptr++ = net_lerp(lo, mid, hi, t);
		}
	}

	for (u32 i = 0; i < NUM_NEURONS; ++i)
	{
		double t = s16_to_uniform(net->layer1.curr_bias[i]);
		*ptr++ = net_lerp(lo, mid, hi, t);
	}
	for (u32 i = NUM_NEURONS; i < width; ++i)
	{
		*ptr++ = 0xFF000000;
	}

	for (u32 y = 0; y < 2560; ++y)
	{
		for (u32 x = 0; x < width; ++x)
		{
			double t = s16_to_uniform(net->layer1.oppo_weights[y * width + x]);
			*ptr++ = net_lerp(lo, mid, hi, t);
		}
	}

	for (u32 i = 0; i < NUM_NEURONS; ++i)
	{
		double t = s16_to_uniform(net->layer1.oppo_bias[i]);
		*ptr++ = net_lerp(lo, mid, hi, t);
	}
	for (u32 i = NUM_NEURONS; i < width; ++i)
	{
		*ptr++ = 0xFF000000;
	}

	for (u32 y = 0; y < HIDDEN_LAYER_SIZE; ++y)
	{
		for (u32 x = 0; x < width; ++x)
		{
			if (x < 2 * NUM_NEURONS)
			{
				double t = s16_to_uniform(net->layer2.weights[y * 2 * NUM_NEURONS + x]);
				*ptr++ = net_lerp(lo, mid, hi, t);
			}
			else
			{
				*ptr++ = 0xFF000000;
			}
		}
	}

	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		double t = s16_to_uniform(net->layer2.bias[i]);
		*ptr++ = net_lerp(lo, mid, hi, t);
	}
	for (u32 i = HIDDEN_LAYER_SIZE; i < width; ++i)
	{
		*ptr++ = 0xFF000000;
	}

	for (u32 i = 0; i < HIDDEN_LAYER_SIZE; ++i)
	{
		double t = s16_to_uniform(net->layer3.weights[i]);
		*ptr++ = net_lerp(lo, mid, hi, t);
	}
	for (u32 i = HIDDEN_LAYER_SIZE; i < width; ++i)
	{
		*ptr++ = 0xFF000000;
	}

	{
		double t = s16_to_uniform(net->layer3.bias);
		*ptr++ = net_lerp(lo, mid, hi, t);
	}
	for (u32 i = 1; i < width; ++i)
	{
		*ptr++ = 0xFF000000;
	}
}

// #todo better and/or more bmp structuring
void nnue_write_to_bitmap(nnue_net *net, const char *path)
{
	// #todo in case the model size changes the dimensions should be chosen procedurally

	color lo = v4(0, 0, 1, 1);
	color mid = v4(0, 1, 0, 1);
	color hi = v4(1, 0, 0, 1);

	#if 0
	// #note raw data dump
	u32 width = 8192;
	u32 height = width;
	image_u32 image = allocate_image(width, height);

	u32 *white_ptr = image.pixels;
	u32 *black_ptr = image.pixels + (dim / 2) * dim;

	// #note write white net
	// #note write white curr weights
	for (u32 i = 0; i < sizeof(net->white_net.layer1.curr_weights); ++i)
	{
		double t = s16_to_uniform(net->white_net.layer1.curr_weights[i]);
		*white_ptr++ = net_lerp(lo, mid, hi, t);
	}

	while (white_ptr < black_ptr)
	{
		*white_ptr++ = 0xFF000000;
	}

	// #note write black net
	// #note write black curr weights
	for (u32 i = 0; i < sizeof(net->black_net.layer1.curr_weights); ++i)
	{
		double t = s16_to_uniform(net->black_net.layer1.curr_weights[i]);
		*black_ptr++ = net_lerp(lo, mid, hi, t);
	}

	while (black_ptr < image.pixels + (dim * dim))
	{
		*black_ptr++ = 0xFF000000;
	}
	#else
	// #note structured data
	u32 width = 4096;
	u32 height = 5157 * 2;
	image_u32 image = allocate_image(width, height);
	u32 *ptr = image.pixels;
	nnue_net_to_pixels(ptr, width, net, lo, mid, hi);
	#endif

#if 0
	typedef struct nnue_layer_1
	{
		s16 curr_weights[NNUE_ACCUMULATOR_SIZE * NUM_NEURONS] ALIGN32;
		s16 curr_bias[NUM_NEURONS] ALIGN32;

		s16 oppo_weights[NNUE_ACCUMULATOR_SIZE * NUM_NEURONS] ALIGN32;
		s16 oppo_bias[NUM_NEURONS] ALIGN32;
	} nnue_layer_1;

	typedef struct nnue_layer_2
	{
		s16 weights[NUM_NEURONS * HIDDEN_LAYER_SIZE * 2] ALIGN32;
		s16 bias[HIDDEN_LAYER_SIZE] ALIGN32;
	} nnue_layer_2;

	typedef struct nnue_layer_3
	{
		s16 weights[HIDDEN_LAYER_SIZE] ALIGN32;
		s16 bias;
	} nnue_layer_3;
#endif

	// #note reverse image
	for (u32 y = 0; y < image.height / 2; ++y)
	{
		for (u32 x = 0; x < image.width; ++x)
		{
			u32 *a = image.pixels + y * image.width + x;
			u32 *b = image.pixels + (image.height - y - 1) * image.width + x;
			u32 temp = *a;
			*a = *b;
			*b = temp;
		}
	}

	write_image(image, path);
	xfree(image.pixels);
}
