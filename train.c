#include "train.h"
#include "gl_math.h"
#include "chess_move.h"
#include "move_generation.h"
#include "search.h"
#include "pgn.h"

#include <time.h>

internal void eval_game(nnue_net *net, chess_move *moves, f64 game_result)
{
	u32 state_index = 0;
	state_info states[MAX_GAME_MOVES];
	chess_board board = start_board(&states[state_index++]);
	nnue_update_net *update_net = xcalloc(1, sizeof(nnue_update_net));
	for (u32 i = 0; i < buf_len(moves); ++i)
	{
		chess_move move = moves[i];
		if (board.state->side == WHITE)
		{
			apply_move(&board, &states[state_index++], move);
			s16 eval = nnue_evaluate(net, board.accumulators[WHITE], board.accumulators[BLACK]);
			f64 target = INT16_MAX * (2.0 * game_result - 1.0);
			nnue_backpropagate(net, board.accumulators[WHITE], board.accumulators[BLACK],
							   update_net, eval, (s16)target);
		}
		else
		{
			apply_move(&board, &states[state_index++], move);
			s16 eval = nnue_evaluate(net, board.accumulators[BLACK], board.accumulators[WHITE]);
			f64 target = INT16_MAX * (2.0 * game_result - 1.0);
			nnue_backpropagate(net, board.accumulators[BLACK], board.accumulators[WHITE],
							   update_net, eval, (s16)target);
		}
	}

	// #note apply updates
	nnue_net_update(net, update_net, buf_len(moves) / 2 + buf_len(moves) % 2);

	xfree(update_net);
}

internal void train_print_move(u32 index, chess_move move)
{
	u32 move_num = index / 2;
	if ((index & 0x1) == 0)
	{
		printf("%3u. ", move_num + 1);
	}
	print_move_uci(move); printf(" ");
	if ((index % 8) == 7)
	{
		printf("\n    ");
	}
	else
	{
		fflush(stdout);
	}
}

chess_move *run_training_game(random_series *rnd, nnue_net *net, u32 num_threads, u64 ns_per_move,
							  chess_result *result)
{
	*result = CR_ONGOING;

	state_info state = { 0 };
	u32 state_index = 0;
	state_info states[MAX_GAME_MOVES] = { 0 };
	chess_board board = start_board(&state);
	// print_board(&board);

	chess_move *game_moves = NULL;
	u32 move_counter = 0;

	// #note select a random first move to stop repetition of games
	u32 num_random_moves = 4;
	for (u32 i = 0; i < num_random_moves; ++i)
	{
		chess_move moves[MAX_LEGAL_MOVES];
		u32 num_moves = generate_all_moves(&board, moves);
		assert(num_moves > 0);
		u32 random_index = rnd_u8_in_range(rnd, 0, num_moves - 1);
		buf_push(game_moves, moves[random_index]);
		apply_move(&board, &states[state_index++], moves[random_index]);
		train_print_move(move_counter++, moves[random_index]);
	}
	// print_board(&board);

	bool game_over = false;
	f64 game_result = 0.0;

	search_parameters sparam = { 0 };
	transposition_table ttable = trans_table_new(128);

	while (!game_over)
	{
		#if 0
		sparam.start_time = get_time_ns();
		sparam.stop_time = sparam.start_time + ns_per_move;
		sparam.time_set = true;
		sparam.depth = MAX_SEARCH_DEPTH;
		#else
		sparam.start_time = get_time_ns();
		sparam.stop_time = 0;
		sparam.time_set = false;
		sparam.depth = 4;
		#endif
		nnue_search_thread_info info;
		info.sparam = &sparam;
		info.ttable = &ttable;
		info.original_board = &board;
		info.net = net;
		info.num_threads = num_threads;
		info.training = true;
		info.best_move = 0;

		nnue_search_threaded(&info);

		assert(info.best_move != 0);

		chess_result new_result = apply_move(&board, &states[state_index++], info.best_move);
		buf_push(game_moves, info.best_move);
		train_print_move(move_counter++, info.best_move);

		chess_move move_check[MAX_LEGAL_MOVES];
		u32 num_legal_next_moves = generate_all_moves(&board, move_check);
		if (num_legal_next_moves == 0)
		{
			// #note no legal moves, so stalemate or checkmate
			game_over = true;
			check_status status = get_check_status(&board);
			if (status.single_check || status.double_check)
			{
				if (board.state->side == WHITE)
				{
					game_result = 0.0;
					*result = CR_BLACK_CHECKMATE;

				}
				else
				{
					game_result = 1.0;
					*result = CR_WHITE_CHECKMATE;
				}
			}
			else
			{
				game_result = 0.5;
				*result = CR_DRAW_STALEMATE;
			}
		}
		else
		{
			*result = new_result;
			switch (new_result)
			{
				case CR_ONGOING: { /* #note skip */ } break;
				case CR_DRAW_FIFTY_MOVE:
				case CR_DRAW_REPETITION:
				case CR_DRAW_INSUFFICIENT_MATERIAL:
				{
					game_over = true;
					game_result = 0.5;
				} break;
				case CR_DRAW_STALEMATE:
				case CR_WHITE_CHECKMATE:
				case CR_BLACK_CHECKMATE:
				default:
				{
					// #note these should be caught earlier
					invalid_code_path;
				} break;

			}
		}
	}

	eval_game(net, game_moves, game_result);

	free(board.repmap.keys);
	free(board.repmap.vals);
	free(ttable.entries);

	return game_moves;
}

u64 train(random_series *rnd, nnue_net *net, u32 num_threads, u64 ns_per_move,
		  bool output_pgn, u32 count, u64 net_game_count)
{
	u32 white_wins = 0;
	u32 black_wins = 0;
	u32 draws = 0;
	for (u32 i = 0; i < count; ++i)
	{
		printf("\x1b[2J\x1b[H");
		printf("game %lu | training %u / %u | %u threads | white: %u | black: %u | draw: %u\n\n    ",
			   net_game_count + 1, i + 1, count, num_threads, white_wins, black_wins, draws);
		chess_result result;
		chess_move *moves = run_training_game(rnd, net, num_threads, ns_per_move, &result);
		switch (result)
		{
			case CR_WHITE_CHECKMATE:
			{
				printf("White wins\n");
				++white_wins;
			} break;
			case CR_BLACK_CHECKMATE:
			{
				printf("Black wins\n");
				++black_wins;
			} break;
			case CR_DRAW_STALEMATE:
			{
				printf("Stalemate\n");
				++draws;
			} break;
			case CR_DRAW_REPETITION:
			{
				printf("Draw, repetition\n");
				++draws;
			} break;
			case CR_DRAW_INSUFFICIENT_MATERIAL:
			{
				printf("Draw, insufficient material\n");
				++draws;
			} break;
			case CR_DRAW_FIFTY_MOVE:
			{
				printf("Draw, fifty move rule\n");
				++draws;
			} break;
			case CR_ONGOING:
			default:
			{
				invalid_code_path;
			} break;
		}

		if (output_pgn)
		{
			pgn_info pgn;
			pgn.event = "Morr Engine Training";
			pgn.site = "Oslo, NOR";
			time_t t = time(NULL);
			struct tm tm = *localtime(&t);
			pgn.year = tm.tm_year + 1900;
			pgn.month = tm.tm_mon + 1;
			pgn.day = tm.tm_mday;
			pgn.round = net_game_count + i;
			pgn.white = "Morr Engine: White";
			pgn.black = "Morr Engine: Black";
			pgn.result = result;

			char *filename = xmalloc(512);
			snprintf(filename, 512, "game_%lu.pgn", net_game_count + 1);
			FILE *file = fopen(filename, "w");
			fprint_pgn(moves, pgn, file);
			fclose(file);
			xfree(filename);
		}
		buf_free(moves);
		moves = NULL;
		++net_game_count;
	}
	return net_game_count;
}

