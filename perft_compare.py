sf = """
b5b4: 4
e6e5: 29
a7a6: 28
c7c6: 28
d7d6: 29
f7f6: 28
g7g6: 28
h7h6: 28
a7a5: 28
c7c5: 29
d7d5: 27
f7f5: 28
g7g5: 27
h7h5: 28
b8a6: 28
b8c6: 28
g8f6: 28
g8h6: 28
g8e7: 29
c8a6: 28
c8b7: 28
f8a3: 27
f8b4: 3
f8c5: 29
f8d6: 28
f8e7: 28
d8h4: 27
d8g5: 26
d8f6: 27
d8e7: 28
e8e7: 29



"""

custom = """

b5b4: 5
e6e5: 29
a7a6: 28
c7c6: 28
d7d6: 29
f7f6: 28
g7g6: 28
h7h6: 28
a7a5: 28
c7c5: 29
d7d5: 27
f7f5: 28
g7g5: 27
h7h5: 28
b8a6: 28
b8c6: 28
g8f6: 28
g8h6: 28
g8e7: 29
c8a6: 28
c8b7: 28
f8a3: 27
f8b4: 3
f8c5: 29
f8d6: 28
f8e7: 28
d8h4: 27
d8g5: 26
d8f6: 27
d8e7: 28
e8e7: 29

"""

print(".-------+--------------+--------------+--------------.")
print("| move  |  stockfish   |     morr     |     diff     |")
print("|-------+--------------+--------------+--------------|")
red = "\x1b[31;1m"
magenta = "\x1b[35;1m"
reset = "\x1b[0m"

def print_row(color, move, sf_val, mr_val, diff):
	print("| {}{}{} | {}{:>12}{} | {}{:>12}{} | {}{:>12}{} |".format( \
		color, move, reset, \
		color, sf_val, reset, \
		color, mr_val, reset, \
		color, diff, reset))


sf_dict = {}
cu_dict = {}
for line in sf.split('\n'):
	if line == '':
		continue
	key = line.split()[0]
	entry = int(line.split()[1])
	sf_dict[key] = entry

for line in custom.split('\n'):
	if line == '':
		continue
	key = line.split()[0]
	entry = int(line.split()[1])
	cu_dict[key] = entry

for key in sf_dict.keys():
	if key in cu_dict.keys():
		diff = cu_dict[key] - sf_dict[key]
		color = reset
		if diff != 0:
			color = red
			
		print_row(color, key, sf_dict[key], cu_dict[key], diff)

	else:
		print_row(magenta, key, sf_dict[key], "N/A", "N/A")

for key in cu_dict.keys():
	if not key in sf_dict.keys():
		print_row(magenta, key, "N/A", cu_dict[key], "N/A")

print("'-------+--------------+--------------+--------------'")
