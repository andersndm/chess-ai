#include "transposition_table.h"

#include "search.h"

#define tt_decode_score(x) ((s16)(((s32)(x) & 0xFFFF) - INF_BOUND))
#define tt_decode_move(x)  ((chess_move)((x >> 16) & 0xFFFF))
#define tt_decode_depth(x) ((u32)((x >> 48) & 0xFF))
#define tt_decode_flag(x)  ((u8)((x >> 56) & 0xFF))
#define tt_decode

//     64     56      48  40  32  24  16   8   0
//       | 7    | 6     | 5 | 4 | 3 | 2 | 1 | 0 |
//       | flag | depth |   |   | move  | score |
#define tt_encode(s, m, d, f) (				\
		(((s32)(s) + INF_BOUND) & 0xFFFF) |	\
		((u64)(m) << 16) |					\
		(((u64)(d) & 0xFF) << 48) |			\
		(((u64)(f) & 0xFF) << 56)				\
		)

// #note if a new table, it is expected to be zero-inited
transposition_table trans_table_new(u32 num_mb)
{
	u32 mem_size = 0x100000 * num_mb;
	u32 count = mem_size / sizeof(transposition_table_entry);
	transposition_table result = { 0 };
	result.capacity = count;
	result.entries = calloc(result.capacity, sizeof(transposition_table_entry));
	assert(result.entries);
	return result;
}

void trans_table_clear(transposition_table *table)
{
	table->age = 0;
	memset(table->entries, 0, table->capacity * sizeof(transposition_table_entry));
}

void trans_table_store(transposition_table *table, u64 pos_hash, u32 ply,
					   u8 flag, u32 depth, chess_move move, s16 score)
{
	assert(score < AB_BOUND && score > -AB_BOUND);
	assert(depth >= 1 && depth < MAX_SEARCH_DEPTH);
	assert(ply >=  0 && ply < MAX_SEARCH_DEPTH);
	assert(flag <= HF_EXACT);
	assert(move != 0);

	u32 index = pos_hash % table->capacity;
	assert(index < table->capacity);
	transposition_table_entry entry = table->entries[index];
	u32 entry_depth = tt_decode_depth(entry.data);
	if (entry.checksum == 0 || entry.age < table->age || entry_depth <= depth)
	{
		if (score > MATE)
		{
			score += ply;
		}
		else if (score < -MATE)
		{
			score -= ply;
		}

		transposition_table_entry new_entry;
		new_entry.data = tt_encode(score, move, depth, flag);
		new_entry.checksum = pos_hash ^ new_entry.data;
		new_entry.age = table->age;
		table->entries[index] = new_entry;
	}
}

// #note move and score are output parameters
bool trans_table_probe(transposition_table *table, u64 pos_hash, u32 ply, s16 alpha, s16 beta,
					   u32 depth, chess_move *move, s16 *score)
{
	assert(depth >= 1 && depth < MAX_SEARCH_DEPTH);
	assert(alpha < beta);
	assert(alpha >= -AB_BOUND && alpha <= AB_BOUND);
	assert(beta >= -AB_BOUND && beta <= AB_BOUND);

	u32 index = pos_hash % table->capacity;
	assert(index < table->capacity);
	transposition_table_entry entry = table->entries[index];
	u64 checksum = pos_hash ^ entry.data;
	if (checksum == entry.checksum)
	{
		u8 eflag = tt_decode_flag(entry.data);
		u32 edepth = tt_decode_depth(entry.data);
		*move = tt_decode_move(entry.data);
		s16 escore = tt_decode_score(entry.data);

		if (edepth >= depth)
		{
			if (escore > MATE) { escore -= ply; }
			else if (escore < -MATE) { escore += ply; }
			*score = escore;

			if (eflag == HF_ALPHA)
			{
				if (*score <= alpha)
				{
					*score = alpha;
					return true;
				}
			}
			else if (eflag == HF_BETA)
			{
				if (*score >= beta)
				{
					*score = beta;
					return true;
				}
			}
			else if (eflag == HF_EXACT)
			{
				return true;
			}
			else
			{
				invalid_code_path;
			}
		}
	}
	return false;
}

// #note 0 is a valid return, it means to stop searching the principal variation
chess_move probe_pv_move(transposition_table *table, u64 pos_hash, s16 *best_score)
{
	chess_move pv_move = 0;

	u32 index = pos_hash % table->capacity;
	assert(index < table->capacity);
	transposition_table_entry entry = table->entries[index];
	u64 checksum = pos_hash ^ entry.data;
	if (entry.checksum == checksum)
	{
		pv_move = tt_decode_move(entry.data);
		*best_score = tt_decode_score(entry.data);
	}
	return pv_move;
}

