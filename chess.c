#include "chess.h"

#include "chess_move.h"
#include "ctype.h"

chess_color opposite_color(chess_color color)
{
	chess_color result = color == WHITE ? BLACK : WHITE;
	return result;
}

chess_piece_type get_piece_type(chess_piece piece)
{
	#if 0
	if (piece == EMPTY_SQ) { return NONE; }
	assert((piece >= W_PAWN && piece <= W_KING) ||
		   (piece >= B_PAWN && piece <= B_KING));
	if (piece >= B_PAWN)
	{
		return piece - 8;
	}
	return (chess_piece_type)piece;
	#else
	return (0x7 & piece);
	#endif
}

chess_color get_piece_color(chess_piece piece)
{
	#if 0
	assert((piece >= W_PAWN && piece <= W_KING) ||
		   (piece >= B_PAWN && piece <= B_KING));
	if (piece < B_PAWN)
	{
		return WHITE;
	}
	return BLACK;
	#else
	return (0x8 & piece) >> 3;
	#endif
}

chess_piece make_piece(chess_piece_type type, chess_color color)
{
	if (color == BLACK)
	{
		return type + 8;
	}
	return (chess_piece)type;
}

u64 square_bit(u8 sq)
{
	return 0x1ull << (u64)sq;
}

internal char *piece_chars = " PNBRQK??pnbrqk";

char get_piece_char(chess_piece piece)
{
	return piece_chars[piece];
}

char get_file_char(u8 pos)
{
	u8 index = pos % 8;
	return 'a' + index;
}

char get_rank_char(u8 pos)
{
	u8 index = pos / 8;
	assert(index < 8);
	return '1' + index;
}

void print_piece(chess_piece piece)
{
	char c = piece_chars[piece];
	printf("%c", c);
}

