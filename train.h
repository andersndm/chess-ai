#ifndef TRAIN_H
#define TRAIN_H

#include "nnue.h"
#include "chess_board.h"

u64 train(random_series *rnd, nnue_net *net, u32 num_threads, u64 ns_per_move,
		  bool output_pgn, u32 count, u64 net_game_count);

#endif // !TRAIN_H
