#ifndef BASIC_EVAL_H
#define BASIC_EVAL_H

#include "chess_board.h"

s16 basic_eval(const chess_board *board);

#endif // !BASIC_EVAL_H

