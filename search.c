#include "search.h"

#include "move_generation.h"
#include "basic_eval.h"

#define INF_BOUND 32000
#define AB_BOUND 30000
#define MATE (AB_BOUND - MAX_SEARCH_DEPTH)

internal bool move_exists(const chess_board *board, chess_move move)
{
	chess_move moves[MAX_LEGAL_MOVES];
	u32 num_moves = generate_all_moves(board, moves);
	for (u32 move_index = 0; move_index < num_moves; ++move_index)
	{
		if (moves[move_index] == move)
		{
			return true;
		}
	}
	return false;
}

// #note board must be returned to original state when the function returns
// #note move_buffer must fit MAX_SEARCH_DEPTH moves
u32 get_principal_variation(chess_board *board, transposition_table *ttable,
							const u32 depth, chess_move *move_buffer, s16 *best_score)
{
	assert(depth < MAX_SEARCH_DEPTH);
	u32 move_index = 0;

	state_info states[MAX_SEARCH_DEPTH];
	chess_move move = probe_pv_move(ttable, board->state->hash, best_score);
	while (move != 0 && move_index < depth)
	{
		if (move_exists(board, move))
		{
			apply_move(board, states + move_index, move);
			move_buffer[move_index++] = move;
			move = probe_pv_move(ttable, board->state->hash, best_score);
		}
		else
		{
			log_error("move does not exist.");
			break;
		}
	}
	#if 0
	if (move_index < depth - 1)
	{
		log_warn("move_index: %u depth: %u", move_index, depth);
		debug_break;
		probe_pv_move(ttable, board->state->hash, best_score);
	}
	#endif

	// #note undo moves so board is in original state
	for (u32 i = move_index; i > 0; --i)
	{
		chess_move umove = move_buffer[i - 1];
		undo_move(board, umove);
	}

	return move_index;
}

// #note check if the time is up, or there is an interrupt etc.
internal void check_search_stop(search_parameters *sparam)
{
	if (sparam->time_set && get_time_ns() > sparam->stop_time)
	{
		sparam->stopped = true;
	}
}

internal void search_prep(search_parameters *sparam, transposition_table *ttable)
{
	++ttable->age;

	sparam->nodes = 0;
	sparam->stopped = false;
	memset(sparam->pv_array, 0, sizeof(sparam->pv_array));
}

#include "platform_common.h"

// #note most valuable victim least valuable attacker (mvvlva)
// scores moves based on the pieces involved in a capture
// #note victim scores
#define empty_vs    0
#define pawn_vs   100
#define knight_vs 200
#define bishop_vs 300
#define rook_vs   400
#define queen_vs  500
#define king_vs   600

#define mvvlva_entry(type) [type] = { 6 - type + empty_vs,  6 - type + pawn_vs, \
									  6 - type + knight_vs, 6 - type + bishop_vs, \
									  6 - type + rook_vs, 6 - type + queen_vs, \
									  6 - type + king_vs }
// #note [attacking_type][capture_type]
internal const s16 mvvlva_scores[NUM_PIECE_TYPES][NUM_PIECE_TYPES] =
{
	mvvlva_entry(NONE), mvvlva_entry(PAWN), mvvlva_entry(KNIGHT), mvvlva_entry(BISHOP),
	mvvlva_entry(ROOK), mvvlva_entry(QUEEN), mvvlva_entry(KING)
};
#undef mvvlva_entry
#undef king_vs
#undef queen_vs
#undef rook_vs
#undef bishop_vs
#undef knight_vs
#undef pawn_vs
#undef empty_vs

// #note select the highest scoring move, starting at move_index, places the best move at
// start index
internal void select_next_move(const chess_board *board, chess_move *move_buffer, s16 *score_buffer,
							   u32 start_index, u32 num_moves)
{
	u32 best_index = start_index;
	s16 best_score = 0;
	for (u32 move_index = start_index; move_index < num_moves; ++move_index)
	{
		if (score_buffer[move_index] > best_score)
		{
			best_score = score_buffer[move_index];
			best_index = move_index;
		}
	}

	chess_move temp_move = move_buffer[start_index];
	move_buffer[start_index] = move_buffer[best_index];
	move_buffer[best_index] = temp_move;

	s16 temp_score = score_buffer[start_index];
	score_buffer[start_index] = score_buffer[best_index];
	score_buffer[best_index] = temp_score;
}

internal void score_moves_quiescence(const chess_board *board, const chess_move *move_buffer,
									 s16 *score_buffer, u32 num_moves)
{
	for (u32 move_index = 0; move_index < num_moves; ++move_index)
	{
		chess_move move = move_buffer[move_index];
		chess_piece_type att_type = get_piece_type(board->squares[move_from(move)]);
		chess_piece_type cap_type = get_piece_type(board->squares[move_to(move)]);
		s16 score = mvvlva_scores[att_type][cap_type];
		if (att_type == PAWN && move_to(move) == board->state->en_passant)
		{
			score = mvvlva_scores[PAWN][PAWN];
		}

		score_buffer[move_index] = score;
	}
}

internal void score_moves(const chess_board *board, search_data *sdata,
						  const chess_move *move_buffer, s16 *score_buffer, u32 num_moves)
{
	for (u32 move_index = 0; move_index < num_moves; ++move_index)
	{
		chess_move move = move_buffer[move_index];
		chess_piece_type att_type = get_piece_type(board->squares[move_from(move)]);
		chess_piece_type cap_type = get_piece_type(board->squares[move_to(move)]);
		s16 score = mvvlva_scores[att_type][cap_type];
		if ((cap_type != NONE) ||
			(att_type == PAWN && move_to(move) == board->state->en_passant))
		{
			score += 20000;
		}
		else if (sdata->killers[0][sdata->ply] == move)
		{
			score = 15000;
		}
		else if (sdata->killers[1][sdata->ply] == move)
		{
			score = 10000;
		}
		else
		{
			score = sdata->history[board->squares[move_from(move)]][move_to(move)];
		}

		score_buffer[move_index] = score;
	}
}

#define PRINT_SEARCH 0
#if PRINT_SEARCH
internal void print_indent(u32 indent)
{
	for (u32 i = 0; i < indent; ++i)
	{
		printf("    ");
	}
}

internal u32 global_indent = 0;

#define indent print_indent(global_indent)
#endif

internal s16 basic_quiescence(search_parameters *sparam, search_data *sdata,  chess_board *board,
							  s16 alpha, s16 beta)
{
	assert(beta > alpha);
	#if PRINT_SEARCH
	indent;
	PC_RED_BOLD("quiescence: ");
	printf("alpha: %d, beta %d\n", alpha, beta);
	#endif

	// #note check if search should stop every 2048th node
	if ((sparam->nodes & 2047) == 0)
	{
		check_search_stop(sparam);
	}

	++sparam->nodes;

	s16 score = basic_eval(board);
	#if PRINT_SEARCH
	indent;
	printf("board eval: %d\n", score);
	#endif
	assert(score > -AB_BOUND && score < AB_BOUND);

	if (sdata->ply > MAX_SEARCH_DEPTH - 1)
	{
	#if PRINT_SEARCH
		indent;
		PC_RED_BOLD("returning: ");
		printf("max search reached\n");
	#endif
		return score;
	}

	if (score >= beta)
	{
	#if PRINT_SEARCH
		indent;
		PC_RED_BOLD("returning: ");
		printf("beta: %d\n", beta);
	#endif
		return beta;
	}

	if (score > alpha)
	{
		alpha = score;
	}

	score = -AB_BOUND;

	chess_move moves[MAX_LEGAL_MOVES];
	u32 num_moves = generate_all_captures(board, moves);

	s16 scores[MAX_LEGAL_MOVES];
	score_moves_quiescence(board, moves, scores, num_moves);

	#if PRINT_SEARCH
	print_move_buffer(moves, scores, num_moves);
	#endif

	for (u32 move_index = 0; move_index < num_moves; ++move_index)
	{
		select_next_move(board, moves, scores, move_index, num_moves);
		chess_move move = moves[move_index];
	#if PRINT_SEARCH
		indent;
		PC_GREEN("applying: "); print_move_uci(move); printf("\n");
	#endif

		++sdata->ply;
		state_info state;
		chess_result result = apply_move(board, &state, move);
		switch (result)
		{
			case CR_ONGOING:
			{
				#if PRINT_SEARCH
				++global_indent;
				#endif
				score = -basic_quiescence(sparam, sdata, board, -beta, -alpha);
				#if PRINT_SEARCH
				--global_indent;
				#endif
			} break;
			case CR_DRAW_REPETITION:
			case CR_DRAW_INSUFFICIENT_MATERIAL:
			case CR_DRAW_FIFTY_MOVE:
			{
				score = 0;
			} break;
			case CR_DRAW_STALEMATE:
			case CR_WHITE_CHECKMATE:
			case CR_BLACK_CHECKMATE:
			{
				invalid_code_path;
			} break;
		}

		--sdata->ply;
		undo_move(board, move);
	#if PRINT_SEARCH
		indent;
		PC_GREEN("undoing: "); print_move_uci(move); printf("\n");
	#endif

		if (sparam->stopped)
		{
			return 0;
		}

		if (score > alpha)
		{
			if (score >= beta)
			{
	#if PRINT_SEARCH
				indent;
				PC_RED_BOLD("returning: ");
				printf("beta: %d\n", beta);
	#endif
				return beta;
			}
			alpha = score;
		}
	}

	#if PRINT_SEARCH
	indent;
	PC_RED_BOLD("returning: ");
	printf("alpha: %d\n", alpha);
	#endif
	return alpha;
}

internal s16 basic_alpha_beta(search_parameters *sparam, search_data *sdata,  chess_board *board,
							  transposition_table *ttable,
							  s16 alpha, s16 beta, u32 depth, bool do_null)
{
	#if PRINT_SEARCH
	indent;
	PC_BLUE_BOLD("alpha-beta: ");
	printf("depth: %u, alpha: %d, beta %d\n", depth, alpha, beta);
	#endif
	assert(beta > alpha);

	if (depth == 0)
	{
		#if PRINT_SEARCH
		++global_indent;
		#endif
		s16 score = basic_quiescence(sparam, sdata, board, alpha, beta);
		#if PRINT_SEARCH
		--global_indent;
		#endif
		return score;
	}

	// #note check if search should stop every 2048th node
	if ((sparam->nodes & 2047) == 0)
	{
		check_search_stop(sparam);
	}

	++sparam->nodes;

	if (sdata->ply > MAX_SEARCH_DEPTH - 1)
	{
		s16 score = basic_eval(board);
	#if PRINT_SEARCH
		indent;
		PC_BLUE_BOLD("returning: ");
		printf("max depth reached, score: %d\n", score);
	#endif
		return score;
	}

	check_status status = get_check_status(board);
	if (status.single_check || status.double_check)
	{
		++depth;
	#if PRINT_SEARCH
		indent;
		PC_MAGENTA("in check, depth: %u\n", depth);
	#endif
	}

	s16 score = -AB_BOUND;
	chess_move pv_move = 0;

	if (trans_table_probe(ttable, board->state->hash, sdata->ply, alpha, beta,
						  depth, &pv_move, &score))
	{
	#if PRINT_SEARCH
		indent;
		PC_BLUE_BOLD("returning: ");
		printf("ttable probe score: %d\n", score);
	#endif
		return score;
	}

	if (do_null &&
		!(status.single_check || status.double_check) &&
		sdata->ply > 0 &&
		depth >= 4 &&
		(board->color_bbs[board->state->side] &
		 (board->type_bbs[KNIGHT] | board->type_bbs[BISHOP] |
		  board->type_bbs[ROOK] | board->type_bbs[QUEEN])) > 0)
	{
		state_info state;
		apply_null_move(board, &state);
		score = -basic_alpha_beta(sparam, sdata, board, ttable, -beta, -beta + 1, depth - 4, false);
		undo_null_move(board);
		if (sparam->stopped)
		{
			return 0;
		}
		if (score >= beta && abs(score) < MATE)
		{
			return beta;
		}
	}

	chess_move moves[MAX_LEGAL_MOVES];
	u32 num_moves = generate_all_moves(board, moves);
	if (num_moves == 0)
	{
		if (status.single_check || status.double_check)
		{
			return -AB_BOUND + sdata->ply;
		}
		else
		{
			// #note stalemate
			return 0;
		}
	}

	s16 scores[MAX_LEGAL_MOVES];
	score_moves(board, sdata, moves, scores, num_moves);

	s16 old_alpha = alpha;
	chess_move best_move = 0;

	s16 best_score = -AB_BOUND;
	score = -AB_BOUND;

	if (pv_move != 0)
	{
		for (u32 move_index = 0; move_index < num_moves; ++move_index)
		{
			if (moves[move_index] == pv_move)
			{
				scores[move_index] = 25000;
				break;
			}
		}
	}

	#if PRINT_SEARCH
	print_move_buffer(moves, scores, num_moves);
	#endif

	for (u32 move_index = 0; move_index < num_moves; ++move_index)
	{
		select_next_move(board, moves, scores, move_index, num_moves);
		chess_move move = moves[move_index];
	#if PRINT_SEARCH
		indent;
		PC_GREEN("applying: "); print_move_uci(move); printf("\n");
	#endif
		++sdata->ply;
		state_info state;
		chess_result result = apply_move(board, &state, move);
		switch (result)
		{
			case CR_ONGOING:
			{
				#if PRINT_SEARCH
				++global_indent;
				#endif
				score = -basic_alpha_beta(sparam, sdata, board, ttable,
										  -beta, -alpha, depth - 1, do_null);
				#if PRINT_SEARCH
				--global_indent;
				#endif
			} break;
			case CR_DRAW_REPETITION:
			case CR_DRAW_INSUFFICIENT_MATERIAL:
			case CR_DRAW_FIFTY_MOVE:
			{
				score = 0;
			} break;
			case CR_DRAW_STALEMATE:
			case CR_WHITE_CHECKMATE:
			case CR_BLACK_CHECKMATE:
			{
				invalid_code_path;
			} break;
		}

		--sdata->ply;
		undo_move(board, move);
	#if PRINT_SEARCH
		indent;
		PC_GREEN("undoing: "); print_move_uci(move); printf("\n");
	#endif

		if (sparam->stopped)
		{
			return 0;
		}

		if (score > best_score)
		{
			best_score = score;
			best_move = move;
			if (score > alpha)
			{
				if (score >= beta)
				{
					if (board->squares[move_to(move)] == EMPTY_SQ)
					{
						sdata->killers[1][sdata->ply] = sdata->killers[0][sdata->ply];
						sdata->killers[0][sdata->ply] = move;
					}

					trans_table_store(ttable, board->state->hash, sdata->ply,
									  HF_BETA, depth, best_move, beta);

	#if PRINT_SEARCH
					indent;
					PC_BLUE_BOLD("returning: ");
					printf("beta: %d\n", beta);
	#endif
					return beta;
				}

				alpha = score;

				if (board->squares[move_to(move)] == EMPTY_SQ)
				{
					sdata->history[board->squares[move_from(move)]][move_to(move)] += depth;
				}
			}
		}

		assert(alpha >= old_alpha);
		if (alpha != old_alpha)
		{
			trans_table_store(ttable, board->state->hash, sdata->ply,
								HF_EXACT, depth, best_move, best_score);
		}
		else
		{
			trans_table_store(ttable, board->state->hash, sdata->ply,
								HF_ALPHA, depth, best_move, alpha);
		}
	}

	#if PRINT_SEARCH
	indent;
	PC_BLUE_BOLD("returning: ");
	printf("alpha: %d\n", alpha);
	#endif
	return alpha;
}

typedef struct search_worker_thread_info
{
	search_parameters *sparam;
	chess_board *board;
	transposition_table *ttable;

	bool is_printer;
} search_worker_thread_info;

void *basic_iterative_deepen(void *data)
{
	search_worker_thread_info *info = data;
	search_data sdata = { 0 };

	chess_move best_move = 0;
	s16 best_score = -AB_BOUND;
	u32 pv_num_moves = 0;

	for (u32 current_depth = 1;
		 current_depth <= info->sparam->depth;
		 ++current_depth)
	{
		best_score = basic_alpha_beta(info->sparam, &sdata, info->board, info->ttable,
									  -AB_BOUND, AB_BOUND, current_depth, true);
		u64 current_end_time = get_time_ns();
		assert(current_end_time >= info->sparam->start_time);
		u64 ms_dt = (current_end_time - info->sparam->start_time) / ms_to_ns(1);

		if (info->sparam->stopped)
		{
			break;
		}

		if (info->is_printer)
		{
			s16 _ignore;
			pv_num_moves = get_principal_variation(info->board, info->ttable, current_depth,
												   info->sparam->pv_array, &_ignore);
			best_move = info->sparam->pv_array[0];
			assert(best_move);
			char move_str[6];
			fill_move_string_uci(best_move, move_str);
			printf("info score cp %6d depth %2d nodes %20lu time %7lu pv",
				best_score, current_depth, info->sparam->nodes, ms_dt);

			for (u32 pv_index = 0; pv_index < pv_num_moves; ++pv_index)
			{
				char move_str[6];
				fill_move_string_uci(info->sparam->pv_array[pv_index], move_str);
				printf(" %s", move_str);
			}
			printf("\n");
		}
	}

	if (info->is_printer)
	{
		info->sparam->stopped = true;
	}

	return NULL;
}

void *basic_search_threaded(void *data)
{
	search_thread_info *info = data;

	chess_move best_move = 0;

	search_prep(info->sparam, info->ttable);

	// #todo selecting and returning a bookmove would go here

	if (best_move == 0)
	{
		search_worker_thread_info *sinfos = malloc(sizeof(search_worker_thread_info) *
												   info->num_threads);
		chess_board *boards = malloc(sizeof(chess_board) * info->num_threads);
		platform_thread *threads = malloc(sizeof(platform_thread) * info->num_threads);
		for (u32 i = 0; i < info->num_threads; ++i)
		{
			chess_board *board = boards + i;
			copy_board(board, info->original_board);

			search_worker_thread_info *sinfo = sinfos + i;
			sinfo->board = board;
			sinfo->sparam = info->sparam;
			sinfo->ttable = info->ttable;
			if (i == 0)
			{
				sinfo->is_printer = true;
			}
			else
			{
				sinfo->is_printer = false;
			}
			*(threads + i) = platform_thread_create(basic_iterative_deepen, sinfo);
		}

		for (u32 i = 0; i < info->num_threads; ++i)
		{
			platform_thread_join(*(threads + i));
			hashmap_clear(&(boards + i)->repmap);
		}

		free(boards);
		free(sinfos);
	}

	best_move = info->sparam->pv_array[0];

	char uci_move[6] = { 0 };
	fill_move_string_uci(best_move, uci_move);
	printf("bestmove %s\n", uci_move);

	// #note freed here because the uci input thread doesn't necessarily know when the search is done.
	free(info);

	return NULL;
}

internal s16 nnue_eval(const nnue_net *net, chess_board *board)
{
	s16 result = 0;
	if (board->state->side == WHITE)
	{
		result = nnue_evaluate(net, board->accumulators[WHITE], board->accumulators[BLACK]);
	}
	else
	{
		result = nnue_evaluate(net, board->accumulators[BLACK], board->accumulators[WHITE]);
	}

	if (result > AB_BOUND)
	{
		result = 25000;
	}
	if (result < -AB_BOUND)
	{
		result = -25000;
	}

	return result;
}

internal s16 nnue_quiescence(search_parameters *sparam, search_data *sdata,  chess_board *board,
							 const nnue_net *net, s16 alpha, s16 beta)
{
	assert(beta > alpha);
	#if PRINT_SEARCH
	indent;
	PC_RED_BOLD("quiescence: ");
	printf("alpha: %d, beta %d\n", alpha, beta);
	#endif

	// #note check if search should stop every 2048th node
	if ((sparam->nodes & 2047) == 0)
	{
		check_search_stop(sparam);
	}

	++sparam->nodes;

	s16 score = nnue_eval(net, board);
	#if PRINT_SEARCH
	indent;
	printf("board eval: %d\n", score);
	#endif
	assert(score > -AB_BOUND && score < AB_BOUND);

	if (sdata->ply > MAX_SEARCH_DEPTH - 1)
	{
	#if PRINT_SEARCH
		indent;
		PC_RED_BOLD("returning: ");
		printf("max search reached\n");
	#endif
		return score;
	}

	if (score >= beta)
	{
	#if PRINT_SEARCH
		indent;
		PC_RED_BOLD("returning: ");
		printf("beta: %d\n", beta);
	#endif
		return beta;
	}

	if (score > alpha)
	{
		alpha = score;
	}

	score = -AB_BOUND;

	chess_move moves[MAX_LEGAL_MOVES];
	u32 num_moves = generate_all_captures(board, moves);

	s16 scores[MAX_LEGAL_MOVES];
	score_moves_quiescence(board, moves, scores, num_moves);

	#if PRINT_SEARCH
	print_move_buffer(moves, scores, num_moves);
	#endif

	for (u32 move_index = 0; move_index < num_moves; ++move_index)
	{
		select_next_move(board, moves, scores, move_index, num_moves);
		chess_move move = moves[move_index];
	#if PRINT_SEARCH
		indent;
		PC_GREEN("applying: "); print_move_uci(move); printf("\n");
	#endif

		++sdata->ply;
		state_info state;
		chess_result result = apply_move(board, &state, move);
		switch (result)
		{
			case CR_ONGOING:
			{
				#if PRINT_SEARCH
				++global_indent;
				#endif
				score = -basic_quiescence(sparam, sdata, board, -beta, -alpha);
				#if PRINT_SEARCH
				--global_indent;
				#endif
			} break;
			case CR_DRAW_REPETITION:
			case CR_DRAW_INSUFFICIENT_MATERIAL:
			case CR_DRAW_FIFTY_MOVE:
			{
				score = 0;
			} break;
			case CR_DRAW_STALEMATE:
			case CR_WHITE_CHECKMATE:
			case CR_BLACK_CHECKMATE:
			{
				invalid_code_path;
			} break;
		}

		--sdata->ply;
		undo_move(board, move);
	#if PRINT_SEARCH
		indent;
		PC_GREEN("undoing: "); print_move_uci(move); printf("\n");
	#endif

		if (sparam->stopped)
		{
			return 0;
		}

		if (score > alpha)
		{
			if (score >= beta)
			{
	#if PRINT_SEARCH
				indent;
				PC_RED_BOLD("returning: ");
				printf("beta: %d\n", beta);
	#endif
				return beta;
			}
			alpha = score;
		}
	}

	#if PRINT_SEARCH
	indent;
	PC_RED_BOLD("returning: ");
	printf("alpha: %d\n", alpha);
	#endif
	return alpha;
}

internal s16 nnue_alpha_beta(search_parameters *sparam, search_data *sdata,  chess_board *board,
							 transposition_table *ttable, const nnue_net *net,
							 s16 alpha, s16 beta, u32 depth, bool do_null)
{
	#if PRINT_SEARCH
	indent;
	PC_BLUE_BOLD("alpha-beta: ");
	printf("depth: %u, alpha: %d, beta %d\n", depth, alpha, beta);
	#endif
	assert(beta > alpha);

	if (depth == 0)
	{
		#if PRINT_SEARCH
		++global_indent;
		#endif
		s16 score = nnue_quiescence(sparam, sdata, board, net, alpha, beta);
		#if PRINT_SEARCH
		--global_indent;
		#endif
		return score;
	}

	// #note check if search should stop every 2048th node
	if ((sparam->nodes & 2047) == 0)
	{
		check_search_stop(sparam);
	}

	++sparam->nodes;

	if (sdata->ply > MAX_SEARCH_DEPTH - 1)
	{
		s16 score = nnue_eval(net, board);
	#if PRINT_SEARCH
		indent;
		PC_BLUE_BOLD("returning: ");
		printf("max depth reached, score: %d\n", score);
	#endif
		return score;
	}

	check_status status = get_check_status(board);
	if (status.single_check || status.double_check)
	{
		++depth;
	#if PRINT_SEARCH
		indent;
		PC_MAGENTA("in check, depth: %u\n", depth);
	#endif
	}

	s16 score = -AB_BOUND;
	chess_move pv_move = 0;

	if (trans_table_probe(ttable, board->state->hash, sdata->ply, alpha, beta,
						  depth, &pv_move, &score))
	{
	#if PRINT_SEARCH
		indent;
		PC_BLUE_BOLD("returning: ");
		printf("ttable probe score: %d\n", score);
	#endif
		return score;
	}

	if (do_null &&
		!(status.single_check || status.double_check) &&
		sdata->ply > 0 &&
		depth >= 4 &&
		(board->color_bbs[board->state->side] &
		 (board->type_bbs[KNIGHT] | board->type_bbs[BISHOP] |
		  board->type_bbs[ROOK] | board->type_bbs[QUEEN])) > 0)
	{
		state_info state;
		apply_null_move(board, &state);
		score = -nnue_alpha_beta(sparam, sdata, board, ttable, net,
								 -beta, -beta + 1, depth - 4, false);
		undo_null_move(board);
		if (sparam->stopped)
		{
			return 0;
		}
		if (score >= beta && abs(score) < MATE)
		{
			return beta;
		}
	}

	chess_move moves[MAX_LEGAL_MOVES];
	u32 num_moves = generate_all_moves(board, moves);
	if (num_moves == 0)
	{
		if (status.single_check || status.double_check)
		{
			return -AB_BOUND + sdata->ply;
		}
		else
		{
			// #note stalemate
			return 0;
		}
	}

	s16 scores[MAX_LEGAL_MOVES];
	score_moves(board, sdata, moves, scores, num_moves);

	s16 old_alpha = alpha;
	chess_move best_move = 0;

	s16 best_score = -AB_BOUND;
	score = -AB_BOUND;

	if (pv_move != 0)
	{
		for (u32 move_index = 0; move_index < num_moves; ++move_index)
		{
			if (moves[move_index] == pv_move)
			{
				scores[move_index] = 25000;
				break;
			}
		}
	}

	#if PRINT_SEARCH
	print_move_buffer(moves, scores, num_moves);
	#endif

	for (u32 move_index = 0; move_index < num_moves; ++move_index)
	{
		select_next_move(board, moves, scores, move_index, num_moves);
		chess_move move = moves[move_index];
	#if PRINT_SEARCH
		indent;
		PC_GREEN("applying: "); print_move_uci(move); printf("\n");
	#endif
		++sdata->ply;
		state_info state;
		chess_result result = apply_move(board, &state, move);
		switch (result)
		{
			case CR_ONGOING:
			{
				#if PRINT_SEARCH
				++global_indent;
				#endif
				score = -nnue_alpha_beta(sparam, sdata, board, ttable, net,
										  -beta, -alpha, depth - 1, do_null);
				#if PRINT_SEARCH
				--global_indent;
				#endif
			} break;
			case CR_DRAW_REPETITION:
			case CR_DRAW_INSUFFICIENT_MATERIAL:
			case CR_DRAW_FIFTY_MOVE:
			{
				score = 0;
			} break;
			case CR_DRAW_STALEMATE:
			case CR_WHITE_CHECKMATE:
			case CR_BLACK_CHECKMATE:
			{
				invalid_code_path;
			} break;
		}

		--sdata->ply;
		undo_move(board, move);
	#if PRINT_SEARCH
		indent;
		PC_GREEN("undoing: "); print_move_uci(move); printf("\n");
	#endif

		if (sparam->stopped)
		{
			return 0;
		}

		if (score > best_score)
		{
			best_score = score;
			best_move = move;
			if (score > alpha)
			{
				if (score >= beta)
				{
					if (board->squares[move_to(move)] == EMPTY_SQ)
					{
						sdata->killers[1][sdata->ply] = sdata->killers[0][sdata->ply];
						sdata->killers[0][sdata->ply] = move;
					}

					trans_table_store(ttable, board->state->hash, sdata->ply,
									  HF_BETA, depth, best_move, beta);

	#if PRINT_SEARCH
					indent;
					PC_BLUE_BOLD("returning: ");
					printf("beta: %d\n", beta);
	#endif
					return beta;
				}

				alpha = score;

				if (board->squares[move_to(move)] == EMPTY_SQ)
				{
					sdata->history[board->squares[move_from(move)]][move_to(move)] += depth;
				}
			}
		}

		assert(alpha >= old_alpha);
		if (alpha != old_alpha)
		{
			trans_table_store(ttable, board->state->hash, sdata->ply,
								HF_EXACT, depth, best_move, best_score);
		}
		else
		{
			trans_table_store(ttable, board->state->hash, sdata->ply,
								HF_ALPHA, depth, best_move, alpha);
		}
	}

	#if PRINT_SEARCH
	indent;
	PC_BLUE_BOLD("returning: ");
	printf("alpha: %d\n", alpha);
	#endif
	return alpha;
}

typedef struct nnue_search_worker_thread_info
{
	search_parameters *sparam;
	chess_board *board;
	transposition_table *ttable;
	const nnue_net *net;

	bool is_printer;
	bool is_training;
} nnue_search_worker_thread_info;

void *nnue_iterative_deepen(void *data)
{
	nnue_search_worker_thread_info *info = data;
	search_data sdata = { 0 };

	chess_move best_move = 0;
	s16 best_score = -AB_BOUND;
	u32 pv_num_moves = 0;

	for (u32 current_depth = 1;
		 current_depth <= info->sparam->depth;
		 ++current_depth)
	{
		best_score = nnue_alpha_beta(info->sparam, &sdata, info->board, info->ttable, info->net,
									  -AB_BOUND, AB_BOUND, current_depth, true);
		u64 current_end_time = get_time_ns();
		assert(current_end_time >= info->sparam->start_time);
		u64 ms_dt = (current_end_time - info->sparam->start_time) / ms_to_ns(1);

		if (info->sparam->stopped)
		{
			break;
		}

		if (info->is_printer)
		{
			s16 _ignore;
			pv_num_moves = get_principal_variation(info->board, info->ttable, current_depth,
												   info->sparam->pv_array, &_ignore);
			best_move = info->sparam->pv_array[0];
			assert(best_move);
			if (!info->is_training)
			{
				char move_str[6];
				fill_move_string_uci(best_move, move_str);
				printf("info score cp %6d depth %2d nodes %20lu time %7lu pv",
					best_score, current_depth, info->sparam->nodes, ms_dt);

				for (u32 pv_index = 0; pv_index < pv_num_moves; ++pv_index)
				{
					char move_str[6];
					fill_move_string_uci(info->sparam->pv_array[pv_index], move_str);
					printf(" %s", move_str);
				}
				printf("\n");
			}
		}
	}

	if (info->is_printer)
	{
		info->sparam->stopped = true;
	}

	return NULL;
}

void *nnue_search_threaded(void *data)
{
	nnue_search_thread_info *info = data;

	chess_move best_move = 0;

	search_prep(info->sparam, info->ttable);

	// #todo selecting and returning a bookmove would go here

	if (best_move == 0)
	{
		nnue_search_worker_thread_info *sinfos = malloc(sizeof(nnue_search_worker_thread_info) *
												   info->num_threads);
		chess_board *boards = malloc(sizeof(chess_board) * info->num_threads);
		platform_thread *threads = malloc(sizeof(platform_thread) * info->num_threads);
		for (u32 i = 0; i < info->num_threads; ++i)
		{
			chess_board *board = boards + i;
			copy_board(board, info->original_board);

			nnue_search_worker_thread_info *sinfo = sinfos + i;
			sinfo->board = board;
			sinfo->sparam = info->sparam;
			sinfo->ttable = info->ttable;
			sinfo->net = info->net;
			sinfo->is_training = info->training;
			if (i == 0)
			{
				sinfo->is_printer = true;
			}
			else
			{
				sinfo->is_printer = false;
			}
			*(threads + i) = platform_thread_create(nnue_iterative_deepen, sinfo);
		}

		for (u32 i = 0; i < info->num_threads; ++i)
		{
			platform_thread_join(*(threads + i));
			hashmap_clear(&(boards + i)->repmap);
		}

		free(boards);
		free(sinfos);
	}

	best_move = info->sparam->pv_array[0];

	if (info->training)
	{
		info->best_move = best_move;
	}
	else
	{
		char uci_move[6] = { 0 };
		fill_move_string_uci(best_move, uci_move);
		printf("bestmove %s\n", uci_move);

		// #note freed here because the uci input thread doesn't necessarily know when the
		// search is done.
		free(info);
	}

	return NULL;
}
