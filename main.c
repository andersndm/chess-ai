#define TIME_AVERAGE 0

#ifdef __linux__
#include <unistd.h> // #note needed for usleep on linux
#endif

#ifndef _WIN32
#include <time.h>
#endif // !_WIN32

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "platform_common.h"

#include "train.h"
#include "uci.h"

bool log_all = true;
void pl_log(log_kind kind, const char *msg, va_list args)
{
	const char *type_str = NULL;
	switch (kind)
	{
		case LOG_WARN:
		{
			type_str = "warn:  ";
		} break;
		case LOG_ERROR:
		{
			type_str = "error: ";
		} break;
		case LOG_FATAL:
		{
			type_str = "fatal: ";
		} break;
		case LOG_INFO:
		{
			type_str = "info:  ";
		} break;
	}

	if (log_all || kind == LOG_FATAL)
	{
		switch (kind)
		{
			case LOG_WARN:
			{
				PC_MAGENTA_BOLD("%s", type_str);
			} break;
			case LOG_ERROR:
			{
				PC_RED_BOLD("%s", type_str);
			} break;
			case LOG_FATAL:
			{
				PC_RED_BOLD("%s", type_str);
			} break;
			case LOG_INFO:
			{
				PC_WHITE_BOLD("%s", type_str);
			} break;
		}
		vprintf(msg, args);
		printf("\n");
	}

	if (kind == LOG_FATAL)
	{
		//SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Fatal Error", log_message_buffer, NULL);
		debug_break;
		exit(1);
	}
}

void error_printer log_info(const char *msg, ...)
{
	va_list args;
	va_start(args, msg);
	pl_log(LOG_INFO, msg, args);
	va_end(args);
}

void error_printer log_warn(const char *msg, ...)
{
	va_list args;
	va_start(args, msg);
	pl_log(LOG_WARN, msg, args);
	va_end(args);
}

void error_printer log_error(const char *msg, ...)
{
	va_list args;
	va_start(args, msg);
	pl_log(LOG_ERROR, msg, args);
	va_end(args);
}

void error_printer log_fatal(const char *msg, ...)
{
	va_list args;
	va_start(args, msg);
	pl_log(LOG_FATAL, msg, args);
	va_end(args);
}

typedef enum flag_kind
{
	FLAG_BOOL,
	FLAG_STR,
	FLAG_ENUM,
	FLAG_INT
} flag_kind;

typedef struct flag_def
{
	flag_kind kind;
	const char *name;
	const char *help;
	const char *arg_name;
	const char **options;
	int num_options;
	struct
	{
		int *i;
		bool *b;
		const char **s;
	} ptr;
} flag_def;

flag_def *flag_defs = NULL;

void add_flag_bool(const char *name, bool *ptr, const char *help)
{
	buf_push(flag_defs, (flag_def){ .kind = FLAG_BOOL, .name = name, .help = help, .ptr.b = ptr });
}

void add_flag_str(const char *name, const char **ptr, const char *arg_name, const char *help)
{
	buf_push(flag_defs, (flag_def){ .kind = FLAG_STR, .name = name, .arg_name = arg_name, .help = help, .ptr.s = ptr });
}

void add_flag_enum(const char *name, int *ptr, const char *help,
				   const char **options, int num_options)
{
	buf_push(flag_defs, (flag_def){ .kind = FLAG_ENUM, .name = name, .help = help, .ptr.i = ptr, .options = options, .num_options = num_options });
}

void add_flag_int(const char *name, int *ptr, const char *help)
{
	buf_push(flag_defs, (flag_def){ .kind = FLAG_INT, .name = name, .help = help, .ptr.i = ptr });
}

flag_def *get_flag_def(const char *name)
{
	for (usize i = 0; i < buf_len(flag_defs); ++i)
	{
		if (strcmp(flag_defs[i].name, name) == 0)
		{
			return &flag_defs[i];
		}
	}
	return 0;
}

void print_flags_usage(const char *program_name)
{
	printf("usage: %s [flag]+\n", program_name);
	printf("flags:\n");
	for (usize i = 0; i < buf_len(flag_defs); ++i)
	{
		flag_def flag = flag_defs[i];
		char note[256] = {0};
		char format[256];
		switch (flag.kind)
		{
			case FLAG_STR:
			{
				snprintf(format, sizeof(format), "%s <%s>", flag.name, flag.arg_name ? flag.arg_name : "value");
				if (*flag.ptr.s)
				{
					snprintf(note, sizeof(note), " (default: %s)", *flag.ptr.s);
				}
			} break;
			case FLAG_ENUM:
			{
				char *end = format + sizeof(format);
				char *ptr = format;
				ptr += snprintf(ptr, (unsigned long)(end - ptr), "%s <", flag.name);
				for (int k = 0; k < flag.num_options; ++k)
				{
					ptr += snprintf(ptr, (unsigned long)(end - ptr), "%s%s", k == 0 ? "" : "|", flag.options[k]);
					if (k == *flag.ptr.i)
					{
						snprintf(note, sizeof(note), " (default: %s)", flag.options[k]);
					}
				}
				snprintf(ptr, (unsigned long)(end - ptr), ">");
			} break;
			case FLAG_BOOL:
			{
				snprintf(format, sizeof(format), "%s", flag.name);
			} break;
			case FLAG_INT:
			{
				snprintf(format, sizeof(format), "%s", flag.name);
				if (*flag.ptr.i)
				{
					snprintf(note, sizeof(note), " (default: %d)", *flag.ptr.i);
				}
			} break;
		}
		printf(" -%-32s %s%s\n", format, flag.help ? flag.help : "", note);
	}
}

const char *parse_flags(int *argc_ptr, const char ***argv_ptr)
{
	int argc = *argc_ptr;
	const char **argv = *argv_ptr;
	const char *program_name = argv[0];
	int i;
	for (i = 1; i < argc; ++i)
	{
		const char *arg = argv[i];
		const char *name = arg;
		if (*name == '-')
		{
			++name;
			if (*name == '-')
			{
				++name;
			}
			flag_def *flag = get_flag_def(name);
			if (!flag)
			{
				log_error("unknown flag: %s\n", arg);
				print_flags_usage(program_name);
				exit(1);
			}

			switch (flag->kind)
			{
				case FLAG_BOOL:
				{
					*flag->ptr.b = true;
				} break;
				case FLAG_STR:
				{
					if (i + 1 < argc)
					{
						++i;
						*flag->ptr.s = argv[i];
					}
					else
					{
						log_error("no value argument after -%s\n", arg);
						print_flags_usage(program_name);
						exit(1);
					}
				} break;
				case FLAG_ENUM:
				{
					const char *option;
					if (i + 1 < argc)
					{
						++i;
						option = argv[i];
					}
					else
					{
						log_error("no value after -%s\n", name, arg);
						print_flags_usage(program_name);
						exit(1);
					}
					bool found = false;
					for (int k = 0; k < flag->num_options; ++k)
					{
						if (strcmp(flag->options[k], option) == 0)
						{
							*flag->ptr.i = k;
							found = true;
							break;
						}
					}
					if (!found)
					{
						log_error("invalid value '%s' for %s\n", option, arg);
						print_flags_usage(program_name);
						exit(1);
					}
				} break;
				case FLAG_INT:
				{
					if (i + 1 < argc)
					{
						++i;
						*flag->ptr.i = atoi(argv[i]);
					}
					else
					{
						log_error("no value after -%s\n", name, arg);
						print_flags_usage(program_name);
						exit(1);
					}
				} break;
				default:
				{
					log_fatal("unhandled flag kind\n");
				} break;
			}
		}
		else
		{
			break;
		}
	}

	*argc_ptr = argc - i;
	*argv_ptr = argv + i;

	return program_name;
}

#include "nnue.h"

int main(int argc, const char * argv[])
{
	bool flag_help = false;
	bool flag_version = false;
	bool flag_no_book = false;
	int flag_num_threads = 8;
	const char *flag_model_path = "debug";
	bool flag_create_net = false;
	bool flag_output_pgn = false;
	int flag_train = 0;

	add_flag_bool("help", &flag_help, "show this message.");
	add_flag_bool("version", &flag_version, "show program version.");
	add_flag_bool("nobook", &flag_no_book, "disable book moves in search and training.");
	add_flag_int("numthreads", &flag_num_threads, "set the number of threads to use (min 1).");
	add_flag_str("model", &flag_model_path, "model path", "the local path to the folder containing the nnue net to use.");
	add_flag_bool("createnet", &flag_create_net, "create a new nnue net.");
	add_flag_bool("outputpgn", &flag_output_pgn, "will output a pgn for each training game.");
	add_flag_int("train", &flag_train, "the number of training games to run (0 means do not train).");
	const char *program_name = parse_flags(&argc, &argv);

	if (flag_help)
	{
		print_flags_usage(program_name);
		return 0;
	}
	if (flag_version)
	{
		printf("%s %d.%d.%d\n", NAME, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
		return 0;
	}

	if (flag_num_threads < 1)
	{
		log_error("program must use a minimum of 1 threads to run.");
		return 1;
	}

	if (flag_num_threads > MAX_THREADS)
	{
		log_warn("maximum thread count currently set to %u.", MAX_THREADS);
		flag_num_threads = MAX_THREADS;
	}

	if (flag_no_book)
	{
		log_warn("book moves are not currently implemented, ignoring.");
	}

	nnue_net *net = malloc(sizeof(nnue_net));
	assert(net);
	char model_path[512];
	snprintf(model_path, 512, "models/%s/model.nnue", flag_model_path);

	u64 net_game_count = 0;

	random_series rnd = rnd_new_series();
	if (flag_create_net)
	{
		nnue_random_init(net, &rnd);
		nnue_to_file(net, model_path, 0);
	}
	else
	{
		net_game_count = nnue_from_file(net, model_path);
	}

	u64 start_time = get_time_ns();

	if (flag_train > 0)
	{
		char bitmap_path[512];
		snprintf(bitmap_path, 512, "%lu_start.bmp", start_time);
		nnue_write_to_bitmap(net, bitmap_path);
		u64 ns_per_move = s_to_ns(5ull);
		u64 games_played = train(&rnd, net, flag_num_threads, ns_per_move, true, flag_train, net_game_count);
		nnue_to_file(net, model_path, games_played);
		snprintf(bitmap_path, 512, "%lu_end.bmp", start_time);
		nnue_write_to_bitmap(net, bitmap_path);
	}
	else
	{
		uci(net, flag_num_threads);
	}

	if (net)
	{
		free(net);
	}

	return 0;
}
