#ifndef THREADING_H
#define THREADING_H

#include "common.h"

#include <pthread.h>
#include <semaphore.h>

typedef void *(*platform_thread_proc)(void *);
typedef sem_t thread_semaphore;
typedef pthread_mutex_t thread_mutex;

typedef struct platform_thread
{
	pthread_t handle;
} platform_thread;

platform_thread platform_thread_create(platform_thread_proc start, void *data);
void platform_thread_join(platform_thread thread);
void platform_thread_kill(platform_thread thread);
u32 platform_get_thread_id(void);

sem_t platform_semaphore_create(void);
void platform_semaphore_release(sem_t *semaphore);
void platform_semaphore_wait(sem_t *semaphore);

pthread_mutex_t platform_mutex_create(void);
void platform_mutex_lock(pthread_mutex_t *mutex);
void platform_mutex_unlock(pthread_mutex_t *mutex);

#define write_barrier wrng_ignore_push(WRNG_EXTENSION_TOKEN); \
	asm volatile ("" ::: "memory"); \
	wrng_ignore_pop; \
	_mm_sfence();

#define read_barrier wrng_ignore_push(WRNG_EXTENSION_TOKEN); \
	asm volatile ("" ::: "memory"); \
	wrng_ignore_pop; \

/*!
	\brief	Compares the value pointed to by ptr with expected_value. If they are equal, new_value
				is written at ptr.

	\returns	Returns the initial value pointed to by ptr.
 */
static inline u32 atomic_compare_exchange_u32(volatile u32 *ptr, u32 expected_value, u32 new_value)
{
	u32 result = __sync_val_compare_and_swap(ptr, expected_value, new_value);
	return result;
}

/*!
	\brief	Atomically increments the value pointed to by ptr.
 */
static inline void atomic_inc_u32(volatile u32 *ptr)
{
	__atomic_fetch_add(ptr, 1, __ATOMIC_SEQ_CST);
}

typedef struct platform_work_queue platform_work_queue;
typedef void (*platform_work_queue_callback)(platform_work_queue *queue, void *data);

typedef struct platform_work_queue_entry
{
	platform_work_queue_callback callback;
	void *data;
} platform_work_queue_entry;

struct platform_work_queue
{
	u32 volatile completion_goal;
	u32 volatile completion_count;

	u32 volatile next_entry_to_write;
	u32 volatile next_entry_to_read;

	thread_semaphore semaphore;

	pthread_mutex_t print_mutex;
	pthread_mutex_t table_mutex;

	platform_work_queue_entry entries[256];

	u32 thread_count;
	platform_thread *threads;
};

void add_work_queue_entry(platform_work_queue *queue,
						  platform_work_queue_callback callback, void *data);
void complete_all_work(platform_work_queue *queue);
bool all_work_completed(platform_work_queue *queue);
void cancel_remaining_work(platform_work_queue *queue);
void make_work_queue(platform_work_queue *queue, u32 thread_count);
void join_work_queue(platform_work_queue *queue);
void destroy_work_queue(platform_work_queue *queue);

#endif // !THREADING_H
