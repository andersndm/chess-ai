#ifndef UCI_H
#define UCI_H

#include "common.h"
#include "nnue.h"

#define NAME "morr"
#define AUTHOR "andm"
#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_PATCH 0

#define MAX_THREADS 32

void uci(nnue_net *net, u32 num_threads);

#endif // !UCI_H

