#ifndef TRANSPOSITION_TABLE_H
#define TRANSPOSITION_TABLE_H

#include "common.h"
#include "chess_move.h"
#include "threading.h"

#define HF_ALPHA 0x0
#define HF_BETA  0x1
#define HF_EXACT 0x2

typedef struct transposition_table_entry
{
	u64 data;
	u64 checksum;
	u32 age;
} transposition_table_entry;

typedef struct transposition_table
{
	transposition_table_entry *entries;
	u32 capacity;
	u16 age;
} transposition_table;

// #todo quick but very memory inefficient hashmap implementation

// #todo if vice's hashing works remove mutex
transposition_table trans_table_new(u32 num_mb);
void trans_table_store(transposition_table *table, u64 pos_hash, u32 ply,
					   u8 flag, u32 depth, chess_move move, s16 score);
bool trans_table_probe(transposition_table *table, u64 pos_hash, u32 ply, s16 alpha, s16 beta,
					   u32 depth, chess_move *move, s16 *score);
void trans_table_clear(transposition_table *table);
chess_move probe_pv_move(transposition_table *table, u64 pos_hash, s16 *best_score);

#endif // !TRANSPOSITION_TABLE_H
