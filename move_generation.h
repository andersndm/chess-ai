#ifndef MOVE_GENERATION_H
#define MOVE_GENERATION_H

#include "chess_move.h"
#include "chess_board.h"

#if 0
#define MAX_KNIGHT_MOVES  8
#define MAX_BISHOP_MOVES 13
#define MAX_ROOK_MOVES   14
#define MAX_QUEEN_MOVES  27
#endif

// #note according to chessprogramming.org the maximum legal moves for a given position ~218,
// certainly does not exceed 256
#define MAX_LEGAL_MOVES 256

u64 find_pawn_checkers(u8 pos, chess_color color, u64 target_pawns);
u64 find_knight_checkers(u8 pos, u64 target_knights);
u64 find_bishop_checkers(u8 pos, u64 target_bishops, u64 occupancy);
u64 find_rook_checkers(u8 pos, u64 target_rooks, u64 occupancy);
u64 find_queen_checkers(u8 pos, u64 target_queens, u64 occupancy);

u32 generate_knight_moves(u64 knights, u64 valid_squares, chess_move *move_buffer);
u32 generate_bishop_moves(u64 bishops, u64 self_pieces, u64 occupancy,
						  chess_move *move_buffer);
u32 generate_rook_moves(u64 rooks, u64 self_pieces, u64 occupancy,
						chess_move *move_buffer);
u32 generate_queen_moves(u64 queens, u64 self_pieces, u64 occupancy,
						 chess_move *move_buffer);

u32 generate_all_moves(const chess_board *board, chess_move *move_buffer);
u32 generate_all_captures(const chess_board *board, chess_move *move_buffer);

#endif // !MOVE_GENERATION_H
