#include "chess_move.h"

/*!
	\brief Monte Carlo Tree Search algorith
	1. Choose the action that maximizes q + u, where u is a function of p and n that increases if an
	action has not been explored much relative to the other actions, or if it has a high prior
	probability.
		Early on u dominates (promoting exploration), and later q becomes more important
	(exploitation).
		u(state, action) =
			c_puct * p(state, action) * sqrt(sum_b(n(state, b))
			                            -----------------------
										(1 + n(state, action)))
		c_puct is a hyper-parameter that balances exploration vs exploitation, standard value is 1.0.
		p(state, action) is the prior probability received from the nn.
		The denominator is the sqrt of the sum of n for all possible actions from the given state.

	2. Continue until a leaf node is reached. The state of the leaf node is passed into the nn which
	returns p, the probability distribution for selecting each possible move, and v, the value of the
	leaf node's state. The distributions are attached to each branching move.

	3. Backup the previous edges adjusting each node's fields:
		n = n + 1
		w = w + v
		q = w / n

	Allegedly repeat 1600 times

	4. Select a move.
		For competitive play select the action with the greatest N (deterministically).
		For training, select randomly from the probability distribution:
			pi(a | s) =     N(s, a)^(1/tau)
			            ----------------------
						sum_b(N(s, b)^(1/tau))
		tau is a temperature parameter, set to 1 at the beginning of the game, and to a very small
		value after a fixed number of moves.

	End:
		The selected move becomes the new root node, discarding all the rest. Retain all the leaves
		in the search tree stemming from the chosen move. Continue repeating each time until the
		game is over.
*/

/*!
	\brief Node in a MCTS

	\field n: u32 - The number of times this state has been explored from the root state
	\field w: f32 - The total value of this state
	\field q: f32 - The mean value of the next state, this is simply the next node's w/n
	\field p: f32 - The prior probability of selecting the action that led to this state,
					it is queried from the nn every time a leaf node is reached
*/
