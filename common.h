#ifndef COMMON_H
#define COMMON_H

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif // !_CRT_SECURE_NO_WARNINGS

#include <math.h>
#include <inttypes.h>
#include <limits.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef _WIN32
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#else
#define strdup _strdup
#endif // !_WIN32

typedef enum log_kind
{
	LOG_INFO,
	LOG_WARN,
	LOG_ERROR,
	LOG_FATAL
} log_kind;

void log_info(const char *msg, ...);
void log_warn(const char *msg, ...);
void log_error(const char *msg, ...);
void log_fatal(const char *msg, ...);

#undef bool
#undef false
#undef true

#define false (0 != 0)
#define true (0 == 0)

#if 1
#define global static
#define internal static
#define persist static
#endif

typedef uint8_t byte;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef size_t usize;

typedef uint32_t bool;

typedef float f32;
typedef double f64;

#ifdef _WIN32
#define PL_WIN

#elif defined(__linux__)
#define PL_LINUX

#elif defined(__APPLE__)
#define PL_OSX

#else
#error "unknown platform"
#endif // _WIN32, __linux__, __APPLE__

#ifdef __clang__
#define WRNG_MISSING_PROTOTYPES "clang diagnostic ignored \"-Wmissing-prototypes\""
#define WRNG_NOT_INLINED "clang diagnostic ignored \"-Wunknown\""
#define WRNG_ADDRESS_OF_PACKED_MEMBER "clang diagnostic ignored \"-Waddress-of-packed-member\""
#define WRNG_UNUSED_PARAMETER "clang diagnostic ignored \"-Wunused-parameter\""
#define WRNG_SWITCH_ENUM "clang diagnostic ignored \"-Wswitch-enum\""
#define WRNG_SWITCH "clang diagnostic ignored \"-Wswitch\""
#define WRNG_PEDANTIC "clang diagnostic ignored \"-Wpedantic\""
#define WRNG_SET_UNUSED_VARIABLE "clang diagnostic ignored \"-Wunused-but-set-variable\""
#define WRNG_EXTENSION_TOKEN "clang diagnostic ignored \"-Wlanguage-extension-token\""

#define wrng_ignore(wrng) _Pragma(wrng)
#define wrng_ignore_push(wrng) _Pragma("clang diagnostic push") _Pragma(wrng)
#define wrng_ignore_pop _Pragma("clang diagnostic pop")
#else
#define wrng_missing_prototypes
#define wrng_not_inlined
#define wrng_address_of_packed_member
#define wrng_unused_parameter
#define wrng_switch_enum
#define wrng_switch

#define wrng_ignore(wrng)
#define wrng_ignore_push(wrng)
#define wrng_ignore_pop
#endif

#ifdef DEBUG
#ifdef _WIN32
#define debug_break __debugbreak()
//#define debug_break
#else
#define debug_break __asm__ volatile("int $0x03")
//#define debug_break
#endif // _WIN32
#else
// #todo internal release vs final release debug_break
#define debug_break __asm__ volatile("int $0x03")
#endif // DEBUG

#if DEBUG
#define assert(expression) if(!(expression)) { log_fatal("assertion failed:\n    %s:%d: \"%s\"", __FILE__, __LINE__, #expression); }
#else
#define assert(expression)
#endif // DEBUG
#define invalid_code_path log_fatal("invalid code path reached! %s:%d", __FILE__, __LINE__);
#define unimplemented log_fatal("unimplemented! %s:%d", __FILE__, __LINE__);

#define array_count(array) (sizeof(array) / sizeof((array)[0]))

#define min(x, y) ((x) <= (y) ? (x) : (y))
#define max(x, y) ((x) >= (y) ? (x) : (y))

#define clamp_min(x, min_val) max(x, min_val)
#define clamp_max(x, max_val) min(x, max_val)

#define is_pow2(x) (((x) != 0) && (((x) & ((x) - 1)) == 0))

#define align_down(n, a) (u64)((n) & (u64)~((s64)(a) - 1))
#define align_up(n, a) ALIGN_DOWN((n) + (u64)((s64)(a) - 1), (a))
#define align_down_ptr(p, a) (((void *)ALIGN_DOWN((uintptr_t)(p), (a))))
#define align_up_ptr(p, a) ((void *)ALIGN_UP((uintptr_t)(p), (a)))

void print_u8_binary(u8 value);

static inline u32 popcount(u64 x)
{
	return __builtin_popcountll(x);
}

static inline u32 lsb(u64 x)
{
	assert(x != 0);
	return __builtin_ctzll(x);
}

static inline u32 lsb_pop(u64 *x)
{
	u32 result = lsb(*x);
	*x &= *x - 1;
	return result;
}

// -------------------------------------------------------------------------------------------------
// #section errors
// -------------------------------------------------------------------------------------------------
// {{{

#ifdef _WIN32
#define error_printer
#define error_printer_loc
#else
#define error_printer __attribute__((__format__ (__printf__, 1, 0)))
#define error_printer_loc __attribute__((__format__ (__printf__, 2, 0)))
#endif

//void error_printer io_error(const char *fmt, ...);

// }}}
// -------------------------------------------------------------------------------------------------
// #section memory
// -------------------------------------------------------------------------------------------------
// {{{

void *xmalloc(usize num_bytes);
void *xcalloc(usize num_elems, usize elem_size);
void *xrealloc(void *ptr, usize num_bytes);
void xfree(void *ptr);

// }}}
// -------------------------------------------------------------------------------------------------
// #section stretchy buffers
// -------------------------------------------------------------------------------------------------
// {{{

// #todo specific vector for moves may allow optimizations
typedef struct buf_header
{
	size_t len;
	size_t cap;
	char buffer[];
} buf_header;

#define buf__hdr(buf) ((buf_header *)((u8 *)(buf) - offsetof(buf_header, buffer)))

#define buf_len(buf) ((buf) ? buf__hdr(buf)->len : 0)
#define buf_cap(buf) ((buf) ? buf__hdr(buf)->cap : 0)
#define buf_end(buf) ((buf) + buf_len(buf))
#define buf_sizeof(buf) ((buf) ? buf_len(buf) * sizeof(*buf) : 0)

#define buf_free(buf) ((buf) ? (xfree(buf__hdr(buf)), (buf) = 0) : 0)
#define buf_fit(buf, n) ((n) <= buf_cap(buf) ? 0 : ((buf) = buf__grow((buf), (n), sizeof(*(buf)))))
#define buf_push(buf, ...) (buf_fit((buf), 1 + buf_len(buf)), (buf)[buf__hdr(buf)->len++] = (__VA_ARGS__))
#define buf_printf(buf, ...) ((buf) = buf__printf((buf), __VA_ARGS__))
#define buf_clear(buf) ((buf) ? buf__hdr(buf)->len = 0 : 0)

void *buf__grow(const void *buf, usize new_len, usize elem_size);
char *buf__printf(char *buf, const char *fmt, ...);

double ticks_to_s(u64 ticks);
u64 get_ticks(void);

// }}}
// -------------------------------------------------------------------------------------------------
// #section hashmap
// -------------------------------------------------------------------------------------------------
// {{{

typedef struct hashmap
{
	u64 *keys;
	u64 *vals;
	usize len;
	usize cap;
} hashmap;

hashmap hashmap_new(u32 size);
// #note sentinel value is 0
u64 hashmap_get(hashmap *map, u64 key);
u64 hashmap_remove(hashmap *map, u64 key);
void hashmap_put(hashmap *map, u64 key, u64 val);
void hashmap_clear(hashmap *map);

// }}}

char *get_stdin(u32 max);

#define us_to_ns(us) (us * 1000)
#define ms_to_ns(ms) (us_to_ns(ms * 1000))
#define s_to_ns(s) (ms_to_ns(s * 1000))

u64 get_time_ns(void);
void print_time_spent(u64 nsec_diff);

#endif // !COMMON_H
