#include "chess_move.h"
#include "chess_board.h"

#include "platform_common.h"

#include "chess.h"

#define FROM_MASK 0x3F

#define TO_MASK   0xFC0
#define TO_SHIFT  6

#define PROMOTION_PIECE_MASK  0x3000
#define PROMOTION_PIECE_SHIFT 12

#define PROMOTION_MOVE    0x4000
#define EN_PASSANT_MOVE   0x8000
#define CASTLING_MOVE     0xC000
#define SPECIAL_MOVE_MASK 0xC000

chess_move gen_push(u8 from, u8 to)
{
	assert(from < 64 && to < 64);
	chess_move result = from;
	result |= (to << TO_SHIFT);
	return result;
}

chess_move gen_promotion(u8 from, u8 to, chess_piece_type type)
{
	assert(type >= KNIGHT && type <= QUEEN);
	chess_move result = gen_push(from, to);
	result |= (type << PROMOTION_PIECE_SHIFT);
	result |= PROMOTION_MOVE;
	return result;
}

chess_move gen_en_passant(u8 from, u8 to)
{
	chess_move result = gen_push(from, to);
	result |= EN_PASSANT_MOVE;
	return result;
}

chess_move gen_castling(u8 from, u8 to)
{
	chess_move result = gen_push(from, to);
	result |= CASTLING_MOVE;
	return result;
}

u8 move_from(chess_move m)
{
	return m & FROM_MASK;
}

u8 move_to(chess_move m)
{
	return (m & TO_MASK) >> TO_SHIFT;
}

u64 move_from_bit(chess_move m)
{
	u64 result = 1ull << move_to(m);
	return result;
}

u64 move_to_bit(chess_move m)
{
	u64 result = 1ull << move_to(m);
	return result;
}

bool is_promotion(chess_move m)
{
	return (m & SPECIAL_MOVE_MASK) == PROMOTION_MOVE;
}

bool is_castling(chess_move m)
{
	return (m & SPECIAL_MOVE_MASK) == CASTLING_MOVE;
}

bool is_en_passant(chess_move m)
{
	return (m & SPECIAL_MOVE_MASK) == EN_PASSANT_MOVE;
}

chess_piece_type promotion_type(chess_move m)
{
	chess_piece_type result = (m & PROMOTION_PIECE_MASK);
	result >>= PROMOTION_PIECE_SHIFT;
	result = result + KNIGHT;
	return result;
}

bool move_ok(chess_move m)
{
	u8 to = move_to(m);
	u8 from = move_from(m);
	return to != from;
}

void print_square(u8 pos)
{
	u8 rank = pos / 8;
	u8 file = pos % 8;
	const char *rank_str = "12345678";
	const char *file_str = "abcdefgh";
	printf("%c%c", file_str[file], rank_str[rank]);
}

// #note str must be at least 6 chars.
void fill_move_string(chess_move move, char *str)
{
	assert(move_ok(move));
	if (is_castling(move))
	{
		if (move_to(move) > move_from(move))
		{
			str[0] = 'O';
			str[1] = '-';
			str[2] = 'O';
			str[3] = 0;
		}
		else
		{
			str[0] = 'O';
			str[1] = '-';
			str[2] = 'O';
			str[3] = '-';
			str[4] = 'O';
			str[5] = 0;
		}
	}
	else
	{
		u8 from = move_from(move);
		u8 to = move_to(move);
		assert(from < 64);
		assert(to < 64);
		str[0] = 'a' + (from % 8);
		str[1] = '1' + (from / 8);
		str[2] = 'a' + (to % 8);
		str[3] = '1' + (to / 8);
		if (is_promotion(move))
		{
			str[4] = get_piece_char(promotion_type(move) | 0x8);
			str[5] = 0;
		}
		else
		{
			str[4] = 0;
		}
	}
}

// #note str must be at least 6 chars.
void fill_move_string_uci(chess_move move, char *str)
{
	assert(move_ok(move));
	u8 from = move_from(move);
	u8 to = move_to(move);
	assert(from < 64);
	assert(to < 64);
	str[0] = 'a' + (from % 8);
	str[1] = '1' + (from / 8);
	str[2] = 'a' + (to % 8);
	str[3] = '1' + (to / 8);
	if (is_promotion(move))
	{
		str[4] = get_piece_char(promotion_type(move) | 0x8);
		str[5] = 0;
	}
	else
	{
		str[4] = 0;
	}
}

// #note must be printed before being applied to the board #todo why?
void print_move(chess_move move)
{
	assert(move_ok(move));
	if (is_castling(move))
	{
		if (move_to(move) > move_from(move))
		{
			printf("O-O");
		}
		else
		{
			printf("O-O-O");
		}
	}
	else
	{
		print_square(move_from(move));
		print_square(move_to(move));
		if (is_promotion(move))
		{
			chess_piece_type type = promotion_type(move);
			printf("->%c", get_piece_char(type | 0x8));
		}
	}
}

void print_move_uci(chess_move move)
{
	assert(move_ok(move));
	print_square(move_from(move));
	print_square(move_to(move));
	if (is_promotion(move))
	{
		chess_piece_type type = promotion_type(move);
		printf("%c", get_piece_char(type | 0x8));
	}
}

void print_move_buffer(chess_move *move_buffer, s16 *score_buffer, u32 num_moves)
{
	for (u32 i = 0; i < num_moves; ++i)
	{
		printf("%2u: ", i);
		print_move_uci(move_buffer[i]);
		printf(" (%04x)", move_buffer[i]);
		if (score_buffer)
		{
			printf(" %d\n", score_buffer[i]);
		}
		else
		{
			printf("\n");
		}
	}
}
