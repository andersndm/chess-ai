cc = clang
tgt = morr

uname_s := $(shell uname -s)
current_dir = $(shell pwd)

ifeq ($(uname_s), Darwin)
	dll = dylib
	os = macOS
else
	dll = so
	os = linux
endif

wrng_add = -Wswitch -Wswitch-enum
wrng = -Wall -Wextra -Wpedantic -pedantic-errors $(wrng_add)
#wrng = -Wall -Wextra $(wrng_add)
wrng_ignore = -Wno-unused-parameter -Wno-unused-function \
			  -Wno-missing-braces

inc = -I/usr/include/freetype2
cflags = -std=gnu11  $(wrng) $(wrng_ignore) $(inc) -march=x86-64-v3
dflags = -gdwarf-4 -DDEBUG=1 -O0
rflags = -gdwarf-4 -O3

srcs = $(filter-out magic_bitboard_test.c, $(wildcard *.c))
#srcs = $(wildcard *.c)
objs = $(addprefix build/, $(srcs:.c=.o))

debug_objs = $(addprefix build/debug/, $(srcs:.c=.o))

libraries = -lm -lpthread

dlflags = -no-pie $(libraries)
rlflags = $(libraries)

.PHONY: all prep clean rebuild run debug release ccls blkc

all: prep debug

run: debug
	@cd data && ./$(tgt)_linux_debug
	@cd ..

rebuild: clean all ccls

prep:
	@mkdir -p data
	@mkdir -p build
	@mkdir -p build/debug

clean:
	@rm -f -r build/debug
	@rm -f -r build
	@rm -f data/$(tgt)_$(os)

debug: data/$(tgt)_$(os)_debug

release: data/$(tgt)_$(os)

ccls:
	@echo $(cc) > .ccls
	@echo "%c" >> .ccls
	@$(foreach cmd, $(inc), echo $(cmd) >> .ccls; )
	@$(foreach cmd, $(dflags), echo $(cmd) >> .ccls; )
	@$(foreach cmd, $(cflags), echo $(cmd) >> .ccls; )

data/$(tgt)_$(os)_debug: $(debug_objs)
	@$(cc) $(dflags) $(cflags) -o $@ $^ $(dlflags)
	@printf "\x1b[34mlinking:   \x1b[0m" && echo $@

data/$(tgt)_$(os): $(objs)
	@$(cc) $(rflags) $(cflags) -o $@ $^ $(rlflags)
	@printf "\x1b[34mlinking:   \x1b[0m" && echo $@

build/debug/%.o: %.c
	@printf "\x1b[32mcompiling: \x1b[0m" && echo $<
	@$(cc) -c -fPIC $(dflags) $(cflags) -o $@ $<

build/%.o: %.c
	@printf "\x1b[32mcompiling: \x1b[0m" && echo $<
	@$(cc) -c -g -fPIC $(rflags) $(cflags) -o $@ $<
