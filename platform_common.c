#include "platform_common.h"

#include <ctype.h>

void set_print_color(console_color color, bool bold)
{
#ifdef _WIN32
	// #todo
#else
	if (bold > 0) { printf("\x1b[%d;1m", color); }
	else { printf("\x1b[%dm", color); }
	//printf("\x1b[0m");
#endif
}

#ifdef _WIN32
HANDLE console_handle;
WORD saved_console_attributes;
#endif

#ifndef _WIN32
__attribute__((__format__ (__printf__, 3, 0)))
#endif // !_WIN32
void print_color(console_color color, bool bold, const char* fmt, ...)
{
#ifdef _WIN32
	if(console_handle == INVALID_HANDLE_VALUE)
	{
		console_handle = GetStdHandle(STD_OUTPUT_HANDLE);
		CONSOLE_SCREEN_BUFFER_INFO console_info;
		GetConsoleScreenBufferInfo(console_handle, &console_info);
		saved_console_attributes = console_info.wAttributes;
	}

	WORD win32_color = 0;
	if (bold) { win32_color = FOREGROUND_INTENSITY; }
	switch (color)
	{
		case CC_BLACK:
		{
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_RED:
		{
			win32_color |= FOREGROUND_RED;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_GREEN:
		{
			win32_color |= FOREGROUND_GREEN;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_YELLOW:
		{
			win32_color |= FOREGROUND_RED | FOREGROUND_GREEN;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_BLUE:
		{
			win32_color |= FOREGROUND_BLUE;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_MAGENTA:
		{
			win32_color |= FOREGROUND_RED | FOREGROUND_BLUE;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_CYAN:
		{
			win32_color |= FOREGROUND_GREEN | FOREGROUND_BLUE;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		case CC_WHITE:
		{
			win32_color |= FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
			SetConsoleTextAttribute(console_handle, win32_color);
		} break;
		default:
		{
			SetConsoleTextAttribute(console_handle, saved_console_attributes);
		} break;

		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);

		SetConsoleTextAttribute(console_handle, saved_console_attributes);
	}
#else
	if (bold > 0) { printf("\x1b[%d;1m", color); }
	else { printf("\x1b[%dm", color); }
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	printf("\x1b[0m");
#endif // PL_WIN
}

u32 safecast_s32_to_u32(s32 val)
{
	if (val < 0)
	{
		assert(!"attempting to cast a negative s32 to u32");
	}
	return (u32)val;
}

s32 safecast_u32_to_s32(u32 val)
{
	if (val > 0x8FFFFFFF)
	{
		assert(!"attempting to cast a u32 that is larger than S32_MAX to s32");
	}
	return (s32)val;
}

u64 safecast_s64_to_u64(s64 val)
{
	if (val < 0)
	{
		assert(!"attempting to cast a negative s64 to u64");
	}
	return (u64)val;
}

s64 safecast_u64_to_s64(u64 val)
{
	if (val > 0x8FFFFFFFFFFFFFFF)
	{
		assert(!"attempting to cast a u32 that is larger than S32_MAX to s32");
	}
	return (s32)val;
}

file_contents read_file(const char *path)
{
	file_contents result = { 0 };
#ifdef _WIN32
	FILE* fp = fopen(path, "rb");
	if (fp == NULL)
	{
		log_error("failed to open file '%s' for read", path);
		return result;
	}
	fseek(fp, 0, SEEK_END);
	long len = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char* buf = xmalloc(len);
	size_t n = fread(buf, len, 1, fp);
	if (len && n != 1)
	{
		fclose(fp);
		free(buf);
		return result;
	}
	fclose(fp);
	result.contents = buf;
	result.size = len;
#else
	char *contents = 0;
	s32 file_ptr = open(path, O_RDONLY);
	if (file_ptr <= 0)
	{
		log_error("failed to open file '%s' for read", path);
		return result;
	}
	struct stat file_stat = { 0 };
	fstat(file_ptr, &file_stat);
	if (file_stat.st_size <= 0)
	{
		log_error("file '%s' is empty", path);
		return result;
	}
	else
	{
		u64 size = safecast_s64_to_u64(file_stat.st_size);
		contents = xmalloc(size);
		ssize_t bytes_read = read(file_ptr, contents, size);
		if (safecast_s64_to_u64(bytes_read) != size)
		{
			log_error("failed to read entire file, '%s'", path);
			xfree(contents);
			contents = 0;
		}
		result.size = size;
		result.contents = contents;
	}
	close(file_ptr);
#endif
	return result;
}

file_contents read_text_file(const char *path)
{
	file_contents result = { 0 };
#ifdef _WIN32
	FILE* fp = fopen(path, "rb");
	if (fp == NULL)
	{
		log_error("failed to open file '%s' for read", path);
		return result;
	}
	fseek(fp, 0, SEEK_END);
	long len = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char* buf = xmalloc((usize)len + 1);
	size_t n = fread(buf, len, 1, fp);
	if (len && n != 1)
	{
		fclose(fp);
		free(buf);
		return result;
	}
	fclose(fp);
	buf[len] = 0;
	result.contents = buf;
	result.size = len;
#else
	char *contents = 0;
	s32 file_ptr = open(path, O_RDONLY);
	if (file_ptr <= 0)
	{
		log_error("failed to open file '%s' for read", path);
		return result;
	}
	struct stat file_stat = { 0 };
	fstat(file_ptr, &file_stat);
	if (file_stat.st_size <= 0)
	{
		log_error("file '%s' is empty", path);
		return result;
	}
	else
	{
		u64 size = safecast_s64_to_u64(file_stat.st_size);
		contents = xmalloc(size + 1);
		ssize_t bytes_read = read(file_ptr, contents, size);
		if (safecast_s64_to_u64(bytes_read) != size)
		{
			log_error("failed to read entire file, '%s'", path);
			xfree(contents);
			contents = 0;
		}
		contents[size] = 0;
		result.size = size;
		result.contents = contents;
	}
	close(file_ptr);
#endif
	return result;
}

bool write_file_append(const char *path, const char *buf, usize len)
{
#ifdef _WIN32
	FILE* fp = fopen(path, "w");
	if (fp == NULL)
	{
		return false;
	}
	size_t n = fwrite(buf, len, 1, fp);
	fclose(fp);
	return n == 1;
#else
	// #note should set the permissions to -rw-r--r--
	s32 file_ptr = open(path, O_WRONLY | O_CREAT | O_APPEND,
						S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (file_ptr <= 0)
	{
		log_error("failed to open file '%s' for write", path);
		return false;
	}
	bool result = true;
	ssize_t bytes_written = write(file_ptr, buf, len);
	if (safecast_s64_to_u64(bytes_written) != len)
	{
		log_error("incorrect number of bytes written to file '%s'", path);
		result = false;
	}

	close(file_ptr);
	return result;
#endif
}

bool write_file(const char *path, const char *buf, usize len)
{
#ifdef _WIN32
	FILE* fp = fopen(path, "w");
	if (fp == NULL)
	{
		return false;
	}
	size_t n = fwrite(buf, len, 1, fp);
	fclose(fp);
	return n == 1;
#else
	// #note should set the permissions to -rw-r--r--
	s32 file_ptr = open(path, O_WRONLY | O_CREAT | O_TRUNC,
						S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (file_ptr <= 0)
	{
		log_error("failed to open file '%s' for write", path);
		return false;
	}
	bool result = true;
	ssize_t bytes_written = write(file_ptr, buf, len);
	if (safecast_s64_to_u64(bytes_written) != len)
	{
		log_error("incorrect number of bytes written to file '%s'", path);
		result = false;
	}

	close(file_ptr);
	return result;
#endif
}

void print_bits_32(u32 x)
{
	char output[33];
	for (s32 i = 31; i >= 0; --i)
	{
		if (x & 1)
		{
			output[i] = '1';
		}
		else
		{
			output[i] = '0';
		}
		x >>= 1;
	}
	output[32] = 0;
	printf("%s", output);
}

void print_num_bytes(u64 num)
{
	if (num < 2048)
	{
		printf("%" PRIu64 " B", num);
	}
	else if (num < (u64)2 * 1024 * 1024)
	{
		printf("%.2f kiB", (double)num / 1024.0);
	}
	else if (num < (u64)2 * 1024 * 1024 * 1024)
	{
		printf("%.2f MiB", (double)num / (1024.0 * 1024.0));
	}
	else
	{
		printf("%.2f GiB", (double)num / (1024.0 * 1024.0 * 1024.0));
	}
}

void print_bytes(const void *data, u32 num_bytes, u32 offset)
{
	u32 max_offset = offset + num_bytes;
	u32 num_digits = 0;
	while (max_offset != 0)
	{
		++num_digits;
		max_offset = max_offset / 0x10;
	}

	char address_fmt[32];
	snprintf(address_fmt, array_count(address_fmt), "%%0%ux", num_digits);

	const u8 *bytes = data;
	u32 bytes_per_line = 16;
	u32 num_lines = num_bytes / bytes_per_line;
	u32 remaining = num_bytes % bytes_per_line;

	PC_RED_BOLD(address_fmt, offset); printf(": ");
	for (u32 i = 0; i < num_lines; ++i)
	{
		for (u32 j = 0; j < bytes_per_line; ++j)
		{
			if (j % 4 == 0 && j != 0)
			{
				printf(" ");
			}
			u8 byte = bytes[i * bytes_per_line + j];
			if (byte == 0)
			{
				PC_WHITE("00 ");
			}
			else if (isprint(byte))
			{
				PC_BLUE("%02x ", byte);
			}
			else
			{
				PC_BLACK("%02x ", byte);
			}
		}

		printf("  ");

		for (u32 j = 0; j < bytes_per_line; ++j)
		{
			if (j % 4 == 0 && j != 0)
			{
				printf(" ");
			}
			u8 byte = bytes[i * bytes_per_line + j];
			if (byte == 0)
			{
				PC_WHITE(".");
			}
			else if (isprint(byte))
			{
				PC_BLUE("%c", byte);
			}
			else
			{
				PC_BLACK(".");
			}
		}
		printf("\n");
		if (i != num_lines - 1)
		{
			PC_RED_BOLD(address_fmt, offset + (i + 1) * bytes_per_line); printf(": ");
		}
	}

	if (remaining > 0)
	{
		if (num_lines > 0) { PC_RED_BOLD(address_fmt, offset + num_lines * bytes_per_line); printf(": "); }
		for (u32 i = bytes_per_line * num_lines; i < num_bytes; ++i)
		{
			if (i % 4 == 0 && i != bytes_per_line * num_lines)
			{
				printf(" ");
			}
			u8 byte = bytes[i];
			if (byte == 0)
			{
				PC_WHITE("00 ");
			}
			else if (isprint(byte))
			{
				PC_BLUE("%02x ", byte);
			}
			else
			{
				PC_BLACK("%02x ", byte);
			}
		}

		for (u32 i = 0; i < bytes_per_line - remaining; ++i)
		{
			if (i + remaining % 4 == 0) { printf(" "); }
			printf("   ");
		}

		printf("  ");

		for (u32 i = bytes_per_line * num_lines; i < num_bytes; ++i)
		{
			if (i % 4 == 0 && i != 0)
			{
				printf(" ");
			}
			u8 byte = bytes[i];
			if (byte == 0)
			{
				PC_WHITE(".");
			}
			else if (isprint(byte))
			{
				PC_BLUE("%c", byte);
			}
			else
			{
				PC_BLACK(".", byte);
			}
		}
	}
	printf("\n");
}

void print_compare_bytes(const void *data0, const void *data1, u32 num_bytes, u32 offset)
{
	u32 max_offset = offset + num_bytes;
	u32 num_digits = 0;
	while (max_offset != 0)
	{
		++num_digits;
		max_offset = max_offset / 0x10;
	}
	
	char address_fmt[32];
	snprintf(address_fmt, array_count(address_fmt), "%%0%ux", num_digits);

	const u8 *bytes = data0;
	const u8 *cmp_bytes = data1;
	u32 bytes_per_line = 16;
	u32 num_lines = num_bytes / bytes_per_line;
	u32 remaining = num_bytes % bytes_per_line;

	for (u32 i = 0; i < num_lines; ++i)
	{
		u64 first8 = *(u64 *)(bytes + i * bytes_per_line);
		u64 second8 = *(u64 *)(bytes + i * bytes_per_line + 8);
		u64 first_cmp = *(u64 *)(cmp_bytes + i * bytes_per_line);
		u64 second_cmp = *(u64 *)(cmp_bytes + i * bytes_per_line + 8);
		if (first8 != first_cmp || second8 != second_cmp)
		{
			PC_RED_BOLD(address_fmt, offset + i * bytes_per_line); printf(": ");
			for (u32 j = 0; j < bytes_per_line; ++j)
			{
				if (j % 4 == 0 && j != 0)
				{
					printf(" ");
				}
				u8 byte = bytes[i * bytes_per_line + j];
				u8 cmp_byte = cmp_bytes[i * bytes_per_line + j];
				if (byte != cmp_byte)
				{
					if (byte == 0)
					{
						PC_MAGENTA("00 ");
					}
					else
					{
						PC_MAGENTA("%02x ", byte);
					}
				}
				else
				{
					if (byte == 0)
					{
						PC_WHITE("00 ");
					}
					else if (isprint(byte))
					{
						PC_BLUE("%02x ", byte);
					}
					else
					{
						PC_BLACK("%02x ", byte);
					}
				}
			}
			printf("\n");
			PC_RED_BOLD(address_fmt, offset + i * bytes_per_line); printf(": ");
			for (u32 j = 0; j < bytes_per_line; ++j)
			{
				if (j % 4 == 0 && j != 0)
				{
					printf(" ");
				}
				u8 byte = bytes[i * bytes_per_line + j];
				u8 cmp_byte = cmp_bytes[i * bytes_per_line + j];
				if (byte != cmp_byte)
				{
					if (cmp_byte == 0)
					{
						PC_MAGENTA("00 ");
					}
					else
					{
						PC_MAGENTA("%02x ", cmp_byte);
					}
				}
				else
				{
					if (cmp_byte == 0)
					{
						PC_WHITE("00 ");
					}
					else if (isprint(cmp_byte))
					{
						PC_BLUE("%02x ", cmp_byte);
					}
					else
					{
						PC_BLACK("%02x ", cmp_byte);
					}
				}
			}
			printf("\n");
		}
	}

	if (remaining > 0)
	{
		bool mismatch = false;
		for (u32 i = bytes_per_line * num_lines; i < num_bytes; ++i)
		{
			if (bytes[i] != cmp_bytes[i])
			{
				mismatch = true;
				break;
			}
		}
		if (mismatch)
		{
			PC_RED_BOLD(address_fmt, offset + num_lines * bytes_per_line); printf(": ");
			for (u32 i = bytes_per_line * num_lines; i < num_bytes; ++i)
			{
				if (i % 4 == 0 && i != bytes_per_line * num_lines)
				{
					printf(" ");
				}
				u8 byte = bytes[i];
				u8 cmp_byte = cmp_bytes[i];
				if (byte != cmp_byte)
				{
					if (byte == 0)
					{
						PC_MAGENTA("00 ");
					}
					else
					{
						PC_MAGENTA("%02x ", byte);
					}
				}
				else
				{
					if (byte == 0)
					{
						PC_WHITE("00 ");
					}
					else if (isprint(byte))
					{
						PC_BLUE("%02x ", byte);
					}
					else
					{
						PC_BLACK("%02x ", byte);
					}
				}
			}

			for (u32 i = 0; i < bytes_per_line - remaining; ++i)
			{
				if (i + remaining % 4 == 0) { printf(" "); }
				printf("   ");
			}
			printf("\n");

			PC_RED_BOLD(address_fmt, offset + num_lines * bytes_per_line); printf(": ");
			for (u32 i = bytes_per_line * num_lines; i < num_bytes; ++i)
			{
				if (i % 4 == 0 && i != bytes_per_line * num_lines)
				{
					printf(" ");
				}
				u8 byte = bytes[i];
				u8 cmp_byte = cmp_bytes[i];
				if (byte != cmp_byte)
				{
					if (cmp_byte == 0)
					{
						PC_MAGENTA("00 ");
					}
					else
					{
						PC_MAGENTA("%02x ", cmp_byte);
					}
				}
				else
				{
					if (cmp_byte == 0)
					{
						PC_WHITE("00 ");
					}
					else if (isprint(cmp_byte))
					{
						PC_BLUE("%02x ", cmp_byte);
					}
					else
					{
						PC_BLACK("%02x ", cmp_byte);
					}
				}
			}

			for (u32 i = 0; i < bytes_per_line - remaining; ++i)
			{
				if (i + remaining % 4 == 0) { printf(" "); }
				printf("   ");
			}
		}
	}
	printf("\n");
}
