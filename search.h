#ifndef SEARCH_H
#define SEARCH_H

#include "chess_board.h"
#include "basic_eval.h"
#include "transposition_table.h"

#define MAX_SEARCH_DEPTH 64
#define INF_BOUND 32000
#define AB_BOUND 30000
#define MATE (AB_BOUND - MAX_SEARCH_DEPTH)

typedef struct search_parameters
{
	bool time_set;
	u64 start_time;
	u64 stop_time;

	bool infinite;
	u32 depth;

	u64 nodes;

	bool quit;
	bool stopped;

	chess_move pv_array[MAX_SEARCH_DEPTH];
} search_parameters;

typedef struct search_data
{
	u32 ply;
	chess_move history[NUM_PIECES][64];
	chess_move killers[2][MAX_SEARCH_DEPTH];
} search_data;

typedef struct search_thread_info
{
	search_parameters *sparam;
	transposition_table *ttable;
	chess_board *original_board;
	u32 num_threads;
} search_thread_info;

void *basic_search_threaded(void *data);
void *basic_search_queued(void *data);

typedef struct nnue_search_thread_info
{
	search_parameters *sparam;
	transposition_table *ttable;
	chess_board *original_board;
	const nnue_net *net;
	u32 num_threads;
	chess_move best_move;
	bool training;
} nnue_search_thread_info;

void *nnue_search_threaded(void *data);
void *nnue_search_queued(void *data);

#endif // !SEARCH_H
