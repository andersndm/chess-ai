// #todo remove and rewrite pgn
#include "pgn.h"
#include "chess_board.h"
#include "move_generation.h"

void fprint_square(s16 index, FILE *stream)
{
	fprintf(stream, "%c%c", (index % 8) + 'a', (index / 8) + '1');
}
// #note must print move before it is applied
void fprint_pgn(chess_move *moves, pgn_info info, FILE *stream)
{
	fprintf(stream, "[Event \"%s\"]\n", info.event);
	fprintf(stream, "[Site \"%s\"]\n", info.site);
	fprintf(stream, "[Date \"%04d.%02d.%02d\"]\n", info.year, info.month, info.day);// tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
	fprintf(stream, "[Round \"%" PRIu64 "\"]\n", info.round);
	fprintf(stream, "[White \"%s\"]\n", info.white);
	fprintf(stream, "[Black \"%s\"]\n", info.black);
	const char *result_str = NULL;
	switch (info.result)
	{
		case CR_WHITE_CHECKMATE:
		{
			result_str = "1-0";
		} break;
		case CR_BLACK_CHECKMATE:
		{
			result_str = "0-1";
		} break;
		case CR_DRAW_STALEMATE:
		case CR_DRAW_REPETITION:
		case CR_DRAW_INSUFFICIENT_MATERIAL:
		case CR_DRAW_FIFTY_MOVE:
		{
			result_str = "1/2-1/2";
		} break;
		case CR_ONGOING:
		default:
		{
			invalid_code_path;
		} break;
	}
	fprintf(stream, "[Result \"%s\"]\n", result_str);

	state_info state;
	chess_board board = start_board(&state);
	state_info *states = malloc(buf_len(moves) * sizeof(state_info));

	for (u32 i = 0; i < buf_len(moves); ++i)
	{
		chess_move move = moves[i];
		chess_color color_to_play = board.state->side;
		if (color_to_play == WHITE)
		{
			fprintf(stream, "%u. ", board.state->ply / 2 + 1);
		}
		u8 to = move_to(move);
		u8 from = move_from(move);
		bool castling = is_castling(move);
		bool promotion = is_promotion(move);
		chess_piece_type promo_type = promotion_type(move);
		chess_piece_type type = get_piece_type(board.squares[from]);

		u64 self_pieces = board.color_bbs[color_to_play];
		u64 oppo_pieces = board.color_bbs[opposite_color(color_to_play)];

		chess_move check_moves[MAX_LEGAL_MOVES];
		switch (type)
		{
			case PAWN:
			{
				// #note no pawn on the same file can capture the same square
				// #note no pawn on the same file can push to the same square
				if (board.squares[to] != EMPTY_SQ)
				{
					fprintf(stream, "%cx", 'a' + from % 8);
					fprint_square(to, stream);
				}
				else
				{
					fprint_square(to, stream);
				}
				if (promotion)
				{
					fprintf(stream, "=");
					fprintf(stream, "%c", get_piece_char(make_piece(promo_type, WHITE)));
				}
			} break;
			case KNIGHT:
			{
				bool another_knight = false;
				bool same_file = false;
				bool same_rank = false;
				u64 self_knights = self_pieces & board.type_bbs[KNIGHT];
				u32 num_moves = generate_knight_moves(self_knights, ~self_pieces, check_moves);
				for (u32 move_index = 0; move_index < num_moves; ++move_index)
				{
					u8 new_to = move_to(check_moves[move_index]);
					u8 new_from = move_from(check_moves[move_index]);
					if (new_from != from)
					{
						if (new_to == to)
						{
							another_knight = true;
							if ((new_from % 8) == (from % 8))
							{
								same_file = true;
							}
							else if ((new_from / 8) == (from / 8))
							{
								same_rank = true;
							}
						}
					}
				}
				fprintf(stream, "N");
				if (another_knight)
				{
					if (same_file && !same_rank)
					{
						fprintf(stream, "%c", '1' + from / 8);
					}
					else if (same_rank && !same_file)
					{
						fprintf(stream, "%c", 'a' + from % 8);
					}
					else
					{
						fprint_square(from, stream);
					}
				}
				if (board.squares[to] != EMPTY_SQ)
				{
					fprintf(stream, "x");
				}
				fprint_square(to, stream);
			} break;
			case BISHOP:
			{
				bool another_bishop = false;
				bool same_rank = false;
				bool same_file = false;
				u64 self_bishops = self_pieces & board.type_bbs[BISHOP];
				u32 num_moves = generate_bishop_moves(self_bishops, self_pieces,
													  self_pieces & oppo_pieces, check_moves);
				for (u32 move_index = 0; move_index < num_moves; ++move_index)
				{
					u8 new_to = move_to(check_moves[move_index]);
					u8 new_from = move_from(check_moves[move_index]);
					if (new_from != from)
					{
						if (new_to == to)
						{
							another_bishop = true;
							if ((from % 8) == (new_from % 8))
							{
								same_file = true;
							}
							else if ((from / 8) == (new_from / 8))
							{
								same_rank = true;
							}
						}
					}
				}
				fprintf(stream, "B");
				if (another_bishop)
				{
					if (same_file && !same_rank)
					{
						fprintf(stream, "%c", '1' + from / 8);
					}
					else if (same_rank && !same_file)
					{
						fprintf(stream, "%c", 'a' + from % 8);
					}
					else
					{
						fprint_square(from, stream);
					}
				}
				if (board.squares[to] != EMPTY_SQ)
				{
					fprintf(stream, "x");
				}
				fprint_square(to, stream);
			} break;
			case ROOK:
			{
				bool another_rook = false;
				bool same_rank = false;
				bool same_file = false;
				u64 self_rooks = self_pieces & board.type_bbs[ROOK];
				u32 num_moves = generate_rook_moves(self_rooks, self_pieces,
													self_pieces & oppo_pieces, check_moves);
				for (u32 move_index = 0; move_index < num_moves; ++move_index)
				{
					u8 new_to = move_to(check_moves[move_index]);
					u8 new_from = move_from(check_moves[move_index]);
					if (new_from != from)
					{
						if (new_to == to)
						{
							another_rook = true;
							if ((from % 8) == (new_from % 8))
							{
								same_file = true;
							}
							else if ((from / 8) == (new_from / 8))
							{
								same_rank = true;
							}
						}
					}
				}
				fprintf(stream, "R");
				if (another_rook)
				{
					if (same_file && !same_rank)
					{
						fprintf(stream, "%c", '1' + from / 8);
					}
					else if (same_rank && !same_file)
					{
						fprintf(stream, "%c", 'a' + from % 8);
					}
					else
					{
						fprint_square(from, stream);
					}
				}
				if (board.squares[to] != EMPTY_SQ)
				{
					fprintf(stream, "x");
				}
				fprint_square(to, stream);
			} break;
			case QUEEN:
			{
				bool another_queen = false;
				bool same_rank = false;
				bool same_file = false;
				u64 self_queens = self_pieces & board.type_bbs[QUEEN];
				u32 num_moves = generate_queen_moves(self_queens, self_pieces,
													self_pieces & oppo_pieces, check_moves);
				for (u32 move_index = 0; move_index < num_moves; ++move_index)
				{
					u8 new_to = move_to(check_moves[move_index]);
					u8 new_from = move_from(check_moves[move_index]);
					if (new_from != from)
					{
						if (new_to == to)
						{
							another_queen = true;
							if ((from % 8) == (new_from % 8))
							{
								same_file = true;
							}
							else if ((from / 8) == (new_from / 8))
							{
								same_rank = true;
							}
						}
					}
				}
				fprintf(stream, "Q");
				if (another_queen)
				{
					if (same_file && !same_rank)
					{
						fprintf(stream, "%c", '1' + from / 8);
					}
					else if (same_rank && !same_file)
					{
						fprintf(stream, "%c", 'a' + from % 8);
					}
					else
					{
						fprint_square(from, stream);
					}
				}
				if (board.squares[to] != EMPTY_SQ)
				{
					fprintf(stream, "x");
				}
				fprint_square(to, stream);
			} break;
			case KING:
			{
				// #note only ever 1 king per color
				if (castling)
				{
					if (to > from)
					{
						fprintf(stream, "O-O");
					}
					else
					{
						fprintf(stream, "O-O-O");
					}
				}
				else
				{
					fprintf(stream, "K");
					if (board.squares[to] != EMPTY_SQ)
					{
						fprintf(stream, "x");
					}
					fprint_square(to, stream);
				}
			} break;
			case EMPTY_SQ:
			case NUM_PIECE_TYPES:
			default:
			{
				invalid_code_path;
			} break;
		}

		apply_move(&board, states + i, move);
		check_status status = get_check_status(&board);
		if (status.single_check || status.double_check)
		{
			if (i == buf_len(moves) - 1)
			{
				fprintf(stream, "#");
			}
			else
			{
				fprintf(stream, "+");
			}
		}
		fprintf(stream, " ");
	}
	switch (info.result)
	{
		case CR_WHITE_CHECKMATE:
		case CR_BLACK_CHECKMATE:
		{
			// #note skip
		} break;
		case CR_DRAW_STALEMATE:
		{
			fprintf(stream, "{ stalemate }");
		} break;
		case CR_DRAW_REPETITION:
		{
			fprintf(stream, "{ threefold repetition }");
		} break;
		case CR_DRAW_INSUFFICIENT_MATERIAL:
		{
			fprintf(stream, "{ insufficient material }");
		} break;
		case CR_DRAW_FIFTY_MOVE:
		{
			fprintf(stream, "{ 50 move rule }");
		} break;
		case CR_ONGOING:
		default:
		{
			invalid_code_path;
		} break;
	}
	fprintf(stream, "\n");
}
