#include "common.h"
#include "chess.h"
#include "chess_board.h"

#include "platform_common.h"

// #note [pos] = popcount(generate_bishop_occupancy_mask(pos))
internal const u8 bishop_relevant_occupancy_bits_count[64] =
{
	6, 5, 5, 5, 5, 5, 5, 6,
	5, 5, 5, 5, 5, 5, 5, 5,
	5, 5, 7, 7, 7, 7, 5, 5,
	5, 5, 7, 9, 9, 7, 5, 5,
	5, 5, 7, 9, 9, 7, 5, 5,
	5, 5, 7, 7, 7, 7, 5, 5,
	5, 5, 5, 5, 5, 5, 5, 5,
	6, 5, 5, 5, 5, 5, 5, 6
};

// #note [pos] = popcount(generate_rook_occupancy_mask(pos))
internal const u8 rook_relevant_occupancy_bits_count[64] =
{
	12, 11, 11, 11, 11, 11, 11, 12,
	11, 10, 10, 10, 10, 10, 10, 11,
	11, 10, 10, 10, 10, 10, 10, 11,
	11, 10, 10, 10, 10, 10, 10, 11,
	11, 10, 10, 10, 10, 10, 10, 11,
	11, 10, 10, 10, 10, 10, 10, 11,
	11, 10, 10, 10, 10, 10, 10, 11,
	12, 11, 11, 11, 11, 11, 11, 12
};

internal const u64 bishop_magic_numbers[64] =
{
	[ 0] = 0x0040040822862081,
    [ 1] = 0x00040810a4108000,
    [ 2] = 0x2008008400920040,
    [ 3] = 0x0061050104000008,
    [ 4] = 0x8282021010016100,
    [ 5] = 0x41008210400a0001,
    [ 6] = 0x03004202104050c0,
    [ 7] = 0x0022010108410402,
    [ 8] = 0x0060400862888605,
    [ 9] = 0x0006311401040228,
    [10] = 0x0000080801082000,
    [11] = 0x802a082080240100,
    [12] = 0x1860061210016800,
    [13] = 0x000401016010a810,
    [14] = 0x1000060545201005,
    [15] = 0x21000c2098280819,
    [16] = 0x2020004242020200,
    [17] = 0x4102100490040101,
    [18] = 0x0114012208001500,
    [19] = 0x0108000682004460,
    [20] = 0x7809000490401000,
    [21] = 0x420b001601052912,
    [22] = 0x00408c8206100300,
    [23] = 0x2231001041180110,
    [24] = 0x8010102008a02100,
    [25] = 0x0204201004080084,
    [26] = 0x0410500058008811,
    [27] = 0x480a040008010820,
    [28] = 0x2194082044002002,
    [29] = 0x2008a20001004200,
    [30] = 0x0040908041041004,
    [31] = 0x0881002200540404,
    [32] = 0x4001082002082101,
    [33] = 0x0008110408880880,
    [34] = 0x8000404040080200,
    [35] = 0x0200020082180080,
    [36] = 0x1184440400114100,
    [37] = 0xc220008020110412,
    [38] = 0x4088084040090100,
    [39] = 0x8822104100121080,
    [40] = 0x100111884008200a,
    [41] = 0x2844040288820200,
    [42] = 0x0090901088003010,
    [43] = 0x001000a218000400,
    [44] = 0x0001102010420204,
    [45] = 0x08414a3483000200,
    [46] = 0x6410849901420400,
    [47] = 0x0201080200901040,
    [48] = 0x0204880808050002,
    [49] = 0x1001008201210000,
    [50] = 0x016a6300a890040a,
    [51] = 0x8049000441108600,
    [52] = 0x2212002060410044,
    [53] = 0x0100086308020020,
    [54] = 0x0484241408020421,
    [55] = 0x105084028429c085,
    [56] = 0x004282480801080c,
    [57] = 0x081c098488088240,
    [58] = 0x1400000090480820,
    [59] = 0x4444000030208810,
    [60] = 0x1020142010820200,
    [61] = 0x2234802004018200,
    [62] = 0x00c2040450820a00,
    [63] = 0x0002101021090020
};

internal const u64 rook_magic_numbers[64] =
{
	[ 0] = 0xa080041440042080,
    [ 1] = 0xa840200410004001,
    [ 2] = 0x0c800c1000200081,
    [ 3] = 0x0100081001000420,
    [ 4] = 0x0200020010080420,
    [ 5] = 0x03001c0002010008,
    [ 6] = 0x8480008002000100,
    [ 7] = 0x2080088004402900,
    [ 8] = 0x0000800098204000,
    [ 9] = 0x2024401000200040,
    [10] = 0x0100802000801000,
    [11] = 0x0120800800801000,
    [12] = 0x0208808088000400,
    [13] = 0x0002802200800400,
    [14] = 0x2200800100020080,
    [15] = 0x0801000060821100,
    [16] = 0x0080044006422000,
    [17] = 0x0100808020004000,
    [18] = 0x12108a0010204200,
    [19] = 0x0140848010000802,
    [20] = 0x0481828014002800,
    [21] = 0x8094004002004100,
    [22] = 0x4010040010010802,
    [23] = 0x0000020008806104,
    [24] = 0x0100400080208000,
    [25] = 0x2040002120081000,
    [26] = 0x0021200680100081,
    [27] = 0x0020100080080080,
    [28] = 0x0002000a00200410,
    [29] = 0x0000020080800400,
    [30] = 0x0080088400100102,
    [31] = 0x0080004600042881,
    [32] = 0x4040008040800020,
    [33] = 0x0440003000200801,
    [34] = 0x0004200011004500,
    [35] = 0x0188020010100100,
    [36] = 0x0014800401802800,
    [37] = 0x2080040080800200,
    [38] = 0x0124080204001001,
    [39] = 0x0200046502000484,
    [40] = 0x0480400080088020,
    [41] = 0x1000422010034000,
    [42] = 0x0030200100110040,
    [43] = 0x0000100021010009,
    [44] = 0x2002080100110004,
    [45] = 0x0202008004008002,
    [46] = 0x0020020004010100,
    [47] = 0x2048440040820001,
    [48] = 0x0101002200408200,
    [49] = 0x0040802000401080,
    [50] = 0x4008142004410100,
    [51] = 0x02060820c0120200,
    [52] = 0x0001001004080100,
    [53] = 0x020c020080040080,
    [54] = 0x2935610830022400,
    [55] = 0x0044440041009200,
    [56] = 0x0280001040802101,
    [57] = 0x2100190040002085,
    [58] = 0x80c0084100102001,
    [59] = 0x4024081001000421,
    [60] = 0x00020030a0244872,
    [61] = 0x0012001008414402,
    [62] = 0x02006104900a0804,
    [63] = 0x0001004081002402

};

internal const u64 bishop_relevant_occupancy_masks[64] =
{
	[ 0] = 0x0040201008040200,
    [ 1] = 0x0000402010080400,
    [ 2] = 0x0000004020100a00,
    [ 3] = 0x0000000040221400,
    [ 4] = 0x0000000002442800,
    [ 5] = 0x0000000204085000,
    [ 6] = 0x0000020408102000,
    [ 7] = 0x0002040810204000,
    [ 8] = 0x0020100804020000,
    [ 9] = 0x0040201008040000,
    [10] = 0x00004020100a0000,
    [11] = 0x0000004022140000,
    [12] = 0x0000000244280000,
    [13] = 0x0000020408500000,
    [14] = 0x0002040810200000,
    [15] = 0x0004081020400000,
    [16] = 0x0010080402000200,
    [17] = 0x0020100804000400,
    [18] = 0x004020100a000a00,
    [19] = 0x0000402214001400,
    [20] = 0x0000024428002800,
    [21] = 0x0002040850005000,
    [22] = 0x0004081020002000,
    [23] = 0x0008102040004000,
    [24] = 0x0008040200020400,
    [25] = 0x0010080400040800,
    [26] = 0x0020100a000a1000,
    [27] = 0x0040221400142200,
    [28] = 0x0002442800284400,
    [29] = 0x0004085000500800,
    [30] = 0x0008102000201000,
    [31] = 0x0010204000402000,
    [32] = 0x0004020002040800,
    [33] = 0x0008040004081000,
    [34] = 0x00100a000a102000,
    [35] = 0x0022140014224000,
    [36] = 0x0044280028440200,
    [37] = 0x0008500050080400,
    [38] = 0x0010200020100800,
    [39] = 0x0020400040201000,
    [40] = 0x0002000204081000,
    [41] = 0x0004000408102000,
    [42] = 0x000a000a10204000,
    [43] = 0x0014001422400000,
    [44] = 0x0028002844020000,
    [45] = 0x0050005008040200,
    [46] = 0x0020002010080400,
    [47] = 0x0040004020100800,
    [48] = 0x0000020408102000,
    [49] = 0x0000040810204000,
    [50] = 0x00000a1020400000,
    [51] = 0x0000142240000000,
    [52] = 0x0000284402000000,
    [53] = 0x0000500804020000,
    [54] = 0x0000201008040200,
    [55] = 0x0000402010080400,
    [56] = 0x0002040810204000,
    [57] = 0x0004081020400000,
    [58] = 0x000a102040000000,
    [59] = 0x0014224000000000,
    [60] = 0x0028440200000000,
    [61] = 0x0050080402000000,
    [62] = 0x0020100804020000,
    [63] = 0x0040201008040200
};

internal const u64 rook_relevant_occupancy_masks[64] =
{
    [ 0] = 0x000101010101017e,
    [ 1] = 0x000202020202027c,
    [ 2] = 0x000404040404047a,
    [ 3] = 0x0008080808080876,
    [ 4] = 0x001010101010106e,
    [ 5] = 0x002020202020205e,
    [ 6] = 0x004040404040403e,
    [ 7] = 0x008080808080807e,
    [ 8] = 0x0001010101017e00,
    [ 9] = 0x0002020202027c00,
    [10] = 0x0004040404047a00,
    [11] = 0x0008080808087600,
    [12] = 0x0010101010106e00,
    [13] = 0x0020202020205e00,
    [14] = 0x0040404040403e00,
    [15] = 0x0080808080807e00,
    [16] = 0x00010101017e0100,
    [17] = 0x00020202027c0200,
    [18] = 0x00040404047a0400,
    [19] = 0x0008080808760800,
    [20] = 0x00101010106e1000,
    [21] = 0x00202020205e2000,
    [22] = 0x00404040403e4000,
    [23] = 0x00808080807e8000,
    [24] = 0x000101017e010100,
    [25] = 0x000202027c020200,
    [26] = 0x000404047a040400,
    [27] = 0x0008080876080800,
    [28] = 0x001010106e101000,
    [29] = 0x002020205e202000,
    [30] = 0x004040403e404000,
    [31] = 0x008080807e808000,
    [32] = 0x0001017e01010100,
    [33] = 0x0002027c02020200,
    [34] = 0x0004047a04040400,
    [35] = 0x0008087608080800,
    [36] = 0x0010106e10101000,
    [37] = 0x0020205e20202000,
    [38] = 0x0040403e40404000,
    [39] = 0x0080807e80808000,
    [40] = 0x00017e0101010100,
    [41] = 0x00027c0202020200,
    [42] = 0x00047a0404040400,
    [43] = 0x0008760808080800,
    [44] = 0x00106e1010101000,
    [45] = 0x00205e2020202000,
    [46] = 0x00403e4040404000,
    [47] = 0x00807e8080808000,
    [48] = 0x007e010101010100,
    [49] = 0x007c020202020200,
    [50] = 0x007a040404040400,
    [51] = 0x0076080808080800,
    [52] = 0x006e101010101000,
    [53] = 0x005e202020202000,
    [54] = 0x003e404040404000,
    [55] = 0x007e808080808000,
    [56] = 0x7e01010101010100,
    [57] = 0x7c02020202020200,
    [58] = 0x7a04040404040400,
    [59] = 0x7608080808080800,
    [60] = 0x6e10101010101000,
    [61] = 0x5e20202020202000,
    [62] = 0x3e40404040404000,
    [63] = 0x7e80808080808000,
};

// #note [pos][occupancies]
internal u64 test_bishop_attacks[64][512] = { 0 };

// #note [pos][occupancies]
internal u64 test_rook_attacks[64][4096] = { 0 };


void print_occupancy_bitboard(u64 bitboard)
{
	for (u32 rank_iter = 0; rank_iter < NUM_RANKS; ++rank_iter)
	{
		u32 rank = NUM_RANKS - rank_iter - 1;
		for (u32 file = 0; file < NUM_FILES; ++file)
		{
			char c = ' ';
			if (bitboard & square_bit(file + rank * NUM_FILES))
			{
				c = 'X';
			}
			PC_RED_BOLD("%c ", c);
		}
		printf("%u\n", rank + 1);
	}
	printf("a b c d e f g h\n");
}

void print_bitboard_with_pos_outer_red(u64 bitboard, u8 pos)
{
	u64 edges = 0xFF818181818181FF;
	if (edges & bitboard)
	{
		print_bitboard(edges & bitboard);
		debug_break;
	}

	for (u32 rank_iter = 0; rank_iter < NUM_RANKS; ++rank_iter)
	{
		u32 rank = NUM_RANKS - rank_iter - 1;
		for (u32 file = 0; file < NUM_FILES; ++file)
		{
			if (rank * NUM_FILES + file == pos)
			{
				assert((bitboard & square_bit(file + rank * NUM_FILES)) == 0);
				if ((rank == RANK_1 || rank == RANK_8) && file != FILE_H)
				{
					PC_GREEN_BOLD("@");
					PC_RED_BOLD("█");
				}
				else
				{
					PC_GREEN_BOLD("@ ");
				}
			}
			else if ((rank == RANK_1 || rank == RANK_8) && file != FILE_H)
			{
				assert((bitboard & square_bit(file + rank * NUM_FILES)) == 0);
				PC_RED_BOLD("██");
			}
			else if (file == FILE_A || file == FILE_H)
			{
				assert((bitboard & square_bit(file + rank * NUM_FILES)) == 0);
				PC_RED_BOLD("█ ");
			}
			else
			{
				char c = ' ';
				if (bitboard & square_bit(file + rank * NUM_FILES))
				{
					c = 'X';
				}
				printf("%c ", c);
			}
		}
		printf("%u\n", rank + 1);
	}
	printf("a b c d e f g h\n");
}

u64 generate_bishop_occupancy_mask(u8 pos)
{
	assert(pos < 64);
	u64 squares = 0;

	chess_rank start_rank = pos / 8;
	chess_file start_file = pos % 8;

	// NE
	for (s8 rank = start_rank + 1, file = start_file + 1;
		 rank < RANK_8 && file < FILE_H;
		 ++rank, ++file)
	{
		squares |= square_bit(rank * NUM_FILES + file);
	}

	// NW
	for (s8 rank = start_rank + 1, file = start_file - 1;
		 rank < RANK_8 && file > FILE_A;
		 ++rank, --file)
	{
		squares |= square_bit(rank * NUM_FILES + file);
	}

	// SE
	for (s8 rank = start_rank - 1, file = start_file + 1;
		 rank > RANK_1 && file < FILE_H;
		 --rank, ++file)
	{
		squares |= square_bit(rank * NUM_FILES + file);
	}

	// SW
	for (s8 rank = start_rank - 1, file = start_file - 1;
		 rank > RANK_1 && file > FILE_A;
		 --rank, --file)
	{
		squares |= square_bit(rank * NUM_FILES + file);
	}

	return squares;
}

u64 generate_valid_bishop_squares(u8 pos, u64 occupied)
{
	assert(pos < 64);
	u64 squares = 0;

	chess_rank start_rank = pos / 8;
	chess_file start_file = pos % 8;

	// NE
	for (s8 rank = start_rank + 1, file = start_file + 1;
		 rank <= RANK_8 && file <= FILE_H;
		 ++rank, ++file)
	{
		u64 bit = square_bit(rank * NUM_FILES + file);
		squares |= bit;
		if (bit & occupied) { break; }
	}

	// NW
	for (s8 rank = start_rank + 1, file = start_file - 1;
		 rank <= RANK_8 && file >= FILE_A;
		 ++rank, --file)
	{
		u64 bit = square_bit(rank * NUM_FILES + file);
		squares |= bit;
		if (bit & occupied) { break; }
	}

	// SE
	for (s8 rank = start_rank - 1, file = start_file + 1;
		 rank >= RANK_1 && file <= FILE_H;
		 --rank, ++file)
	{
		u64 bit = square_bit(rank * NUM_FILES + file);
		squares |= bit;
		if (bit & occupied) { break; }
	}

	// SW
	for (s8 rank = start_rank - 1, file = start_file - 1;
		 rank >= RANK_1 && file >= FILE_A;
		 --rank, --file)
	{
		u64 bit = square_bit(rank * NUM_FILES + file);
		squares |= bit;
		if (bit & occupied) { break; }
	}

	return squares;
}

u64 generate_rook_occupancy_mask(u8 pos)
{
	assert(pos < 64);
	u64 squares = 0;

	chess_rank start_rank = pos / 8;
	chess_file start_file = pos % 8;

	// N
	for (s8 rank = start_rank + 1; rank < RANK_8; ++rank)
	{
		squares |= square_bit(rank * NUM_FILES + start_file);
	}
	// S
	if (start_rank > RANK_1)
	{
		for (s8 rank = start_rank - 1; rank > RANK_1; --rank)
		{
			squares |= square_bit(rank * NUM_FILES + start_file);
		}
	}

	// E
	for (s8 file = start_file + 1; file < FILE_H; ++file)
	{
		squares |= square_bit(start_rank * NUM_FILES + file);
	}
	// W
	if (start_file > FILE_A)
	{
		for (s8 file = start_file - 1; file > FILE_A; --file)
		{
			squares |= square_bit(start_rank * NUM_FILES + file);
		}
	}

	return squares;
}

u64 generate_valid_rook_squares(u8 pos, u64 occupied)
{
	assert(pos < 64);
	u64 squares = 0;

	chess_rank start_rank = pos / 8;
	chess_file start_file = pos % 8;

	// N
	for (s8 rank = start_rank + 1; rank <= RANK_8; ++rank)
	{
		u64 bit = square_bit(rank * NUM_FILES + start_file);
		squares |= bit;
		if (bit & occupied) { break; }
	}
	// S
	for (s8 rank = start_rank - 1; rank >= RANK_1; --rank)
	{
		u64 bit = square_bit(rank * NUM_FILES + start_file);
		squares |= bit;
		if (bit & occupied) { break; }
	}

	// E
	for (s8 file = start_file + 1; file <= FILE_H; ++file)
	{
		u64 bit = square_bit(start_rank * NUM_FILES + file);
		squares |= bit;
		if (bit & occupied) { break; }
	}
	// W
	for (s8 file = start_file - 1; file >= FILE_A; --file)
	{
		u64 bit = square_bit(start_rank * NUM_FILES + file);
		squares |= bit;
		if (bit & occupied) { break; }
	}

	return squares;
}

u64 set_occupancy(u32 index, u64 mask, u32 count)
{
	u64 occupancy = 0;

	for (u32 i = 0; i < count; ++i)
	{
		//if (mask == 0) { break; }
		u8 square = lsb_pop(&mask);

		if (index & square_bit(i))
		{
			occupancy |= square_bit(square);
		}
	}

	return occupancy;
}


internal u32 rnd_state = 1804289383;

internal u32 get_random_u32(void)
{
	u32 x = rnd_state;

	// #note xor-shift algorithm
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;

	rnd_state = x;

	return x;
}

internal u64 get_random_u64(void)
{
	u64 x0 = get_random_u32() & 0xFFFF;
	u64 x1 = get_random_u32() & 0xFFFF;
	u64 x2 = get_random_u32() & 0xFFFF;
	u64 x3 = get_random_u32() & 0xFFFF;

	u64 result = x0 | (x1 << 16) | (x2 << 32) | (x3 << 48);
	return result;
}

internal u64 generate_magic_number_candidate(void)
{
	u64 x0 = get_random_u64();
	u64 x1 = get_random_u64();
	u64 x2 = get_random_u64();
	return x0 & x1 & x2;
}

internal u64 find_magic_number(u8 pos, bool bishop)
{
	assert(pos < 64);

	u64 occupancies[4096];
	u64 attacks[4096];
	u64 used_attacks[4096];

	u8 relevant_bit_count = bishop ? bishop_relevant_occupancy_bits_count[pos] :
									 rook_relevant_occupancy_bits_count[pos];

	u64 attack_mask = bishop ? generate_bishop_occupancy_mask(pos) :
							   generate_rook_occupancy_mask(pos);

	u32 occupancy_indices = 1 << relevant_bit_count;

	for (u32 index = 0; index < occupancy_indices; ++index)
	{
		occupancies[index] = set_occupancy(index, attack_mask, relevant_bit_count);
		attacks[index] = bishop ? generate_valid_bishop_squares(pos, occupancies[index]) :
								  generate_valid_rook_squares(pos, occupancies[index]);
	}

	for (u32 rnd_index = 0; rnd_index < 100000000; ++rnd_index)
	{
		u64 magic_number = generate_magic_number_candidate();
		if (popcount((attack_mask * magic_number) & 0xFF00000000000000) < 6)
		{
			continue;
		}

		memset(used_attacks, 0, sizeof(used_attacks));

		bool failed = false;
		for (u32 index = 0; !failed && index < occupancy_indices; ++index)
		{
			u64 magic_index = (occupancies[index] * magic_number) >> (64 - relevant_bit_count);

			if (used_attacks[magic_index] == 0)
			{
				used_attacks[magic_index] = attacks[index];
			}
			else if (used_attacks[magic_index] !=  attacks[index])
			{
				failed = true;
			}
		}

		if (!failed)
		{
			return magic_number;
		}
	}

	printf("no magic number found!\n");
	return 0;
}

void generate_magic_numbers(void)
{
	printf("bishop magic numbers:\n");
	for (u8 pos = 0; pos < 64; ++pos)
	{
		printf("    [%2u] = 0x%016lx,\n", pos, find_magic_number(pos, true));
	}
	printf("\n");

	printf("rook magic numbers:\n");
	for (u8 pos = 0; pos < 64; ++pos)
	{
		printf("    [%2u] = 0x%016lx,\n", pos, find_magic_number(pos, false));
	}
}

void generate_bishop_attacks(void)
{
	for (u8 pos = 0; pos < 64; ++pos)
	{
		u64 occupancy_mask = bishop_relevant_occupancy_masks[pos];
		u8 relevant_bits_count = bishop_relevant_occupancy_bits_count[pos];
		u32 occupancy_indices = 1 << relevant_bits_count;

		for (u32 index = 0; index < occupancy_indices; ++index)
		{
			u64 occupancy = set_occupancy(index, occupancy_mask, relevant_bits_count);
			u32 magic_index = (occupancy * bishop_magic_numbers[pos]) >> (64 - relevant_bits_count);
			assert(magic_index < 512);
			u64 valid_mask = generate_valid_bishop_squares(pos, occupancy);

			if (test_bishop_attacks[pos][magic_index] != 0)
			{
				if (test_bishop_attacks[pos][magic_index] != valid_mask)
				{
					printf("[%2u][%3u] = 0x%016lx,\n", pos, magic_index, valid_mask);
					print_bitboard_with_pos_outer_red(occupancy_mask, pos);
					print_occupancy_bitboard(occupancy);
					print_bitboard_with_pos(valid_mask, pos);
					print_bitboard_with_pos(test_bishop_attacks[pos][magic_index], pos);
					assert(0);
				}
			}
			else
			{
				test_bishop_attacks[pos][magic_index] = valid_mask;
			}
			printf("\t[%2u][%3u] = 0x%016lx,\n", pos, magic_index, valid_mask);
		}
	}
}

void generate_rook_attacks(void)
{
	for (u8 pos = 0; pos < 64; ++pos)
	{
		u64 occupancy_mask = rook_relevant_occupancy_masks[pos];
		u8 relevant_bits_count = rook_relevant_occupancy_bits_count[pos];
		u32 occupancy_indices = 1 << relevant_bits_count;

		for (u32 index = 0; index < occupancy_indices; ++index)
		{
			u64 occupancy = set_occupancy(index, occupancy_mask, relevant_bits_count);
			u32 magic_index = (occupancy * rook_magic_numbers[pos]) >> (64 - relevant_bits_count);
			assert(magic_index < 4096);
			u64 valid_mask = generate_valid_rook_squares(pos, occupancy);

			if (test_rook_attacks[pos][magic_index] != 0)
			{
				if (test_rook_attacks[pos][magic_index] != valid_mask)
				{
					printf("[%2u][%3u] = 0x%016lx,\n", pos, magic_index, valid_mask);
					print_bitboard_with_pos(occupancy_mask, pos);
					print_occupancy_bitboard(occupancy);
					print_bitboard_with_pos(valid_mask, pos);
					print_bitboard_with_pos(test_rook_attacks[pos][magic_index], pos);
					assert(0);
				}
			}
			else
			{
				test_rook_attacks[pos][magic_index] = valid_mask;
			}
			printf("\t[%2u][%4u] = 0x%016lx,\n", pos, magic_index, valid_mask);
		}
	}
}

#if 0
#include "sliding_piece_lookup.inl"

u64 bishop_valid_move_squares(u8 pos, u64 occupancy)
{
	u64 magic_index = occupancy & bishop_relevant_occupancy_masks[pos];
	magic_index *= bishop_magic_numbers[pos];
	magic_index >>= 64 - bishop_relevant_occupancy_bits_count[pos];
	u64 result = bishop_valid_moves[pos][magic_index];
	return result;
}

u64 rook_valid_move_squares(u8 pos, u64 occupancy)
{
	u64 magic_index = occupancy & rook_relevant_occupancy_masks[pos];
	magic_index *= rook_magic_numbers[pos];
	magic_index >>= 64 - rook_relevant_occupancy_bits_count[pos];
	u64 result = rook_valid_moves[pos][magic_index];
	return result;
}

u64 queen_valid_move_squares(u8 pos, u64 occupancy)
{
	u64 bishop_moves = bishop_valid_move_squares(pos, occupancy);
	u64 rook_moves = rook_valid_move_squares(pos, occupancy);
	return bishop_moves | rook_moves;
}
#endif

u64 in_between_bitboard(u8 a, u8 b) {
	const u64 full = 0xFFFFFFFFFFFFFFFF;
	const u64 a2a7 = 0x0001010101010100;
	const u64 b2g7 = 0x0040201008040200;
	const u64 h1b7 = 0x0002040810204080;

	#if 0
	PC_CYAN("full\n");
	print_bitboard_small(full);
	PC_CYAN("a2a7\n");
	print_bitboard_small(a2a7);
	PC_CYAN("b2g7\n");
	print_bitboard_small(b2g7);
	PC_CYAN("h1b7\n");
	print_bitboard_small(h1b7);
	#endif

	u64 between = (full << a) ^ (full << b);
	//PC_CYAN("between\n");
	//print_bitboard_with_2_pos(between, a, b);
	u64 file = (b & 7) - (a & 7);
	u64 rank = ((b | 7) - a) >> 3 ;
	u64 line = ((file & 7) - 1) & a2a7; /* a2a7 if same file */
	line += 2 * (((rank & 7) - 1) >> 58); /* b1g1 if same rank */
	line += (((rank - file) & 15) - 1) & b2g7; /* b2g7 if same diagonal */
	line += (((rank + file) & 15) - 1) & h1b7; /* h1b7 if same antidiag */
	line *= between & -between; /* mul acts like shift by smaller square */
	return line & between;   /* return the bits on that line in-between */
}

#include <ctype.h>

void magic_bitboard_test(void)
{
	#if 0
	// #note generate occupancy count arrays
	printf("bishop occupancy count:\n");
	for (chess_rank rank = RANK_1; rank < NUM_RANKS; ++rank)
	{
		for (chess_file file = FILE_A; file < NUM_FILES; ++file)
		{
			u8 pos = rank * NUM_FILES + file;
			printf("%u, ", popcount(generate_bishop_occupancy_mask(pos)));
		}
		printf("\n");
	}
	printf("rook occupancy count:\n");
	for (chess_rank rank = RANK_1; rank < NUM_RANKS; ++rank)
	{
		for (chess_file file = FILE_A; file < NUM_FILES; ++file)
		{
			u8 pos = rank * NUM_FILES + file;
			printf("%u, ", popcount(generate_rook_occupancy_mask(pos)));
		}
		printf("\n");
	}
	#endif

	#if 0
	// #note generate occupancy masks
	printf("bishop occupancy masks:\n");
	for (u8 pos = 0; pos < 64; ++pos)
	{
		printf("    [%2u] = 0x%016lx,\n", pos, generate_bishop_occupancy_mask(pos));
	}
	printf("rook occupancy masks:\n");
	for (u8 pos = 0; pos < 64; ++pos)
	{
		printf("    [%2u] = 0x%016lx,\n", pos, generate_rook_occupancy_mask(pos));
	}
	#endif

	// generate_magic_numbers();

	#if 0
	printf("const u64 bishop_valid_moves[64][512] =\n{\n");
	generate_bishop_attacks();
	printf("};\n\nconst u64 rook_valid_moves[64][4096] = \n{\n");
	generate_rook_attacks();
	printf("};\n");
	#endif

	printf("const u64 squares_between[64][64] = \n{\n");
	for (u8 a = 0; a < 64; ++a)
	{
		for (u8 b = 0; b < 64; ++b)
		{
			u64 between = in_between_bitboard(a, b);
			printf("\t[%c%c][%c%c] = 0x%016lx,", toupper(get_file_char(a)), get_rank_char(a),
												 toupper(get_file_char(b)), get_rank_char(b),
												 between);
			if (b % 2)
			{
				printf("\n");
			}
			else
			{
				printf(" ");
			}
			#if 0
			PC_CYAN_BOLD("between %c%c and %c%c\n", get_file_char(a), get_rank_char(a),
													get_file_char(b), get_rank_char(b));
			print_bitboard_with_2_pos(between, a, b);
			#endif
		}
	}
	printf("};\n");
}
