#ifndef NNUE_H
#define NNUE_H

#include "common.h"

#include "chess.h"
#include "chess_move.h"
#include "random.h"
#include "threading.h"
// #note NNUE implementation description https://github.com/glinscott/nnue-pytorch/blob/master/docs/nnue.md
/*
	Because one should play differently as white or black (probably better) use different weights

	layer 1:
	#note layer 1 should 
	40960 inputs for curr -> 4 neurons (40960 * 4 = 163840 weights) + 4 bias
	40960 inputs for oppo -> 4 neurons (40960 * 4 = 163840 weights) + 4 bias

	4 neurons for curr
	4 neurons for oppo

	layer 2:
	8 * 8 weights + 8 bias

	output:
	8 weights + bias
*/

#define NNUE_ACCUMULATOR_SIZE 64 * 64 * 5 * 2
typedef u8 nnue_acc[NNUE_ACCUMULATOR_SIZE];

#define NUM_NEURONS 256
// #note #important hidden layer size must be 32 for intrinsics
#define HIDDEN_LAYER_SIZE 32

_Static_assert(HIDDEN_LAYER_SIZE % 32 == 0, "HIDDEN_LAYER_SIZE must be a multiple of 32.");
_Static_assert(NUM_NEURONS % 32 == 0, "NUM_NEURONS must be a multiple of 32.");
_Static_assert(NNUE_ACCUMULATOR_SIZE % 8 == 0, "NNUE_ACCUMULATOR_SIZE must be a multiple of 8.");

#define align8   __attribute__((aligned   (8)))
#define align16  __attribute__((aligned  (16)))
#define align32  __attribute__((aligned  (32)))
#define align64  __attribute__((aligned  (64)))
#define align128 __attribute__((aligned (128)))

typedef struct nnue_layer1_out
{
	s16 curr_output[NUM_NEURONS] align32;
	s16 oppo_output[NUM_NEURONS] align32;
} nnue_layer1_out;

typedef struct nnue_layer2_out
{
	s16 output[HIDDEN_LAYER_SIZE] align128;
} nnue_layer2_out;

typedef struct nnue_layer_1
{
	// #note layout: [acc][neuron]
	s16 curr_weights[NNUE_ACCUMULATOR_SIZE * NUM_NEURONS] align32;
	s16 curr_bias[NUM_NEURONS] align32;

	s16 oppo_weights[NNUE_ACCUMULATOR_SIZE * NUM_NEURONS] align32;
	s16 oppo_bias[NUM_NEURONS] align32;
} nnue_layer_1;

typedef struct nnue_layer_2
{
	// #todo this likely affects the current net updating
	// #note layout: [curr/oppo][hidden_layer_size][num_neurons]
	s16 weights[NUM_NEURONS * HIDDEN_LAYER_SIZE * 2] align32;
	s16 bias[HIDDEN_LAYER_SIZE] align32;
} nnue_layer_2;

typedef struct nnue_layer_3
{
	s16 weights[HIDDEN_LAYER_SIZE] align32;
	s16 bias;
} nnue_layer_3;

typedef struct nnue_net
{
	nnue_layer_1 layer1;
	nnue_layer_2 layer2;
	nnue_layer_3 layer3;
} nnue_net;

typedef struct nnue_update_net
{
	f64 curr_weights[NNUE_ACCUMULATOR_SIZE * NUM_NEURONS];
	f64 curr_bias[NUM_NEURONS];

	f64 oppo_weights[NNUE_ACCUMULATOR_SIZE * NUM_NEURONS];
	f64 oppo_bias[NUM_NEURONS];

	f64 l2_weights[NUM_NEURONS * HIDDEN_LAYER_SIZE * 2];
	f64 l2_bias[HIDDEN_LAYER_SIZE];

	f64 l3_weights[HIDDEN_LAYER_SIZE];
	f64 l3_bias;
} nnue_update_net;

//void nnue_update_accumulators(nnue_net *net, chess_board *board);
void nnue_random_init(nnue_net *net, random_series *rnd);
void nnue_to_file(nnue_net *net, const char *file, u64 num_games);
u64 nnue_from_file(nnue_net *net, const char *file);
s16 nnue_evaluate(const nnue_net *net, const u8 *curr_acc, const u8 *oppo_acc);

void nnue_backpropagate(nnue_net *net, const u8 *curr_acc, const u8 *oppo_acc, nnue_update_net *update_net, s16 eval, s16 target);
void nnue_net_update(nnue_net *net, nnue_update_net *unet, usize num_moves);

void nnue_write_to_bitmap(nnue_net *net, const char *path);

void nnue_test(const nnue_net *net, const u8 *curr_acc, const u8 *oppo_acc);

// #todo remove, just for testing
extern u64 num_calls;
extern u64 num_ticks;

#endif // !NNUE_H
