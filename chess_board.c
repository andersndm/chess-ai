#include "chess_board.h"

#include "ctype.h"
#include "move_generation.h"

/*!
	\title HalfKP NNUE

	num_features = 64 * 64 * 5 * 2 = 40960
	               possible king squares  = 64
				   possible piece squares = 64
				   color is 0 or 1
				   piece is empty (0) or a non-king piece

	note: 0 for the piece means the square is empty, the king piece enum (6) is never used, the king square
	is always passed as separately.
*/

static inline u8 nnue_piece_index(chess_piece_type type, chess_color color)
{
	assert(type < KING);
	assert(type > NONE);
	assert(color == WHITE || color == BLACK);
	u8 result = (type - 1) * 2 + color;
	return result;
}

static inline u16 nnue_halfpk_index(u8 king_square, u8 piece_square,
									chess_piece_type type, chess_color color)
{
	u8 piece_index = nnue_piece_index(type, color);
	u16 result = piece_square + (piece_index + king_square * 10) * 64;
	return result;
}

internal void init_piece(chess_board *board, u8 pos, chess_piece piece)
{
	assert(pos < 64);
	board->squares[pos] = piece;

	if (piece != EMPTY_SQ)
	{
		chess_piece_type type = get_piece_type(piece);
		chess_color color = get_piece_color(piece);

		u64 pos_bit = square_bit(pos);
		board->type_bbs[type] |= pos_bit;
		board->color_bbs[color] |= pos_bit;
	}
}

internal void init_accumulator_type(chess_board *board, u8 king_pos,
									chess_color color, chess_piece_type type)
{
	u64 w_pieces = board->color_bbs[WHITE] & board->type_bbs[type];
	while (w_pieces != 0)
	{
		u8 pos = lsb_pop(&w_pieces);
		u32 acc_index = nnue_halfpk_index(king_pos, pos, type, WHITE);
		assert(acc_index < NNUE_ACCUMULATOR_SIZE);
		board->accumulators[color][acc_index] = 1;
	}

	u64 b_pieces = board->color_bbs[BLACK] & board->type_bbs[type];
	while (b_pieces != 0)
	{
		u8 pos = lsb_pop(&b_pieces);
		u32 acc_index = nnue_halfpk_index(king_pos, pos, type, BLACK);
		assert(acc_index < NNUE_ACCUMULATOR_SIZE);
		board->accumulators[color][acc_index] = 1;
	}
}

internal void init_accumulator_color(chess_board *board, chess_color color)
{
	memset(board->accumulators[color], 0, sizeof(board->accumulators[color]));
	u64 king_bit = board->color_bbs[color] & board->type_bbs[KING];
	assert(king_bit != 0);
	u8 king_pos = lsb(king_bit);
	init_accumulator_type(board, king_pos, color, PAWN);
	init_accumulator_type(board, king_pos, color, KNIGHT);
	init_accumulator_type(board, king_pos, color, BISHOP);
	init_accumulator_type(board, king_pos, color, ROOK);
	init_accumulator_type(board, king_pos, color, QUEEN);
}

void init_accumulators(chess_board *board)
{
	init_accumulator_color(board, WHITE);
	init_accumulator_color(board, BLACK);
}

void add_piece(chess_board *board, u8 pos, chess_piece piece)
{
	assert(pos < 64);

	board->squares[pos] = piece;

	if (piece != EMPTY_SQ)
	{
		chess_piece_type type = get_piece_type(piece);
		chess_color color = get_piece_color(piece);
		assert(type != KING);

		u64 pos_bit = square_bit(pos);
		board->type_bbs[type] |= pos_bit;
		board->color_bbs[color] |= pos_bit;

		u64 king_bit = board->color_bbs[color] & board->type_bbs[KING];
		assert(king_bit != 0);
		u8 king_pos = lsb(king_bit);
		u32 acc_index = nnue_halfpk_index(king_pos, pos, type, color);
		board->accumulators[color][acc_index] = 1;

		chess_color oppo_color = opposite_color(color);
		u64 oppo_king_bit = board->color_bbs[oppo_color] & board->type_bbs[KING];
		assert(oppo_king_bit != 0);
		u8 oppo_king_pos = lsb(oppo_king_bit);
		acc_index = nnue_halfpk_index(oppo_king_pos, pos, type, color);
		board->accumulators[oppo_color][acc_index] = 1;
	}
}

void remove_piece(chess_board *board, u8 pos)
{
	assert(pos >= 0 && pos < 64);

	chess_piece old_piece = board->squares[pos];
	board->squares[pos] = EMPTY_SQ;

	if (old_piece != EMPTY_SQ)
	{
		chess_piece_type type = get_piece_type(old_piece);
		chess_color color = get_piece_color(old_piece);
		assert(type != KING);

		u64 not_pos_bit = ~square_bit(pos);
		board->type_bbs[type] &= not_pos_bit;
		board->color_bbs[color] &= not_pos_bit;

		u64 king_bit = board->color_bbs[color] & board->type_bbs[KING];
		assert(king_bit != 0);
		u8 king_pos = lsb(king_bit);
		u32 acc_index = nnue_halfpk_index(king_pos, pos, type, color);
		board->accumulators[color][acc_index] = 0;

		chess_color oppo_color = opposite_color(color);
		u64 oppo_king_bit = board->color_bbs[oppo_color] & board->type_bbs[KING];
		assert(oppo_king_bit != 0);
		u8 oppo_king_pos = lsb(oppo_king_bit);
		acc_index = nnue_halfpk_index(oppo_king_pos, pos, type, color);
		board->accumulators[oppo_color][acc_index] = 0;
	}
}

void move_king(chess_board *board, u8 from, u8 to)
{
	assert(from >= 0 && from < 64);
	assert(to >= 0 && to < 64);

	chess_piece old_piece = board->squares[from];

	chess_piece_type type = get_piece_type(old_piece);
	chess_color color = get_piece_color(old_piece);
	assert(type == KING);

	board->squares[from] = EMPTY_SQ;
	if (board->squares[to] != EMPTY_SQ)
	{
		remove_piece(board, to);
	}
	board->squares[to] = old_piece;

	u64 not_from_bit = ~square_bit(from);
	board->type_bbs[type] &= not_from_bit;
	board->color_bbs[color] &= not_from_bit;

	u64 to_bit = square_bit(to);
	board->type_bbs[type] |= to_bit;
	board->color_bbs[color] |= to_bit;

	init_accumulator_color(board, color);
}

chess_board start_board(state_info *state)
{
	chess_board result = { 0 };
	init_piece(&result, FILE_A, W_ROOK);
	init_piece(&result, FILE_B, W_KNIGHT);
	init_piece(&result, FILE_C, W_BISHOP);
	init_piece(&result, FILE_D, W_QUEEN);
	init_piece(&result, FILE_E, W_KING);
	init_piece(&result, FILE_F, W_BISHOP);
	init_piece(&result, FILE_G, W_KNIGHT);
	init_piece(&result, FILE_H, W_ROOK);

	for (u32 file = FILE_A; file < NUM_FILES; ++file)
	{
		init_piece(&result, file + RANK_2 * NUM_FILES, W_PAWN);
	}

	for (u32 rank = RANK_3; rank < RANK_7; ++rank)
	{
		for (u32 file = FILE_A; file < NUM_FILES; ++file)
		{
			init_piece(&result, file + rank * NUM_FILES, EMPTY_SQ);
		}
	}

	for (u32 file = FILE_A; file < NUM_FILES; ++file)
	{
		init_piece(&result, file + RANK_7 * NUM_FILES, B_PAWN);
	}

	init_piece(&result, FILE_A + RANK_8 * NUM_FILES, B_ROOK);
	init_piece(&result, FILE_B + RANK_8 * NUM_FILES, B_KNIGHT);
	init_piece(&result, FILE_C + RANK_8 * NUM_FILES, B_BISHOP);
	init_piece(&result, FILE_D + RANK_8 * NUM_FILES, B_QUEEN);
	init_piece(&result, FILE_E + RANK_8 * NUM_FILES, B_KING);
	init_piece(&result, FILE_F + RANK_8 * NUM_FILES, B_BISHOP);
	init_piece(&result, FILE_G + RANK_8 * NUM_FILES, B_KNIGHT);
	init_piece(&result, FILE_H + RANK_8 * NUM_FILES, B_ROOK);

	result.state = state;
	result.state->castling_rights = WHITE_KINGSIDE | WHITE_QUEENSIDE | BLACK_KINGSIDE | BLACK_QUEENSIDE;
	result.state->en_passant = 0;
	result.state->captured_piece = EMPTY_SQ;
	result.state->fifty_rule = 0;
	result.state->ply = 0;
	result.state->side = WHITE;
	result.state->hash = position_hash(&result);

	result.state->previous = NULL;

	init_accumulators(&result);
	result.repmap = hashmap_new(64);

	return result;
}

internal const char *skip_spaces(const char *str)
{
	while (*str == ' ') { ++str; }
	return str;
}

#include "platform_common.h"
internal void fen_error(const char *fen, u64 offset, const char *msg, ...)
{
	va_list args;
	va_start(args, msg);
	PC_RED_BOLD("fen error: ");
	vprintf(msg, args);
	va_end(args);

	printf("%s\n", fen);
	while (offset--) { printf(" "); }
	printf("^\n");
}

bool board_from_fen(chess_board *board, state_info *state, const char *fen)
{
	memset(board, 0, sizeof(chess_board));
	board->state = state;
	board->state->previous = NULL;

	const char *str = fen;
	u32 file = FILE_A;
	u32 rank = RANK_8;

	u32 num_wkings = 0;
	u32 num_bkings = 0;

	while (*str != ' ')
	{
		char c = *str++;
		switch (c)
		{
			case 'p':
			{
				init_piece(board, file + rank * NUM_FILES, B_PAWN);
				++file;
			} break;
			case 'n':
			{
				init_piece(board, file + rank * NUM_FILES, B_KNIGHT);
				++file;
			} break;
			case 'b':
			{
				init_piece(board, file + rank * NUM_FILES, B_BISHOP);
				++file;
			} break;
			case 'r':
			{
				init_piece(board, file + rank * NUM_FILES, B_ROOK);
				++file;
			} break;
			case 'q':
			{
				init_piece(board, file + rank * NUM_FILES, B_QUEEN);
				++file;
			} break;
			case 'k':
			{
				init_piece(board, file + rank * NUM_FILES, B_KING);
				++num_bkings;
				++file;
			} break;
			case 'P':
			{
				init_piece(board, file + rank * NUM_FILES, W_PAWN);
				++file;
			} break;
			case 'N':
			{
				init_piece(board, file + rank * NUM_FILES, W_KNIGHT);
				++file;
			} break;
			case 'B':
			{
				init_piece(board, file + rank * NUM_FILES, W_BISHOP);
				++file;
			} break;
			case 'R':
			{
				init_piece(board, file + rank * NUM_FILES, W_ROOK);
				++file;
			} break;
			case 'Q':
			{
				init_piece(board, file + rank * NUM_FILES, W_QUEEN);
				++file;
			} break;
			case 'K':
			{
				init_piece(board, file + rank * NUM_FILES, W_KING);
				++num_wkings;
				++file;
			} break;
			case '/':
			{
				file = FILE_A;
				--rank;
			} break;
			default:
			{
				if (c >= '1' && c <= '8')
				{
					u32 times = c - '0';
					for (u32 i = 0; i < times; ++i)
					{
						init_piece(board, file + rank * NUM_FILES, EMPTY_SQ);
						++file;
					}
				}
				else
				{
					fen_error(fen, str - fen, "invalid character in fen-string: %c\n", c);
					return false;
				}
			} break;
		}
	}

	if (num_wkings == 0)
	{
		PC_RED_BOLD("fen error: ");
		printf("must include a white king.\n");
		return false;
	}
	else if (num_wkings > 1)
	{
		PC_RED_BOLD("fen error: ");
		printf("only 1 white king permitted.\n");
		return false;
	}

	if (num_bkings == 0)
	{
		PC_RED_BOLD("fen error: ");
		printf("must include a black king.\n");
		return false;
	}
	else if (num_bkings > 1)
	{
		PC_RED_BOLD("fen error: ");
		printf("only 1 black king permitted.\n");
		return false;
	}

	str = skip_spaces(str);
	if (*str == 'w')
	{
		board->state->side = WHITE;
	}
	else if (*str == 'b')
	{
		board->state->side = BLACK;
	}
	else
	{
		fen_error(fen, str - fen, "invalid character in fen-string: %c\n", *str);
		return false;
	}
	str = skip_spaces(++str);

	board->state->castling_rights = 0;
	if (*str != '-')
	{
		if (*str == 'K') { board->state->castling_rights |= WHITE_KINGSIDE; ++str; }
		if (*str == 'Q') { board->state->castling_rights |= WHITE_QUEENSIDE; ++str; }
		if (*str == 'k') { board->state->castling_rights |= BLACK_KINGSIDE; ++str; }
		if (*str == 'q') { board->state->castling_rights |= BLACK_QUEENSIDE; ++str; }
		assert(board->state->castling_rights < 16);
		if (board->state->castling_rights == 0)
		{
			fen_error(fen, str - fen, "invalid character in fen-string: %c\n", *str);
			return false;
		}
	}
	else
	{
		board->state->castling_rights = 0;
		++str;
	}
	skip_spaces(++str);

	if (*str != '-')
	{
		if (str[0] < 'a' || str[0] > 'h')
		{
			fen_error(fen, str - fen, "invalid character in fen-string: %c\n", str[0]);
			return false;
		}
		if (str[1] < '1' || str[1] > '8')
		{
			fen_error(fen, (str + 1) - fen, "invalid character in fen-string: %c\n", str[1]);
			return false;
		}
		file = str[0] - 'a';
		rank = str[1] - '1';

		assert(file <= FILE_H);
		assert(rank <= RANK_8);

		board->state->en_passant = file + rank * NUM_FILES;
		str += 2;
	}
	else
	{
		board->state->en_passant = 0;
		++str;
	}
	skip_spaces(str);

	board->state->fifty_rule = 0;
	if (isdigit(*str))
	{
		char c = *str++;
		board->state->fifty_rule *= 10;
		board->state->fifty_rule += c - '0';
	}
	skip_spaces(str);

	board->state->ply = 0;
	if (isdigit(*str))
	{
		char c = *str++;
		board->state->ply *= 10;
		board->state->ply += c - '0';
	}

	board->state->captured_piece = EMPTY_SQ;
	board->state->hash = position_hash(board);
	init_accumulators(board);
	board->repmap = hashmap_new(64);

	return true;
}

const char *to_fen(const chess_board *board)
{
	char *buffer = NULL;
	//s32 count = 0;
	s32 empty_count = 0;
	for (s32 r = 8; r > 0; --r)
	{
		for (s32 f = 0; f < 8; ++f)
		{
			chess_piece piece = board->squares[f + (r - 1) * 8];

			if (empty_count > 0 && piece != EMPTY_SQ)
			{
				buf_push(buffer, '0' + empty_count);
				empty_count = 0;
			}

			if (piece == EMPTY_SQ)
			{
				++empty_count;
			}
			else
			{
				buf_push(buffer, get_piece_char(piece));
			}
		}
		if (empty_count > 0)
		{
			buf_push(buffer, '0' + empty_count);
			empty_count = 0;
		}
		if (r > 1) { buf_push(buffer, '/'); }
	}
	buf_push(buffer, ' ');
	buf_push(buffer, board->state->side == WHITE ? 'w' : 'b');
	buf_push(buffer, ' ');
	if (board->state->castling_rights == 0)
	{
		buf_push(buffer, '-');
	}
	else
	{
		if (board->state->castling_rights & WHITE_KINGSIDE)
		{
			buf_push(buffer, 'K');
		}
		if (board->state->castling_rights & WHITE_QUEENSIDE)
		{
			buf_push(buffer, 'Q');
		}
		if (board->state->castling_rights & BLACK_KINGSIDE)
		{
			buf_push(buffer, 'k');
		}
		if (board->state->castling_rights & BLACK_QUEENSIDE)
		{
			buf_push(buffer, 'q');
		}
	}
	buf_push(buffer, ' ');
	if (board->state->en_passant == 0)
	{
		buf_push(buffer, '-');
	}
	else
	{
		buf_push(buffer, 'a' + (board->state->en_passant % 8));
		buf_push(buffer, '1' + (board->state->en_passant / 8));
	}
	buf_push(buffer, ' ');
	u32 fifty_move = board->state->fifty_rule;
	char fifty_arr[16];
	snprintf(fifty_arr, array_count(fifty_arr), "%u", fifty_move);
	const char *fifty_str = fifty_arr;
	while (*fifty_str)
	{
		buf_push(buffer, *fifty_str++);
	}
	buf_push(buffer, ' ');
	u32 ply = board->state->ply;
	char ply_arr[16];
	snprintf(ply_arr, array_count(ply_arr), "%u", ply);
	const char *ply_str = ply_arr;
	while (*ply_str)
	{
		buf_push(buffer, *ply_str++);
	}

	buf_push(buffer, 0);
	return buffer;
}

void copy_board(chess_board *dst, const chess_board *src)
{
	memcpy(dst, src, sizeof(chess_board));
	dst->repmap = hashmap_new(src->repmap.cap);
	for (u32 i = 0; i < src->repmap.cap; ++i)
	{
		if (src->repmap.keys != 0)
		{
			assert(src->repmap.vals != 0);
			dst->repmap.keys[i] = src->repmap.keys[i];
			dst->repmap.vals[i] = src->repmap.vals[i];
		}
	}
}

internal bool insufficient_material(const chess_board *board)
{
	u32 P = popcount(board->color_bbs[WHITE] & board->type_bbs[PAWN]);
	u32 p = popcount(board->color_bbs[BLACK] & board->type_bbs[PAWN]);
	u32 N = popcount(board->color_bbs[WHITE] & board->type_bbs[KNIGHT]);
	u32 n = popcount(board->color_bbs[BLACK] & board->type_bbs[KNIGHT]);
	u32 B = popcount(board->color_bbs[WHITE] & board->type_bbs[BISHOP]);
	u32 b = popcount(board->color_bbs[BLACK] & board->type_bbs[BISHOP]);
	u32 R = popcount(board->color_bbs[WHITE] & board->type_bbs[ROOK]);
	u32 r = popcount(board->color_bbs[BLACK] & board->type_bbs[ROOK]);
	u32 Q = popcount(board->color_bbs[WHITE] & board->type_bbs[QUEEN]);
	u32 q = popcount(board->color_bbs[BLACK] & board->type_bbs[QUEEN]);

	if (p == 0 && P == 0 && r == 0 && R == 0 && q == 0 && Q == 0)
	{
		// #note only kings left
		if (!n && !N && !b && !B)
		{
			return true;
		}
		// #note black king vs white king and one bishop
		else if (!n && !b && !N && B < 2)
		{
			return true;
		}
		// #note white king vs black king and one bishop
		else if (!N && !B && !n && b < 2)
		{
			return true;
		}
		// #note black king vs white king and 2 knights
		else if (!n && !b && !B && N < 3)
		{
			return true;
		}
		// #note white king vs black king and 2 knights
		else if (!N && !B && !b && n < 3)
		{
			return true;
		}
		else if (n + b == 1 && N + B == 1)
		{
			return true;
		}
	}
	return false;
}

// #note does not return check/stalemates!!!
chess_result apply_move(chess_board *board, state_info *new_state, chess_move move)
{
	// #todo when initializing with moves it is easiest to reuse the state
	//assert(new_state != board->state);
	memcpy(new_state, board->state, sizeof(*new_state));
	new_state->previous = board->state;
	board->state = new_state;

	assert(move_ok(move));
	u8 to = move_to(move);
	u8 from = move_from(move);
	bool castling = is_castling(move);
	bool en_passant = is_en_passant(move);
	bool promotion = is_promotion(move);

	chess_piece_type type = get_piece_type(board->squares[from]);
	chess_color color = get_piece_color(board->squares[from]);
	chess_piece_type new_type = type;

	bool capture = board->squares[to] != EMPTY_SQ;

	if (capture || type == PAWN)
	{
		board->state->fifty_rule = 0;
	}

	if (castling)
	{
		chess_castling castling_kind = 0;
		if (color == WHITE)
		{
			if (to > from)
			{
				castling_kind = WHITE_KINGSIDE;
			}
			else
			{
				assert(to < from);
				castling_kind = WHITE_QUEENSIDE;
			}
		}
		else
		{
			if (to > from)
			{
				castling_kind = BLACK_KINGSIDE;
			}
			else
			{
				assert(to < from);
				castling_kind = BLACK_QUEENSIDE;
			}
		}
		assert((board->state->castling_rights & castling_kind));
		switch (castling_kind)
		{
		case WHITE_KINGSIDE:
		{
			remove_piece(board, H1);
			add_piece(board, F1, W_ROOK);
		}
		break;
		case WHITE_QUEENSIDE:
		{
			remove_piece(board, A1);
			add_piece(board, D1, W_ROOK);
		}
		break;
		case BLACK_KINGSIDE:
		{
			remove_piece(board, H8);
			add_piece(board, F8, B_ROOK);
		}
		break;
		case BLACK_QUEENSIDE:
		{
			remove_piece(board, A8);
			add_piece(board, D8, B_ROOK);
		}
		break;
		}
	}
	if (castling || type == KING)
	{
		if (color == WHITE)
		{
			board->state->castling_rights &= 0xc;
		}
		else
		{
			board->state->castling_rights &= 0x3;
		}
	}
	else if (type == ROOK)
	{
		if (color == WHITE)
		{
			if (from == A1)
			{
				board->state->castling_rights &= ~WHITE_QUEENSIDE;
			}
			else if (from == H1)
			{
				board->state->castling_rights &= ~WHITE_KINGSIDE;
			}
		}
		else
		{
			if (from == A8)
			{
				board->state->castling_rights &= ~BLACK_QUEENSIDE;
			}
			else if (from == H8)
			{
				board->state->castling_rights &= ~BLACK_KINGSIDE;
			}
		}
	}
	else if (type == PAWN)
	{
		if (to == board->state->en_passant && board->state->en_passant != INVALID_EN_PASSANT)
		{
			s8 delta = color == WHITE ? -NUM_FILES : NUM_FILES;
			board->state->captured_piece = board->squares[to + delta];
			remove_piece(board, to + delta);
			capture = true;
		}
		else if (en_passant)
		{
			board->state->en_passant = color == WHITE ? from + NUM_FILES : from - NUM_FILES;
		}
		else if (promotion)
		{
			new_type = promotion_type(move);
		}
	}

	if (!en_passant)
	{
		board->state->en_passant = INVALID_EN_PASSANT;
	}

	if (board->squares[to] != EMPTY_SQ)
	{
		board->state->captured_piece = board->squares[to];
		chess_piece_type capture_type = get_piece_type(board->squares[to]);
		chess_color capture_color = get_piece_color(board->squares[to]);
		assert(capture_color != color);
		if (capture_type == ROOK)
		{
			if (capture_color == WHITE)
			{
				if (to == A1)
				{
					board->state->castling_rights &= ~WHITE_QUEENSIDE;
				}
				else if (to == H1)
				{
					board->state->castling_rights &= ~WHITE_KINGSIDE;
				}
			}
			else
			{
				if (to == A8)
				{
					board->state->castling_rights &= ~BLACK_QUEENSIDE;
				}
				else if (to == H8)
				{
					board->state->castling_rights &= ~BLACK_KINGSIDE;
				}
			}
		}
		#if 1
		assert(capture_type != KING);
		#else
		if (capture_type == KING)
		{
			print_move(move); printf("\n");
			print_board(board);
			u8 king_pos = to;
			chess_color oppo_color = board->state->side;
			chess_color king_color = opposite_color(oppo_color);
			u64 oppo_queens = board->color_bbs[oppo_color] & board->type_bbs[QUEEN];
			debug_break;
			int a = 5;
		}
		#endif
		remove_piece(board, to);
	}

	if (!capture)
	{
		board->state->captured_piece = EMPTY_SQ;
	}

	if (type == KING)
	{
		move_king(board, from, to);
	}
	else
	{
		remove_piece(board, from);
		add_piece(board, to, make_piece(new_type, color));
	}

	chess_result result = CR_ONGOING;
	board->state->side = opposite_color(board->state->side);
	++board->state->ply;
	++board->state->fifty_rule;
	if (board->state->fifty_rule > 100)
	{
		result = CR_DRAW_FIFTY_MOVE;
	}
	else if (insufficient_material(board))
	{
		result = CR_DRAW_INSUFFICIENT_MATERIAL;
	}

	u64 hash = position_hash(board);
	board->state->hash = hash;
	// #note 0 is sentinel, so must add 1 more than the actual rep count
	u64 rep_count = hashmap_get(&board->repmap, hash);
	++rep_count;
	if (rep_count > 3)
	{
		result = CR_DRAW_REPETITION;
	}
	hashmap_put(&board->repmap, hash, rep_count);

	return result;
}

void undo_move(chess_board *board, chess_move move)
{
	u64 hash = position_hash(board);
	u64 rep_count = hashmap_get(&board->repmap, hash);
	// #note 0 is sentinel, so must add 1 more than the actual rep count
	if (rep_count > 1)
	{
		--rep_count;
		hashmap_put(&board->repmap, hash, rep_count);
	}
	else
	{
		hashmap_remove(&board->repmap, hash);
	}

	assert(move_ok(move));
	u8 to = move_to(move);
	u8 from = move_from(move);
	bool castling = is_castling(move);
	bool promotion = is_promotion(move);

	chess_piece piece = board->squares[to];
	chess_color color = get_piece_color(piece);
	if (castling)
	{
		move_king(board, to, from);
		if (color == WHITE)
		{
			if (to > from)
			{
				remove_piece(board, F1);
				add_piece(board, H1, W_ROOK);
			}
			else
			{
				remove_piece(board, D1);
				add_piece(board, A1, W_ROOK);
			}
		}
		else
		{
			if (to > from)
			{
				remove_piece(board, F8);
				add_piece(board, H8, B_ROOK);
			}
			else
			{
				remove_piece(board, D8);
				add_piece(board, A8, B_ROOK);
			}
		}
	}
	else
	{
		if (get_piece_type(piece) == KING)
		{
			move_king(board, to, from);
		}
		else
		{
			remove_piece(board, to);
			if (promotion)
			{
				add_piece(board, from, make_piece(PAWN, color));
			}
			else
			{
				add_piece(board, from, piece);
			}
		}
		if (board->state->captured_piece != EMPTY_SQ)
		{
			u8 capture_square = to;
			if (to == board->state->previous->en_passant &&
				board->state->previous->en_passant != INVALID_EN_PASSANT)
			{
				s8 delta = color == WHITE ? -NUM_FILES : NUM_FILES;
				capture_square += delta;
			}
			add_piece(board, capture_square, board->state->captured_piece);
		}
	}

	board->state = board->state->previous;
}

void apply_null_move(chess_board *board, state_info *new_state)
{
	memcpy(new_state, board->state, sizeof(*new_state));
	new_state->previous = board->state;
	board->state = new_state;
	board->state->side = opposite_color(board->state->side);
	++board->state->ply;
	board->state->en_passant = 0;

	u64 hash = position_hash(board);
	board->state->hash = hash;
	// #note 0 is sentinel, so must add 1 more than the actual rep count
	u64 rep_count = hashmap_get(&board->repmap, hash);
	++rep_count;
	hashmap_put(&board->repmap, hash, rep_count);
}

void undo_null_move(chess_board *board)
{
	u64 hash = position_hash(board);
	u64 rep_count = hashmap_get(&board->repmap, hash);
	// #note 0 is sentinel, so must add 1 more than the actual rep count
	if (rep_count > 1)
	{
		--rep_count;
		hashmap_put(&board->repmap, hash, rep_count);
	}
	else
	{
		hashmap_remove(&board->repmap, hash);
	}
	board->state = board->state->previous;
}

check_status get_check_status(const chess_board *board)
{
	check_status result;
	result.double_check = false;
	result.single_check = false;
	result.capture_mask = 0xFFFFFFFFFFFFFFFF;
	result.push_mask = 0xFFFFFFFFFFFFFFFF;

	u32 num_checkers = 0;
	u64 checkers = 0;

	chess_color color_to_play = board->state->side;
	chess_color king_color = color_to_play;
	chess_color oppo_color = opposite_color(color_to_play);
	u64 self_pieces = board->color_bbs[king_color];
	u64 king_bit = board->type_bbs[KING] & self_pieces;
	assert(king_bit != 0);
	u8 king_pos = lsb(king_bit);
	u64 oppo_pieces = board->color_bbs[oppo_color];
	u64 occupancy = self_pieces | oppo_pieces;

	// #note first check for each piece type if there are any pieces of opposing color
	// giving check. This can be done by calculating the moves for that piece type from
	// the king square. If any of them hit their piece type, that piece is giving check.
	u64 oppo_pawns = oppo_pieces & board->type_bbs[PAWN];
	u64 pawn_checkers = find_pawn_checkers(king_pos, king_color, oppo_pawns);
	num_checkers += popcount(pawn_checkers);
	checkers |= pawn_checkers;

	u64 target_knights = oppo_pieces & board->type_bbs[KNIGHT];
	u64 knight_checkers = find_knight_checkers(king_pos, target_knights);
	num_checkers += popcount(knight_checkers);
	checkers |= knight_checkers;

	u64 target_bishops = oppo_pieces & board->type_bbs[BISHOP];
	u64 bishop_checkers = find_bishop_checkers(king_pos, target_bishops, occupancy);
	num_checkers += popcount(bishop_checkers);
	checkers |= bishop_checkers;

	u64 target_rooks = oppo_pieces & board->type_bbs[ROOK];
	u64 rook_checkers = find_rook_checkers(king_pos, target_rooks, occupancy);
	num_checkers += popcount(rook_checkers);
	checkers |= rook_checkers;

	u64 target_queens = oppo_pieces & board->type_bbs[QUEEN];
	u64 queen_checkers = find_queen_checkers(king_pos, target_queens, occupancy);
	num_checkers += popcount(queen_checkers);
	checkers |= queen_checkers;

	result.double_check = num_checkers > 1;
	result.single_check = num_checkers == 1;
	// #note multiple checkers require the king to move, single checkers can be blocked
	// or captured.
	if (result.single_check)
	{
		result.capture_mask = checkers;
		u8 pos = lsb(checkers);
		chess_piece_type type = get_piece_type(board->squares[pos]);
		result.push_mask = 0;
		if (type == BISHOP || type == ROOK || type == QUEEN)
		{
			chess_file king_file = king_pos % NUM_RANKS;
			chess_rank king_rank = king_pos / NUM_RANKS;
			chess_file check_file = pos % NUM_RANKS;
			chess_rank check_rank = pos / NUM_RANKS;
			s16 file_delta = king_file - check_file;
			s16 rank_delta = king_rank - check_rank;
			if (file_delta != 0)
			{
				file_delta /= abs(file_delta);
			}
			if (rank_delta != 0)
			{
				rank_delta /= abs(rank_delta);
			}

			chess_file current_file = check_file + file_delta;
			chess_rank current_rank = check_rank + rank_delta;
			while (current_file != king_file || current_rank != king_rank)
			{
				result.push_mask |= square_bit((current_file + current_rank * NUM_FILES));
				current_file += file_delta;
				current_rank += rank_delta;
			}
		}
	}

	return result;
}

// #note stride of 12
internal u64 piece_hash_table[12 * 64] =
{
	// white pawn		black pawn			white knight		black knight		white bishop		black bishop		white rook			black rook			white queen			black queen			white king			black king
	0x26bf1895ad80b550, 0x52936688890ddc95, 0x92283870662e6008, 0x0b9af164d8e6cc92, 0xae334fa8b02c18cb, 0xe97b3cd53414095a,	0x2d847e0b63eac95e, 0x4251e13617369a3b, 0x0a4d968512702cc9, 0x95bf597db2c7d481, 0xa0b674740cde2850, 0x88f3be30889a8a6f,
	0x68484880e58964d4, 0xbf1051fb19a8b4ad, 0x742ee3e56c551c6a, 0x5ed0a10036b919a4, 0x149ebfefcb8ac432, 0x2bb6852462ba3ef0, 0x9ce01621631f8266, 0xab843f0a4bffc7c2, 0x5209806549a74093, 0x5a74af924dabc31a, 0x7f480d25656276b2, 0x72dca883e7cf25ad,
	0x2fbdf05a29eda92a, 0x1669221df9b99c91, 0x2c2b4e9c87f5ab6e, 0xda25f688bba5789f, 0x5279b73d22ec8b34, 0xbf748ca6750c5df7, 0x473c36b936136103, 0x41230331e0e0eff3, 0xf201b52b3869b07f, 0xf85b60bcc43f50f9, 0x4bd597da91a19924, 0x07d173cf9b0af813,
	0xc3079a2e58f625f5, 0x922ecc98d5c42920, 0x8fabe6024bc2e347, 0xd2620fb05a7e413e, 0x05a4bcc4ad55c15c, 0xb65f6fc9e6136875, 0x08d4571038fc2417, 0xf6df2f092ddb2dde, 0x0cd14cb99641e3ea, 0x8dd3d3cc35c22f85, 0x7ab81cae4f7ad19c, 0xedb1d614ecf1660a,
	0x06259aae484c7299, 0x9e27528ec454da45, 0x381af205fcf2f0a6, 0xc7a52654560d4487, 0x149d8717d008747c, 0x30868036e108ba13, 0xae17847c7ba6d7d9, 0xa2ffaee80e307252, 0x103fa8f4c543f7af, 0xf557267aef3c4feb, 0x4ef720f701632059, 0x7790260ee79f7978,
	0x3708b3dea53a8d36, 0x5210665ec862e5e4, 0x2c4e8c8707458c60, 0x443d6d6bcf81c502, 0x1d9d063491984851, 0x568d539cd2cfe8bc, 0xb0ba38c4d1290eb0, 0x573ac3e5dc62993b, 0xa2400257544f348c, 0x8ad2ae68e67c2235, 0x4a497cad7f1b2d32, 0x63600388f6d42b9e,
	0x03cfea716106faf8, 0xa43b59992769de0b, 0x893d0980b132505f, 0x6c4e58552696ef17, 0xadb0f72e1398951b, 0x6e0091dff8b5a1b5, 0x026582041c0e6ce3, 0x3b7749d2c54b7349, 0xaccabb6f90278981, 0x93b839db9313526f, 0x5716a15d49d166b9, 0xbad8bcdafda433f7,
	0x57c41b57c574c923, 0x9fb283329e1bd4e9, 0x0de0aae2814c5227, 0x23025f724d147ad8, 0x7ed464de3821cdfc, 0xb147709fd9a4c14b, 0xcddb8c4689932ca5, 0xe56bc2b6a269259b, 0xeee8023a4ef9ef3e, 0x89fb5de90eccf600, 0xcfe1e039ed609035, 0x80ee4a15eb320ada,
	0xa072da1174349ed3, 0x7ef3e77471dbb4df, 0x35757864d1ef194c, 0xc59d966ed4373249, 0x5e04ce4b6b81d784, 0x811720231f71a6a0, 0x72e7a614212276cd, 0x8202ea6359516685, 0x0f58c7ba34816290, 0xe6fb9e070ab202e3, 0xc7d3e92b792e08d1, 0xff302a25d5b5c33b,
	0x525af9cacd979ebd, 0x7accd2266d7b0f25, 0x39161b312381fa66, 0x10b3e566fa25a4a7, 0x2eec744288aa4822, 0xd30a4656479cea76, 0x83f60442ae3877c7, 0xda87f4bb99d317f5, 0x6e67bf8dd317f04c, 0xf9f9f645ca2ddca1, 0xec37e1a963df9211, 0xc6767b4b754350a0,
	0x3ac353abf0e9082c, 0xe9bdae4494cd4b4a, 0x35af4a70364811e4, 0x6e1dfb4306c7607e, 0x18aa71c4a96898e6, 0x215e0d8430b0ba81, 0x71bcfc74014c7277, 0x6acdab5f2af5a94d, 0x25ddfffd4f5e4f61, 0x533b203d998899da, 0x316bfe638f8ced79, 0x33e6e5b290abc2ad,
	0x7cb0bc0ae046bebf, 0x9aa4dc564112810e, 0x7a7e55956093a268, 0xcf088b6a42cb4ad3, 0x35d82bcdf356cde4, 0xd4fc417488058bde, 0x9a5aaf3670d4cb78, 0xae20d9634eac39b2, 0x4d050c8de13030e3, 0x5ad97f3c26b67277, 0x996cdb5a30b0fba3, 0x1ac1d0f42bc626f7,
	0x4e1eb806eee8b1a1, 0xb84abc48a9ea5828, 0x867638f47dee179b, 0xa3cb76ea9e653e78, 0xf5ba0bfb51b0fa7f, 0xd07bda7676270468, 0xed8bc7857c4e6fae, 0x43821de5edfafaf6, 0x992712e9d4a291ab, 0xd4394a54ec19135e, 0x89024d1478d15554, 0x2aad4813577cf2e2,
	0x5563385a092a3e30, 0xfbbe9f3a3be41214, 0xf449748d38340511, 0x35a2743b536cec47, 0xc7b92c899351a57e, 0xe23497c043f25c6a, 0x59c8a3240d353dcc, 0x71081b7aca582ab1, 0x5c8dc815f9d3b215, 0x40530ef697e6471a, 0xed2e99fb9225d4bf, 0x66bc5bbb9f5206d1,
	0xb48139f0ee27a185, 0x8963ebfe90580191, 0xaa784464260a587b, 0xa05a85cdb5cbaba3, 0x2cecb11eb2788b55, 0x7f971d5772e449c9, 0xddf4768ab4c117e2, 0xd5e791daaed4867e, 0x575608e11e39162a, 0xea903e823d44b005, 0x3d052e157d991958, 0x09e6732215da9dc3,
	0xf83d27694c175e25, 0x5a409b72496b1032, 0x394b8851073add10, 0xa809f6a0ee518d13, 0xd37855e2fed85853, 0xbf2da629bfbf2737, 0xa7a73e52fbddad5a, 0x17caf4fde25bfe28, 0xc975b6b7b2242ab3, 0x9b8152be63bbfd7b, 0x9f7dce45d294468a, 0xfc0ec9da1aa61a49,
	0x5abcc4d57426db9c, 0x007285ba5aaa404a, 0x13d86faea17d87e9, 0x61eb0ea6fd811812, 0x34d66e4f6f16e461, 0x3cf19a3f89560d1f, 0x188f9338ee783a4f, 0xfed0e9a26eb2f180, 0xc60d952d15abd829, 0x05b9472257a6dde5, 0x7e1f20afaba873ae, 0x6f6a3b5f502e92e0,
	0x5bff76a39370f771, 0xde9ffb2ec3294536, 0x7160e193f69c9766, 0x40a2922c0b72d724, 0x1a6962c1664ad435, 0x9bf166752721c677, 0xaa916cade18cf33c, 0xbd7619942f46a403, 0x3fecafac52f7759b, 0xaf47ad8a1345664e, 0x7466d25b9ee51044, 0x21a76e0400c2b6bf,
	0x3f9224480eba8740, 0x49ad73f2c3ce34ec, 0xc739c3071c7ff504, 0xd2b8d896e1dd58c4, 0xba68f1038829c938, 0xe34a32a7a6176446, 0xedbc21d5fbc0a25e, 0x8644d31febd8c847, 0x2fdaf7c1a39e281d, 0xa1c1bb8a9cb51597, 0xd4396ac2825ba332, 0xa4ba4a3a2fab659c,
	0x345afc8c61d0f677, 0xcbcb3d5ec1c619e3, 0x95bf09471bff1ba5, 0xe9a57e7a3112cac6, 0xedc2f333ef322d31, 0x07e9e76e3ee885c4, 0x8784426f8c5c7fa6, 0x45eb827594b32869, 0x02bf739337b94d51, 0xb124367f3f658ad4, 0x0429766558902150, 0x89648109a9acd497,
	0x0295086181d4d895, 0xe057df45bda954d9, 0x7b3326f3befe5c6e, 0xd09d155a13dddf39, 0x8614a009ee5fa87f, 0x15c14a8a20800bbe, 0xd4698aa938ef5176, 0x96eaa71de6e8bc5e, 0x42f8dadac1f5afa4, 0xb1f398ce2e9aa8aa, 0x49f1faa8d5d84e2e, 0xc827e28b5afff372,
	0xcd2a67ce315f0b9a, 0x95554e7cdb49ad40, 0xe06104bb980763e2, 0x4713264ff4b235e4, 0xca3a2dc0412a3801, 0xc6b6505073072f7e, 0x12a21f2ffe761b80, 0x17e8ea3ac4141c8e, 0xa1876c311c1871be, 0xc1ff183f726a6e02, 0xf40c91920aa444a7, 0xe5fb3503ddb325a5,
	0x1b625794cd87381f, 0xef1540e80f46411d, 0x5bf1904fcae9d880, 0xea2d914120d92648, 0xe8e61cc6314f55b0, 0xdb67d653aaf7fea3, 0x4babc920d4b65b25, 0x874df16c3d1a58c5, 0xa85b42eac7ae9292, 0x33ab83c1965c2c02, 0xf73003da7fa68dea, 0xf2a573a1e8671e35,
	0x198bb09a7b88f9e7, 0x9fa2c6af49b6a6f4, 0xd0ef046111a123bb, 0x8b88fb74d3803f41, 0xdfefae6f68dc4caa, 0x7a6bb98961fd2272, 0x2b5fcc020b810a6f, 0x733b93d2bac94301, 0x0220007d6e45e371, 0xc962a4619142707f, 0x24dbb2bc543f97e1, 0x661b351ce376d616,
	0x5f48c09aace64ba2, 0x3af8c7b4ec91b16d, 0xfbe4ddf4da39280a, 0x12b4bec1f853aecb, 0x69b265e165bb947b, 0x4d774775d15b7fc6, 0xd545381234b3f79b, 0xa92fe0391c68a1f1, 0xbced31324074f6a7, 0x0d192f2127c93e64, 0x2227c99eeaef3587, 0xf4f72416819f80d8,
	0xe0bf76815662dc14, 0x9bb572367da0f7a6, 0x8faa2454d06d556f, 0xb34ded64b7a49a23, 0xacbb064ecceabf37, 0x8ef7754d846c5ea6, 0xf26e6deb12a1b988, 0xfcf1c2ca6b7e2aa6, 0x5cfa74b82ed3fbb8, 0xddd63a06a5e01a72, 0x96171abc89c5d1c5, 0x282a70db858417af,
	0x47631d9262752014, 0x5e2c475c4a0b4b07, 0x0f269944f17efec7, 0xe4a6cdce1e592e6c, 0x104d13b570e94863, 0x68e735889c5f0ba3, 0x42b797d348d2e68a, 0x1206730c4de024c3, 0x3e4a86b0387ea46a, 0xc3d5341b879df417, 0xae0f4f9ddd27bd6e, 0x9baa1bcf8f9c2ff8,
	0x9d156043f134e466, 0x2186b259e12c27d3, 0x5fb002e3fc0ae7ff, 0x0cda8cf2cd576bf3, 0x4f84c40942dfaf93, 0xc54734770e725db0, 0x4b289a2d3f564afb, 0xab1f08fca9153f73, 0xff93963eb1a480ab, 0x48fdb753dc22b3f3, 0x1fec3ba9b0bcae64, 0xfe121f703843cd0b,
	0x1f69b7ad4852558d, 0x3f21c4b79f30b035, 0x44f45902a569fa37, 0x7da2c6bf8106c926, 0x79e012f98cf6ec89, 0xb40a17471f42cc2b, 0xb53b31941bc8c40c, 0x03232cb4eb44f300, 0x12d49593176e8b51, 0x949da988cf4e6618, 0x2353ac11eb1d5fc1, 0xd597232e5f9f9f79,
	0xaf1c1f9e8ea7a2a5, 0x181fb9236aba5199, 0x497e039db9120c0a, 0xbcfd52bdd97a0681, 0x4195981cb86adf96, 0xf0bd67d541c2b606, 0x72a4828a8ded4080, 0xc4bb76d49e16932b, 0x69ab3509d6a4e5f9, 0xe5ece173280d0896, 0x3727312293d3e358, 0xbcb6fdca8b8eddab,
	0x7a796ac1b50d0e5a, 0x48f69be53f35b292, 0xdde0ea833b6a224c, 0xa74cd55260cf6025, 0x82f94b28945ff6e5, 0xcc232a9e4a23b941, 0x7cf0608db9bbc736, 0x680f61353a548ab4, 0xb948f1bc0817561f, 0xe650b482aa97a0da, 0xee9d14f0b42fa37b, 0x3ccde78230dbc019,
	0xc9616d91377b2f39, 0xee3c2c26947ef1fa, 0xefcd0f2dcbf0c3d9, 0xf4ecbe1eda709b44, 0x0fa256452143a683, 0x6f9fb6d08021a6ac, 0x556046333fb71133, 0x8a02973556d24da7, 0x5073dd515917317c, 0xd48da62c644cf4ee, 0xd86fc33d0abb7471, 0xf4db8a9242a62fa8,
	0x6da1c6127eb49d55, 0x85a09f724f36e217, 0xaa04522776fb1949, 0x2a50bca13d0ddabd, 0x8d10b8bb036b8a79, 0x34d43b3bc23a0d99, 0x237255c0da4a8e41, 0x474b37f0f873d3e3, 0xa5330415e213f13c, 0x4fbface002551b97, 0xe338544e2798f01f, 0xe85a3a04235dd8fa,
	0x6b20390899a054a0, 0x788d686e97693a97, 0xfcd99d379cf86bb2, 0x89144e3426fc4fd2, 0x05651289833fb75d, 0x2e442ef97da4a3a8, 0x0203d9036f6cf850, 0xec45f5ce026d28b7, 0xfb053e99ec3de5d5, 0x0d822eebb150f560, 0xc79c910608f07dd9, 0x32d8af5f3e415384,
	0x0846a3646d1b0ff1, 0xe8ede0ae7a6459df, 0x4031dc8d50b7ad52, 0x846e47378a404799, 0xe7a9a2ddc0773a25, 0x764acdb79e87b4ea, 0xc76bce3154a5b41b, 0x94753ce3cecfad0d, 0x4606c79e9951432c, 0x85ab23f44ad2bf90, 0xecfd313621c23098, 0x5fe4be9cb8855713,
	0xf903d6a7d28efeb5, 0x120e0dac86a3bc4a, 0xc49cc765095bc5ef, 0x7f62d905b1e87c2e, 0x6912fb32dafff917, 0x477f4b9d0f3ed836, 0xbc44277b97c4ff30, 0x1edd94f09c9cb8d9, 0x2d59b9fbb7cf2a39, 0x5f0c9cf58d7b42ef, 0xde2b3c2d4a5db88a, 0x8876c17e33e2bdf1,
	0x704f5f76d79ba618, 0x215a93a64da19789, 0x53844cf3cceea46e, 0xf50d14e72e18add0, 0xb430b847eb87f7e8, 0x40a04bb507086c96, 0xbd89e767d035c0be, 0xc9eefbf0b7748ec8, 0xa7c42d6d98d5b547, 0xdd641a0d917573d2, 0x9e7b2eabe04896d7, 0xa5e2ea35abc9940e,
	0x3d9aa61e794564db, 0x75a739ac70446a91, 0x32a7d26b6d21b3f5, 0xe3333dfeea28f047, 0xb073423592e3613c, 0x06d3be2e938c52cb, 0xb69bc51d9f047430, 0xa93af6a6500c49cd, 0x0ab8ccd2f971f9d4, 0x651e1be7ed6c8438, 0xe913ad1b22b91f52, 0xce794119b6acc6be,
	0x59c857c51b9f4291, 0xcfe7d3681eac870f, 0x38a9ecae540d4a52, 0x6e64e00d4b01c599, 0x4d4ab44254795b62, 0x5b90f4b4e68e7693, 0x43eacd0be83870dc, 0xbaec8122edd06d68, 0x7e6de6ded6e973e6, 0x5f669a26b47ed30c, 0xeb49743ee0c19dd1, 0xf83ce73144ec5dcd,
	0x6371c2b41b3c74f2, 0xdaf6b02b0386f3e8, 0xd62c858a575fa7d4, 0xc7c387fb95dbc92b, 0xfa10497825613ce9, 0xdd9689254a681bfb, 0x2bd3844f52eacf4c, 0xadc5ac3a41e4f814, 0xe70cb1722f4fde24, 0x4f3e1c561d04dc2c, 0x93309e749bdb8863, 0x052875fc521d6ca1,
	0x84310fedeca48ba6, 0xbf5d8ca0bffb0b4d, 0xc9722abce2c5144f, 0xf315596f368ac3f5, 0x141b488d2bc99293, 0x2e1512b2cc950d7a, 0x30a43ed2350dcf6f, 0x2485e166c9dc9dba, 0xc76c6b4b5ad15632, 0x372932222d7535a6, 0xf3cc0459da6ba34a, 0xdd3ce045072bec85,
	0xf92c16f03840124c, 0x1571e9ad67abb604, 0x3361cf3a4f922db8, 0x6f10879bfdec700c, 0x95e278d03a0bcb06, 0xda51301d6cc726a1, 0xc9705f170c798b06, 0x6ec1a06c88cd6be1, 0x3b4b2be903e5490d, 0x456ae1f919f71360, 0xeb137d1dbe0a3b57, 0xb139e47fc691639a,
	0xe17a792a16b98fda, 0xe47176b0655a6fe0, 0x895a9ce7fff620ae, 0x746ccae6d1020911, 0x8677f79a6e353249, 0xfc7ea7c83181f63b, 0x3df1d7254351fccc, 0xae6e0dcb22fd8dab, 0x86a3b8eaf7e4a322, 0x365680dd057395ef, 0xe4bf9c9a7e0b5764, 0xd87e79c2c42b27fa,
	0x1841c9ee57430883, 0xabcf1e2149d3745e, 0xe5fdfeb18c831332, 0xf250bf6935c23ce4, 0x8b0905127795aefd, 0xb2f00d03de8a3f98, 0x30de5342654a797a, 0x2f81657a9e976fa6, 0xdfaeae99c3ecba39, 0xbec26b9b6b0072e5, 0x702c0e7c9e2f1181, 0x159838b4deae9dc6,
	0xb28a9c1fa70bf649, 0xe8cc45d8af7932be, 0x1c176c412e7c5c3f, 0x3cf1b81c9dc65d23, 0xba287cff35a82469, 0x90ac451dbeda61b4, 0x2c6be5d7cc7c9203, 0x5af67f5ee3b8da8f, 0x27318445ad52d099, 0x5cb0c5f53b9ed39d, 0xa6225088dd883f25, 0xf9f6ae555c385be7,
	0x245110d3840ec0b1, 0x20e67eb77a1ef391, 0xec2eab303415d6be, 0x5eaea59c90a46c72, 0xe7bd0f4d9c851a96, 0x7344f9fa398040a3, 0xc9df2d196c3b41e9, 0xbefc834c9722a287, 0xbb38a21281c1ad73, 0x0b04e3b42f46da6b, 0xb64894ab69773d71, 0xa6853cc7054c9122,
	0x2f5d3dd3e507fa94, 0x5d2a4615ef02d55f, 0x01402dc1b0fa14f5, 0x4600b9f4feba7e6b, 0x5e80aea1ca05fb9a, 0x3031a570f67da849, 0xe67ee836625cbbb5, 0xe2224a2f49356638, 0xc2c75eaf229c7a13, 0x5bd1e624f3c528f8, 0x68708d0bb4919f47, 0x9c085134950c8f3c,
	0x6b69e9e9d2e78884, 0xec919b6286d3709d, 0xef8d2465351dbe0d, 0x5d9a8b6ae025c52a, 0xd680f4e517ef0497, 0x6582ca809af97ff7, 0x9d832a056779e233, 0x80775ff61416e660, 0xc34020a9b60e29fa, 0x5456ea1d9ff5b635, 0x44f5bb017751419f, 0x6a7aeaecaf17f2d4,
	0x22d0a401771358ad, 0xa08acca32c2f2ae2, 0x5c9ab3f1f9b6e142, 0x3226016e12750935, 0xe7917826ab96c671, 0xdffedc78126572ef, 0x6353dc6e27ebd7b1, 0x2309cf6e78afa847, 0xa8c8bce1fcfa871f, 0x64154724a28a00e3, 0xef6dc7ec1741bb5a, 0x6c5a78ecf4edf448,
	0xc612e7c8ea44ba19, 0x823b0344c618af9e, 0xc828866f1f3fdb12, 0x8251d7c7cb6f1289, 0xa33dc221a0d9e175, 0xa4cf5afab497d004, 0x19be6b341c17b3f5, 0xaedf49201e8a14f8, 0xf50fa42b4eedd277, 0xc2bc663de5b7c131, 0xc11e41f54a081d80, 0xe536065d32edb002,
	0x75efe35ce2497d4a, 0xf97d09e1b4eb53a5, 0xdc5a8f3bdc8d1e74, 0xf580ac0f2639aae9, 0x30833c29b3c91ad7, 0x348fb4233154d68b, 0xe7ce83720f3cb245, 0xfab69f3f6591845b, 0x60ca72fa583e22d9, 0x0b5fff7b60978d0d, 0xcb4ae9e32ee28d0b, 0x43f310158b76e407,
	0x8299a30fa8b57cc3, 0x267b9f28d4c7b026, 0x558040db512af287, 0x2a1d54baa5573ee0, 0x272ba71f175399fa, 0x1ad8c82c5b368219, 0x217d4b0db135094f, 0xdb66825482cf0aa8, 0xd9500a37cd6e980f, 0x605271cf213187d5, 0x73c54318e28b9fde, 0x27829b844f365bdd,
	0x48badd3a7b922b68, 0x7500f57363855c28, 0x7ade0514c9970b5a, 0x853c8699b6c3cd43, 0xea6ad91a248fb70a, 0x906ab84336b0e483, 0xac4f3b29a16e756f, 0x0a65896ea4eaa14b, 0x057e2ec617261d41, 0x6c23567fc847160b, 0xa0305ed5a27bbe70, 0xb1d4616a363a80a1,
	0x89c24478b9394dc6, 0xd1bfecbee18d12cc, 0x4fd192c05a84b1bb, 0x0fa5ff133754fc8f, 0x8600be0e1c33e0f7, 0x0485ef74c9a35e89, 0x4e6a17cc13b11052, 0x3768d856eae3c91b, 0x512a4530504a1d1b, 0x668e66d3627d5a86, 0xb288b70b91a78cfc, 0xc53b9f24b58a2ae8,
	0x06d0952ba54c01ea, 0x98eaa2629e636c57, 0x4ad007dfb05a9e70, 0x17e97928122af4e9, 0x44ecc4d085aa9517, 0xde3f69f284bba7be, 0x2bd8fbb086f698a2, 0x0b6a5937f993dcb4, 0x362fd3986ab6cc8f, 0x7fd1d79ba9824f2e, 0x0c3a489d1f6e1a37, 0x7a6a7b4e09a14cd5,
	0x788a99c3ab8a3bb3, 0xa341cb0d2ca27c6b, 0x9ad535bcd023551a, 0xf964676169bc8dfe, 0x0158a94f271456b5, 0x65f05f2021eeb26b, 0xed0f9f9e58c9e133, 0x7db66611ea67aecb, 0xa2c9e62ce61002ca, 0x226ed20798fdbe68, 0xa0ddf180ded5d599, 0xdc955321414f472f,
	0x81087309608ed971, 0x18cc171df604a718, 0x93ef0628079503b9, 0xcbe58b69cccdf086, 0x4f90468401ce4554, 0x841bd9a061037e11, 0x1eeaa7f268792624, 0x1ecebece46985a5e, 0x9ac5fc8c0f031697, 0xbde21f548575e2ec, 0xa5950ad83363daee, 0x30270f638c04b14c,
	0x18e2f990da1840fa, 0xd411bbb2391f47f9, 0xba4909c7b73ac2d8, 0x53d0e170e414f6e8, 0xdd0053bc43066d1d, 0xf0afc374a72af956, 0xf836c6168f900321, 0xeb384010251d2b2c, 0x9bf8bb6d9e71681f, 0x16eedc61810b3642, 0xe1e74e5bc202e8b3, 0xe0ef25d43f1872f0,
	0x2c67291c45ae3c78, 0x230c1777ee8a3413, 0x3cbe8198410ed8ec, 0xd79983fa4d0bb8e9, 0x165a6d429115b49d, 0xa0886efc9413859b, 0x6040a7166777dac9, 0x0c7e6a12b71aa5fd, 0x3832d423aca489af, 0x00bfa39ccd332b50, 0xc46e0f77dbf1749f, 0xf9838273e3d8b3d3,
	0x79746fb13850b3ca, 0x674a749627b1766a, 0x9028d531f32acc74, 0xcb0a89c2e58d2ff3, 0x3b041562063e3b57, 0x7fb343908142644a, 0x49a9f60451c5d892, 0x86defc8088565606, 0x033b0c212e64e2d7, 0xc96e23c4f73e9df7, 0x24f5d330eaf5ecbe, 0xb7558185a9148386,
	0x38daf4a493840c20, 0x48e4af25a7a8281f, 0x99fa531a1fe432cc, 0xf895753840589a92, 0xd0dff05cff1ce268, 0xf851f6cca9e587e2, 0x0bc182430db90a2a, 0xd782d1b9f8c3bea0, 0xde63407c8e2b0bba, 0x63280a637d4d539c, 0x77c0d7f9f38ddb24, 0xc5219dac041b4ea3,
	0x7f5be4bbe1dd2f49, 0x0bf698c35025d06f, 0xa8e02358304d27c6, 0xb5881acb41950ea5, 0x77731f1f70ef7df4, 0x7c7aa8232d7e8dad, 0x8ec5124644772a9f, 0x91fed9022a53b228, 0x21a3cdab1bfd0af1, 0xa23639d51999f8ce, 0x7f2b532d996f5e8f, 0x6acbd1b915d0c884,
	0x08f996b75882b839, 0x2a5bff49472edae9, 0xffa5813d5726dd28, 0xe7c98eaa832229b4, 0xf0cc537e247ead2b, 0x2912df720574e1d3, 0x26000b20eb2289c8, 0x37f0ba4e0766dcc9, 0xffd739ee520ca01e, 0x30a387a2cfbc69b6, 0x3bfb49ffa977256f, 0x4dfc56a7f632ba8f,
	0xa6a642f99d56c4b4, 0xa90f88805a6b13fa, 0xafeeae3823e99f46, 0x9299e03e9939960a, 0xc7c73343ec1a1368, 0x02c16b8f189ec5b4, 0xdfb73a8356df491b, 0xddf419cb101dbe2b, 0xa8f1f6039830c36e, 0x55610efcd0169424, 0xfd4a7c8a46768f08, 0x8f43bd97765e4776
};

internal u64 en_passant_hash_table[64] =
{
	0x9d4839a738c6f994, 0x31656ac9e4f2fa76, 0x8a64857a87808ae3, 0xd9afeaeb3ceda693,
	0xe650e61d663e0f4c, 0x3ad3211c7ffe5ea3, 0xa4618cce6e879b10, 0x70434320a38c8e32,
	0xcee63cd469ff7b0d, 0x183eb71a01ad1542, 0x1aca2d44256d3cbe, 0xfa9d105ce54b9c37,
	0x9cc5db7c6068affc, 0xf665104c014a958b, 0xf9b112615510e5f6, 0xeeda44bd96d2b0f3,
	0x3622578e84c91510, 0xb42e2a1296c3671e, 0x18c27249865c2fba, 0x462ff22d788df20f,
	0xd78223d554e7d67f, 0xdf7cfb3a306801e0, 0x36b7772f043dc8f3, 0x56e0b6ad82bf954c,
	0xa3de42c4e3c45c43, 0x2dc2ef82ca76c318, 0x278b443c1ae4bde0, 0xf8f2ea8653bba684,
	0x4f8d785d7e68dc67, 0x85c13632bc9a94fa, 0x432b1a3ffb1900f8, 0xa6d41719104dc595,
	0xbeafc3d2455345c9, 0x76659f248d8b156a, 0x7bf72b78aa8ad01e, 0x0073bf373e73849a,
	0xf4442f2584001dee, 0x03c93053b2f87c3d, 0xf8fec2a831f4e0d7, 0x7a00d6b6eec183fc,
	0xb1d6ac1ff807b900, 0x8ba77978dec44eb6, 0xf03233deb2740913, 0x69735afa57a173b0,
	0x368bd65b345b9b6a, 0xa8981475d2214c14, 0x7ba7ff81b37074cf, 0x0b220cec6ab935fe,
	0x34951cfd5bce6885, 0x0774cdee90966ad1, 0x52f11b9a21ee8473, 0x43a519e97e20474a,
	0x140c402f05a7abd3, 0x53fa8156d91ddc72, 0x77890074dd96b157, 0xf46bc2e797ccf6fc,
	0xd939f84eed5c7b09, 0xfa05f3735cb51ed5, 0xe610078cf857c606, 0x7f635f6cf126960f,
	0xd985692a96365a14, 0xcc04393ff2461b85, 0xfb05228a6f93ff57, 0x13af9a0eb1969ca9
};

internal u64 castling_hash_table[16] =
{
	0x1474e0d253bc91c6, 0x66ef98510ed6599f, 0xbf38f06c77259801, 0x4d522321095af833,
	0x289dff8ba7b2011d, 0x2241fdea83f62eee, 0x98f0be589994a777, 0xb4b93ef8513588f2,
	0x98ffe3a1e790b7db, 0xf513d2350d32ca16, 0x49937c9db333f656, 0x92fc4fba97289ac3,
	0xfcc3b1854b14c43c, 0x7071de8d77dac3c3, 0xd5356dbb618c5735, 0xf706cc3226cdf74c
};

internal u64 side_hash_table[2] =
{
	0x8652ac2f3dbe7310, 0x9ff5109cf8eb0dea
};

u64 position_hash(chess_board *board)
{
	u64 hash = 0;
	for (u32 sq = 0; sq < array_count(board->squares); ++sq)
	{
		u32 type = get_piece_type(board->squares[sq]);
		if (type != NONE)
		{
			u32 col = get_piece_color(board->squares[sq]);
			u32 hash_index = 12 * sq + 2 * type + col;
			u64 value = piece_hash_table[hash_index];
			hash ^= value;
		}
	}

	// #todo should the side to play be part of the hash?
	hash ^= side_hash_table[board->state->side];

	return hash;
}

internal u64 update_zobrist_hash(chess_board *board)
{
	u64 hash = position_hash(board);

	assert(board->state->en_passant < array_count(en_passant_hash_table));
	hash ^= en_passant_hash_table[board->state->en_passant];

	assert(board->state->castling_rights < 16);
	hash ^= castling_hash_table[board->state->castling_rights];

	return hash;
}

void print_board(const chess_board *board)
{
	printf("%u: %u - %s ", board->state->ply / 2, board->state->ply,
		   board->state->side == WHITE ? "w" : "b");
	if (board->state->en_passant != 0)
	{
		print_square(board->state->en_passant);
		printf(" ");
	}
	if (board->state->castling_rights & WHITE_KINGSIDE)  { printf("K"); }
	if (board->state->castling_rights & WHITE_QUEENSIDE) { printf("Q"); }
	if (board->state->castling_rights & BLACK_KINGSIDE)  { printf("k"); }
	if (board->state->castling_rights & BLACK_QUEENSIDE) { printf("q"); }
	printf("\n");
	printf("+---+---+---+---+---+---+---+---+\n");
	for (u32 rank_iter = 0; rank_iter < NUM_RANKS; ++rank_iter)
	{
		u32 rank = NUM_RANKS - rank_iter - 1;
		for (u32 file = 0; file < NUM_FILES; ++file)
		{
			u8 pos = file + NUM_RANKS * rank;
			printf("| %c ", get_piece_char(board->squares[pos]));
		}
		printf("| %u\n", rank + 1);
		printf("+---+---+---+---+---+---+---+---+\n");
	}
	printf("  a   b   c   d   e   f   g   h\n");

	// #todo print fen
	const char *fen = to_fen(board);
	printf("Fen: %s\n", fen);
	buf_free(fen);
	printf("Key: %016lX\n", board->state->hash);
}

void print_bitboard(u64 bitboard)
{
	printf("+---+---+---+---+---+---+---+---+\n");
	for (u32 rank_iter = 0; rank_iter < NUM_RANKS; ++rank_iter)
	{
		u32 rank = NUM_RANKS - rank_iter - 1;
		for (u32 file = 0; file < NUM_FILES; ++file)
		{
			char c = ' ';
			if (bitboard & square_bit(file + rank * NUM_FILES))
			{
				c = 'X';
			}
			printf("| %c ", c);
		}
		printf("| %u\n", rank + 1);
		printf("+---+---+---+---+---+---+---+---+\n");
	}
	printf("  a   b   c   d   e   f   g   h\n");
}

void print_bitboard_small(u64 bitboard)
{
	for (u32 rank_iter = 0; rank_iter < NUM_RANKS; ++rank_iter)
	{
		u32 rank = NUM_RANKS - rank_iter - 1;
		for (u32 file = 0; file < NUM_FILES; ++file)
		{
			char c = ' ';
			if (bitboard & square_bit(file + rank * NUM_FILES))
			{
				c = 'X';
			}
			printf("%c ", c);
		}
		printf("%u\n", rank + 1);
	}
	printf("a b c d e f g h\n");
}

void print_full_bitboard(const chess_board *board)
{
	printf("empty_squares\n");
	print_bitboard(~(board->color_bbs[WHITE] | board->color_bbs[BLACK]));
	printf("white_pieces\n");
	print_bitboard(board->color_bbs[WHITE]);
	printf("black_pieces\n");
	print_bitboard(board->color_bbs[BLACK]);

	printf("pawns\n");
	print_bitboard(board->type_bbs[PAWN]);
	printf("knights\n");
	print_bitboard(board->type_bbs[KNIGHT]);
	printf("bishops\n");
	print_bitboard(board->type_bbs[BISHOP]);
	printf("rooks\n");
	print_bitboard(board->type_bbs[ROOK]);
	printf("queens\n");
	print_bitboard(board->type_bbs[QUEEN]);
	printf("kings\n");
	print_bitboard(board->type_bbs[KING]);
}

void print_compare_bitboards(u64 a, u64 b)
{
	u64 ab = a & b;
	printf("a                b\n");
	for (u32 rank_iter = 0; rank_iter < NUM_RANKS; ++rank_iter)
	{
		u32 rank = NUM_RANKS - rank_iter - 1;
		for (u32 file = 0; file < NUM_FILES; ++file)
		{
			u64 pos_bit = square_bit(file + rank * NUM_FILES);
			if (ab & pos_bit)
			{
				printf("X ");
			}
			else if (a & pos_bit)
			{
				PC_MAGENTA("X ");
			}
			else if (b & pos_bit)
			{
				PC_RED("X ");
			}
			else
			{
				printf("  ");
			}
		}
		printf("%u ", rank + 1);
		for (u32 file = 0; file < NUM_FILES; ++file)
		{
			u64 pos_bit = square_bit(file + rank * NUM_FILES);
			if (ab & pos_bit)
			{
				printf("X ");
			}
			else if (a & pos_bit)
			{
				PC_RED("X ");
			}
			else if (b & pos_bit)
			{
				PC_MAGENTA("X ");
			}
			else
			{
				printf("  ");
			}
		}
		printf("%u\n", rank + 1);
	}
	printf("a b c d e f g h   a b c d e f g h\n");
}

void print_bitboard_with_pos(u64 bitboard, u8 pos)
{
	for (u32 rank_iter = 0; rank_iter < NUM_RANKS; ++rank_iter)
	{
		u32 rank = NUM_RANKS - rank_iter - 1;
		for (u32 file = 0; file < NUM_FILES; ++file)
		{
			if (rank * NUM_FILES + file == pos)
			{
				if ((bitboard & square_bit(file + rank * NUM_FILES)) != 0)
				{
					PC_RED_BOLD("@ ");
				}
				else
				{
					PC_GREEN_BOLD("@ ");
				}
			}
			else
			{
				char c = ' ';
				if (bitboard & square_bit(file + rank * NUM_FILES))
				{
					c = 'X';
				}
				printf("%c ", c);
			}
		}
		printf("%u\n", rank + 1);
	}
	printf("a b c d e f g h\n");
}

void print_bitboard_with_2_pos(u64 bitboard, u8 a, u8 b)
{
	for (u32 rank_iter = 0; rank_iter < NUM_RANKS; ++rank_iter)
	{
		u32 rank = NUM_RANKS - rank_iter - 1;
		for (u32 file = 0; file < NUM_FILES; ++file)
		{
			u8 pos = rank * NUM_FILES + file;
			if (pos == a || pos == b)
			{
				if ((bitboard & square_bit(pos)) != 0)
				{
					PC_RED_BOLD("@ ");
				}
				else
				{
					PC_GREEN_BOLD("@ ");
				}
			}
			else
			{
				char c = ' ';
				if (bitboard & square_bit(pos))
				{
					c = 'X';
				}
				printf("%c ", c);
			}
		}
		printf("%u\n", rank + 1);
	}
	printf("a b c d e f g h\n");
}

