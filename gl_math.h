#ifndef GL_MATH_H
#define GL_MATH_H

#ifdef _WIN32
#include <math.h>
#include <float.h>
#else
#include <math.h>
#include <float.h>
#endif

#include "common.h"

/// ----------------------------------------------------------------------------
///	#section: Constants
/// ----------------------------------------------------------------------------
/// {{{

//! \brief Value of pi
#define PI     3.14159265359f
//! \brief Value of pi/4
#define PI_025 0.78539816339f
//! \brief Value of pi/2
#define PI_05  1.57079632679f
//! \brief Value of 2*pi
#define PI_2   6.28318530718f

#define SQRT2    1.41421356237f
#define SQRT2_05 0.70710678119f

#define F32_TOLERANCE 0.00001f

/// }}}
/// ----------------------------------------------------------------------------
///	#section: Type Declarations
/// ----------------------------------------------------------------------------
/// {{{

/// {{{
typedef union integer_vector2D
{
	s32 e[2];
	struct
	{
		s32 x, y;
	};
	struct
	{
		s32 w, h;
	};
	struct
	{
		s32 u, v;
	};
	struct
	{
		s32 width, height;
	};
	
} ivec2;

typedef union integer_vector3D
{
	s32 e[3];
	struct
	{
		s32 x, y, z;
	};
	struct
	{
		s32 r, g, b;
	};
} ivec3;

typedef union integer_vector4D
{
	s32 e[4];
	struct
	{
		s32 x, y, z, w;
	};
	struct
	{
		s32 r, g, b, a;
	};
	struct
	{
		s32 i, j, k, real;
	};
	struct
	{
		ivec2 xy;
		ivec2 zw;
	};
	struct
	{
		ivec3 rgb;
	};
} ivec4;

/// }}}

typedef union vector2D
{
	f32 e[2];
	struct
	{
		f32 x, y;
	};
	struct
	{
		f32 w, h;
	};
	struct
	{
		f32 u, v;
	};
} vec2;

typedef union vector3D
{
	f32 e[3];
	struct
	{
		f32 x, y, z;
	};
	struct
	{
		f32 r, g, b;
	};
} vec3;

typedef union vector4D
{
	f32 e[4];
	struct
	{
		f32 x, y, z, w;
	};
	struct
	{
		f32 r, g, b, a;
	};
	struct
	{
		f32 i, j, k, real;
	};
	struct
	{
		vec2 xy;
		vec2 zw;
	};
	struct
	{
		vec3 xyz;
	};
	
	struct
	{
		vec3 rgb;
	};
} vec4, color, quaternion;

typedef union matrix4x4
{
	f32 e[16];
	struct
	{
		f32 a11, a12, a13, a14;
		f32 a21, a22, a23, a24;
		f32 a31, a32, a33, a34;
		f32 a41, a42, a43, a44;
	};
} mat4;

typedef struct rectangle
{
	// #note pos is bottom left
	vec2 pos;
	vec2 dim;
} rect;

typedef struct integer_rectangle
{
	// #note pos is bottom left
	ivec2 pos;
	ivec2 dim;
} irect;

typedef struct transform3d
{
	vec3 pos;
	quaternion rot;
	vec3 scale;
} transform3d;

typedef struct tile_frustum
{
	vec3 pos;
	vec3 pos_adjusted;
	vec3 forward;
	f32 distance_sq;
	f32 cos_angle;

	vec2 min, max;
} tile_frustum;

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Scalar Operators
/// ----------------------------------------------------------------------------
/// {{{

bool valid_f32(f32 f);
s16 abss16(s16 value);
s32 abss32(s32 value);
f32 absf32(f32 value);
f32 minf32(f32 a, f32 b);
f32 maxf32(f32 a, f32 b);
f32 to_radians(f32 deg);
f32 sinf32(f32 angle);
f32 cosf32(f32 angle);
f32 tanf32(f32 angle);
f32 asinf32(f32 angle);
f32 acosf32(f32 angle);
f32 atanf32(f32 angle);
f32 sqrtf32(f32 value);
f32 floorf32(f32 value);
f32 ceilf32(f32 value);
f32 roundf32(f32 value);
f32 roundf32_up(f32 value);
f32 roundf32_down(f32 value);
f32 fmodf32(f32 x, f32 y);
f32 clampf32(f32 val, f32 min, f32 max);
f64 sigmoidf64(f64 x);
// #note only works for 0 < x < 1
f64 logitf64(f64 x);
// #note works for all values
f64 safe_logitf64(f64 x);
f64 squaref64(f64 x);
f32 lerp(f32 start, f32 end, f32 t);
f32 smooth_start2(f32 t);
f32 smooth_start3(f32 t);
f32 smooth_start4(f32 t);
f32 smooth_stop2(f32 t);
f32 smooth_stop3(f32 t);
f32 smooth_stop4(f32 t);
f32 smooth_step2(f32 t);
f32 smooth_step3(f32 t);
f32 smooth_step4(f32 t);
f32 smooth_damp2(f32 current, f32 target, f32 *vel, f32 inc);
f32 smooth_damp3(f32 current, f32 target, f32 *vel, f32 inc);
f32 smooth_damp4(f32 current, f32 target, f32 *vel, f32 inc);
f32 smooth_damp(float current, float target, float *vel,
		float smooth_time, float max_speed, float dt);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: 2D Vector Operations
/// ----------------------------------------------------------------------------
/// {{{

ivec2 iv2(s32 x, s32 y);
ivec2 iv2_zero(void);
ivec2 iv2_add(ivec2 u, ivec2 v);
ivec2 iv2_sub(ivec2 u, ivec2 v);
s32   iv2_magnitude_sq(ivec2 v);

vec2 v2(f32 x, f32 y);
vec2 v2_zero(void);
vec2 v2_scale(f32 s, vec2 u);
vec2 v2_add(vec2 u, vec2 v);

/*!
  \brief Subtracts a 2D vector, v, from the 2D vector u.

  \param u vec2: the 2D vector to subtract from
  \param v vec2: the 2D vector to subtract with

  \returns the resultant 2D vector
 */
vec2 v2_sub(vec2 u, vec2 v);
vec2 v2_neg(vec2 u);
f32 v2_magnitude_sq(vec2 v);
f32 v2_magnitude(vec2 v);
vec2 v2_normal(vec2 v);
f32 v2_dot(vec2 u, vec2 v);
vec2 v2_rotate(vec2 u, f32 angle);
vec2 absv2(vec2 u);

vec3 v3_scale_sse(f32 s, vec3 u);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: 3D Vector Operations
/// ----------------------------------------------------------------------------
/// {{{

vec3 v3(f32 x, f32 y, f32 z);
vec3 v3_zero(void);
vec3 v3_scale(f32 s, vec3 u);
vec3 v3_add(vec3 u, vec3 v);
vec3 v3_sub(vec3 u, vec3 v);
vec3 v3_neg(vec3 u);
f32 v3_magnitude_sq(vec3 v);
f32 v3_magnitude(vec3 v);
vec3 v3_normal(vec3 v);
vec3 v3_cross(vec3 u, vec3 v);
f32 v3_dot(vec3 u, vec3 v);
vec3 v3_rotate(vec3 v, quaternion q);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: 4D Vector Operations
/// ----------------------------------------------------------------------------
/// {{{

vec4 v4(f32 x, f32 y, f32 z, f32 w);
vec4 v4_zero(void);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Color Operations
/// ----------------------------------------------------------------------------
/// {{{

color color_from_hex(u32 hex);
color color_from_rgb255(u8 r, u8 g, u8 b, u8 a);
color color_from_cmyk(void);
color color_from_hsv(void);
color color_from_hsl(void);

color color_lerp(color lo, color hi, float t);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Quaternion Operations
/// ----------------------------------------------------------------------------
/// {{{

quaternion q4_identity(void);
quaternion q4(f32 x, f32 y, f32 z, f32 w);
quaternion q4_axis_angle(vec3 axis, f32 angle);
f32 q4_magnitude(quaternion q);
quaternion q4_normal(quaternion q);
quaternion q4_conjugate(quaternion q);
quaternion q4_mul_q4(quaternion a, quaternion b);
quaternion q4_mul_v3(quaternion q, vec3 v);
quaternion q4_look_at(vec3 target, vec3 current, vec3 eye);
vec3 q4_forward(quaternion q);
vec3 q4_back(quaternion q);
vec3 q4_up(quaternion q);
vec3 q4_down(quaternion q);
vec3 q4_right(quaternion q);
vec3 q4_left(quaternion q);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Transform3D Operations
/// ----------------------------------------------------------------------------
/// {{{

transform3d transform_identity(void);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Rectangle Operations
/// ----------------------------------------------------------------------------
/// {{{

rect rect_zero(void);
rect rect_from_center(vec2 center, vec2 dim);
rect rect_top_left(vec2 top_left, vec2 dim);
rect rect_bottom_left(vec2 bottom_left, vec2 dim);

irect irect_identity(void);
irect irect_new(ivec2 pos, ivec2 dim);
ivec2 irect_center(irect r);
bool irect_intersect(irect a, irect b);
bool irect_contains_point(irect r, ivec2 point);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Matrix Operations
/// ----------------------------------------------------------------------------
/// {{{

mat4 m4_zero(void);
mat4 m4_identity(void);
mat4 m4_perspective(f32 fov, f32 aspect_ratio, f32 znear, f32 zfar);
// #note for ortho y+ is up (or down), z is depth
mat4 m4_ortho(f32 left, f32 right, f32 bottom, f32 top, f32 znear, f32 zfar);
vec4 m4_mul_v4(mat4 m, vec4 v);
mat4 m4_scale(vec3 scale);
mat4 m4_translation(vec3 pos);
mat4 m4_rotation(quaternion q);
mat4 m4_from_basis_vectors(vec3 forward, vec3 up, vec3 right);
mat4 m4_from_vector_angle(f32 angle, vec3 v);
mat4 m4_mul_m4(mat4 a, mat4 b);
mat4 m4_look_at(vec3 eye, vec3 tgt, vec3 up);
mat4 m4_mul_f32(mat4 m, f32 f);
f32 m4_determinant(mat4 m);
mat4 m4_adjugate(mat4 m);
mat4 m4_inverse(mat4 m);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Frustum
/// ----------------------------------------------------------------------------
/// {{{

bool frustum_contains_tile(tile_frustum *frustum, ivec2 tile);

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Type Printers
/// ----------------------------------------------------------------------------
/// {{{

void print_v2(vec2* v, const char *name);
void print_v3(vec3* v, const char *name);
void print_v4(vec4* v, const char *name);
void print_m4(mat4* m, const char *name);

/// }}}

#endif // !GL_MATH_H
