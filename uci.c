#include "uci.h"

#include "common.h"
#include "chess_board.h"
#include "search.h"
#include "move_generation.h"
#include "perft.h"

#define MAX_HASH 1024
#define MAX_INPUT_SIZE 400 * 6

// #todo better string parsing, possibly implement a form of lexer.

internal void parse_go(const char *line, chess_board *board, nnue_net *net, search_parameters *sparam,
					   transposition_table *ttable, u32 thread_count)
{
	s32 depth = -1, moves_to_go = 30;
	s64 time = -1, move_time = -1, inc = -1;
	sparam->time_set = false;

	const char *ptr = NULL;

	if ((ptr = strstr(line, "perft")))
	{
		depth = atoi(ptr + 6);
		if (depth > 0)
		{
			// #note freed by the thread
			run_perft_thread_info *tinfo = malloc(sizeof(run_perft_thread_info));
			tinfo->board = board;
			tinfo->thread_count = thread_count;
			tinfo->depth = depth;
			platform_thread_create(run_perft, tinfo);
		}
		return;
	}

	if ((ptr = strstr(line, "infinite")))
	{
		// #note ignore, default is infinite
	}

	if ((ptr = strstr(line, "binc")) && board->state->side == BLACK)
	{
		inc = atoi(ptr + 5);
	}

	if ((ptr = strstr(line, "winc")) && board->state->side == WHITE)
	{
		inc = atoi(ptr + 5);
	}

	if ((ptr = strstr(line, "btime")) && board->state->side == BLACK)
	{
		time = atoi(ptr + 6);
	}

	if ((ptr = strstr(line, "wtime")) && board->state->side == WHITE)
	{
		time = atoi(ptr + 6);
	}

	if ((ptr = strstr(line, "movestogo")))
	{
		moves_to_go = atoi(ptr + 10);
	}

	if ((ptr = strstr(line, "movetime")))
	{
		move_time = atoi(ptr + 9);
	}

	if ((ptr = strstr(line, "depth")))
	{
		depth = atoi(ptr + 6);
	}

	if (move_time > 0)
	{
		time = move_time;
		moves_to_go = 1;
	}

	sparam->start_time = get_time_ns();

	if (depth > 0)
	{
		sparam->depth = depth;
	}
	else
	{
		sparam->depth = MAX_SEARCH_DEPTH;
	}

	if (inc < 0) { inc = 0; }

	if (time > 0)
	{
		sparam->time_set = true;
		time /= moves_to_go;
		time *= ms_to_ns(1);
		if (time > ms_to_ns(50))
		{
			time -= ms_to_ns(50);
		}
		sparam->stop_time = sparam->start_time + time + inc;
	}
	else
	{
		time = 0;
	}

	printf("time: %ld, start: %lu, stop: %lu, depth: %u, time_set: %s\n",
		   time / ms_to_ns(1), sparam->start_time / ms_to_ns(1), sparam->stop_time / ms_to_ns(1),
		   sparam->depth, sparam->time_set ? "true" : "false");

	// #note freed by the thread
	nnue_search_thread_info *tinfo = malloc(sizeof(nnue_search_thread_info));
	tinfo->original_board = board;
	tinfo->sparam = sparam;
	tinfo->ttable = ttable;
	tinfo->num_threads = thread_count;
	tinfo->net = net;
	tinfo->training = false;
	#if 0
	platform_thread_create(basic_search_threaded, tinfo);
	#else
	platform_thread_create(nnue_search_threaded, tinfo);
	#endif
}

internal chess_move parse_move(const char *str, const chess_board *board)
{
	if (str[0] > 'h' || str[0] < 'a') { return 0; }
	if (str[1] > '8' || str[1] < '1') { return 0; }
	if (str[2] > 'h' || str[2] < 'a') { return 0; }
	if (str[3] > '8' || str[3] < '1') { return 0; }

	u8 from = (str[0] - 'a') + (str[1] - '1') * NUM_FILES;
	u8 to = (str[2] - 'a') + (str[3] - '1') * NUM_FILES;

	assert(from < 64);
	assert(to < 64);

	chess_move moves[MAX_LEGAL_MOVES];
	u32 num_moves = generate_all_moves(board, moves);

	for (u32 move_index = 0; move_index < num_moves; ++move_index)
	{
		chess_move move = moves[move_index];
		if (move_from(move) == from && move_to(move) == to)
		{
			if (is_promotion(move))
			{
				chess_piece_type prom_type = promotion_type(move);
				if ((prom_type == KNIGHT && str[4] == 'k') ||
					(prom_type == BISHOP && str[4] == 'b') ||
					(prom_type == ROOK   && str[4] == 'r') ||
					(prom_type == QUEEN  && str[4] == 'q'))
				{
					return move;
				}
				continue;
			}
			return move;
		}
	}

	return 0;
}

internal void parse_position(const char *line, chess_board *board)
{
	usize line_len = strlen(line);
	if (line_len > 12)
	{
		line += 9;
		const char *ptr = NULL;
		if (strncmp(line, "startpos", 8) == 0)
		{
			*board = start_board(board->state);
		}
		else
		{
			ptr = strstr(line, "fen");
			if (ptr == NULL)
			{
				*board = start_board(board->state);
			}
			else
			{
				ptr += 4;
				if (!board_from_fen(board, board->state, ptr))
				{
					*board = start_board(board->state);
				}
			}
		}

		ptr = strstr(line, "moves");
		chess_move move = 0;

		if (ptr)
		{
			ptr += 6;
			while (*ptr)
			{
				move = parse_move(ptr, board);
				if (move)
				{
					apply_move(board, board->state, move);
					board->state->ply = 0;
					while (*ptr && *ptr != ' ') { ++ptr; }
					++ptr;
				}
				else
				{
					break;
				}
			}
		}
	}
}

#define START_FEN "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
#define A_FEN "r3kb1r/3n1pp1/p6p/2pPp2q/Pp2N3/3B2PP/1PQ2P2/R3K2R w KQkq -"
#define FINE_70 "8/k7/3p4/p2P1p2/P2P1P2/8/8/K7 w - -"
#define LCT_1 "r3kb1r/3n1pp1/p6p/2pPp2q/Pp2N3/3B2PP/1PQ2P2/R3K2R w KQkq -"
#define WAC_2 "8/7p/5k2/5p2/p1p2P2/Pr1pPK2/1P1R3P/8 b - -"

#define UCI_FEN WAC_2

void uci(nnue_net *net, u32 num_threads)
{
	setbuf(stdin, NULL);
	setbuf(stdout, NULL);

	printf("id name %s %d.%d.%d\n", NAME, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
	printf("id author %s\n", AUTHOR);
	printf("uciok\n");

	state_info state;
	chess_board board = start_board(&state);

	search_parameters sparam = { 0 };
	transposition_table ttable = trans_table_new(128);

	for (;;)
	{
		fflush(stdout);
		const char *line = get_stdin(MAX_INPUT_SIZE);
		if (line == NULL)
		{
			continue;
		}

		if (line[0] == '\n')
		{
			buf_free(line);
			continue;
		}

		if (strncmp(line, "go", 2) == 0)
		{
			parse_go(line, &board, net, &sparam, &ttable, num_threads);
		}
		else if (strncmp(line, "run\n", 4) == 0)
		{
			board_from_fen(&board, &state, WAC_2);
			parse_go("go depth 17", &board, net, &sparam, &ttable, num_threads);
		}
		else if (strncmp(line, "position", 8) == 0)
		{
			parse_position(line, &board);
		}
		else if (strncmp(line, "quit\n", 5) == 0)
		{
			sparam.quit = true;
		}
		else if (strncmp(line, "isready\n", 8) == 0)
		{
			printf("readyok\n");
		}
		else if (strncmp(line, "d\n", 2) == 0)
		{
			print_board(&board);
		}
		else if (strncmp(line, "ucinewgame\n", 11) == 0)
		{
			trans_table_clear(&ttable);
			board = start_board(&state);
		}
		else if (strncmp(line, "uci\n", 4) == 0)
		{
			printf("id name %s %d.%d.%d\n", NAME, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
			printf("id author %s\n", AUTHOR);
			printf("uciok\n");
		}
		else if (!strncmp(line, "setoption name Hash value ", 26))
		{
			u32 mb = 0;
			sscanf(line,"%*s %*s %*s %*s %u",&mb);
			if(mb < 4) mb = 4;
			if(mb > MAX_HASH) mb = MAX_HASH;
			printf("set hash table size to %u mb\n",mb);
			free(ttable.entries);
			memset(&ttable, 0, sizeof(transposition_table));
			ttable = trans_table_new(mb);
		}
		else if (strncmp(line, "stop\n", 5) == 0)
		{
			sparam.stopped = true;
		}
		else if (strncmp(line, "eval\n", 5) == 0)
		{
			printed_basic_eval(&board);
		}

		buf_free(line);
		if (sparam.quit)
		{
			break;
		}
	}
	sparam.stopped = true;

	free(board.repmap.keys);
	free(board.repmap.vals);
	free(ttable.entries);
}
