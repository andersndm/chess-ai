def print_bitboard(bitboard: int):
	print("+---+---+---+---+---+---+---+---+");
	for rank_iter in range(0, 8):
		rank = 8 - rank_iter - 1
		for file in range(0, 8):
			c = ' '
			if bitboard & (1 << (file + rank * 8)):
				c = 'X'
			print("| {} ".format(c), end="")

		print("| {}".format(rank + 1))
		print("+---+---+---+---+---+---+---+---+");

	print("  a   b   c   d   e   f   g   h");

files = [ "a", "b", "c", "d", "e", "f", "g", "h" ]
ranks = [ "1", "2", "3", "4", "5", "6", "7", "8" ]
pieces = [ "n", "b", "r", "q" ]

def print_move(move: int):
	result = ""
	f = move & 0x3F
	t = (move & 0xFC0) >> 6
	castling = (move & 0xC000) == 0xC000
	if castling:
		if t > f:
			result = "O-O"
		else:
			result = "O-O-O"
	else:
		result = "{}{}{}{}".format(files[f % 8], ranks[f // 8], files[t % 8], ranks[t // 8])
	promotion = (move & 0xC000) == 0x4000
	if promotion:
		result += "->{}".format(pieces[(move & 0x3000) >> 12])

	result += " {}-{} {:04x}".format(f, t, move)
	return result