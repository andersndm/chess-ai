#include "gl_math.h"

#include <immintrin.h>

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Scalar Operators
/// ----------------------------------------------------------------------------
/// {{{

bool valid_f32(f32 f)
{
	return !isnan(f) && !isinf(f);
}

s16 abss16(s16 value)
{
	if (value < 0)
	{
		return -value;
	}
	return value;
}

s32 abss32(s32 value)
{
	if(value < 0)
	{
		return -value;
	}
	return value;
}

f32 absf32(f32 value)
{
	if(value < 0.0f)
	{
		return -value;
	}
	return value;
}

f32 minf32(f32 a, f32 b)
{
	if (a > b)
	{
		return b;
	}
	return a;
}

f32 maxf32(f32 a, f32 b)
{
	if (a < b)
	{
		return b;
	}
	return a;
}

f32 to_radians(f32 deg)
{
	return deg * PI / 180.0f;
}

f32 sinf32(f32 angle)
{
#ifdef _WIN32
	return sinf(angle);
#else
	return sin(angle);
#endif
}

f32 cosf32(f32 angle)
{
#ifdef _WIN32
	return cosf(angle);
#else
	return cos(angle);
#endif
}

f32 tanf32(f32 angle)
{
#ifdef _WIN32
	return tanf(angle);
#else
	return tan(angle);
#endif
}

f32 asinf32(f32 angle)
{
#ifdef _WIN32
	return asinf(angle);
#else
	return asin(angle);
#endif
}

f32 acosf32(f32 angle)
{
#ifdef _WIN32
	return acosf(angle);
#else
	return acos(angle);
#endif
}

f32 atanf32(f32 angle)
{
#ifdef _WIN32
	return atanf(angle);
#else
	return atan(angle);
#endif
}

f32 sqrtf32(f32 value)
{
#ifdef _WIN32
	return sqrtf(value);
#else
	return sqrt(value);
#endif
}

f32 floorf32(f32 value)
{
	f32 result = 0.0f;
	if(value < 0.0f)
	{
		result = ceilf(value);
	}
	else
	{
		result = floorf(value);
	}
	return result;
}

f32 ceilf32(f32 value)
{
	f32 result = 0.0f;
	if(value < 0.0f)
	{
		result = floorf(value);
	}
	else
	{
		result = ceilf(value);
	}
	return result;
}

f32 roundf32(f32 value)
{
	return roundf(value);
}

f32 roundf32_up(f32 value)
{
	return floorf(value + 0.5f);
}

f32 roundf32_down(f32 value)
{
	return ceilf(value - 0.5f);
}

f32 fmodf32(f32 x, f32 y)
{
	f32 result = fmodf(x, y);
	return result;
}

f32 clampf32(f32 val, f32 min, f32 max)
{
	if (val < min) { return min; }
	if (val > max) { return max; }
	return val;
}

f64 sigmoidf64(f64 x)
{
	return 1.0 / (1.0 + exp(-x));
}

f64 logitf64(f64 x)
{
	return log(x / (1.0 - x));
}

f64 safe_logitf64(f64 x)
{
	f64 epsilon = 1e-10;
	if (x >= 1.0)
	{
		return logitf64(1.0 - epsilon);
	}
	else if (x == 0.0)
	{
		return logitf64(epsilon);
	}
	else if (x < 0.0)
	{
		if (x < -1.0)
		{
			return -logitf64(-1.0 + epsilon);
		}
		else
		{
			return -logitf64(-x);
		}
	}
	else
	{
		return logitf64(x);
	}
}

f64 squaref64(f64 x)
{
	return x * x;
}

f32 lerp(f32 start, f32 end, f32 t)
{
	f32 result = start + t * (end - start);
	return result;
}

f32 smooth_start2(f32 t)
{
	// NOTE: Ease in quadratic
	f32 result = t * t;
	return result;
}

f32 smooth_start3(f32 t)
{
	// NOTE: Ease in cubic
	f32 result = t * t * t;
	return result;
}

f32 smooth_start4(f32 t)
{
	// NOTE: Ease in quartic
	f32 result = t * t * t * t;
	return result;
}

f32 smooth_stop2(f32 t)
{
	// NOTE: Ease out quadratic
	f32 result = 1.0f - t;
	result = result * result;
	result = 1.0f - result;
	return result;
}

f32 smooth_stop3(f32 t)
{
	// NOTE: Ease out quadratic
	f32 result = 1.0f - t;
	result = result * result * result;
	result = 1.0f - result;
	return result;
}

f32 smooth_stop4(f32 t)
{
	// NOTE: Ease out quadratic
	f32 result = 1.0f - t;
	result = result * result * result * result;
	result = 1.0f - result;
	return result;
}

f32 smooth_step2(f32 t)
{
	//f32 result = lerp(smooth_start2(t), smooth_stop2(t), t);
	f32 result = 3 * t * t - 2 * t * t * t;
	return result;
#if 0
	if (t < 0.5f)
	{
		return smooth_start2(t);
	}
	else
	{
		return smooth_stop2(t);
	}
#endif
}

f32 smooth_step3(f32 t)
{
	if (t < 0.5f)
	{
		return smooth_start3(t);
	}
	else
	{
		return smooth_stop3(t);
	}
}

f32 smooth_step4(f32 t)
{
	if (t < 0.5f)
	{
		return smooth_start4(t);
	}
	else
	{
		return smooth_stop4(t);
	}
}

f32 smooth_damp2(f32 current, f32 target, f32 *vel, f32 inc)
{
	*vel += inc;
	if (*vel > 1.0f)
	{
		*vel = 1.0f;
	}
	f32 t = smooth_step2(*vel);
	f32 result = lerp(current, target, t);
	result = t;
	return result;
}

f32 smooth_damp3(f32 current, f32 target, f32 *vel, f32 inc)
{
	*vel += inc;
	f32 t = smooth_step3(*vel);
	f32 result = lerp(current, target, t);
	return result;
}

f32 smooth_damp4(f32 current, f32 target, f32 *vel, f32 inc)
{
	*vel += inc;
	f32 t = smooth_step4(*vel);
	f32 result = lerp(current, target, t);
	return result;
}

f32 smooth_damp(float current, float target, float *vel,
		float smooth_time, float max_speed, float dt)
{
	smooth_time = maxf32(0.0001f, smooth_time);
	f32 num = 2.0f / smooth_time;
	f32 num2 = num * dt;
	f32 num3 = 1.0f / (1.0f + num2 + 0.48f * num2 * num2 +
			0.235f * num2 * num2 * num2);
	f32 num4 = current - target;
	f32 num5 = target;
	f32 num6 = max_speed * smooth_time;
	num4 = clampf32(num4, -num6, num6);
	target = current - num4;
	f32 num7 = (*vel + num * num4) * dt;
	*vel = (*vel - num * num7) * num3;
	f32 num8 = target + (num4 + num7) * num3;
	if (num5 - current > 0.0f && num8 > num5)
	{
		num8 = num5;
		*vel = (num8 - num5) / dt;
	}
	return num8;
}

/// }}}
/// ----------------------------------------------------------------------------
/// #section: 2D Vector Operations
/// ----------------------------------------------------------------------------
/// {{{

ivec2 iv2(s32 x, s32 y)
{
	return (ivec2){ x, y };
}

ivec2 iv2_zero(void)
{
	return (ivec2){ 0, 0 };
}

ivec2 iv2_add(ivec2 u, ivec2 v)
{
	ivec2 result;
	result.x = u.x + v.x;
	result.y = u.y + v.y;
	return result;
}

ivec2 iv2_sub(ivec2 u, ivec2 v)
{
	ivec2 result;
	result.x = u.x - v.x;
	result.y = u.y - v.y;
	return result;
}

s32 iv2_magnitude_sq(ivec2 v)
{
	return v.x * v.x + v.y * v.y;
}

vec2 v2(f32 x, f32 y)
{
	vec2 result;
	result.x = x; result.y = y;
	return result;
}

vec2 v2_zero(void)
{
	return v2(0.0f, 0.0f);
}

vec2 v2_scale(f32 s, vec2 u)
{
	return v2(s * u.x, s * u.y);
}

vec2 v2_add(vec2 u, vec2 v)
{
	return v2(u.x + v.x, u.y + v.y);
}

vec2 v2_sub(vec2 u, vec2 v)
{
	return v2(u.x - v.x, u.y - v.y);
}

vec2 v2_neg(vec2 u)
{
	return v2(-u.x, -u.y);
}

f32 v2_magnitude_sq(vec2 v)
{
	return v.x * v.x + v.y * v.y;
}

f32 v2_magnitude(vec2 v)
{
	return sqrtf32(v2_magnitude_sq(v));
}

vec2 v2_normal(vec2 v)
{
	f32 mag = v2_magnitude(v);
	vec2 result = v2_zero();
	if(mag == 0.0f)
	{
		return result;
	}

	result.x = v.x / mag;
	result.y = v.y / mag;

	return result;
}

f32 v2_dot(vec2 u, vec2 v)
{
	return u.x * v.x + u.y * v.y;
}

vec2 v2_rotate(vec2 u, f32 angle)
{
	return v2(
		u.x * cosf(angle) - u.y * sinf(angle),
		u.x * sinf(angle) + u.y * cosf(angle)
	);
}

vec2 absv2(vec2 u)
{
	return v2(absf32(u.x), absf32(u.y));
}

/// }}}
/// ----------------------------------------------------------------------------
/// #section: 3D Vector Operations
/// ----------------------------------------------------------------------------
/// {{{

vec3 v3(f32 x, f32 y, f32 z)
{
	vec3 result;
	result.x = x; result.y = y; result.z = z;
	return result;
}

vec3 v3_zero(void)
{
	return v3(0.0f, 0.0f, 0.0f);
}

vec3 v3_scale(f32 s, vec3 u)
{
	return v3(s * u.x, s * u.y, s * u.z);
}

vec3 v3_scale_sse(f32 s, vec3 u)
{
	__m128 vec = _mm_set_ps(u.x, u.y, u.z, 0.0f);
	__m128 scale = _mm_set1_ps(s);
	__m128 result = _mm_mul_ps(scale, vec);
	float temp[4];
	_mm_store_ps(temp, result);
	u.x = temp[0]; u.y = temp[1]; u.z = temp[2];
	return u;
}

vec3 v3_add(vec3 u, vec3 v)
{
	return v3(u.x + v.x, u.y + v.y, u.z + v.z);
}

vec3 v3_sub(vec3 u, vec3 v)
{
	return v3(u.x - v.x, u.y - v.y, u.z - v.z);
}

vec3 v3_neg(vec3 u)
{
	return v3(-u.x, -u.y, -u.z);
}

f32 v3_magnitude_sq(vec3 v)
{
	return v.x * v.x + v.y * v.y + v.z * v.z;
}

f32 v3_magnitude(vec3 v)
{
	return sqrtf32(v3_magnitude_sq(v));
}

vec3 v3_normal(vec3 v)
{
	f32 mag = v3_magnitude(v);
	vec3 result = v3_zero();
	if(mag == 0.0f)
	{
		return result;
	}

	result.x = v.x / mag;
	result.y = v.y / mag;
	result.z = v.z / mag;

	return result;
}

vec3 v3_cross(vec3 u, vec3 v)
{
	vec3 result = v3_zero();

	result.x = u.y * v.z - u.z * v.y;
	result.y = u.z * v.x - u.x * v.z;
	result.z = u.x * v.y - u.y * v.x;

	return result;
}

f32 v3_dot(vec3 u, vec3 v)
{
	return u.x * v.x + u.y * v.y + u.z * v.z;
}

vec3 v3_rotate(vec3 v, quaternion q)
{
	quaternion conjugate = q4_conjugate(q);
	quaternion w = q4_mul_q4(q4_mul_v3(q, v), conjugate);
	return v3(w.x, w.y, w.z);
}

/// }}}
/// ----------------------------------------------------------------------------
/// #section: 4D Vector Operations
/// ----------------------------------------------------------------------------
/// {{{

vec4 v4(f32 x, f32 y, f32 z, f32 w)
{
	vec4 result;
	result.x = x; result.y = y; result.z = z; result.w = w;
	return result;
}

vec4 v4_zero(void)
{
	return v4(0.0f, 0.0f, 0.0f, 0.0f);
}

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Color Operations
/// ----------------------------------------------------------------------------
/// {{{

color color_from_hex(u32 hex)
{
	color result;
	result.r = (f32)((hex & 0xFF000000) >> 24) / (f32)(0xFF);
	result.g = (f32)((hex & 0x00FF0000) >> 16) / (f32)(0xFF);
	result.b = (f32)((hex & 0x0000FF00) >> 8)  / (f32)(0xFF);
	result.a = (f32)((hex & 0x000000FF))       / (f32)(0xFF);
	return result;
}

color color_from_rgb255(u8 r, u8 g, u8 b, u8 a)
{
	color result;
	result.r = (f32)r / 255.0f;
	result.g = (f32)g / 255.0f;
	result.b = (f32)b / 255.0f;
	result.a = (f32)a / 255.0f;
	return result;
}

color color_from_cmyk(void)
{
	color result = v4_zero();
	unimplemented;
	return result;
}
color color_from_hsv(void)
{
	color result = v4_zero();
	unimplemented;
	return result;
}
color color_from_hsl(void)
{
	color result = v4_zero();
	unimplemented;
	return result;
}

// #todo color transformations
color color_lerp(color lo, color hi, float t)
{
	return v4(lerp(lo.r, hi.r, t),
			  lerp(lo.g, hi.g, t),
			  lerp(lo.b, hi.b, t),
			  lerp(lo.a, hi.a, t));
}

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Quaternion Operations
/// ----------------------------------------------------------------------------
/// {{{

quaternion q4_identity(void)
{
	return v4(0.0f, 0.0f, 0.0f, 1.0f);
}

quaternion q4(f32 x, f32 y, f32 z, f32 w)
{
	return v4(x, y, z, w);
}

quaternion q4_axis_angle(vec3 axis, f32 angle)
{
	f32 half_angle = 0.5f * angle;
	f32 sin_half_angle = sinf32(half_angle);
	f32 cos_half_angle = cosf32(half_angle);

	quaternion result;
	result.x = axis.x * sin_half_angle;
	result.y = axis.y * sin_half_angle;
	result.z = axis.z * sin_half_angle;
	result.w = cos_half_angle;
	return result;
}

f32 q4_magnitude(quaternion q)
{
	return sqrtf32(q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w);
}

quaternion q4_normal(quaternion q)
{
	f32 magnitude = q4_magnitude(q);
	q.x /= magnitude;
	q.y /= magnitude;
	q.z /= magnitude;
	q.w /= magnitude;
	return q;
}

quaternion q4_conjugate(quaternion q)
{
	return q4(-q.x, -q.y, -q.z, q.w);
}

quaternion q4_mul_q4(quaternion a, quaternion b)
{
	f32 new_w = a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z;
	f32 new_x = a.x * b.w + a.w * b.x + a.y * b.z - a.z * b.y;
	f32 new_y = a.y * b.w + a.w * b.y + a.z * b.x - a.x * b.z;
	f32 new_z = a.z * b.w + a.w * b.z + a.x * b.y - a.y * b.x;
	return q4(new_x, new_y, new_z, new_w);
}

quaternion q4_mul_v3(quaternion q, vec3 v)
{
	f32 new_x = q.w * v.x + q.y * v.z - q.z * v.y;
	f32 new_y = q.w * v.y + q.z * v.x - q.x * v.z;
	f32 new_z = q.w * v.z + q.x * v.y - q.y * v.x;
	f32 new_w = -q.x * v.x - q.y * v.y - q.z * v.z;
	return q4(new_x, new_y, new_z, new_w);
}

// #note current means current target, not 
// #todo if up is ever not +y add it back in as a parameter
quaternion q4_look_at(vec3 target, vec3 current, vec3 eye)
{
	// #todo for particles
	#if 1
	return q4_identity();
	#else
	vec3 up = { 0.0f, 1.0f, 0.0f };
	// turn into unit vectors
	vec3 n1 = v3_normal(v3_sub(current, eye)); // to_current
	vec3 n2 = v3_normal(v3_sub(target, eye));  // to_target

	f32 d = v3_dot(n1, n2);
	f32 epsilon = 0.9999998f; // #note precision needed for fidelity, specifically negative case
	// vectors are essentially inline
	if (d > epsilon) { return q4_identity(); }
	// vectors are so close to opposite that they need some perturbation
	#if 1
	if (d < -epsilon) { return q4_axis_angle(up, PI); }
	#else
	if (d < -epsilon) { return q4_axis_angle(up, PI); }
	#endif
	vec3 axis = v3_normal(v3_cross(n1, n2));
	quaternion to_target = q4_normal(q4_axis_angle(v3(axis.x, axis.y, axis.z), -acos(d)));

	// need to twist around the pointing direction so that up is as close to up as possible
	#if 1
	vec3 q_up = q4_up(to_target);
	print_v3(&q_up, "q_up");
	vec3 up_proj = v3_normal(v3_sub(up, v3_scale(v3_dot(up, n2), n2)));
	print_v3(&up_proj, "up_proj");
	d = v3_dot(q_up, up_proj);
	printf("d: %f, acos(d): %f\n", d, acos(d));
	//quaternion twist = q4_axis_angle(v3(n2.x, n2.y, n2.z), -acos(d));
	quaternion twist = q4_axis_angle(n2, acos(d));
	quaternion rot = q4_mul_q4(to_target, twist);
	vec3 test_twist = v3_rotate(q_up, twist);
	vec3 test_full = v3_rotate(v3(0, 0, 1), rot);
	return rot;
	#else
	return to_target;
	#endif
	#endif
}

vec3 q4_forward(quaternion q)
{
	return v3_rotate(v3(0.0f, 0.0f, 1.0f), q);
}

vec3 q4_back(quaternion q)
{
	return v3_rotate(v3(0.0f, 0.0f, -1.0f), q);
}

vec3 q4_up(quaternion q)
{
	return v3_rotate(v3(0.0f, 1.0f, 0.0f), q);
}

vec3 q4_down(quaternion q)
{
	return v3_rotate(v3(0.0f, -1.0f, 0.0f), q);
}

vec3 q4_right(quaternion q)
{
	return v3_rotate(v3(1.0f, 0.0f, 0.0f), q);
}

vec3 q4_left(quaternion q)
{
	return v3_rotate(v3(-1.0f, 0.0f, 0.0f), q);
}

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Transform3D Operations
/// ----------------------------------------------------------------------------
/// {{{

transform3d transform_identity(void)
{
	transform3d result;
	result.pos = v3_zero();
	result.rot = q4_identity();
	result.scale = v3(1.0f, 1.0f, 1.0f);
	return result;
}

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Rectangle Operations
/// ----------------------------------------------------------------------------
/// {{{

// #todo add different initializers
rect rect_zero(void)
{
	return (rect){ v2_zero(), v2_zero() };
}

rect rect_from_center(vec2 center, vec2 dim)
{
	return (rect){ v2(center.x - dim.x / 2, center.y + dim.y / 2), dim };
}

rect rect_top_left(vec2 top_left, vec2 dim)
{
	return (rect){ top_left, dim };
}

rect rect_bottom_left(vec2 bottom_left, vec2 dim)
{
	return (rect){ v2(bottom_left.x, bottom_left.y + dim.y), dim };
}

irect irect_identity(void)
{
	return (irect){ iv2_zero(), iv2(1, 1) };
}

irect irect_new(ivec2 pos, ivec2 dim)
{
	return (irect){ pos, dim };
}

ivec2 irect_center(irect r)
{
	return iv2(r.pos.x + r.dim.x / 2, r.pos.y + r.dim.y / 2);
}

bool irect_intersect(irect a, irect b)
{
	bool x_intersect = (a.pos.x <= b.pos.x + b.dim.x) && (a.pos.x + a.dim.x >= b.pos.x);
	bool y_intersect = (a.pos.y <= b.pos.y + b.dim.y) && (a.pos.y + a.dim.y >= b.pos.y);
	return x_intersect && y_intersect;
}

bool irect_contains_point(irect r, ivec2 point)
{
	bool x_intersect = (point.x >= r.pos.x) && (point.x < r.pos.x + r.dim.w);
	bool y_intersect = (point.y >= r.pos.y) && (point.y < r.pos.y + r.dim.h);
	return x_intersect && y_intersect;
}

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Matrix Operations
/// ----------------------------------------------------------------------------
/// {{{

mat4 m4_zero(void)
{
	mat4 result =
	{
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f
	};
	return result;
}

mat4 m4_identity(void)
{
	mat4 result = m4_zero();
	result.a11 = 1.0f;
	result.a22 = 1.0f;
	result.a33 = 1.0f;
	result.a44 = 1.0f;

	return result;
}

mat4 m4_perspective(f32 fov, f32 aspect_ratio, f32 znear, f32 zfar)
{
	mat4 result = m4_zero();
	f32 tfov = tanf32(fov * 0.5f);

	result.a11 = 1.0f / (tfov * aspect_ratio);
	result.a22 = 1.0f / tfov;
	#if 1
	result.a33 = (zfar / (znear - zfar));
	result.a34 = -1.0f;
	result.a43 = -((2.0f * zfar * znear) / (zfar - znear));
	#else
	result.a33 = (-znear - zfar) / (znear - zfar);
	result.a34 = (2.0f * zfar * znear) / (znear - zfar);
	result.a43 = 1.0f;
	#endif

	return result;
}

mat4 m4_ortho(f32 left, f32 right, f32 bottom, f32 top, f32 znear, f32 zfar)
{
	mat4 result = m4_identity();

	result.a11 = 2.0f / (right - left);
	result.a22 = 2.0f / (top - bottom);
	result.a33 = -2.0f / (zfar - znear);
	result.a41 = -(right + left) / (right - left);
	result.a42 = -(top + bottom) / (top - bottom);
	result.a43 = -(zfar + znear) / (zfar - znear);

	return result;
}

vec4 m4_mul_v4(mat4 m, vec4 v)
{
	vec4 result = v4_zero();

	result.x = m.a11 * v.x + m.a12 * v.y + m.a13 * v.z + m.a14 * v.w;
	result.y = m.a21 * v.x + m.a22 * v.y + m.a23 * v.z + m.a24 * v.w;
	result.z = m.a31 * v.x + m.a32 * v.y + m.a33 * v.z + m.a34 * v.w;
	result.w = m.a41 * v.x + m.a42 * v.y + m.a43 * v.z + m.a44 * v.w;

	return result;
}

mat4 m4_scale(vec3 scale)
{
	mat4 result = m4_zero();

	result.a11 = scale.x;
	result.a22 = scale.y;
	result.a33 = scale.z;
	result.a44 = 1.0f;

	return result;
}

mat4 m4_translation(vec3 pos)
{
	mat4 result = m4_identity();

	result.a41 = pos.x;
	result.a42 = pos.y;
	result.a43 = pos.z;

	return result;
}

mat4 m4_rotation(quaternion q)
{
	vec3 forward = v3(2.0f * (q.x * q.z - q.w * q.y),        2.0f * (q.y * q.z + q.w * q.x),        1.0f - 2.0f * (q.x * q.x + q.y * q.y));
	vec3 up =      v3(2.0f * (q.x * q.y + q.w * q.z),        1.0f - 2.0f * (q.x * q.x + q.z * q.z), 2.0f * (q.y * q.z - q.w * q.x));
	vec3 right =   v3(1.0f - 2.0f * (q.y * q.y + q.z * q.z), 2.0f * (q.x * q.y - q.w * q.z),        2.0f * (q.x * q.z + q.w * q.y));
	return m4_from_basis_vectors(forward, up, right);
}

mat4 m4_from_basis_vectors(vec3 forward, vec3 up, vec3 right)
{
	vec3 f = v3_normal(forward);
	vec3 u = v3_normal(up);
	vec3 r = v3_normal(right);

	mat4 result;
	result.a11 = r.x;  result.a12 = r.y;  result.a13 = r.z;  result.a14 = 0.0f;
	result.a21 = u.x;  result.a22 = u.y;  result.a23 = u.z;  result.a24 = 0.0f;
	result.a31 = f.x;  result.a32 = f.y;  result.a33 = f.z;  result.a34 = 0.0f;
	result.a41 = 0.0f; result.a42 = 0.0f; result.a43 = 0.0f; result.a44 = 1.0f;
	return result;
}

mat4 m4_from_vector_angle(f32 angle, vec3 v)
{
	mat4 result = m4_identity();

	f32 cosa = cosf32(angle);
	f32 sina = sinf32(angle);
	f32 icos = 1 - cosa;

	result.a11 =        cosa + v.x * v.x * icos;
	result.a12 = -v.z * sina + v.x * v.y * icos;
	result.a13 =  v.y * sina + v.x * v.z * icos;

	result.a21 =  v.z * sina + v.y * v.x * icos;
	result.a22 =        cosa + v.y * v.y * icos;
	result.a23 = -v.x * sina + v.y * v.z * icos;

	result.a31 = -v.y * sina + v.z * v.x * icos;
	result.a32 =  v.x * sina + v.z * v.y * icos;
	result.a33 =        cosa + v.z * v.z * icos;

	return result;
}

mat4 m4_mul_m4(mat4 a, mat4 b)
{
	mat4 result = m4_zero();

	// #todo optimization, simd?
	result.a11 = b.a11*a.a11 + b.a12*a.a21 + b.a13*a.a31 + b.a14*a.a41;
	result.a12 = b.a11*a.a12 + b.a12*a.a22 + b.a13*a.a32 + b.a14*a.a42;
	result.a13 = b.a11*a.a13 + b.a12*a.a23 + b.a13*a.a33 + b.a14*a.a43;
	result.a14 = b.a11*a.a14 + b.a12*a.a24 + b.a13*a.a34 + b.a14*a.a44;

	result.a21 = b.a21*a.a11 + b.a22*a.a21 + b.a23*a.a31 + b.a24*a.a41;
	result.a22 = b.a21*a.a12 + b.a22*a.a22 + b.a23*a.a32 + b.a24*a.a42;
	result.a23 = b.a21*a.a13 + b.a22*a.a23 + b.a23*a.a33 + b.a24*a.a43;
	result.a24 = b.a21*a.a14 + b.a22*a.a24 + b.a23*a.a34 + b.a24*a.a44;

	result.a31 = b.a31*a.a11 + b.a32*a.a21 + b.a33*a.a31 + b.a34*a.a41;
	result.a32 = b.a31*a.a12 + b.a32*a.a22 + b.a33*a.a32 + b.a34*a.a42;
	result.a33 = b.a31*a.a13 + b.a32*a.a23 + b.a33*a.a33 + b.a34*a.a43;
	result.a34 = b.a31*a.a14 + b.a32*a.a24 + b.a33*a.a34 + b.a34*a.a44;

	result.a41 = b.a41*a.a11 + b.a42*a.a21 + b.a43*a.a31 + b.a44*a.a41;
	result.a42 = b.a41*a.a12 + b.a42*a.a22 + b.a43*a.a32 + b.a44*a.a42;
	result.a43 = b.a41*a.a13 + b.a42*a.a23 + b.a43*a.a33 + b.a44*a.a43;
	result.a44 = b.a41*a.a14 + b.a42*a.a24 + b.a43*a.a34 + b.a44*a.a44;

	return result;
}

mat4 m4_look_at(vec3 eye_position, vec3 target_position, vec3 up_vector)
{
	vec3 forward = v3_normal(v3_sub(target_position, eye_position));
	vec3 side = v3_normal(v3_cross(forward, up_vector));
	vec3 up = v3_cross(side, forward);

	mat4 result = m4_identity();
	// almost works, but the axes, at least x and z are inverted, i.e. the meshes end up inside out
	#if 0
	result.a11 = s.x;
	result.a21 = s.y;
	result.a31 = s.z;
	result.a12 = u.x;
	result.a22 = u.y;
	result.a32 = u.z;
	result.a13 = -f.x;
	result.a23 = -f.y;
	result.a33 = -f.z;
	result.a41 = -v3_dot(s, eye_position);
	result.a42 = -v3_dot(u, eye_position);
	result.a43 = v3_dot(f, eye_position);
	#endif
	result = m4_from_basis_vectors(forward, up, side);

	return result;
}

mat4 m4_mul_f32(mat4 m, f32 f)
{
	// #todo optimization, simd?
	m.a11 *= f;
	m.a12 *= f;
	m.a13 *= f;
	m.a14 *= f;

	m.a21 *= f;
	m.a22 *= f;
	m.a23 *= f;
	m.a24 *= f;

	m.a31 *= f;
	m.a32 *= f;
	m.a33 *= f;
	m.a34 *= f;

	m.a41 *= f;
	m.a42 *= f;
	m.a43 *= f;
	m.a44 *= f;

	return m;
}

f32 m4_determinant(mat4 m)
{
	// #todo ripe for simd
	f32 a12_23_34 = m.a12 * m.a23 * m.a34;
	f32 a12_24_33 = m.a12 * m.a24 * m.a33;
	f32 a12_23_44 = m.a12 * m.a23 * m.a44;
	f32 a12_24_43 = m.a12 * m.a24 * m.a43;
	f32 a12_33_44 = m.a12 * m.a33 * m.a44;
	f32 a12_34_43 = m.a12 * m.a34 * m.a43;

	f32 a13_22_34 = m.a13 * m.a22 * m.a34;
	f32 a13_22_44 = m.a13 * m.a22 * m.a44;
	f32 a13_24_32 = m.a13 * m.a24 * m.a32;
	f32 a13_24_42 = m.a13 * m.a24 * m.a42;
	f32 a13_32_44 = m.a13 * m.a32 * m.a44;
	f32 a13_34_42 = m.a13 * m.a34 * m.a42;

	f32 a14_22_33 = m.a14 * m.a22 * m.a33;
	f32 a14_23_32 = m.a14 * m.a23 * m.a32;
	f32 a14_22_43 = m.a14 * m.a22 * m.a43;
	f32 a14_23_42 = m.a14 * m.a23 * m.a42;
	f32 a14_32_43 = m.a14 * m.a32 * m.a43;
	f32 a14_33_42 = m.a14 * m.a33 * m.a42;

	f32 a22_33_44 = m.a22 * m.a33 * m.a44;
	f32 a22_34_43 = m.a22 * m.a34 * m.a43;

	f32 a23_32_44 = m.a23 * m.a32 * m.a44;
	f32 a23_34_42 = m.a23 * m.a34 * m.a42;

	f32 a24_32_43 = m.a24 * m.a32 * m.a43;
	f32 a24_33_42 = m.a24 * m.a33 * m.a42;

	f32 result;
	result  = m.a11 * (a22_33_44 + a23_34_42 + a24_32_43 - a24_33_42 - a23_32_44 - a22_34_43);
	result -= m.a21 * (a12_33_44 + a13_34_42 + a14_32_43 - a14_33_42 - a13_32_44 - a12_34_43);
	result += m.a31 * (a12_23_44 + a13_24_42 + a14_22_43 - a14_23_42 - a13_22_44 - a12_24_43);
	result -= m.a41 * (a12_23_34 + a13_24_32 + a14_22_33 - a14_23_32 - a13_22_34 - a12_24_33);
	return result;
}

mat4 m4_adjugate(mat4 m)
{
	// #todo variable optimization, simd
	mat4 result = m4_zero();

	f32 a11_22_33 = m.a11 * m.a22 * m.a33;
	f32 a11_22_34 = m.a11 * m.a22 * m.a34;
	f32 a11_22_43 = m.a11 * m.a22 * m.a43;
	f32 a11_22_44 = m.a11 * m.a22 * m.a44;
	f32 a11_23_32 = m.a11 * m.a23 * m.a32;
	f32 a11_23_34 = m.a11 * m.a23 * m.a34;
	f32 a11_23_42 = m.a11 * m.a23 * m.a42;
	f32 a11_23_44 = m.a11 * m.a23 * m.a44;
	f32 a11_24_32 = m.a11 * m.a24 * m.a32;
	f32 a11_24_33 = m.a11 * m.a24 * m.a33;
	f32 a11_24_42 = m.a11 * m.a24 * m.a42;
	f32 a11_24_43 = m.a11 * m.a24 * m.a43;
	f32 a11_32_43 = m.a11 * m.a32 * m.a43;
	f32 a11_32_44 = m.a11 * m.a32 * m.a44;
	f32 a11_33_42 = m.a11 * m.a33 * m.a42;
	f32 a11_33_44 = m.a11 * m.a33 * m.a44;
	f32 a11_34_42 = m.a11 * m.a34 * m.a42;
	f32 a11_34_43 = m.a11 * m.a34 * m.a43;

	f32 a12_21_33 = m.a12 * m.a21 * m.a33;
	f32 a12_21_34 = m.a12 * m.a21 * m.a34;
	f32 a12_21_43 = m.a12 * m.a21 * m.a43;
	f32 a12_21_44 = m.a12 * m.a21 * m.a44;
	f32 a12_23_31 = m.a12 * m.a23 * m.a31;
	f32 a12_23_34 = m.a12 * m.a23 * m.a34;
	f32 a12_23_41 = m.a12 * m.a23 * m.a41;
	f32 a12_23_44 = m.a12 * m.a23 * m.a44;
	f32 a12_24_31 = m.a12 * m.a24 * m.a31;
	f32 a12_24_33 = m.a12 * m.a24 * m.a33;
	f32 a12_24_41 = m.a12 * m.a24 * m.a41;
	f32 a12_24_43 = m.a12 * m.a24 * m.a43;
	f32 a12_31_43 = m.a12 * m.a31 * m.a43;
	f32 a12_31_44 = m.a12 * m.a31 * m.a44;
	f32 a12_33_41 = m.a12 * m.a33 * m.a41;
	f32 a12_33_44 = m.a12 * m.a33 * m.a44;
	f32 a12_34_41 = m.a12 * m.a34 * m.a41;
	f32 a12_34_43 = m.a12 * m.a34 * m.a43;

	f32 a13_21_32 = m.a13 * m.a21 * m.a32;
	f32 a13_21_34 = m.a13 * m.a21 * m.a34;
	f32 a13_21_42 = m.a13 * m.a21 * m.a42;
	f32 a13_21_44 = m.a13 * m.a21 * m.a44;
	f32 a13_22_31 = m.a13 * m.a22 * m.a31;
	f32 a13_22_34 = m.a13 * m.a22 * m.a34;
	f32 a13_22_41 = m.a13 * m.a22 * m.a41;
	f32 a13_22_44 = m.a13 * m.a22 * m.a44;
	f32 a13_24_31 = m.a13 * m.a24 * m.a31;
	f32 a13_24_32 = m.a13 * m.a24 * m.a32;
	f32 a13_24_41 = m.a13 * m.a24 * m.a41;
	f32 a13_24_42 = m.a13 * m.a24 * m.a42;
	f32 a13_31_42 = m.a13 * m.a31 * m.a42;
	f32 a13_31_44 = m.a13 * m.a31 * m.a44;
	f32 a13_32_41 = m.a13 * m.a32 * m.a41;
	f32 a13_32_44 = m.a13 * m.a32 * m.a44;
	f32 a13_34_41 = m.a13 * m.a34 * m.a41;
	f32 a13_34_42 = m.a13 * m.a34 * m.a42;

	f32 a14_21_32 = m.a14 * m.a21 * m.a32;
	f32 a14_21_33 = m.a14 * m.a21 * m.a33;
	f32 a14_21_42 = m.a14 * m.a21 * m.a42;
	f32 a14_21_43 = m.a14 * m.a21 * m.a43;
	f32 a14_22_31 = m.a14 * m.a22 * m.a31;
	f32 a14_22_33 = m.a14 * m.a22 * m.a33;
	f32 a14_22_41 = m.a14 * m.a22 * m.a41;
	f32 a14_22_43 = m.a14 * m.a22 * m.a43;
	f32 a14_23_31 = m.a14 * m.a23 * m.a31;
	f32 a14_23_32 = m.a14 * m.a23 * m.a32;
	f32 a14_23_41 = m.a14 * m.a23 * m.a41;
	f32 a14_23_42 = m.a14 * m.a23 * m.a42;
	f32 a14_31_42 = m.a14 * m.a31 * m.a42;
	f32 a14_31_43 = m.a14 * m.a31 * m.a43;
	f32 a14_32_41 = m.a14 * m.a32 * m.a41;
	f32 a14_32_43 = m.a14 * m.a32 * m.a43;
	f32 a14_33_41 = m.a14 * m.a33 * m.a41;
	f32 a14_33_42 = m.a14 * m.a33 * m.a42;

	f32 a21_32_43 = m.a21 * m.a32 * m.a43;
	f32 a21_32_44 = m.a21 * m.a32 * m.a44;
	f32 a21_33_42 = m.a21 * m.a33 * m.a42;
	f32 a21_33_44 = m.a21 * m.a33 * m.a44;
	f32 a21_34_42 = m.a21 * m.a34 * m.a42;
	f32 a21_34_43 = m.a21 * m.a34 * m.a43;

	f32 a22_31_44 = m.a22 * m.a31 * m.a44;
	f32 a22_33_44 = m.a22 * m.a33 * m.a44;
	f32 a22_34_41 = m.a22 * m.a34 * m.a41;
	f32 a22_34_43 = m.a22 * m.a34 * m.a43;

	f32 a22_31_43 = m.a22 * m.a31 * m.a43;
	f32 a23_31_44 = m.a23 * m.a31 * m.a44;
	f32 a23_32_44 = m.a23 * m.a32 * m.a44;
	f32 a22_33_41 = m.a22 * m.a33 * m.a41;
	f32 a23_34_41 = m.a23 * m.a34 * m.a41;
	f32 a23_34_42 = m.a23 * m.a34 * m.a42;

	f32 a23_31_42 = m.a23 * m.a31 * m.a42;
	f32 a23_32_41 = m.a23 * m.a32 * m.a41;
	f32 a24_31_42 = m.a24 * m.a31 * m.a42;
	f32 a24_31_43 = m.a24 * m.a31 * m.a43;
	f32 a24_32_41 = m.a24 * m.a32 * m.a41;
	f32 a24_32_43 = m.a24 * m.a32 * m.a43;
	f32 a24_33_41 = m.a24 * m.a33 * m.a41;
	f32 a24_33_42 = m.a24 * m.a33 * m.a42;

	result.a11 =   a22_33_44 + a23_34_42 + a24_32_43 - a24_33_42 - a23_32_44 - a22_34_43;
	result.a12 = - a12_33_44 - a13_34_42 - a14_32_43 + a14_33_42 + a13_32_44 + a12_34_43;
	result.a13 =   a12_23_44 + a13_24_42 + a14_22_43 - a14_23_42 - a13_22_44 - a12_24_43;
	result.a14 = - a12_23_34 - a13_24_32 - a14_22_33 + a14_23_32 + a13_22_34 + a12_24_33;

	result.a21 = - a21_33_44 - a23_34_41 - a24_31_43 + a24_33_41 + a23_31_44 + a21_34_43;
	result.a22 =   a11_33_44 + a13_34_41 + a14_31_43 - a14_33_41 - a13_31_44 - a11_34_43;
	result.a23 = - a11_23_44 - a13_24_41 - a14_21_43 + a14_23_41 + a13_21_44 + a11_24_43;
	result.a24 =   a11_23_34 + a13_24_31 + a14_21_33 - a14_23_31 - a13_21_34 - a11_24_33;

	//result.a31 =   a12_32_44 + a13_24_42 + a14_22_43 - a14_23_42 - a13_22_44 - a12_23_44;
	result.a31 =   a21_32_44 + a22_34_41 + a24_31_42 - a24_32_41 - a22_31_44 - a21_34_42;
	result.a32 = - a11_32_44 - a12_34_41 - a14_31_42 + a14_32_41 + a12_31_44 + a11_34_42;
	result.a33 =   a11_22_44 + a12_24_41 + a14_21_42 - a14_22_41 - a12_21_44 - a11_24_42;
	result.a34 = - a11_22_34 - a12_24_31 - a14_21_32 + a14_22_31 + a12_21_34 + a11_24_32;

	result.a41 = - a21_32_43 - a22_33_41 - a23_31_42 + a23_32_41 + a22_31_43 + a21_33_42;
	result.a42 =   a11_32_43 + a12_33_41 + a13_31_42 - a13_32_41 - a12_31_43 - a11_33_42;
	result.a43 = - a11_22_43 - a12_23_41 - a13_21_42 + a13_22_41 + a12_21_43 + a11_23_42;
	result.a44 =   a11_22_33 + a12_23_31 + a13_21_32 - a13_22_31 - a12_21_33 - a11_23_32;

	return result;
}

mat4 m4_inverse(mat4 m)
{
	f32 det = m4_determinant(m);
	mat4 adj = m4_adjugate(m);
	mat4 result = m4_mul_f32(adj, 1.0f / det);

	return result;
}

/// }}}
/// ----------------------------------------------------------------------------
/// #section: Type Printers
/// ----------------------------------------------------------------------------
/// {{{

void print_v2(vec2* v, const char *name)
{
	printf("| %.2f %.2f | %s\n",
			v->x, v->y, name == 0 ? "" : name);
}

void print_v3(vec3* v, const char *name)
{
	printf("| %.2f %.2f %.2f | %s\n",
			v->x, v->y, v->z, name == 0 ? "" : name);
}

void print_v4(vec4* v, const char *name)
{
	printf("| %.2f %.2f %.2f %.2f | %s\n",
			v->x, v->y, v->z, v->w, name == 0 ? "" : name);
}

void print_float(f32 value)
{
	if(value == 0.0f) { value = 0.0f; } // NOTE: remove -0.0 in printf
	if(value < 0.0f)
	{
		printf("%.2f ", value);
	}
	else
	{
		printf(" %.2f ", value);
	}
}

void print_m4(mat4* m, const char *name)
{
	// #todo for proper spacing get longest float string
	printf("┏                         ┓\n");
	for(u32 i = 0; i < 4; ++i) // row
	{
		printf("┃ ");
		for(u32 j = 0; j < 4; ++j) // column
		{
			f32 value = *(m->e + i * 4 + j);
			if(value == 0.0f) { value = 0.0f; } // NOTE: remove -0.0 in printf
			if(value < 0.0f)
			{
				printf("%.2f ", value);
			}
			else
			{
				printf(" %.2f ", value);
			}
		}
		printf("┃");
		if(i == 0)
		{
			printf(" %s\n", name == 0 ? "" : name);
		}
		else
		{
			printf("\n");
		}
	}
	printf("┗                         ┛\n");
	#if 0
	printf("┏                         ┓\n");
	printf("┃ "); print_float(m->a11); print_float(m->a12); print_float(m->a13); print_float(m->a14); printf("┃");
	printf(" %s2\n", name == 0 ? "" : name);
	printf("┃ "); print_float(m->a21); print_float(m->a22); print_float(m->a23); print_float(m->a24); printf("┃\n");
	printf("┃ "); print_float(m->a31); print_float(m->a32); print_float(m->a33); print_float(m->a34); printf("┃\n");
	printf("┃ "); print_float(m->a41); print_float(m->a42); print_float(m->a43); print_float(m->a44); printf("┃\n");
	printf("┗                         ┛\n");
	#endif
}

/// }}}
