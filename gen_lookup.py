RANK_1 = 0
RANK_2 = 1
RANK_3 = 2
RANK_4 = 3
RANK_5 = 4
RANK_6 = 5
RANK_7 = 6
RANK_8 = 7
RANK_NONE = 8
ranks = [
	RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE,
	RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE,
	RANK_NONE,	  RANK_1,	 RANK_1,	RANK_1,	   RANK_1,	  RANK_1,	 RANK_1,	RANK_1,	   RANK_1, RANK_NONE,
	RANK_NONE,	  RANK_2,	 RANK_2,	RANK_2,	   RANK_2,	  RANK_2,	 RANK_2,	RANK_2,	   RANK_2, RANK_NONE,
	RANK_NONE,	  RANK_3,	 RANK_3,	RANK_3,	   RANK_3,	  RANK_3,	 RANK_3,	RANK_3,	   RANK_3, RANK_NONE,
	RANK_NONE,	  RANK_4,	 RANK_4,	RANK_4,	   RANK_4,	  RANK_4,	 RANK_4,	RANK_4,	   RANK_4, RANK_NONE,
	RANK_NONE,	  RANK_5,	 RANK_5,	RANK_5,	   RANK_5,	  RANK_5,	 RANK_5,	RANK_5,	   RANK_5, RANK_NONE,
	RANK_NONE,	  RANK_6,	 RANK_6,	RANK_6,	   RANK_6,	  RANK_6,	 RANK_6,	RANK_6,	   RANK_6, RANK_NONE,
	RANK_NONE,	  RANK_7,	 RANK_7,	RANK_7,	   RANK_7,	  RANK_7,	 RANK_7,	RANK_7,	   RANK_7, RANK_NONE,
	RANK_NONE,	  RANK_8,	 RANK_8,	RANK_8,	   RANK_8,	  RANK_8,	 RANK_8,	RANK_8,	   RANK_8, RANK_NONE,
	RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE,
	RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE,
	]

map_to_64 = [
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65,	 0,	 1,	 2,	 3,	 4,	 5,	 6,	 7, 65,
	65,	 8,	 9, 10, 11, 12, 13, 14, 15, 65,
	65, 16, 17, 18, 19, 20, 21, 22, 23, 65,
	65, 24, 25, 26, 27, 28, 29, 30, 31, 65,
	65, 32, 33, 34, 35, 36, 37, 38, 39, 65,
	65, 40, 41, 42, 43, 44, 45, 46, 47, 65,
	65, 48, 49, 50, 51, 52, 53, 54, 55, 65,
	65, 56, 57, 58, 59, 60, 61, 62, 63, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65
	]

map_to_120 = [
	21, 22, 23, 24, 25, 26, 27, 28,
	31, 32, 33, 34, 35, 36, 37, 38,
	41, 42, 43, 44, 45, 46, 47, 48,
	51, 52, 53, 54, 55, 56, 57, 58,
	61, 62, 63, 64, 65, 66, 67, 68,
	71, 72, 73, 74, 75, 76, 77, 78,
	81, 82, 83, 84, 85, 86, 87, 88,
	91, 92, 93, 94, 95, 96, 97, 98
	]

diagonal_deltas = [
	10 - 1, 10 + 1, -10 - 1, -10 + 1
	]

ortho_deltas = [
	-1, 1, 10, -10
	]

knight_deltas = [
	-12, -8, -21, -19, 8,  12,  19,  21
	]

king_deltas = [
	-11, -10, -9, -1, 1, 9, 10, 11
	]

pos_names = [
	"A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1",
	"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2",
	"A3", "B3", "C3", "D3", "E3", "F3", "G3", "H3",
	"A4", "B4", "C4", "D4", "E4", "F4", "G4", "H4",
	"A5", "B5", "C5", "D5", "E5", "F5", "G5", "H5",
	"A6", "B6", "C6", "D6", "E6", "F6", "G6", "H6",
	"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7",
	"A8", "B8", "C8", "D8", "E8", "F8", "G8", "H8"
	]

class move_list:
	def __init__(self, start_pos):
		self.start_pos = start_pos
		self.moves = []

	def __str__(self):
		result = ""

		valid_count = 0
		for move_index, move in enumerate(self.moves):
			result += "1ull << {}".format(pos_names[move])
			if move_index != len(self.moves) - 1:
				result += " | "
		return result	

class chess_ray:
	def __init__(self, start_pos):
		self.start_pos = start_pos
		self.rays = []
		self.rays.append([])
		self.rays.append([])
		self.rays.append([])
		self.rays.append([])

	def __str__(self):
		result = "\t\t.rays =\n"
		result += "\t\t{\n"
		valid_count = 0
		rays_written = 0
		for ray_index, ray in enumerate(self.rays):
			if len(ray):
				valid_count += 1
				result += "\t\t\t{\n"
				result += "\t\t\t\t.moves = { "
				for move_index, move in enumerate(ray):
					result += "({} << 6) | {}".format(pos_names[move], pos_names[self.start_pos])
					if move_index != 6:
						result += ", "
				#for i in range(len(ray), 7):
					#result += "0"
					#if i != 6:
						#result += ", "
				if len(ray) < 7:
					result += "0"

				result += " },\n"
				result += "\t\t\t\t.count = {}\n".format(len(ray))
				result += "\t\t\t}"
				if rays_written != 3:
					result += ",\n"
				else:
					result += "\n"
				rays_written += 1

		for i in range(valid_count, len(self.rays)):
			result += "\t\t\t{\n"
			result += "\t\t\t\t.moves = { 0 },\n"
			result += "\t\t\t\t.count =  0\n"
			result += "\t\t\t}"
			if rays_written != 3:
				result += ",\n"
			else:
				result += "\n"
			rays_written += 1

		result += "\t\t},\n"
		result += "\t\t.valid_count = {}\n".format(valid_count)
		return result

def write_knight_moves(out_file):
	out_file.write("const u64 knight_move_list[64] =\n")
	out_file.write("{\n");
	for pos in range(0, 64):
		pos120 = map_to_120[pos]
		out_file.write("\t[{}] = ".format(pos_names[pos]))

		moves = move_list(pos)
		for delta in knight_deltas:
			new_pos120 = pos120 + delta
			if ranks[new_pos120] != RANK_NONE:
				new_pos = map_to_64[new_pos120]
				moves.moves.append(new_pos)

		out_file.write(str(moves))

		if pos != 63:
			out_file.write(",\n")
		else:
			out_file.write("\n")
	out_file.write("};\n\n")

def write_king_moves(out_file):
	out_file.write("const u64 king_move_list[64] =\n")
	out_file.write("{\n");
	for pos in range(0, 64):
		pos120 = map_to_120[pos]
		out_file.write("\t[{}] = ".format(pos_names[pos]))

		moves = move_list(pos)
		for delta in king_deltas:
			new_pos120 = pos120 + delta
			if ranks[new_pos120] != RANK_NONE:
				new_pos = map_to_64[new_pos120]
				moves.moves.append(new_pos)

		out_file.write(str(moves))

		if pos != 63:
			out_file.write(",\n")
		else:
			out_file.write("\n")
	out_file.write("};\n\n")

def write_bishop_rays(out_file):
	out_file.write("const chess_ray_lookup diagonal_ray_lookup[64] =\n")
	out_file.write("{\n");
	for pos in range(0, 64):
		pos120 = map_to_120[pos]
		out_file.write("\t// #note {}\n".format(pos_names[pos]))
		out_file.write("\t(chess_ray_lookup){\n")

		ray = chess_ray(pos)
		for i, delta in enumerate(diagonal_deltas):
			counter = 0
			new_pos120 = pos120 + delta
			while counter < 7:
				counter += 1
				if ranks[new_pos120] != RANK_NONE:
					new_pos = map_to_64[new_pos120]
					ray.rays[i].append(new_pos)
					new_pos120 += delta
				else:
					break

		out_file.write(str(ray))

		if pos != 63:
			out_file.write("\t},\n")
		else:
			out_file.write("\t}\n")
	out_file.write("};\n\n")

def write_rook_rays(out_file):
	out_file.write("const chess_ray_lookup orthogonal_ray_lookup[64] =\n")
	out_file.write("{\n");
	for pos in range(0, 64):
		pos120 = map_to_120[pos]
		out_file.write("\t// #note {}\n".format(pos_names[pos]))
		out_file.write("\t(chess_ray_lookup){\n")

		ray = chess_ray(pos)
		for i, delta in enumerate(ortho_deltas):
			counter = 0
			new_pos120 = pos120 + delta
			while counter < 7:
				counter += 1
				if ranks[new_pos120] != RANK_NONE:
					new_pos = map_to_64[new_pos120]
					ray.rays[i].append(new_pos)
					new_pos120 += delta
				else:
					break

		out_file.write(str(ray))

		if pos != 63:
			out_file.write("\t},\n")
		else:
			out_file.write("\t}\n")
	out_file.write("};\n\n")

if __name__ == "__main__":
	with open("chess_move_lookup.h", "w") as out_file:
		out_file.write("#ifndef CHESS_MOVE_LOOKUP_H\n")
		out_file.write("#define CHESS_MOVE_LOOKUP_H\n\n")
		out_file.write("#include \"chess_move.h\"\n\n")
		out_file.write("#define MAX_RAY_MOVES 7\n")
		out_file.write("typedef struct chess_ray\n")
		out_file.write("{\n")
		out_file.write("\tchess_move moves[MAX_RAY_MOVES];\n")
		out_file.write("\tu32 count;\n")
		out_file.write("} chess_ray;\n\n")
		out_file.write("typedef struct chess_ray_lookup\n")
		out_file.write("{\n")
		out_file.write("\tchess_ray rays[4];\n")
		out_file.write("\tu32 valid_count;\n")
		out_file.write("} chess_ray_lookup;\n\n")
		out_file.write("extern const u64 knight_move_list[64];\n\n")
		out_file.write("//#note the move list does not include castling moves\n");
		out_file.write("extern const u64 king_move_list[64];\n\n")
		out_file.write("extern const chess_ray_lookup diagonal_ray_lookup[64];\n\n")
		out_file.write("extern const chess_ray_lookup orthogonal_ray_lookup[64];\n\n")
		out_file.write("#endif // !CHESS_MOVE_LOOKUP_H\n")
	out_file.close()

	with open("chess_move_lookup.c", "w") as out_file:
		out_file.write("#include \"chess_move_lookup.h\"\n\n")
		write_knight_moves(out_file)
		write_king_moves(out_file)
		write_bishop_rays(out_file)
		write_rook_rays(out_file)
	out_file.close()
