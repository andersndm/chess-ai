RANK_1 = 0
RANK_2 = 1
RANK_3 = 2
RANK_4 = 3
RANK_5 = 4
RANK_6 = 5
RANK_7 = 6
RANK_8 = 7
RANK_NONE = 8
ranks = [
	RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE,
	RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE,
	RANK_NONE,	  RANK_1,	 RANK_1,	RANK_1,	   RANK_1,	  RANK_1,	 RANK_1,	RANK_1,	   RANK_1, RANK_NONE,
	RANK_NONE,	  RANK_2,	 RANK_2,	RANK_2,	   RANK_2,	  RANK_2,	 RANK_2,	RANK_2,	   RANK_2, RANK_NONE,
	RANK_NONE,	  RANK_3,	 RANK_3,	RANK_3,	   RANK_3,	  RANK_3,	 RANK_3,	RANK_3,	   RANK_3, RANK_NONE,
	RANK_NONE,	  RANK_4,	 RANK_4,	RANK_4,	   RANK_4,	  RANK_4,	 RANK_4,	RANK_4,	   RANK_4, RANK_NONE,
	RANK_NONE,	  RANK_5,	 RANK_5,	RANK_5,	   RANK_5,	  RANK_5,	 RANK_5,	RANK_5,	   RANK_5, RANK_NONE,
	RANK_NONE,	  RANK_6,	 RANK_6,	RANK_6,	   RANK_6,	  RANK_6,	 RANK_6,	RANK_6,	   RANK_6, RANK_NONE,
	RANK_NONE,	  RANK_7,	 RANK_7,	RANK_7,	   RANK_7,	  RANK_7,	 RANK_7,	RANK_7,	   RANK_7, RANK_NONE,
	RANK_NONE,	  RANK_8,	 RANK_8,	RANK_8,	   RANK_8,	  RANK_8,	 RANK_8,	RANK_8,	   RANK_8, RANK_NONE,
	RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE,
	RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE, RANK_NONE,
	]

map_to_64 = [
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65,	 0,	 1,	 2,	 3,	 4,	 5,	 6,	 7, 65,
	65,	 8,	 9, 10, 11, 12, 13, 14, 15, 65,
	65, 16, 17, 18, 19, 20, 21, 22, 23, 65,
	65, 24, 25, 26, 27, 28, 29, 30, 31, 65,
	65, 32, 33, 34, 35, 36, 37, 38, 39, 65,
	65, 40, 41, 42, 43, 44, 45, 46, 47, 65,
	65, 48, 49, 50, 51, 52, 53, 54, 55, 65,
	65, 56, 57, 58, 59, 60, 61, 62, 63, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65,
	65, 65, 65, 65, 65, 65, 65, 65, 65, 65
	]

map_to_120 = [
	21, 22, 23, 24, 25, 26, 27, 28,
	31, 32, 33, 34, 35, 36, 37, 38,
	41, 42, 43, 44, 45, 46, 47, 48,
	51, 52, 53, 54, 55, 56, 57, 58,
	61, 62, 63, 64, 65, 66, 67, 68,
	71, 72, 73, 74, 75, 76, 77, 78,
	81, 82, 83, 84, 85, 86, 87, 88,
	91, 92, 93, 94, 95, 96, 97, 98
	]

diagonal_deltas = [
	10 - 1, 10 + 1, -10 - 1, -10 + 1
	]

ortho_deltas = [
	-1, 1, 10, -10
	]

def num_bishop_moves(pos):
	num_moves = 0
	pos120 = map_to_120[pos]
	for delta in diagonal_deltas:
		new_pos120 = pos120 + delta
		while ranks[new_pos120] != RANK_NONE:
			new_pos120 += delta
			num_moves += 1
	return num_moves

def max_bishop_moves():
	max_moves = 0
	for pos in range(0, 64):
		num_moves = num_bishop_moves(pos)
		print("bishop: pos:", pos, "num:", num_moves)
		if num_moves > max_moves:
			max_moves = num_moves
	return max_moves

def num_rook_moves(pos):
	num_moves = 0
	pos120 = map_to_120[pos]
	for delta in ortho_deltas:
		new_pos120 = pos120 + delta
		while ranks[new_pos120] != RANK_NONE:
			new_pos120 += delta
			num_moves += 1
	return num_moves

def max_rook_moves():
	max_moves = 0
	for pos in range(0, 64):
		num_moves = num_rook_moves(pos)
		print("rook:   pos:", pos, "num:", num_moves)
		if num_moves > max_moves:
			max_moves = num_moves
	return max_moves

def num_queen_moves(pos):
	num_moves = 0
	pos120 = map_to_120[pos]
	for delta in ortho_deltas:
		new_pos120 = pos120 + delta
		while ranks[new_pos120] != RANK_NONE:
			new_pos120 += delta
			num_moves += 1
	for delta in diagonal_deltas:
		new_pos120 = pos120 + delta
		while ranks[new_pos120] != RANK_NONE:
			new_pos120 += delta
			num_moves += 1
	return num_moves

def max_queen_moves():
	max_moves = 0
	for pos in range(0, 64):
		num_moves = num_queen_moves(pos)
		print("queen:  pos:", pos, "num:", num_moves)
		if num_moves > max_moves:
			max_moves = num_moves
	return max_moves

if __name__ == "__main__":
	print("max bishop moves:", max_bishop_moves())
	print("max rook moves:	", max_rook_moves())
	print("max queen moves: ", max_queen_moves())
