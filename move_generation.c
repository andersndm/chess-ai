#include "move_generation.h"

// -------------------------------------------------------------------------------------------------
// #section pawn moves
// -------------------------------------------------------------------------------------------------
// {{{

internal const u64 rank_bbs[NUM_RANKS] =
{
	0x00000000000000FF,
	0x000000000000FF00,
	0x0000000000FF0000,
	0x00000000FF000000,
	0x000000FF00000000,
	0x0000FF0000000000,
	0x00FF000000000000,
	0xFF00000000000000
};

internal const u64 file_bbs[NUM_FILES] =
{
	0x0101010101010101,
	0x0202020202020202,
	0x0404040404040404,
	0x0808080808080808,
	0x1010101010101010,
	0x2020202020202020,
	0x4040404040404040,
	0x8080808080808080
};

internal inline u64 pawn_push_bb(u64 pawns, chess_color color)
{
	return (color == WHITE ? pawns << 8 : pawns >> 8);
}

internal inline u8 pawn_push_from(u8 pos, chess_color color)
{
	return (color == WHITE ? pos - 8 : pos + 8);
}

internal inline u8 pawn_double_push_from(u8 pos, chess_color color)
{
	return (color == WHITE ? pos - 16 : pos + 16);
}

internal inline u64 pawn_attack_left_bb(u64 pawns, chess_color color)
{
	return (color == WHITE ? pawns << 7 : pawns >> 9);
}

internal inline u8 pawn_attack_left_from(u8 pos, chess_color color)
{
	return (color == WHITE ? pos - 7 : pos + 9);
}

internal inline u64 pawn_attack_right_bb(u64 pawns, chess_color color)
{
	return (color == WHITE ? pawns << 9 : pawns >> 7);
}

internal inline u8 pawn_attack_right_from(u8 pos, chess_color color)
{
	return (color == WHITE ? pos - 9 : pos + 7);
}

internal bool pawn_legal_en_passant_capture(const chess_board *board, u8 to, u8 from)
{
	// #todo more efficient way to do this?
	if (to == board->state->en_passant && board->state->en_passant != 0)
	{
		chess_color color = board->state->side;
		u8 king_pos = lsb(board->color_bbs[color] & board->type_bbs[KING]);
		if (from / 8 == king_pos / 8)
		{
			u8 capture_pos = color == WHITE ? board->state->en_passant - 8 : board->state->en_passant + 8;
			u8 king_rank_a_file = king_pos - (u8)(king_pos % NUM_FILES);
			u8 king_rank_h_file = king_rank_a_file + (NUM_FILES - 1);
			// #note start at the left side of the king and move to the left side of the board
			// #note can only happen on the 2nd or 7th rank, so no need to worry about
			// over/underflow
			for (u8 check_pos = king_pos - 1; check_pos >= king_rank_a_file; --check_pos)
			{
				chess_piece check_piece = board->squares[check_pos];
				if (check_piece == EMPTY_SQ || check_pos == from || check_pos == capture_pos)
				{
					continue;
				}
				chess_piece_type check_type = get_piece_type(check_piece);
				chess_color check_color = get_piece_color(check_piece);
				if (check_type == QUEEN || check_type == ROOK)
				{
					if (check_color != color)
					{
						return false;
					}
					break;
				}
				else
				{
					break;
				}
			}
			// #note start at the right side of the king and move to the right side of the board
			for (u8 check_pos = king_pos + 1; check_pos <= king_rank_h_file; ++check_pos)
			{
				chess_piece check_piece = board->squares[check_pos];
				if (check_piece == EMPTY_SQ || check_pos == from || check_pos == capture_pos)
				{
					continue;
				}
				chess_piece_type check_type = get_piece_type(check_piece);
				chess_color check_color = get_piece_color(check_piece);
				if (check_type == QUEEN || check_type == ROOK)
				{
					if (check_color != color)
					{
						return false;
					}
					break;
				}
				else
				{
					break;
				}
			}
		}
	}
	return true;
}

internal u32 generate_pawn_push_no_pin(u64 pawns, chess_color color, u64 valid_squares,
									   chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle normal push moves
	u64 push_bb = pawn_push_bb(pawns, color) & valid_squares;
	while (push_bb)
	{
		u8 to = lsb_pop(&push_bb);
		u8 from = pawn_push_from(to, color);
		chess_move move = gen_push(from, to);
		move_buffer[move_index++] = move;
	}

	return move_index;
}

internal u32 generate_pawn_push_pin(u64 pawns, chess_color color, u64 valid_squares,
									const u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle normal push moves
	u64 push_bb = pawn_push_bb(pawns, color) & valid_squares;
	while (push_bb)
	{
		u8 to = lsb_pop(&push_bb);
		u8 from = pawn_push_from(to, color);
		if (square_bit(to) & pin_buffer[from])
		{
			chess_move move = gen_push(from, to);
			move_buffer[move_index++] = move;
		}
	}

	return move_index;
}

internal u32 generate_pawn_double_push_no_pin(u64 pawns, chess_color color,
											  u64 valid_squares, u64 valid_final_squares,
											  chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle double push for pawns on start rank
	u64 double_push_bb = pawn_push_bb(pawns, color) & valid_squares;
	double_push_bb = pawn_push_bb(double_push_bb, color) & valid_final_squares;
	while (double_push_bb)
	{
		u8 to = lsb_pop(&double_push_bb);
		u8 from = pawn_double_push_from(to, color);
		chess_move move = gen_en_passant(from, to);
		move_buffer[move_index++] = move;
	}

	return move_index;
}

internal u32 generate_pawn_double_push_pin(u64 pawns, chess_color color,
										   u64 valid_squares, u64 valid_final_squares,
										   const u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle double push for pawns on start rank
	u64 double_push_bb = pawn_push_bb(pawns, color) & valid_squares;
	double_push_bb = pawn_push_bb(double_push_bb, color) & valid_final_squares;
	while (double_push_bb)
	{
		u8 to = lsb_pop(&double_push_bb);
		u8 from = pawn_double_push_from(to, color);
		if (square_bit(to) & pin_buffer[from])
		{
			chess_move move = gen_en_passant(from, to);
			move_buffer[move_index++] = move;
		}
	}

	return move_index;
}

internal u32 generate_pawn_attacks_no_pin_no_ep(u64 pawns, chess_color color, u64 valid_squares,
												chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle 'left' attacks
	u64 left_attack_bb = pawn_attack_left_bb(pawns & ~file_bbs[FILE_A], color) & valid_squares;
	while (left_attack_bb)
	{
		u8 to = lsb_pop(&left_attack_bb);
		u8 from = pawn_attack_left_from(to, color);
		chess_move move = gen_push(from, to);
		move_buffer[move_index++] = move;
	}

	// #note handle 'right' attacks
	u64 right_attack_bb = pawn_attack_right_bb(pawns & ~file_bbs[FILE_H], color) & valid_squares;
	while (right_attack_bb)
	{
		u8 to = lsb_pop(&right_attack_bb);
		u8 from = pawn_attack_right_from(to, color);
		chess_move move = gen_push(from, to);
		move_buffer[move_index++] = move;
	}
	return move_index;
}

internal u32 generate_pawn_attacks_no_pin(u64 pawns, chess_color color, u64 valid_squares,
										  const chess_board *board, chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle 'left' attacks
	u64 left_attack_bb = pawn_attack_left_bb(pawns & ~file_bbs[FILE_A], color) & valid_squares;
	while (left_attack_bb)
	{
		u8 to = lsb_pop(&left_attack_bb);
		u8 from = pawn_attack_left_from(to, color);
		if (pawn_legal_en_passant_capture(board, to, from))
		{
			chess_move move = gen_push(from, to);
			move_buffer[move_index++] = move;
		}
	}

	// #note handle 'right' attacks
	u64 right_attack_bb = pawn_attack_right_bb(pawns & ~file_bbs[FILE_H], color) & valid_squares;
	while (right_attack_bb)
	{
		u8 to = lsb_pop(&right_attack_bb);
		u8 from = pawn_attack_right_from(to, color);
		if (pawn_legal_en_passant_capture(board, to, from))
		{
			chess_move move = gen_push(from, to);
			move_buffer[move_index++] = move;
		}
	}
	return move_index;
}

internal u32 generate_pawn_attacks_no_ep(u64 pawns, chess_color color, u64 valid_squares,
										 const u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;
	// #note handle 'left' attacks
	u64 left_attack_bb = pawn_attack_left_bb(pawns & ~file_bbs[FILE_A], color) & valid_squares;
	while (left_attack_bb)
	{
		u8 to = lsb_pop(&left_attack_bb);
		u8 from = pawn_attack_left_from(to, color);
		if (square_bit(to) & pin_buffer[from])
		{
			chess_move move = gen_push(from, to);
			move_buffer[move_index++] = move;
		}
	}

	// #note handle 'right' attacks
	u64 right_attack_bb = pawn_attack_right_bb(pawns & ~file_bbs[FILE_H], color) & valid_squares;
	while (right_attack_bb)
	{
		u8 to = lsb_pop(&right_attack_bb);
		u8 from = pawn_attack_right_from(to, color);
		if (square_bit(to) & pin_buffer[from])
		{
			chess_move move = gen_push(from, to);
			move_buffer[move_index++] = move;
		}
	}
	return move_index;
}

internal u32 generate_pawn_attacks_pin_and_ep(u64 pawns, chess_color color, u64 valid_squares,
											  const chess_board *board, const u64 *pin_buffer,
											  chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle 'left' attacks
	u64 left_attack_bb = pawn_attack_left_bb(pawns & ~file_bbs[FILE_A], color) & valid_squares;
	while (left_attack_bb)
	{
		u8 to = lsb_pop(&left_attack_bb);
		u8 from = pawn_attack_left_from(to, color);
		if (square_bit(to) & pin_buffer[from])
		{
			if (pawn_legal_en_passant_capture(board, to, from))
			{
				chess_move move = gen_push(from, to);
				move_buffer[move_index++] = move;
			}
		}
	}

	// #note handle 'right' attacks
	u64 right_attack_bb = pawn_attack_right_bb(pawns & ~file_bbs[FILE_H], color) & valid_squares;
	while (right_attack_bb)
	{
		u8 to = lsb_pop(&right_attack_bb);
		u8 from = pawn_attack_right_from(to, color);
		if (square_bit(to) & pin_buffer[from])
		{
			if (pawn_legal_en_passant_capture(board, to, from))
			{
				chess_move move = gen_push(from, to);
				move_buffer[move_index++] = move;
			}
		}
	}
	return move_index;
}

internal u32 generate_pawn_push_promotions_no_pin(u64 pawns, chess_color color, u64 valid_squares,
												  chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle push promotions
	u64 push_promote_bb = pawn_push_bb(pawns, color) & valid_squares;
	while (push_promote_bb)
	{
		u8 to = lsb_pop(&push_promote_bb);
		u8 from = pawn_push_from(to, color);
		chess_move move = gen_promotion(from, to, KNIGHT);
		move_buffer[move_index++] = move;
		move = gen_promotion(from, to, BISHOP);
		move_buffer[move_index++] = move;
		move = gen_promotion(from, to, ROOK);
		move_buffer[move_index++] = move;
		move = gen_promotion(from, to, QUEEN);
		move_buffer[move_index++] = move;
	}

	return move_index;
}

internal u32 generate_pawn_push_promotions_pin(u64 pawns, chess_color color, u64 valid_squares,
											   const u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle push promotions
	u64 push_promote_bb = pawn_push_bb(pawns, color) & valid_squares;
	while (push_promote_bb)
	{
		u8 to = lsb_pop(&push_promote_bb);
		u8 from = pawn_push_from(to, color);
		if (square_bit(to) & pin_buffer[from])
		{
			chess_move move = gen_promotion(from, to, KNIGHT);
			move_buffer[move_index++] = move;
			move = gen_promotion(from, to, BISHOP);
			move_buffer[move_index++] = move;
			move = gen_promotion(from, to, ROOK);
			move_buffer[move_index++] = move;
			move = gen_promotion(from, to, QUEEN);
			move_buffer[move_index++] = move;
		}
	}

	return move_index;
}

internal u32 generate_pawn_attack_promotions_no_pin(u64 pawns, chess_color color, u64 valid_squares,
													chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle attack promotions
	u64 left_attack_promote_bb = pawn_attack_left_bb(pawns & ~file_bbs[FILE_A], color) & valid_squares;
	while (left_attack_promote_bb)
	{
		u8 to = lsb_pop(&left_attack_promote_bb);
		u8 from = pawn_attack_left_from(to, color);
		chess_move move = gen_promotion(from, to, KNIGHT);
		move_buffer[move_index++] = move;
		move = gen_promotion(from, to, BISHOP);
		move_buffer[move_index++] = move;
		move = gen_promotion(from, to, ROOK);
		move_buffer[move_index++] = move;
		move = gen_promotion(from, to, QUEEN);
		move_buffer[move_index++] = move;
	}

	u64 right_attack_promote_bb = pawn_attack_right_bb(pawns & ~file_bbs[FILE_H], color) & valid_squares;
	while (right_attack_promote_bb)
	{
		u8 to = lsb_pop(&right_attack_promote_bb);
		u8 from = pawn_attack_right_from(to, color);
		chess_move move = gen_promotion(from, to, KNIGHT);
		move_buffer[move_index++] = move;
		move = gen_promotion(from, to, BISHOP);
		move_buffer[move_index++] = move;
		move = gen_promotion(from, to, ROOK);
		move_buffer[move_index++] = move;
		move = gen_promotion(from, to, QUEEN);
		move_buffer[move_index++] = move;
	}

	return move_index;
}

internal u32 generate_pawn_attack_promotions_pin(u64 pawns, chess_color color, u64 valid_squares,
												 const u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	// #note handle attack promotions
	u64 left_attack_promote_bb = pawn_attack_left_bb(pawns & ~file_bbs[FILE_A], color) & valid_squares;
	while (left_attack_promote_bb)
	{
		u8 to = lsb_pop(&left_attack_promote_bb);
		u8 from = pawn_attack_left_from(to, color);
		if (square_bit(to) & pin_buffer[from])
		{
			chess_move move = gen_promotion(from, to, KNIGHT);
			move_buffer[move_index++] = move;
			move = gen_promotion(from, to, BISHOP);
			move_buffer[move_index++] = move;
			move = gen_promotion(from, to, ROOK);
			move_buffer[move_index++] = move;
			move = gen_promotion(from, to, QUEEN);
			move_buffer[move_index++] = move;
		}
	}

	u64 right_attack_promote_bb = pawn_attack_right_bb(pawns & ~file_bbs[FILE_H], color) & valid_squares;
	while (right_attack_promote_bb)
	{
		u8 to = lsb_pop(&right_attack_promote_bb);
		u8 from = pawn_attack_right_from(to, color);
		if (square_bit(to) & pin_buffer[from])
		{
			chess_move move = gen_promotion(from, to, KNIGHT);
			move_buffer[move_index++] = move;
			move = gen_promotion(from, to, BISHOP);
			move_buffer[move_index++] = move;
			move = gen_promotion(from, to, ROOK);
			move_buffer[move_index++] = move;
			move = gen_promotion(from, to, QUEEN);
			move_buffer[move_index++] = move;
		}
	}

	return move_index;
}

// #note a single pawn not on the promotion rank can at most move forward 1 or 2 and attack either
// side, so 4 moves. A pawn on the promotion rank can move forward 1 and promote or attack either
// side and promote, so 3 * 4 moves. Max moves for a single pawn is then 3 * 4 = 12. Max moves
// for all pawns is 8 * 12 = 96.
// #note move_buffer must have a minimum capacity of 96
internal u32 generate_pawn_moves(const chess_board *board, const check_status *status,
								 chess_move *move_buffer)
{
	u32 move_index = 0;

	chess_color color = board->state->side;
	chess_color oppo_color = opposite_color(color);

	u64 pawns = board->color_bbs[color] & board->type_bbs[PAWN];
	assert((pawns & rank_bbs[RANK_1]) == 0);
	assert((pawns & rank_bbs[RANK_8]) == 0);

	u64 empty = ~(board->color_bbs[WHITE] | board->color_bbs[BLACK]);
	u64 oppo_pieces = board->color_bbs[oppo_color];
	u64 push_mask = status->push_mask;
	u64 capture_mask = status->capture_mask;

	// #note if an en passant pawn is giving check to the king the en passant
	// square needs to be added to the pawns' capture mask
	if (board->state->en_passant != 0)
	{
		u64 en_passant_bit = square_bit(board->state->en_passant);
		oppo_pieces |= en_passant_bit;
		if (color == WHITE)
		{
			u64 adjusted = en_passant_bit >> 8;
			if (adjusted & capture_mask)
			{
				capture_mask |= en_passant_bit;
			}
		}
		else
		{
			u64 adjusted = en_passant_bit << 8;
			if (adjusted & capture_mask)
			{
				capture_mask |= en_passant_bit;
			}
		}
	}

	u64 start_ranks[NUM_COLORS] =
	{
		[WHITE] = rank_bbs[RANK_2],
		[BLACK] = rank_bbs[RANK_7]
	};

	u64 on_start = start_ranks[color] & pawns;
	u64 on_promote = start_ranks[opposite_color(color)] & pawns;
	u64 not_on_promote = (~start_ranks[opposite_color(color)]) & pawns;

	u64 valid_push_squares = empty & push_mask;
	u64 valid_attacking_squares = oppo_pieces & capture_mask;

	move_index += generate_pawn_push_no_pin(not_on_promote, color, valid_push_squares,
											move_buffer + move_index);
	move_index += generate_pawn_double_push_no_pin(on_start, color, empty, valid_push_squares,
													move_buffer + move_index);

	if (board->state->en_passant == 0)
	{
		move_index += generate_pawn_attacks_no_pin_no_ep(not_on_promote, color,
															valid_attacking_squares,
															move_buffer + move_index);
	}
	else
	{
		move_index += generate_pawn_attacks_no_pin(not_on_promote, color,
													valid_attacking_squares, board,
													move_buffer + move_index);
	}

	move_index += generate_pawn_push_promotions_no_pin(on_promote, color, valid_push_squares,
														move_buffer + move_index);
	move_index += generate_pawn_attack_promotions_no_pin(on_promote, color, valid_attacking_squares,
															move_buffer + move_index);

	return move_index;
}

// #note a single pawn not on the promotion rank can at most move forward 1 or 2 and attack either
// side, so 4 moves. A pawn on the promotion rank can move forward 1 and promote or attack either
// side and promote, so 3 * 4 moves. Max moves for a single pawn is then 3 * 4 = 12. Max moves
// for all pawns is 8 * 12 = 96.
// #note move_buffer must have a minimum capacity of 96
internal u32 generate_pawn_moves_with_pin(const chess_board *board, const check_status *status,
								 		  u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	chess_color color = board->state->side;
	chess_color oppo_color = opposite_color(color);

	u64 pawns = board->color_bbs[color] & board->type_bbs[PAWN];
	assert((pawns & rank_bbs[RANK_1]) == 0);
	assert((pawns & rank_bbs[RANK_8]) == 0);

	u64 empty = ~(board->color_bbs[WHITE] | board->color_bbs[BLACK]);
	u64 oppo_pieces = board->color_bbs[oppo_color];
	u64 push_mask = status->push_mask;
	u64 capture_mask = status->capture_mask;

	// #note if an en passant pawn is giving check to the king the en passant
	// square needs to be added to the pawns' capture mask
	if (board->state->en_passant != 0)
	{
		u64 en_passant_bit = square_bit(board->state->en_passant);
		oppo_pieces |= en_passant_bit;
		if (color == WHITE)
		{
			u64 adjusted = en_passant_bit >> 8;
			if (adjusted & capture_mask)
			{
				capture_mask |= en_passant_bit;
			}
		}
		else
		{
			u64 adjusted = en_passant_bit << 8;
			if (adjusted & capture_mask)
			{
				capture_mask |= en_passant_bit;
			}
		}
	}

	u64 start_ranks[NUM_COLORS] =
	{
		[WHITE] = rank_bbs[RANK_2],
		[BLACK] = rank_bbs[RANK_7]
	};

	u64 on_start = start_ranks[color] & pawns;
	u64 on_promote = start_ranks[opposite_color(color)] & pawns;
	u64 not_on_promote = (~start_ranks[opposite_color(color)]) & pawns;

	u64 valid_push_squares = empty & push_mask;
	u64 valid_attacking_squares = oppo_pieces & capture_mask;

	move_index += generate_pawn_push_pin(not_on_promote, color, valid_push_squares,
											pin_buffer, move_buffer + move_index);
	move_index += generate_pawn_double_push_pin(on_start, color, empty, valid_push_squares,
												pin_buffer, move_buffer + move_index);

	if (board->state->en_passant == 0)
	{
		move_index += generate_pawn_attacks_no_ep(not_on_promote, color,
													valid_attacking_squares, pin_buffer,
													move_buffer + move_index);
	}
	else
	{
		move_index += generate_pawn_attacks_pin_and_ep(not_on_promote, color,
														valid_attacking_squares,
														board, pin_buffer,
														move_buffer + move_index);
	}

	move_index += generate_pawn_push_promotions_pin(on_promote, color, valid_push_squares,
													pin_buffer, move_buffer + move_index);
	move_index += generate_pawn_attack_promotions_pin(on_promote, color, valid_attacking_squares,
														pin_buffer, move_buffer + move_index);

	return move_index;
}

internal u32 generate_pawn_captures(const chess_board *board, const check_status *status,
								 chess_move *move_buffer)
{
	u32 move_index = 0;

	chess_color color = board->state->side;
	chess_color oppo_color = opposite_color(color);

	u64 pawns = board->color_bbs[color] & board->type_bbs[PAWN];
	assert((pawns & rank_bbs[RANK_1]) == 0);
	assert((pawns & rank_bbs[RANK_8]) == 0);

	u64 oppo_pieces = board->color_bbs[oppo_color];
	u64 capture_mask = status->capture_mask;

	// #note if an en passant pawn is giving check to the king the en passant
	// square needs to be added to the pawns' capture mask
	if (board->state->en_passant != 0)
	{
		u64 en_passant_bit = square_bit(board->state->en_passant);
		oppo_pieces |= en_passant_bit;
		if (color == WHITE)
		{
			u64 adjusted = en_passant_bit >> 8;
			if (adjusted & capture_mask)
			{
				capture_mask |= en_passant_bit;
			}
		}
		else
		{
			u64 adjusted = en_passant_bit << 8;
			if (adjusted & capture_mask)
			{
				capture_mask |= en_passant_bit;
			}
		}
	}

	u64 start_ranks[NUM_COLORS] =
	{
		[WHITE] = rank_bbs[RANK_2],
		[BLACK] = rank_bbs[RANK_7]
	};

	u64 on_promote = start_ranks[opposite_color(color)] & pawns;
	u64 not_on_promote = (~start_ranks[opposite_color(color)]) & pawns;

	u64 valid_attacking_squares = oppo_pieces & capture_mask;

	if (board->state->en_passant == 0)
	{
		move_index += generate_pawn_attacks_no_pin_no_ep(not_on_promote, color,
															valid_attacking_squares,
															move_buffer + move_index);
	}
	else
	{
		move_index += generate_pawn_attacks_no_pin(not_on_promote, color,
													valid_attacking_squares, board,
													move_buffer + move_index);
	}

	move_index += generate_pawn_attack_promotions_no_pin(on_promote, color, valid_attacking_squares,
															move_buffer + move_index);

	return move_index;
}

internal u32 generate_pawn_captures_with_pin(const chess_board *board, const check_status *status,
								 		  u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	chess_color color = board->state->side;
	chess_color oppo_color = opposite_color(color);

	u64 pawns = board->color_bbs[color] & board->type_bbs[PAWN];
	assert((pawns & rank_bbs[RANK_1]) == 0);
	assert((pawns & rank_bbs[RANK_8]) == 0);

	u64 oppo_pieces = board->color_bbs[oppo_color];
	u64 capture_mask = status->capture_mask;

	// #note if an en passant pawn is giving check to the king the en passant
	// square needs to be added to the pawns' capture mask
	if (board->state->en_passant != 0)
	{
		u64 en_passant_bit = square_bit(board->state->en_passant);
		oppo_pieces |= en_passant_bit;
		if (color == WHITE)
		{
			u64 adjusted = en_passant_bit >> 8;
			if (adjusted & capture_mask)
			{
				capture_mask |= en_passant_bit;
			}
		}
		else
		{
			u64 adjusted = en_passant_bit << 8;
			if (adjusted & capture_mask)
			{
				capture_mask |= en_passant_bit;
			}
		}
	}

	u64 start_ranks[NUM_COLORS] =
	{
		[WHITE] = rank_bbs[RANK_2],
		[BLACK] = rank_bbs[RANK_7]
	};

	u64 on_promote = start_ranks[opposite_color(color)] & pawns;
	u64 not_on_promote = (~start_ranks[opposite_color(color)]) & pawns;

	u64 valid_attacking_squares = oppo_pieces & capture_mask;

	if (board->state->en_passant == 0)
	{
		move_index += generate_pawn_attacks_no_ep(not_on_promote, color,
													valid_attacking_squares, pin_buffer,
													move_buffer + move_index);
	}
	else
	{
		move_index += generate_pawn_attacks_pin_and_ep(not_on_promote, color,
														valid_attacking_squares,
														board, pin_buffer,
														move_buffer + move_index);
	}

	move_index += generate_pawn_attack_promotions_pin(on_promote, color, valid_attacking_squares,
														pin_buffer, move_buffer + move_index);

	return move_index;
}

// }}}
// -------------------------------------------------------------------------------------------------
// #section leaping moves
// -------------------------------------------------------------------------------------------------
// {{{

#include "leaping_piece_lookup.inl"

// #note valid squares being check valid and not own pieces
u32 generate_knight_moves(u64 knights, u64 valid_squares, chess_move *move_buffer)
{
	u32 move_index = 0;

	while (knights)
	{
		u8 pos = lsb_pop(&knights);
		u64 valid_moves = knight_move_list[pos] & valid_squares;
		while (valid_moves)
		{
			u8 to = lsb_pop(&valid_moves);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

// #note valid squares being check valid and not own pieces
internal u32 generate_knight_moves_with_pin(u64 knights, u64 valid_squares,
											u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	while (knights)
	{
		u8 pos = lsb_pop(&knights);
		u64 valid_moves = knight_move_list[pos] & valid_squares;
		while (valid_moves)
		{
			u8 to = lsb_pop(&valid_moves);
			u64 to_bit = square_bit(to);
			if (to_bit & pin_buffer[pos])
			{
				move_buffer[move_index++] = gen_push(pos, to);
			}
		}
	}

	return move_index;
}

internal u32 generate_knight_captures(u64 knights, u64 target_pieces,
						  		  chess_move *move_buffer)
{
	u32 move_index = 0;

	while (knights)
	{
		u8 pos = lsb_pop(&knights);
		u64 captures = knight_move_list[pos] & target_pieces;
		while (captures)
		{
			u8 to = lsb_pop(&captures);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

// #note target_squares being status.capture_mask & target_pieces
internal u32 generate_knight_captures_with_pin(u64 knights, u64 target_squares,
											   u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	while (knights)
	{
		u8 pos = lsb_pop(&knights);
		u64 captures = knight_move_list[pos] & target_squares;
		while (captures)
		{
			u8 to = lsb_pop(&captures);
			u64 to_bit = square_bit(to);
			if (to_bit & pin_buffer[pos])
			{
				move_buffer[move_index++] = gen_push(pos, to);
			}
		}
	}

	return move_index;
}

internal u32 generate_king_moves(const chess_board *board, const check_status *status, u64 attack,
								 chess_move *move_buffer)
{
	u32 move_index = 0;

	chess_color color = board->state->side;
	u64 king_bit = board->color_bbs[color] & board->type_bbs[KING];
	assert(popcount(king_bit) == 1);
	u8 king_pos = lsb(king_bit);
	u64 basic_moves = king_move_list[king_pos];
	u64 valid_moves = basic_moves & ~board->color_bbs[color];
	u64 legal_moves = valid_moves & ~attack;

	while (legal_moves)
	{
		u8 to = lsb_pop(&legal_moves);
		move_buffer[move_index++] = gen_push(king_pos, to);
	}
	if (!(status->single_check || status->double_check))
	{
		if (can_castle_kingside(board->state->castling_rights, color))
		{
			assert(king_pos == E1 || king_pos == E8);
			u64 attack_mask = color == WHITE ? 0x70 : 0x7000000000000000;
			//print_old_and_new_bitboards(attack, attack_mask);
			u8 to = king_pos + 2;
			if (((attack & attack_mask) == 0) &&
				board->squares[king_pos + 1] == EMPTY_SQ &&
				board->squares[king_pos + 2] == EMPTY_SQ)
			{
				chess_move move = gen_castling(king_pos, to);
				move_buffer[move_index++] = move;
			}
		}
		if (can_castle_queenside(board->state->castling_rights, color))
		{
			assert(king_pos == E1 || king_pos == E8);
			u64 attack_mask = color == WHITE ? 0x1C : 0x1C00000000000000;
			//print_old_and_new_bitboards(attack, attack_mask);
			u8 to = king_pos - 2;
			if (((attack & attack_mask) == 0) &&
				board->squares[king_pos - 1] == EMPTY_SQ &&
				board->squares[king_pos - 2] == EMPTY_SQ &&
				board->squares[king_pos - 3] == EMPTY_SQ)
			{
				chess_move move = gen_castling(king_pos, to);
				move_buffer[move_index++] = move;
			}
		}
	}

	return move_index;
}

internal u32 generate_king_captures(const chess_board *board, const check_status *status,
									u64 attack, chess_move *move_buffer)
{
	u32 move_index = 0;

	chess_color color = board->state->side;
	u64 king_bit = board->color_bbs[color] & board->type_bbs[KING];
	assert(popcount(king_bit) == 1);
	u8 king_pos = lsb(king_bit);
	u64 basic_moves = king_move_list[king_pos];
	u64 valid_moves = basic_moves & ~board->color_bbs[color];
	u64 legal_moves = valid_moves & ~attack;
	u64 capture_moves = legal_moves & board->color_bbs[opposite_color(color)];

	while (capture_moves)
	{
		u8 to = lsb_pop(&capture_moves);
		move_buffer[move_index++] = gen_push(king_pos, to);
	}

	return move_index;
}

// }}}
// -------------------------------------------------------------------------------------------------
// #section sliding moves
// -------------------------------------------------------------------------------------------------
// {{{

// #todo documentation for sliding_piece_lookup
#include "sliding_piece_lookup.inl"

internal u64 bishop_move_mask(u8 pos, u64 occupancy)
{
	u64 magic_index = occupancy & bishop_relevant_occupancy_masks[pos];
	magic_index *= bishop_magic_numbers[pos];
	magic_index >>= 64 - bishop_relevant_occupancy_bits_count[pos];
	u64 result = bishop_valid_moves[pos][magic_index];
	return result;
}

internal u64 rook_move_mask(u8 pos, u64 occupancy)
{
	u64 magic_index = occupancy & rook_relevant_occupancy_masks[pos];
	magic_index *= rook_magic_numbers[pos];
	magic_index >>= 64 - rook_relevant_occupancy_bits_count[pos];
	u64 result = rook_valid_moves[pos][magic_index];
	return result;
}

internal u64 queen_move_mask(u8 pos, u64 occupancy)
{
	u64 bishop_moves = bishop_move_mask(pos, occupancy);
	u64 rook_moves = rook_move_mask(pos, occupancy);
	return bishop_moves | rook_moves;
}

u32 generate_bishop_moves(u64 bishops, u64 self_pieces, u64 occupancy,
						  chess_move *move_buffer)
{
	u32 move_index = 0;

	while (bishops)
	{
		u8 pos = lsb_pop(&bishops);
		u64 valid_squares = bishop_move_mask(pos, occupancy);
		u64 legal_squares = valid_squares & ~self_pieces;
		while (legal_squares)
		{
			u8 to = lsb_pop(&legal_squares);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

internal u32 generate_bishop_moves_pin_or_check(u64 bishops, u64 self_pieces, u64 occupancy,
												u64 check_valid_squares, u64 *pin_buffer,
												chess_move *move_buffer)
{
	u32 move_index = 0;

	while (bishops)
	{
		u8 pos = lsb_pop(&bishops);
		u64 valid_squares = bishop_move_mask(pos, occupancy);
		u64 legal_squares = valid_squares & ~self_pieces & check_valid_squares & pin_buffer[pos];
		while (legal_squares)
		{
			u8 to = lsb_pop(&legal_squares);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

internal u32 generate_bishop_captures(u64 bishops, u64 target_pieces, u64 occupancy,
									  chess_move *move_buffer)
{
	u32 move_index = 0;

	while (bishops)
	{
		u8 pos = lsb_pop(&bishops);
		u64 valid_squares = bishop_move_mask(pos, occupancy);
		u64 captures = valid_squares & target_pieces;
		while (captures)
		{
			u8 to = lsb_pop(&captures);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

// #note target_squares is target_pieces & status.capture_mask
internal u32 generate_bishop_captures_pin_or_check(u64 bishops, u64 target_squares, u64 occupancy,
												   u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	while (bishops)
	{
		u8 pos = lsb_pop(&bishops);
		u64 valid_squares = bishop_move_mask(pos, occupancy);
		u64 captures = valid_squares & target_squares & pin_buffer[pos];
		while (captures)
		{
			u8 to = lsb_pop(&captures);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

u32 generate_rook_moves(u64 rooks, u64 self_pieces, u64 occupancy,
						chess_move *move_buffer)
{
	u32 move_index = 0;

	while (rooks)
	{
		u8 pos = lsb_pop(&rooks);
		u64 valid_squares = rook_move_mask(pos, occupancy);
		u64 legal_squares = valid_squares & ~self_pieces;
		while (legal_squares)
		{
			u8 to = lsb_pop(&legal_squares);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

internal u32 generate_rook_moves_pin_or_check(u64 rooks, u64 self_pieces, u64 occupancy,
											  u64 check_valid_squares, u64 *pin_buffer,
											  chess_move *move_buffer)
{
	u32 move_index = 0;

	while (rooks)
	{
		u8 pos = lsb_pop(&rooks);
		u64 valid_squares = rook_move_mask(pos, occupancy);
		u64 legal_squares = valid_squares & ~self_pieces & check_valid_squares & pin_buffer[pos];
		while (legal_squares)
		{
			u8 to = lsb_pop(&legal_squares);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

internal u32 generate_rook_captures(u64 rooks, u64 target_pieces, u64 occupancy,
									chess_move *move_buffer)
{
	u32 move_index = 0;

	while (rooks)
	{
		u8 pos = lsb_pop(&rooks);
		u64 valid_squares = rook_move_mask(pos, occupancy);
		u64 captures = valid_squares & target_pieces;
		while (captures)
		{
			u8 to = lsb_pop(&captures);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

// #note target_squares is target_pieces & status.capture_mask
internal u32 generate_rook_captures_pin_or_check(u64 rooks, u64 target_squares, u64 occupancy,
												 u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	while (rooks)
	{
		u8 pos = lsb_pop(&rooks);
		u64 valid_squares = rook_move_mask(pos, occupancy);
		u64 captures = valid_squares & target_squares & pin_buffer[pos];
		while (captures)
		{
			u8 to = lsb_pop(&captures);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

u32 generate_queen_moves(u64 queens, u64 self_pieces, u64 occupancy,
						 chess_move *move_buffer)
{
	u32 move_index = 0;

	while (queens)
	{
		u8 pos = lsb_pop(&queens);
		u64 valid_squares = queen_move_mask(pos, occupancy);
		u64 legal_squares = valid_squares & ~self_pieces;
		while (legal_squares)
		{
			u8 to = lsb_pop(&legal_squares);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

internal u32 generate_queen_moves_pin_or_check(u64 queens, u64 self_pieces, u64 occupancy,
											   u64 check_valid_squares, u64 *pin_buffer,
											   chess_move *move_buffer)
{
	u32 move_index = 0;

	while (queens)
	{
		u8 pos = lsb_pop(&queens);
		u64 valid_squares = queen_move_mask(pos, occupancy);
		u64 legal_squares = valid_squares & ~self_pieces & check_valid_squares & pin_buffer[pos];
		while (legal_squares)
		{
			u8 to = lsb_pop(&legal_squares);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

internal u32 generate_queen_captures(u64 queens, u64 target_pieces, u64 occupancy,
									 chess_move *move_buffer)
{
	u32 move_index = 0;

	while (queens)
	{
		u8 pos = lsb_pop(&queens);
		u64 valid_squares = queen_move_mask(pos, occupancy);
		u64 captures = valid_squares & target_pieces;
		while (captures)
		{
			u8 to = lsb_pop(&captures);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}

// #note target_squares is target_pieces & status.capture_mask
internal u32 generate_queen_captures_pin_or_check(u64 queens, u64 target_squares, u64 occupancy,
									  		  u64 *pin_buffer, chess_move *move_buffer)
{
	u32 move_index = 0;

	while (queens)
	{
		u8 pos = lsb_pop(&queens);
		u64 valid_squares = queen_move_mask(pos, occupancy);
		u64 captures = valid_squares & target_squares & pin_buffer[pos];
		while (captures)
		{
			u8 to = lsb_pop(&captures);
			move_buffer[move_index++] = gen_push(pos, to);
		}
	}

	return move_index;
}
// }}}
// -------------------------------------------------------------------------------------------------
// #section pins
// -------------------------------------------------------------------------------------------------
// {{{

#define PIN_BUFFER_COUNT 64
//#include "platform_common.h"

// #todo documentation for squares_between_lookup
#include "squares_between_lookup.inl"

internal u64 bishop_xray_targets(u64 occupancy, u64 blockers, u8 pos)
{
	u64 attacks = bishop_move_mask(pos, occupancy);
	blockers &= attacks;
	return attacks ^ bishop_move_mask(pos, occupancy ^ blockers);
}

internal u64 rook_xray_targets(u64 occupancy, u64 blockers, u8 pos)
{
	u64 attacks = rook_move_mask(pos, occupancy);
	blockers &= attacks;
	return attacks ^ rook_move_mask(pos, occupancy ^ blockers);
}

internal bool find_pins(const chess_board *board, u64 *pin_buffer)
{
	bool found_pins = false;
	// #note set each pin bitboard to full valid (i.e. set all the bits)
	memset(pin_buffer, 0xFF, PIN_BUFFER_COUNT * sizeof(u64));

	chess_color color = board->state->side;
	chess_color oppo_color = opposite_color(color);
	u64 king_bit = board->color_bbs[color] & board->type_bbs[KING];
	assert(king_bit);
	u8 king_pos = lsb(king_bit);

	u64 occupancy = board->color_bbs[WHITE] | board->color_bbs[BLACK];
	u64 blockers = board->color_bbs[color];
	u64 bishop_xray = bishop_xray_targets(occupancy, blockers, king_pos);
	u64 bishop_pinners = bishop_xray & board->color_bbs[oppo_color] &
				  		 (board->type_bbs[BISHOP] | board->type_bbs[QUEEN]);
	while (bishop_pinners)
	{
		u8 pos = lsb_pop(&bishop_pinners);
		u64 ray = squares_between[pos][king_pos];
		u64 pinned_pieces = ray & board->color_bbs[color];
		u64 pin_legal_squares = (ray | square_bit(pos)) & (~(pinned_pieces));
		while (pinned_pieces)
		{
			u8 pin_pos = lsb_pop(&pinned_pieces);
			pin_buffer[pin_pos] = pin_legal_squares;
			found_pins = true;
		}
	}

	u64 rook_xray = rook_xray_targets(occupancy, blockers, king_pos);
	u64 rook_pinners = rook_xray & board->color_bbs[oppo_color] &
				  	   (board->type_bbs[ROOK] | board->type_bbs[QUEEN]);
	while (rook_pinners)
	{
		u8 pos = lsb_pop(&rook_pinners);
		u64 ray = squares_between[pos][king_pos];
		u64 pinned_pieces = ray & board->color_bbs[color];
		u64 pin_legal_squares = (ray | square_bit(pos)) & (~(pinned_pieces));
		while (pinned_pieces)
		{
			u8 pin_pos = lsb_pop(&pinned_pieces);
			pin_buffer[pin_pos] = pin_legal_squares;
			found_pins = true;
		}
	}

	return found_pins;
}

// }}}
// -------------------------------------------------------------------------------------------------
// #section kingless attack bitboard
// -------------------------------------------------------------------------------------------------
// {{{

internal u64 pawn_attack_bb(u64 pawns, chess_color color)
{
	assert((pawns & rank_bbs[RANK_1]) == 0);
	assert((pawns & rank_bbs[RANK_8]) == 0);

	u64 left_attack_bb = pawn_attack_left_bb(pawns & ~file_bbs[FILE_A], color);
	u64 right_attack_bb = pawn_attack_right_bb(pawns & ~file_bbs[FILE_H], color);
	u64 result = left_attack_bb | right_attack_bb;
	return result;
}

internal u64 knight_attack_bb(u64 knights)
{
	u64 result = 0;
	while (knights)
	{
		u8 pos = lsb_pop(&knights);
		u64 basic_moves = knight_move_list[pos];
		result |= basic_moves;
	}
	return result;
}

internal u64 bishop_attack_bb(u64 bishops, u64 occupancy)
{
	u64 result = 0;
	while (bishops)
	{
		u8 pos = lsb_pop(&bishops);
		u64 valid_squares = bishop_move_mask(pos, occupancy);
		result |= valid_squares;
	}
	return result;
}

internal u64 rook_attack_bb(u64 rooks, u64 occupancy)
{
	u64 result = 0;
	while (rooks)
	{
		u8 pos = lsb_pop(&rooks);
		u64 valid_squares = rook_move_mask(pos, occupancy);
		result |= valid_squares;
	}
	return result;
}

internal u64 queen_attack_bb(u64 queens, u64 occupancy)
{
	u64 result = 0;
	while (queens)
	{
		u8 pos = lsb_pop(&queens);
		u64 bishop_squares = bishop_move_mask(pos, occupancy);
		u64 rook_squares = rook_move_mask(pos, occupancy);
		u64 valid_squares = bishop_squares | rook_squares;
		result |= valid_squares;
	}
	return result;
}

internal u64 king_attack_bb(u64 king_bit)
{
	assert(popcount(king_bit) == 1);
	u8 king_pos = lsb(king_bit);
	u64 result = king_move_list[king_pos];
	return result;
}

internal u64 kingless_attack_bb(const chess_board *board, chess_color color)
{
	u64 result = 0;

	chess_color oppo_color = opposite_color(color);
	u64 self_pieces = board->color_bbs[color];
	u64 oppo_pieces = board->color_bbs[oppo_color] &
		~(board->color_bbs[oppo_color] & board->type_bbs[KING]);
	u64 occupancy = self_pieces | oppo_pieces;

	u64 pawns = self_pieces & board->type_bbs[PAWN];
	result |= pawn_attack_bb(pawns, color);
	u64 knights = self_pieces & board->type_bbs[KNIGHT];
	result |= knight_attack_bb(knights);
	u64 bishops = self_pieces & board->type_bbs[BISHOP];
	result |= bishop_attack_bb(bishops, occupancy);
	u64 rooks = self_pieces & board->type_bbs[ROOK];
	result |= rook_attack_bb(rooks, occupancy);
	u64 queens = self_pieces & board->type_bbs[QUEEN];
	result |= queen_attack_bb(queens, occupancy);
	u64 kings = board->color_bbs[color] & board->type_bbs[KING];
	result |= king_attack_bb(kings);

	return result;
}

// }}}
// -------------------------------------------------------------------------------------------------
// #section gen all
// -------------------------------------------------------------------------------------------------
// {{{
u32 generate_all_moves(const chess_board *board, chess_move *move_buffer)
{
	u32 num_moves = 0;
	u64 kingless_attack = kingless_attack_bb(board, opposite_color(board->state->side));
	check_status status = get_check_status(board);

	if (status.double_check)
	{
		// #note early out
		num_moves += generate_king_moves(board, &status, kingless_attack, move_buffer);
		return num_moves;
	}

	u64 self_pieces = board->color_bbs[board->state->side];
	u64 target_pieces = board->color_bbs[opposite_color(board->state->side)];
	u64 occupancy = self_pieces | target_pieces;
	u64 check_valid_squares = status.push_mask | status.capture_mask;

	// #todo remove board from pawn move generator?
	//u64 pawns = self_pieces & board->type_bbs[PAWN];
	u64 knights = self_pieces & board->type_bbs[KNIGHT];
	u64 bishops = self_pieces & board->type_bbs[BISHOP];
	u64 rooks = self_pieces & board->type_bbs[ROOK];
	u64 queens = self_pieces & board->type_bbs[QUEEN];

	u64 pin_buffer[PIN_BUFFER_COUNT];
	bool pins_found = find_pins(board, pin_buffer);
	if (pins_found)
	{
		u32 pawn_moves = generate_pawn_moves_with_pin(board, &status, pin_buffer, move_buffer);
		num_moves += pawn_moves;

		u32 knight_moves = generate_knight_moves_with_pin(knights,
														  check_valid_squares & ~self_pieces,
														  pin_buffer, move_buffer + num_moves);
		num_moves += knight_moves;
	}
	else
	{
		u32 pawn_moves = generate_pawn_moves(board, &status, move_buffer);
		num_moves += pawn_moves;

		u32 knight_moves = generate_knight_moves(knights, check_valid_squares & ~self_pieces,
												 move_buffer + num_moves);
		num_moves += knight_moves;
	}

	if (pins_found || status.single_check)
	{
		u32 bishop_moves = generate_bishop_moves_pin_or_check(bishops, self_pieces, occupancy,
															  check_valid_squares, pin_buffer,
															  move_buffer + num_moves);
		num_moves += bishop_moves;

		u32 rook_moves = generate_rook_moves_pin_or_check(rooks, self_pieces, occupancy,
														  check_valid_squares, pin_buffer,
														  move_buffer + num_moves);
		num_moves += rook_moves;

		u32 queen_moves = generate_queen_moves_pin_or_check(queens, self_pieces, occupancy,
															check_valid_squares, pin_buffer,
															move_buffer + num_moves);
		num_moves += queen_moves;
	}
	else
	{
		u32 bishop_moves = generate_bishop_moves(bishops, self_pieces, occupancy,
												 move_buffer + num_moves);
		num_moves += bishop_moves;

		u32 rook_moves = generate_rook_moves(rooks, self_pieces, occupancy,
											 move_buffer + num_moves);
		num_moves += rook_moves;

		u32 queen_moves = generate_queen_moves(queens, self_pieces, occupancy,
											   move_buffer + num_moves);
		num_moves += queen_moves;
	}


	u32 king_moves = generate_king_moves(board, &status, kingless_attack,
									 move_buffer + num_moves);
	num_moves += king_moves;

	return num_moves;
}

u32 generate_all_captures(const chess_board *board, chess_move *move_buffer)
{
	u32 num_moves = 0;
	u64 kingless_attack = kingless_attack_bb(board, opposite_color(board->state->side));
	check_status status = get_check_status(board);

	if (status.double_check)
	{
		// #todo only king moves
		// #note early out
		num_moves += generate_king_captures(board, &status, kingless_attack, move_buffer);
		return num_moves;
	}

	u64 self_pieces = board->color_bbs[board->state->side];
	u64 target_pieces = board->color_bbs[opposite_color(board->state->side)];
	u64 target_squares = status.capture_mask & target_pieces;
	u64 occupancy = self_pieces | target_pieces;

	// #todo remove board from pawn move generator?
	//u64 pawns = self_pieces & board->type_bbs[PAWN];
	u64 knights = self_pieces & board->type_bbs[KNIGHT];
	u64 bishops = self_pieces & board->type_bbs[BISHOP];
	u64 rooks = self_pieces & board->type_bbs[ROOK];
	u64 queens = self_pieces & board->type_bbs[QUEEN];

	u64 pin_buffer[PIN_BUFFER_COUNT];
	bool pins_found = find_pins(board, pin_buffer);
	if (pins_found)
	{
		u32 pawn_moves = generate_pawn_captures_with_pin(board, &status, pin_buffer, move_buffer);
		num_moves += pawn_moves;

		u32 knight_moves = generate_knight_captures_with_pin(knights, target_squares, pin_buffer,
															 move_buffer + num_moves);
		num_moves += knight_moves;
	}
	else
	{
		u32 pawn_moves = generate_pawn_captures(board, &status, move_buffer);
		num_moves += pawn_moves;

		u32 knight_moves = generate_knight_captures(knights, target_squares,
													move_buffer + num_moves);
		num_moves += knight_moves;
	}

	if (pins_found || status.single_check)
	{
		u32 bishop_moves = generate_bishop_captures_pin_or_check(bishops, target_squares, occupancy,
																 pin_buffer, move_buffer + num_moves);
		num_moves += bishop_moves;

		u32 rook_moves = generate_rook_captures_pin_or_check(rooks, target_squares, occupancy,
															 pin_buffer, move_buffer + num_moves);
		num_moves += rook_moves;

		u32 queen_moves = generate_queen_captures_pin_or_check(queens, target_squares, occupancy,
															   pin_buffer, move_buffer + num_moves);
		num_moves += queen_moves;
	}
	else
	{
		u32 bishop_moves = generate_bishop_captures(bishops, target_pieces, occupancy,
													move_buffer + num_moves);
		num_moves += bishop_moves;

		u32 rook_moves = generate_rook_captures(rooks, target_pieces, occupancy,
												move_buffer + num_moves);
		num_moves += rook_moves;

		u32 queen_moves = generate_queen_captures(queens, target_pieces, occupancy,
												  move_buffer + num_moves);
		num_moves += queen_moves;
	}


	u32 king_moves = generate_king_captures(board, &status, kingless_attack,
									 move_buffer + num_moves);
	num_moves += king_moves;

	return num_moves;
}

// }}}
// -------------------------------------------------------------------------------------------------
// #section find checkers
// -------------------------------------------------------------------------------------------------
// {{{

u64 find_pawn_checkers(u8 pos, chess_color color, u64 target_pawns)
{
	u64 result = 0;
	u64 pos_bit = square_bit(pos);
	u64 left_attack_bb = pawn_attack_left_bb(pos_bit & ~file_bbs[FILE_A], color);
	result = left_attack_bb & target_pawns;
	if (result == 0)
	{
		u64 right_attack_bb = pawn_attack_right_bb(pos_bit & ~file_bbs[FILE_H], color);
		result |= right_attack_bb & target_pawns;
	}

	assert(result == 0 || popcount(result) == 1);

	return result;
}

u64 find_knight_checkers(u8 pos, u64 target_knights)
{
	u64 knight_squares = knight_move_list[pos];
	u64 knights = knight_squares & target_knights;
	return knights;
}

u64 find_bishop_checkers(u8 pos, u64 target_bishops, u64 occupancy)
{
	u64 bishop_squares = bishop_move_mask(pos, occupancy);
	u64 bishops = bishop_squares & target_bishops;
	return bishops;
}

u64 find_rook_checkers(u8 pos, u64 target_rooks, u64 occupancy)
{
	u64 rook_squares = rook_move_mask(pos, occupancy);
	u64 rooks = rook_squares & target_rooks;
	return rooks;
}

u64 find_queen_checkers(u8 pos, u64 target_queens, u64 occupancy)
{
	u64 queen_squares = queen_move_mask(pos, occupancy);
	u64 queens = queen_squares & target_queens;
	return queens;
}

// }}}
