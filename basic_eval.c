#include "basic_eval.h"
#include "chess_board.h"
#include "move_generation.h"

internal const u64 file_bbs[NUM_FILES] =
{
	0x0101010101010101,
	0x0202020202020202,
	0x0404040404040404,
	0x0808080808080808,
	0x1010101010101010,
	0x2020202020202020,
	0x4040404040404040,
	0x8080808080808080
};

internal const u64 white_passed_mask[64] =
{
	[A1] = 0x0303030303030300, [B1] = 0x0707070707070700, [C1] = 0x0e0e0e0e0e0e0e00, [D1] = 0x1c1c1c1c1c1c1c00,
	[E1] = 0x3838383838383800, [F1] = 0x7070707070707000, [G1] = 0xe0e0e0e0e0e0e000, [H1] = 0xc0c0c0c0c0c0c000,
	[A2] = 0x0303030303030000, [B2] = 0x0707070707070000, [C2] = 0x0e0e0e0e0e0e0000, [D2] = 0x1c1c1c1c1c1c0000,
	[E2] = 0x3838383838380000, [F2] = 0x7070707070700000, [G2] = 0xe0e0e0e0e0e00000, [H2] = 0xc0c0c0c0c0c00000,
	[A3] = 0x0303030303000000, [B3] = 0x0707070707000000, [C3] = 0x0e0e0e0e0e000000, [D3] = 0x1c1c1c1c1c000000,
	[E3] = 0x3838383838000000, [F3] = 0x7070707070000000, [G3] = 0xe0e0e0e0e0000000, [H3] = 0xc0c0c0c0c0000000,
	[A4] = 0x0303030300000000, [B4] = 0x0707070700000000, [C4] = 0x0e0e0e0e00000000, [D4] = 0x1c1c1c1c00000000,
	[E4] = 0x3838383800000000, [F4] = 0x7070707000000000, [G4] = 0xe0e0e0e000000000, [H4] = 0xc0c0c0c000000000,
	[A5] = 0x0303030000000000, [B5] = 0x0707070000000000, [C5] = 0x0e0e0e0000000000, [D5] = 0x1c1c1c0000000000,
	[E5] = 0x3838380000000000, [F5] = 0x7070700000000000, [G5] = 0xe0e0e00000000000, [H5] = 0xc0c0c00000000000,
	[A6] = 0x0303000000000000, [B6] = 0x0707000000000000, [C6] = 0x0e0e000000000000, [D6] = 0x1c1c000000000000,
	[E6] = 0x3838000000000000, [F6] = 0x7070000000000000, [G6] = 0xe0e0000000000000, [H6] = 0xc0c0000000000000,
	[A7] = 0x0300000000000000, [B7] = 0x0700000000000000, [C7] = 0x0e00000000000000, [D7] = 0x1c00000000000000,
	[E7] = 0x3800000000000000, [F7] = 0x7000000000000000, [G7] = 0xe000000000000000, [H7] = 0xc000000000000000,
	[A8] = 0x0000000000000000, [B8] = 0x0000000000000000, [C8] = 0x0000000000000000, [D8] = 0x0000000000000000,
	[E8] = 0x0000000000000000, [F8] = 0x0000000000000000, [G8] = 0x0000000000000000, [H8] = 0x0000000000000000
};

internal const u64 black_passed_mask[64] =
{
	[A1] = 0x0000000000000000, [B1] = 0x0000000000000000, [C1] = 0x0000000000000000, [D1] = 0x0000000000000000,
	[E1] = 0x0000000000000000, [F1] = 0x0000000000000000, [G1] = 0x0000000000000000, [H1] = 0x0000000000000000,
	[A2] = 0x0000000000000003, [B2] = 0x0000000000000007, [C2] = 0x000000000000000e, [D2] = 0x000000000000001c,
	[E2] = 0x0000000000000038, [F2] = 0x0000000000000070, [G2] = 0x00000000000000e0, [H2] = 0x00000000000000c0,
	[A3] = 0x0000000000000303, [B3] = 0x0000000000000707, [C3] = 0x0000000000000e0e, [D3] = 0x0000000000001c1c,
	[E3] = 0x0000000000003838, [F3] = 0x0000000000007070, [G3] = 0x000000000000e0e0, [H3] = 0x000000000000c0c0,
	[A4] = 0x0000000000030303, [B4] = 0x0000000000070707, [C4] = 0x00000000000e0e0e, [D4] = 0x00000000001c1c1c,
	[E4] = 0x0000000000383838, [F4] = 0x0000000000707070, [G4] = 0x0000000000e0e0e0, [H4] = 0x0000000000c0c0c0,
	[A5] = 0x0000000003030303, [B5] = 0x0000000007070707, [C5] = 0x000000000e0e0e0e, [D5] = 0x000000001c1c1c1c,
	[E5] = 0x0000000038383838, [F5] = 0x0000000070707070, [G5] = 0x00000000e0e0e0e0, [H5] = 0x00000000c0c0c0c0,
	[A6] = 0x0000000303030303, [B6] = 0x0000000707070707, [C6] = 0x0000000e0e0e0e0e, [D6] = 0x0000001c1c1c1c1c,
	[E6] = 0x0000003838383838, [F6] = 0x0000007070707070, [G6] = 0x000000e0e0e0e0e0, [H6] = 0x000000c0c0c0c0c0,
	[A7] = 0x0000030303030303, [B7] = 0x0000070707070707, [C7] = 0x00000e0e0e0e0e0e, [D7] = 0x00001c1c1c1c1c1c,
	[E7] = 0x0000383838383838, [F7] = 0x0000707070707070, [G7] = 0x0000e0e0e0e0e0e0, [H7] = 0x0000c0c0c0c0c0c0,
	[A8] = 0x0003030303030303, [B8] = 0x0007070707070707, [C8] = 0x000e0e0e0e0e0e0e, [D8] = 0x001c1c1c1c1c1c1c,
	[E8] = 0x0038383838383838, [F8] = 0x0070707070707070, [G8] = 0x00e0e0e0e0e0e0e0, [H8] = 0x00c0c0c0c0c0c0c0
};

internal const u64 isolated_mask[64] =
{
	[A1] = 0x0202020202020202, [B1] = 0x0505050505050505, [C1] = 0x0a0a0a0a0a0a0a0a, [D1] = 0x1414141414141414,
	[E1] = 0x2828282828282828, [F1] = 0x5050505050505050, [G1] = 0xa0a0a0a0a0a0a0a0, [H1] = 0x4040404040404040,
	[A2] = 0x0202020202020202, [B2] = 0x0505050505050505, [C2] = 0x0a0a0a0a0a0a0a0a, [D2] = 0x1414141414141414,
	[E2] = 0x2828282828282828, [F2] = 0x5050505050505050, [G2] = 0xa0a0a0a0a0a0a0a0, [H2] = 0x4040404040404040,
	[A3] = 0x0202020202020202, [B3] = 0x0505050505050505, [C3] = 0x0a0a0a0a0a0a0a0a, [D3] = 0x1414141414141414,
	[E3] = 0x2828282828282828, [F3] = 0x5050505050505050, [G3] = 0xa0a0a0a0a0a0a0a0, [H3] = 0x4040404040404040,
	[A4] = 0x0202020202020202, [B4] = 0x0505050505050505, [C4] = 0x0a0a0a0a0a0a0a0a, [D4] = 0x1414141414141414,
	[E4] = 0x2828282828282828, [F4] = 0x5050505050505050, [G4] = 0xa0a0a0a0a0a0a0a0, [H4] = 0x4040404040404040,
	[A5] = 0x0202020202020202, [B5] = 0x0505050505050505, [C5] = 0x0a0a0a0a0a0a0a0a, [D5] = 0x1414141414141414,
	[E5] = 0x2828282828282828, [F5] = 0x5050505050505050, [G5] = 0xa0a0a0a0a0a0a0a0, [H5] = 0x4040404040404040,
	[A6] = 0x0202020202020202, [B6] = 0x0505050505050505, [C6] = 0x0a0a0a0a0a0a0a0a, [D6] = 0x1414141414141414,
	[E6] = 0x2828282828282828, [F6] = 0x5050505050505050, [G6] = 0xa0a0a0a0a0a0a0a0, [H6] = 0x4040404040404040,
	[A7] = 0x0202020202020202, [B7] = 0x0505050505050505, [C7] = 0x0a0a0a0a0a0a0a0a, [D7] = 0x1414141414141414,
	[E7] = 0x2828282828282828, [F7] = 0x5050505050505050, [G7] = 0xa0a0a0a0a0a0a0a0, [H7] = 0x4040404040404040,
	[A8] = 0x0202020202020202, [B8] = 0x0505050505050505, [C8] = 0x0a0a0a0a0a0a0a0a, [D8] = 0x1414141414141414,
	[E8] = 0x2828282828282828, [F8] = 0x5050505050505050, [G8] = 0xa0a0a0a0a0a0a0a0, [H8] = 0x4040404040404040
};

const s16 pawn_isolated = -10;
const s16 pawn_passed[NUM_RANKS] = { 0, 5, 10, 20, 35, 60, 100, 200 };
const s16 rook_open_file = 10;
const s16 rook_semi_open_file = 5;
const s16 queen_open_file = 5;
const s16 queen_semi_open_file = 3;
const s16 bishop_pair = 30;

internal const s16 pawn_table[64] =
{
	  0,   0,   0,   0,   0,   0,   0,   0,
	 10,  10,   0, -10, -10,   0,  10,  10,
	  5,   0,   0,   5,   5,   0,   0,   5,
	  0,   0,  10,  20,  20,  10,   0,   0,
	  5,   5,   5,  10,  10,   5,   5,   5,
	 10,  10,  10,  20,  20,  10,  10,  10,
	 20,  20,  20,  30,  30,  20,  20,  20,
	  0,   0,   0,   0,   0,   0,   0,   0
};

internal const s16 knight_table[64] =
{
	  0, -10,   0,   0,   0,   0, -10,   0,
	  0,   0,   0,   5,   5,   0,   0,   0,
	  0,   0,  10,  10,  10,  10,   0,   0,
	  0,   0,  10,  20,  20,  10,   5,   0,
	  5,  10,  15,  20,  20,  15,  10,   5,
	  5,  10,  10,  20,  20,  10,  10,   5,
	  0,   0,   5,  10,  10,   5,   0,   0,
	  0,   0,   0,   0,   0,   0,   0,   0
};

internal const s16 bishop_table[64] =
{
	   0,   0, -10,   0,   0, -10,   0,   0,
	   0,   0,   0,  10,  10,   0,   0,   0,
	   0,   0,  10,  15,  15,  10,   0,   0,
	   0,  10,  15,  20,  20,  15,  10,   0,
	   0,  10,  15,  20,  20,  15,  10,   0,
	   0,   0,  10,  15,  15,  10,   0,   0,
	   0,   0,   0,  10,  10,   0,   0,   0,
	   0,   0,   0,   0,   0,   0,   0,   0
};

internal const s16 rook_table[64] =
{
	   0,   0,   5,  10,  10,   5,   0,   0,
	   0,   0,   5,  10,  10,   5,   0,   0,
	   0,   0,   5,  10,  10,   5,   0,   0,
	   0,   0,   5,  10,  10,   5,   0,   0,
	   0,   0,   5,  10,  10,   5,   0,   0,
	   0,   0,   5,  10,  10,   5,   0,   0,
	  25,  25,  25,  25,  25,  25,  25,  25,
	   0,   0,   5,  10,  10,   5,   0,   0
};

internal const s16 king_opening_table[64] =
{
	  0,   5,   5, -10, -10,   0,  10,   5,
	-10, -10, -10, -10, -10, -10, -10, -10,
	-30, -30, -30, -30, -30, -30, -30, -30,
	-70, -70, -70, -70, -70, -70, -70, -70,
	-70, -70, -70, -70, -70, -70, -70, -70,
	-70, -70, -70, -70, -70, -70, -70, -70,
	-70, -70, -70, -70, -70, -70, -70, -70,
	-70, -70, -70, -70, -70, -70, -70, -70
};

internal const s16 king_endgame_table[64] =
{
	-50, -10,   0,   0,   0,   0, -10, -50,
	-10,   0,  10,  10,  10,  10,   0, -10,
	  0,  10,  20,  20,  20,  20,  10,   0,
	  0,  10,  20,  40,  40,  20,  10,   0,
	  0,  10,  20,  40,  40,  20,  10,   0,
	  0,  10,  20,  20,  20,  20,  10,   0,
	-10,   0,  10,  10,  10,  10,   0, -10,
	-50, -10,   0,   0,   0,   0, -10, -50
};

internal u8 mirror[64] =
{
	56, 57, 58, 59, 60, 61, 62, 63,
	48, 49, 50, 51, 52, 53, 54, 55,
	40, 41, 42, 43, 44, 45, 46, 47,
	32, 33, 34, 35, 36, 37, 38, 39,
	24, 25, 26, 27, 28, 29, 30, 31,
	16, 17, 18, 19, 20, 21, 22, 23,
	 8,  9, 10, 11, 12, 13, 14, 15,
	 0,  1,  2,  3,  4,  5,  6,  7
};

internal s16 piece_value[NUM_PIECE_TYPES] =
{
	[NONE]   = 0,
	[PAWN]   = 100,
	[KNIGHT] = 325,
	[BISHOP] = 325,
	[ROOK]   = 550,
	[QUEEN]  = 1000,
	[KING]   = 0
};

internal s16 material_score_type(const chess_board *board, chess_color color, chess_piece_type type)
{
	return popcount(board->color_bbs[color] & board->type_bbs[type]) * piece_value[type];
}

internal s16 material_score(const chess_board *board, chess_color color)
{
	s16 value = 0;
	value += material_score_type(board, color, PAWN);
	value += material_score_type(board, color, KNIGHT);
	value += material_score_type(board, color, BISHOP);
	value += material_score_type(board, color, ROOK);
	value += material_score_type(board, color, QUEEN);
	return value;
}

s16 basic_eval(const chess_board *board)
{
	const s16 endgame_mat = piece_value[ROOK] + 2 * piece_value[KNIGHT] + 2 * piece_value[PAWN];
	s16 white_score = material_score(board, WHITE);
	s16 black_score = material_score(board, BLACK);
	const u64 white_pieces = board->color_bbs[WHITE];
	const u64 black_pieces = board->color_bbs[BLACK];
	const u64 pawns = board->type_bbs[PAWN];
	const u64 knights = board->type_bbs[KNIGHT];
	const u64 bishops = board->type_bbs[BISHOP];
	const u64 rooks = board->type_bbs[ROOK];
	const u64 queens = board->type_bbs[QUEEN];
	const u64 kings = board->type_bbs[KING];

	u64 wps = white_pieces & pawns;
	while (wps)
	{
		u8 pos = lsb_pop(&wps);
		white_score += pawn_table[pos];

		if ((isolated_mask[pos] & white_pieces & pawns) == 0)
		{
			white_score += pawn_isolated;
		}

		if ((white_passed_mask[pos] & black_pieces & pawns) == 0)
		{
			white_score += pawn_passed[pos / 8];
		}
	}

	u64 bps = black_pieces & pawns;
	while (bps)
	{
		u8 pos = lsb_pop(&bps);
		black_score += pawn_table[mirror[pos]];

		if ((isolated_mask[pos] & black_pieces & pawns) == 0)
		{
			black_score += pawn_isolated;
		}

		if ((black_passed_mask[pos] & white_pieces & pawns) == 0)
		{
			black_score += pawn_passed[7 - (pos / 8)];
		}
	}

	u64 wns = white_pieces & knights;
	while (wns)
	{
		u8 pos = lsb_pop(&wns);
		white_score += knight_table[pos];
	}

	u64 bns = black_pieces & knights;
	while (bns)
	{
		u8 pos = lsb_pop(&bns);
		black_score += knight_table[mirror[pos]];
	}

	u64 wbs = white_pieces & bishops;
	if (popcount(wbs) >= 2)
	{
		white_score += bishop_pair;
	}
	while (wbs)
	{
		u8 pos = lsb_pop(&wbs);
		white_score += bishop_table[pos];
	}

	u64 bbs = black_pieces & bishops;
	if (popcount(bbs) >= 2)
	{
		black_score += bishop_pair;
	}
	while (bbs)
	{
		u8 pos = lsb_pop(&bbs);
		black_score += bishop_table[mirror[pos]];
	}

	u64 wrs = white_pieces & rooks;
	while (wrs)
	{
		u8 pos = lsb_pop(&wrs);
		white_score += rook_table[pos];

		if ((pawns & file_bbs[pos % 8]) == 0)
		{
			white_score += rook_open_file;
		}
		else if ((pawns & white_pieces & file_bbs[pos % 8]) == 0)
		{
			white_score += rook_semi_open_file;
		}
	}

	u64 brs = black_pieces & rooks;
	while (brs)
	{
		u8 pos = lsb_pop(&brs);
		black_score += rook_table[mirror[pos]];

		if ((pawns & file_bbs[pos % 8]) == 0)
		{
			black_score += rook_open_file;
		}
		else if ((pawns & black_pieces & file_bbs[pos % 8]) == 0)
		{
			black_score += rook_semi_open_file;
		}
	}

	u64 wqs = white_pieces & queens;
	while (wqs)
	{
		u8 pos = lsb_pop(&wqs);

		if ((pawns & file_bbs[pos % 8]) == 0)
		{
			white_score += queen_open_file;;
		}
		else if ((pawns & white_pieces & file_bbs[pos % 8]) == 0)
		{
			white_score += queen_semi_open_file;
		}
	}

	u64 bqs = black_pieces & queens;
	while (bqs)
	{
		u8 pos = lsb_pop(&bqs);

		if ((pawns & file_bbs[pos % 8]) == 0)
		{
			black_score += queen_open_file;
		}
		else if ((pawns & black_pieces & file_bbs[pos % 8]) == 0)
		{
			black_score += queen_semi_open_file;
		}
	}

	u64 wk = white_pieces & kings;
	assert(popcount(wk) == 1);
	u8 wk_pos = lsb(wk);
	if (material_score(board, BLACK) <= endgame_mat)
	{
		white_score += king_endgame_table[wk_pos];
	}
	else
	{
		white_score += king_opening_table[wk_pos];
	}

	u64 bk = black_pieces & kings;
	assert(popcount(bk) == 1);
	u8 bk_pos = lsb(bk);
	if (material_score(board, WHITE) <= endgame_mat)
	{
		black_score += king_endgame_table[mirror[bk_pos]];
	}
	else
	{
		black_score += king_opening_table[mirror[bk_pos]];
	}

	s16 score = white_score - black_score;
	if (board->state->side == WHITE)
	{
		return score;
	}
	else
	{
		return -score;
	}
}
