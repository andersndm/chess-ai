#ifndef CHESS_AI_RANDOM_H
#define CHESS_AI_RANDOM_H

#include "common.h"

#ifndef DEBUG_RANDOM
#define RANDOM_BUFFER_SIZE 2048
#else
#if DEBUG_RANDOM
#define RANDOM_BUFFER_SIZE 512
#else
#define RANDOM_BUFFER_SIZE 2048
#endif
#endif

// #todo rnd functions should be tested for quality

// #note the struct should be zero initialized
typedef struct random_series
{
	u32 idx;
	u8 bytes[RANDOM_BUFFER_SIZE];
	u32 seed;
} random_series;

random_series rnd_new_series(void);
random_series rnd_new_series_with_seed(u32 seed);

bool rnd_bool(random_series *rnd);

u8 rnd_u8(random_series *rnd);
u16 rnd_u16(random_series *rnd);
u32 rnd_u32(random_series *rnd);
u64 rnd_u64(random_series *rnd);

s8 rnd_s8(random_series *rnd);
s16 rnd_s16(random_series *rnd);
s32 rnd_s32(random_series *rnd);
s64 rnd_s64(random_series *rnd);

u8 rnd_u8_in_range(random_series *rnd, u8 min, u8 max);
u16 rnd_u16_in_range(random_series *rnd, u16 min, u16 max);
u32 rnd_u32_in_range(random_series *rnd, u32 min, u32 max);
u64 rnd_u64_in_range(random_series *rnd, u64 min, u64 max);

u32 rnd_choice_u32(random_series *rnd, u32 count);

f32 rnd_unilateral32(random_series *rnd);
f64 rnd_unilateral64(random_series *rnd);

f32 rnd_bilateral32(random_series *rnd);
f64 rnd_bilateral64(random_series *rnd);

f32 rnd_uniform_in_range32(random_series *rnd, f32 min, f32 max);
f64 rnd_uniform_in_range64(random_series *rnd, f64 min, f64 max);

#endif // !CHESS_AI_RANDOM_H
