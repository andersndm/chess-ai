#ifndef CHESS_H
#define CHESS_H

/* #todo
	remove squares structure and the use of file and rank for search, remove dead/unuseful code
*/

#include "common.h"

#define INVALID_EN_PASSANT 0

typedef enum chess_color
{
	WHITE,
	BLACK,
	NUM_COLORS
} chess_color;

chess_color opposite_color(chess_color color);

typedef enum chess_piece_type
{
	NONE     = 0x0, // 0000 0000
	PAWN     = 0x1, // 0000 0001
	KNIGHT   = 0x2, // 0000 0010
	BISHOP   = 0x3, // 0000 0011
	ROOK     = 0x4, // 0000 0100
	QUEEN    = 0x5, // 0000 0101
	// #note #important king must be 6 for the neural network
	KING     = 0x6, // 0000 0110
	NUM_PIECE_TYPES
} chess_piece_type;

typedef enum chess_piece
{
	EMPTY_SQ,
	W_PAWN = PAWN,     W_KNIGHT, W_BISHOP, W_ROOK, W_QUEEN, W_KING,
	B_PAWN = PAWN + 8, B_KNIGHT, B_BISHOP, B_ROOK, B_QUEEN, B_KING,

	NUM_PIECES
} chess_piece;

typedef enum chess_castling
{
	WHITE_KINGSIDE  = 0x1, // 0001
	WHITE_QUEENSIDE = 0x2, // 0010
	BLACK_KINGSIDE  = 0x4, // 0100
	BLACK_QUEENSIDE = 0x8  // 1000
} chess_castling;

static inline bool can_castle_kingside(u8 rights, chess_color color)
{
	return color == WHITE ? (rights & WHITE_KINGSIDE) != 0 : (rights & BLACK_KINGSIDE) != 0;
}

static inline bool can_castle_queenside(u8 rights, chess_color color)
{
	return color == WHITE ? (rights & WHITE_QUEENSIDE) != 0 : (rights & BLACK_QUEENSIDE) != 0;
}

typedef enum chess_rank
{
	RANK_1, RANK_2, RANK_3, RANK_4, RANK_5, RANK_6, RANK_7, RANK_8, RANK_NONE, NUM_RANKS = RANK_NONE
} chess_rank;

typedef enum chess_file
{
	FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, FILE_NONE, NUM_FILES = FILE_NONE
} chess_file;

typedef enum chess_positions
{
	A1, B1, C1, D1, E1, F1, G1, H1,
	A2, B2, C2, D2, E2, F2, G2, H2,
	A3, B3, C3, D3, E3, F3, G3, H3,
	A4, B4, C4, D4, E4, F4, G4, H4,
	A5, B5, C5, D5, E5, F5, G5, H5,
	A6, B6, C6, D6, E6, F6, G6, H6,
	A7, B7, C7, D7, E7, F7, G7, H7,
	A8, B8, C8, D8, E8, F8, G8, H8
} chess_positions;

typedef struct check_status
{
	bool double_check;
	bool single_check;
	u64 capture_mask;
	u64 push_mask;
} check_status;

chess_piece_type get_piece_type(chess_piece piece);
chess_color get_piece_color(chess_piece piece);
chess_piece make_piece(chess_piece_type type, chess_color color);

// #todo note which result from apply move and which must be set otherwise, if any
typedef enum chess_result
{
	CR_ONGOING,
	CR_DRAW_REPETITION,
	CR_DRAW_INSUFFICIENT_MATERIAL,
	CR_DRAW_FIFTY_MOVE,
	CR_DRAW_STALEMATE,
	CR_PERFT_NO_IGNORE = CR_DRAW_STALEMATE,
	CR_WHITE_CHECKMATE,
	// #note perft ignores any draws, so first non-ignored result is white checkmate
	CR_BLACK_CHECKMATE
} chess_result;


u64 square_bit(u8 sq);

// #note printers
void print_piece(chess_piece piece);
char get_piece_char(chess_piece piece);
char get_file_char(u8 pos);
char get_rank_char(u8 pos);
void print_square(u8 pos);

#endif // !CHESS_H
