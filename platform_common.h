#ifndef RL2_PL_COMMON_H
#define RL2_PL_COMMON_H

#include "common.h"

typedef enum console_color
{
	CC_BLACK = 30,
	CC_RED,
	CC_GREEN,
	CC_YELLOW,
	CC_BLUE,
	CC_MAGENTA,
	CC_CYAN,
	CC_WHITE
} console_color;

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif // _WIN32

#ifdef _WIN32
extern HANDLE console_handle;
extern WORD saved_console_attributes;
#endif // _WIN32

void set_print_color(console_color color, bool bold);
/*
  @brief print the string in color
  @param color: the color to print in
  @param bold: whether to print in bold
  @param fmt: the format string to print
 */
#ifndef _WIN32
__attribute__((__format__ (__printf__, 3, 0)))
#endif // !_WIN32
void print_color(console_color color, bool bold, const char* fmt, ...);

#define PC_BLACK(...)  print_color(CC_BLACK, 0, __VA_ARGS__)
#define PC_RED(...) print_color(CC_RED, 0, __VA_ARGS__)
#define PC_GREEN(...) print_color(CC_GREEN, 0, __VA_ARGS__)
#define PC_YELLOW(...) print_color(CC_YELLOW, 0, __VA_ARGS__)
#define PC_BLUE(...) print_color(CC_BLUE, 0, __VA_ARGS__)
#define PC_MAGENTA(...) print_color(CC_MAGENTA, 0, __VA_ARGS__)
#define PC_CYAN(...) print_color(CC_CYAN, 0, __VA_ARGS__)
#define PC_WHITE(...) print_color(CC_WHITE, 0, __VA_ARGS__)
#define PC_BLACK_BOLD(...) print_color(CC_BLACK, 1, __VA_ARGS__)
#define PC_RED_BOLD(...) print_color(CC_RED, 1, __VA_ARGS__)
#define PC_GREEN_BOLD(...) print_color(CC_GREEN, 1, __VA_ARGS__)
#define PC_YELLOW_BOLD(...) print_color(CC_YELLOW, 1, __VA_ARGS__)
#define PC_BLUE_BOLD(...) print_color(CC_BLUE, 1, __VA_ARGS__)
#define PC_MAGENTA_BOLD(...) print_color(CC_MAGENTA, 1, __VA_ARGS__)
#define PC_CYAN_BOLD(...) print_color(CC_CYAN, 1, __VA_ARGS__)
#define PC_WHITE_BOLD(...) print_color(CC_WHITE, 1, __VA_ARGS__)

void print_bits_32(u32 x);
void print_num_bytes(u64 num);
void print_bytes(const void *data, u32 num_bytes, u32 offset);
void print_compare_bytes(const void *data0, const void *data1, u32 num_bytes, u32 offset);

#define MAGIC4(a, b, c, d) (((u32)a) | ((u32)b << 8) | ((u32)c << 16) | ((u32)d << 24))

//void pl_log(log_kind kind, const char *msg, va_list args);

u32 safecast_s32_to_u32(s32 val);
s32 safecast_u32_to_s32(u32 val);
u64 safecast_s64_to_u64(s64 val);
s64 safecast_u64_to_s64(u64 val);

typedef struct file_contents
{
	char *contents;
	size_t size;
} file_contents;

file_contents read_file(const char *path);
file_contents read_text_file(const char *path);
bool write_file(const char *path, const char *buf, usize len);
bool write_file_append(const char *path, const char *buf, usize len);

#endif // !RL2_PL_COMMON_H
